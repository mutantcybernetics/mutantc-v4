# mutantC-v4
# Checkout the poject website from [here](https://mutantc.gitlab.io).
### A Raspberry-pi handheld platform with a physical keyboard, Display and Expansion header for custom baords (Like Ardinuo Shield).
### It is fully open-source hardware with MIT. So you can modify or hack it as you wish.
### It is platform like Arduino. You can make your expansion-card like gps, Radio etc.
### So help us to make a Community around it.
### [Parts list](https://gitlab.com/mutantC/mutantc-v4/-/blob/master/parts_list) Or [Parts list Lite](https://gitlab.com/mutantC/mutantc-v4/-/blob/master/parts_list_lite).

### [Youtube](http://www.youtube.com/c/mutantC).

# Download
### Download the project from [here](https://a360.co/3bJqvVq).

# Software used
### Autodesk fusion 360 - 3d parts
### Eagle - PCB
### Ardinuo IDE - Device Firmware

<img src="p1.JPG" width="500">
<img src="p8.JPG" width="500"> 
<img src="p2.JPG" width="500"> 
<img src="p2.png" width="500">
<img src="p1.png" width="500"> 
<img src="p5.JPG" width="500"> 
<img src="p7.JPG" width="500">

# Feedback
We need your feedback to improve the mutantC.
Send us your feedback through GitLab [issues](https://gitlab.com/groups/mutantC/-/issues).
