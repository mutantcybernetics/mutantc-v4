# Just for reference, Not maintained
# firmware V4.1 for mutantC v4

import time
import adafruit_matrixkeypad
import digitalio
from digitalio import DigitalInOut, Direction, Pull
import board
import neopixel
from analogio import AnalogIn

import usb_hid
from adafruit_hid.mouse import Mouse
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode

keymapState= False

KMAP_SWITCH = 0x100
EXPANSION_SWITCH = 0x102
LCD_SWITCH = 0x103
NOTIFICATION_LED = 0x104
KMOUSE_LEFT = 0x105
KMOUSE_RIGHT = 0x106
KBATTERY_STATE = 0x107
NOTIFICATION_BUZZER = 0x108

m = Mouse(usb_hid.devices)
kbd = Keyboard(usb_hid.devices)

range = 2
upState = DigitalInOut(board.IO10)
downState = DigitalInOut(board.IO4)
rightState = DigitalInOut(board.IO8)
leftState = DigitalInOut(board.IO9)
LED_DISPLAY = DigitalInOut(board.IO5)
VIBRATION = DigitalInOut(board.IO45)
BUZZER = DigitalInOut(board.IO40)
POWER_MAIN = DigitalInOut(board.IO37)
POWER_OFF_BTN = DigitalInOut(board.IO26)
PI_STATE = DigitalInOut(board.IO41)
BATTERY_VOLTAGE = AnalogIn(board.IO18)
POWER_EXP = DigitalInOut(board.IO38)
POWER_LCD = DigitalInOut(board.IO39)

POWER_MAIN.direction = Direction.OUTPUT
POWER_MAIN.value = True

VIBRATION.direction = Direction.OUTPUT
LED_DISPLAY.direction = Direction.OUTPUT
BUZZER.direction = Direction.OUTPUT
POWER_OFF_BTN.direction = Direction.INPUT
POWER_OFF_BTN.pull = Pull.UP
PI_STATE.direction = Direction.INPUT
PI_STATE.pull = Pull.UP
POWER_EXP.direction = Direction.OUTPUT
POWER_LCD.direction = Direction.OUTPUT

upState.direction = Direction.INPUT
upState.pull = Pull.UP
downState.direction = Direction.INPUT
downState.pull = Pull.UP
rightState.direction = Direction.INPUT
rightState.pull = Pull.UP
leftState.direction = Direction.INPUT
leftState.pull = Pull.UP

LED_BODY = neopixel.NeoPixel(board.IO36, 1, brightness=0.2)

cols = [DigitalInOut(x)for x in (board.IO6, board.IO42, board.IO21, board.IO33, board.IO34, board.IO35, board.IO16, board.IO1, board.IO14, board.IO15, board.IO7,)]
rows = [DigitalInOut(x)for x in (board.IO3, board.IO2, board.IO11, board.IO12, board.IO13)]

keymapAlpha = (
    Keycode.GUI, Keycode.CAPS_LOCK, Keycode.TAB, Keycode.ESCAPE, EXPANSION_SWITCH, KBATTERY_STATE, NOTIFICATION_BUZZER, Keycode.ONE, Keycode.ONE, KMAP_SWITCH, LCD_SWITCH,
    Keycode.LEFT_ARROW, Keycode.ONE, Keycode.TWO, Keycode.THREE, Keycode.FOUR, Keycode.FIVE, Keycode.SIX, Keycode.SEVEN, Keycode.EIGHT, Keycode.NINE, Keycode.ZERO,
    Keycode.RIGHT_ARROW, Keycode.Q, Keycode.W, Keycode.E, Keycode.R, Keycode.T, Keycode.Y, Keycode.U, Keycode.I, Keycode.O, Keycode.P,
    KMOUSE_LEFT, Keycode.A, Keycode.S, Keycode.D, Keycode.F, Keycode.G, Keycode.H, Keycode.J, Keycode.K, Keycode.L, Keycode.BACKSPACE,
    KMOUSE_RIGHT, Keycode.CONTROL, Keycode.Z, Keycode.X, Keycode.C, Keycode.V, Keycode.B, Keycode.N, Keycode.M, Keycode.SPACEBAR, Keycode.RETURN,
)

keymapSymbols = (
    Keycode.GUI, Keycode.CAPS_LOCK, Keycode.TAB, Keycode.ESCAPE, EXPANSION_SWITCH, KBATTERY_STATE, NOTIFICATION_BUZZER, Keycode.ONE, Keycode.ONE, KMAP_SWITCH, LCD_SWITCH,
    Keycode.LEFT_ARROW, Keycode.ONE, Keycode.TWO, Keycode.THREE, Keycode.FOUR, Keycode.FIVE, Keycode.SIX, Keycode.SEVEN, Keycode.EIGHT, Keycode.NINE, Keycode.ZERO,
    Keycode.RIGHT_ARROW, Keycode.UP_ARROW, Keycode.PAGE_UP, Keycode.LEFT_BRACKET, Keycode.RIGHT_BRACKET, Keycode.BACKSLASH, Keycode.POUND, Keycode.SEMICOLON, Keycode.QUOTE, Keycode.GRAVE_ACCENT, Keycode.P,
    KMOUSE_LEFT, Keycode.DOWN_ARROW, Keycode.PAGE_DOWN, Keycode.D, Keycode.F, Keycode.G, Keycode.H, Keycode.J, Keycode.K, Keycode.L, Keycode.BACKSPACE ,
    KMOUSE_RIGHT, Keycode.CONTROL, Keycode.KEYPAD_PLUS, Keycode.KEYPAD_MINUS, Keycode.KEYPAD_ASTERISK, Keycode.FORWARD_SLASH, Keycode.EQUALS, Keycode.COMMA, Keycode.PERIOD, Keycode.SPACEBAR, Keycode.RETURN,
)

dummyKeypad = (
  (00,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10),
  (11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21),
  (22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32),
  (33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43),
  (44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54)
)

keypad = adafruit_matrixkeypad.Matrix_Keypad(rows, cols, dummyKeypad)

def thumbstick():
    # calculate the movement distance based on the button states:
    xDistance = (leftState.value - rightState.value) * range
    yDistance = (upState.value - downState.value) * range

    # if X or Y is non-zero, move:
    if xDistance != 0 or yDistance != 0:
        m.move(xDistance, yDistance, 0)

    m.release_all()

def mouse_left():
    if POWER_LCD.value:
        return
    # Click the left mouse button.
    m.click(Mouse.LEFT_BUTTON)
    time.sleep(0.2)
    m.release_all()

def mouse_right():
    if POWER_LCD.value:
        return
    # Click the left mouse button.
    m.click(Mouse.RIGHT_BUTTON)
    time.sleep(0.2)
    m.release_all()

def lcd_power_toggle():
    if POWER_LCD.value:
        POWER_LCD.value = False
    else:
        POWER_LCD.value = True

def expansion_power_toggle():
    if POWER_EXP.value:
        POWER_EXP.value = False
    else:
        POWER_EXP.value = True

def notification_Buzzer():
    print("Pressed: notification_Buzzer")
    BUZZER.value = True
    time.sleep(0.2)
    BUZZER.value = False
    time.sleep(0.2)
    BUZZER.value = True
    time.sleep(0.2)
    BUZZER.value = False
    time.sleep(0.2)
    BUZZER.value = True
    time.sleep(0.2)
    BUZZER.value = False
    print("Pressed: notification_Vibration")
    VIBRATION.value = True
    time.sleep(0.8)
    VIBRATION.value = False
    time.sleep(0.2)
    VIBRATION.value = True
    time.sleep(0.8)
    VIBRATION.value = False
    time.sleep(0.5)
    VIBRATION.value = True
    time.sleep(0.8)
    VIBRATION.value = False

def power_off():
    print("Pressed: POWER_EXP")
    POWER_MAIN.value = False

def battery_state():
    print("battery_state", BATTERY_VOLTAGE.value)

def switch_Keymap():
    global keymapState
    keymapState = not keymapState

    # Change the LED_Body.
    if keymapState:
        LED_BODY[0] = (0, 43, 240, 240)
    else:
        LED_BODY[0] = (0, 0, 0, 0)


def mapKey(char):
    if keymapState:
        return keymapSymbols[char]
    else:
        return keymapAlpha[char]

# Counter to run from 0-255 to cycle the colors of the rainbow.
colorCount = 0
delayCount = 10
colorIncrease = True # Will increase the color values or decrease
canDelay = False
def notification_LED(w):
    # Breath LED when the LCD is Off
    if w:

        print("jji")
        if canDelay:
            delayCount = delayCount -1
            if delayCount <= 0:
                canDelay = False
                delayCount = 10

        else:
            print("uuu")
            if colorIncrease:
                colorCount=colorCount+3.6
            else:
                colorCount=colorCount-3.6

            # Set the new color on the pixel.
            LED_BODY[0] = (0,colorCount,colorCount , 0)

            # Cycle the colors at the end.
            if colorCount > 254:
                colorIncrease = False
                canDelay = False
            elif colorCount <= 1:
                colorIncrease = True
                canDelay = True

            print(delayCount)


        time.sleep(0.2)

    else:

        LED_BODY[0] = (0, 0, 0, 0)
        delayCount = 0
        colorCount=0
        canDelay = False;
        colorIncrease = True;


powered_on = 100

def color_chase(color, wait):
    for i in range(num_pixels):
        pixels[i] = color
        time.sleep(wait)
        pixels.show()
    time.sleep(0.5)

while True:
    if powered_on > 0:
        powered_on = powered_on - 1
    else:
        if not POWER_OFF_BTN.value:
            print("POWER_OFF_BTN")
            power_off()

    thumbstick()

    LED_BODY[0] = (0, 0, 0, 0)
    BUZZER.value = True
    time.sleep(0.1)
    BUZZER.value = False
    LED_BODY[0] = (0, 255, 0, 0)
    time.sleep(0.5)


    keys = keypad.pressed_keys
    if keys:
        realKeys = []
        for key in keys:
            gh = mapKey(key)
            if gh == KMAP_SWITCH:
                switch_Keymap()
            elif gh == KBATTERY_STATE:
                battery_state()
            elif gh == KMOUSE_LEFT:
                mouse_left()
            elif gh == KMOUSE_RIGHT:
                mouse_right()
            elif gh == NOTIFICATION_BUZZER:
                notification_Buzzer()
            elif gh == EXPANSION_SWITCH:
                expansion_power_toggle()
            elif gh == LCD_SWITCH:
                lcd_power_toggle()
            else:
                realKeys.append(mapKey(key))
        if realKeys:
            kbd.send(*realKeys[:6])
    time.sleep(0.2)

