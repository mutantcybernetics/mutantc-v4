<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="2.54" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.254" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="61" name="stand" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="4" visible="no" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="no" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="no" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="aaaaaa" urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A">
<packages>
<package name="SW_M" urn="urn:adsk.eagle:footprint:3097553/4" locally_modified="yes" library_version="6">
<description>WS-TSW-6x6 mm washable J-Bend SMD Tact Switch,4 pins</description>
<smd name="3" x="-3.35" y="-2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="4" x="3.35" y="-2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="1" x="-3.35" y="2.2" dx="1.7" dy="1.4" layer="1"/>
<smd name="2" x="3.35" y="2.2" dx="1.7" dy="1.4" layer="1"/>
<text x="1.408" y="4.1759" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-4.567" y="-4.9334" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.84" y="1.455" size="1.27" layer="51">1</text>
<text x="1.305" y="-2.34" size="1.27" layer="51">4</text>
<text x="1.06" y="1.555" size="1.27" layer="51">2</text>
<text x="-1.64" y="-2.295" size="1.27" layer="51">3</text>
<wire x1="-3.1" y1="3.1" x2="-3.1" y2="-3.1" width="0.127" layer="51"/>
<wire x1="-3.1" y1="-3.1" x2="3.1" y2="-3.1" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.1" x2="3.1" y2="3.1" width="0.127" layer="51"/>
<wire x1="3.1" y1="3.1" x2="-3.1" y2="3.1" width="0.127" layer="21"/>
<wire x1="-3.1" y1="1.01" x2="-3.1" y2="-1.07" width="0.127" layer="21"/>
<wire x1="3.1" y1="1.02" x2="3.1" y2="-1.06" width="0.127" layer="21"/>
<polygon width="0.127" layer="39" pour="solid">
<vertex x="-4.38" y="-3.35"/>
<vertex x="4.38" y="-3.35"/>
<vertex x="4.38" y="3.35"/>
<vertex x="-4.38" y="3.35"/>
</polygon>
<circle x="-3.7" y="3.2" radius="0.05" width="0.2" layer="21"/>
</package>
<package name="MUTANTC_TEXT" library_version="120">
<rectangle x1="4.99" y1="-0.65" x2="5.09" y2="-0.55" layer="25"/>
<rectangle x1="10.49" y1="-0.65" x2="10.59" y2="-0.55" layer="25"/>
<rectangle x1="4.99" y1="-0.55" x2="5.29" y2="-0.45" layer="25"/>
<rectangle x1="10.49" y1="-0.55" x2="10.79" y2="-0.45" layer="25"/>
<rectangle x1="4.99" y1="-0.45" x2="5.39" y2="-0.35" layer="25"/>
<rectangle x1="10.49" y1="-0.45" x2="10.99" y2="-0.35" layer="25"/>
<rectangle x1="4.99" y1="-0.35" x2="5.39" y2="-0.25" layer="25"/>
<rectangle x1="10.49" y1="-0.35" x2="10.99" y2="-0.25" layer="25"/>
<rectangle x1="4.99" y1="-0.25" x2="5.39" y2="-0.15" layer="25"/>
<rectangle x1="10.49" y1="-0.25" x2="10.99" y2="-0.15" layer="25"/>
<rectangle x1="4.99" y1="-0.15" x2="5.39" y2="-0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.15" x2="10.99" y2="-0.05" layer="25"/>
<rectangle x1="4.99" y1="-0.05" x2="5.39" y2="0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.05" x2="10.99" y2="0.05" layer="25"/>
<rectangle x1="4.99" y1="0.05" x2="5.39" y2="0.15" layer="25"/>
<rectangle x1="10.49" y1="0.05" x2="10.99" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.15" x2="0.39" y2="0.25" layer="25"/>
<rectangle x1="3.29" y1="0.05" x2="4.39" y2="0.15" layer="25"/>
<rectangle x1="4.99" y1="0.15" x2="5.39" y2="0.25" layer="25"/>
<rectangle x1="7.19" y1="0.15" x2="7.49" y2="0.25" layer="25"/>
<rectangle x1="9.09" y1="0.15" x2="9.39" y2="0.25" layer="25"/>
<rectangle x1="10.49" y1="0.15" x2="10.99" y2="0.25" layer="25"/>
<rectangle x1="12.59" y1="0.15" x2="13.19" y2="0.25" layer="25"/>
<rectangle x1="0.09" y1="0.25" x2="0.59" y2="0.35" layer="25"/>
<rectangle x1="3.19" y1="0.15" x2="4.49" y2="0.25" layer="25"/>
<rectangle x1="4.99" y1="0.25" x2="5.39" y2="0.35" layer="25"/>
<rectangle x1="6.39" y1="0.25" x2="7.69" y2="0.35" layer="25"/>
<rectangle x1="7.99" y1="0.25" x2="8.39" y2="0.35" layer="25"/>
<rectangle x1="9.09" y1="0.25" x2="9.59" y2="0.35" layer="25"/>
<rectangle x1="10.49" y1="0.25" x2="10.99" y2="0.35" layer="25"/>
<rectangle x1="12.19" y1="0.25" x2="13.59" y2="0.35" layer="25"/>
<rectangle x1="0.09" y1="0.35" x2="0.59" y2="0.45" layer="25"/>
<rectangle x1="3.09" y1="0.25" x2="4.59" y2="0.35" layer="25"/>
<rectangle x1="4.99" y1="0.35" x2="5.39" y2="0.45" layer="25"/>
<rectangle x1="6.29" y1="0.35" x2="7.69" y2="0.45" layer="25"/>
<rectangle x1="7.99" y1="0.35" x2="8.39" y2="0.45" layer="25"/>
<rectangle x1="9.09" y1="0.35" x2="9.59" y2="0.45" layer="25"/>
<rectangle x1="10.49" y1="0.35" x2="10.99" y2="0.45" layer="25"/>
<rectangle x1="12.09" y1="0.35" x2="13.69" y2="0.45" layer="25"/>
<rectangle x1="0.09" y1="0.45" x2="0.59" y2="0.55" layer="25"/>
<rectangle x1="3.09" y1="0.35" x2="4.59" y2="0.45" layer="25"/>
<rectangle x1="4.99" y1="0.45" x2="5.39" y2="0.55" layer="25"/>
<rectangle x1="6.19" y1="0.45" x2="7.69" y2="0.55" layer="25"/>
<rectangle x1="7.99" y1="0.45" x2="8.39" y2="0.55" layer="25"/>
<rectangle x1="9.09" y1="0.45" x2="9.59" y2="0.55" layer="25"/>
<rectangle x1="10.49" y1="0.45" x2="10.99" y2="0.55" layer="25"/>
<rectangle x1="11.99" y1="0.45" x2="13.79" y2="0.55" layer="25"/>
<rectangle x1="0.09" y1="0.55" x2="0.59" y2="0.65" layer="25"/>
<rectangle x1="3.09" y1="0.45" x2="3.49" y2="0.55" layer="25"/>
<rectangle x1="4.19" y1="0.45" x2="4.59" y2="0.55" layer="25"/>
<rectangle x1="4.99" y1="0.55" x2="5.39" y2="0.65" layer="25"/>
<rectangle x1="6.19" y1="0.55" x2="7.69" y2="0.65" layer="25"/>
<rectangle x1="7.99" y1="0.55" x2="8.39" y2="0.65" layer="25"/>
<rectangle x1="9.09" y1="0.55" x2="9.59" y2="0.65" layer="25"/>
<rectangle x1="10.49" y1="0.55" x2="10.99" y2="0.65" layer="25"/>
<rectangle x1="11.99" y1="0.55" x2="13.79" y2="0.65" layer="25"/>
<rectangle x1="0.09" y1="0.65" x2="0.59" y2="0.75" layer="25"/>
<rectangle x1="3.09" y1="0.55" x2="3.49" y2="0.65" layer="25"/>
<rectangle x1="4.19" y1="0.55" x2="4.59" y2="0.65" layer="25"/>
<rectangle x1="4.99" y1="0.65" x2="5.39" y2="0.75" layer="25"/>
<rectangle x1="6.19" y1="0.65" x2="6.59" y2="0.75" layer="25"/>
<rectangle x1="7.19" y1="0.65" x2="7.69" y2="0.75" layer="25"/>
<rectangle x1="7.99" y1="0.65" x2="8.39" y2="0.75" layer="25"/>
<rectangle x1="9.09" y1="0.65" x2="9.59" y2="0.75" layer="25"/>
<rectangle x1="10.49" y1="0.65" x2="10.99" y2="0.75" layer="25"/>
<rectangle x1="11.99" y1="0.65" x2="12.39" y2="0.75" layer="25"/>
<rectangle x1="13.39" y1="0.65" x2="13.79" y2="0.75" layer="25"/>
<rectangle x1="0.09" y1="0.75" x2="0.59" y2="0.85" layer="25"/>
<rectangle x1="3.09" y1="0.65" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="4.19" y1="0.65" x2="4.59" y2="0.75" layer="25"/>
<rectangle x1="4.99" y1="0.75" x2="5.39" y2="0.85" layer="25"/>
<rectangle x1="6.19" y1="0.75" x2="6.59" y2="0.85" layer="25"/>
<rectangle x1="7.19" y1="0.75" x2="7.69" y2="0.85" layer="25"/>
<rectangle x1="7.99" y1="0.75" x2="8.39" y2="0.85" layer="25"/>
<rectangle x1="9.09" y1="0.75" x2="9.59" y2="0.85" layer="25"/>
<rectangle x1="10.49" y1="0.75" x2="10.99" y2="0.85" layer="25"/>
<rectangle x1="11.99" y1="0.75" x2="12.39" y2="0.85" layer="25"/>
<rectangle x1="0.09" y1="0.85" x2="0.59" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.75" x2="3.49" y2="0.85" layer="25"/>
<rectangle x1="4.19" y1="0.75" x2="4.59" y2="0.85" layer="25"/>
<rectangle x1="4.99" y1="0.85" x2="5.39" y2="0.95" layer="25"/>
<rectangle x1="6.19" y1="0.85" x2="6.59" y2="0.95" layer="25"/>
<rectangle x1="7.19" y1="0.85" x2="7.69" y2="0.95" layer="25"/>
<rectangle x1="7.99" y1="0.85" x2="8.39" y2="0.95" layer="25"/>
<rectangle x1="9.09" y1="0.85" x2="9.59" y2="0.95" layer="25"/>
<rectangle x1="10.49" y1="0.85" x2="10.99" y2="0.95" layer="25"/>
<rectangle x1="11.99" y1="0.85" x2="12.39" y2="0.95" layer="25"/>
<rectangle x1="0.09" y1="0.95" x2="0.59" y2="1.05" layer="25"/>
<rectangle x1="3.09" y1="0.85" x2="3.49" y2="0.95" layer="25"/>
<rectangle x1="4.19" y1="0.85" x2="4.59" y2="0.95" layer="25"/>
<rectangle x1="4.99" y1="0.95" x2="5.39" y2="1.05" layer="25"/>
<rectangle x1="6.19" y1="0.95" x2="6.59" y2="1.05" layer="25"/>
<rectangle x1="7.19" y1="0.95" x2="7.69" y2="1.05" layer="25"/>
<rectangle x1="7.99" y1="0.95" x2="8.39" y2="1.05" layer="25"/>
<rectangle x1="11.99" y1="0.95" x2="12.39" y2="1.05" layer="25"/>
<rectangle x1="0.09" y1="1.05" x2="0.59" y2="1.15" layer="25"/>
<rectangle x1="1.29" y1="1.05" x2="1.69" y2="1.15" layer="25"/>
<rectangle x1="2.39" y1="1.05" x2="2.89" y2="1.15" layer="25"/>
<rectangle x1="3.09" y1="0.95" x2="3.49" y2="1.05" layer="25"/>
<rectangle x1="4.19" y1="0.95" x2="4.59" y2="1.05" layer="25"/>
<rectangle x1="4.99" y1="1.05" x2="5.39" y2="1.15" layer="25"/>
<rectangle x1="6.19" y1="1.05" x2="6.59" y2="1.15" layer="25"/>
<rectangle x1="7.19" y1="1.05" x2="7.69" y2="1.15" layer="25"/>
<rectangle x1="7.99" y1="1.05" x2="8.39" y2="1.15" layer="25"/>
<rectangle x1="11.99" y1="1.05" x2="12.39" y2="1.15" layer="25"/>
<rectangle x1="0.09" y1="1.15" x2="0.59" y2="1.25" layer="25"/>
<rectangle x1="1.29" y1="1.15" x2="1.69" y2="1.25" layer="25"/>
<rectangle x1="2.39" y1="1.15" x2="2.89" y2="1.25" layer="25"/>
<rectangle x1="3.09" y1="1.05" x2="3.49" y2="1.15" layer="25"/>
<rectangle x1="4.19" y1="1.05" x2="4.59" y2="1.15" layer="25"/>
<rectangle x1="4.99" y1="1.15" x2="5.39" y2="1.25" layer="25"/>
<rectangle x1="6.19" y1="1.15" x2="6.69" y2="1.25" layer="25"/>
<rectangle x1="7.09" y1="1.15" x2="7.69" y2="1.25" layer="25"/>
<rectangle x1="7.99" y1="1.15" x2="8.39" y2="1.25" layer="25"/>
<rectangle x1="11.99" y1="1.15" x2="12.39" y2="1.25" layer="25"/>
<rectangle x1="13.49" y1="1.15" x2="13.79" y2="1.25" layer="25"/>
<rectangle x1="0.09" y1="1.25" x2="0.59" y2="1.35" layer="25"/>
<rectangle x1="1.29" y1="1.25" x2="1.69" y2="1.35" layer="25"/>
<rectangle x1="2.39" y1="1.25" x2="2.89" y2="1.35" layer="25"/>
<rectangle x1="3.09" y1="1.15" x2="3.49" y2="1.25" layer="25"/>
<rectangle x1="4.99" y1="1.25" x2="5.39" y2="1.35" layer="25"/>
<rectangle x1="6.19" y1="1.25" x2="7.69" y2="1.35" layer="25"/>
<rectangle x1="7.99" y1="1.25" x2="8.39" y2="1.35" layer="25"/>
<rectangle x1="11.99" y1="1.25" x2="12.39" y2="1.35" layer="25"/>
<rectangle x1="0.09" y1="1.35" x2="0.59" y2="1.45" layer="25"/>
<rectangle x1="1.29" y1="1.35" x2="1.69" y2="1.45" layer="25"/>
<rectangle x1="2.39" y1="1.35" x2="2.89" y2="1.45" layer="25"/>
<rectangle x1="3.09" y1="1.25" x2="3.49" y2="1.35" layer="25"/>
<rectangle x1="4.99" y1="1.35" x2="5.39" y2="1.45" layer="25"/>
<rectangle x1="6.19" y1="1.35" x2="7.69" y2="1.45" layer="25"/>
<rectangle x1="7.99" y1="1.35" x2="8.39" y2="1.45" layer="25"/>
<rectangle x1="9.09" y1="1.35" x2="9.59" y2="1.45" layer="25"/>
<rectangle x1="10.49" y1="1.35" x2="10.99" y2="1.45" layer="25"/>
<rectangle x1="11.99" y1="1.35" x2="12.39" y2="1.45" layer="25"/>
<rectangle x1="0.09" y1="1.45" x2="0.59" y2="1.55" layer="25"/>
<rectangle x1="1.29" y1="1.45" x2="1.69" y2="1.55" layer="25"/>
<rectangle x1="2.39" y1="1.45" x2="2.89" y2="1.55" layer="25"/>
<rectangle x1="3.09" y1="1.35" x2="3.49" y2="1.45" layer="25"/>
<rectangle x1="4.99" y1="1.45" x2="5.39" y2="1.55" layer="25"/>
<rectangle x1="6.29" y1="1.45" x2="7.69" y2="1.55" layer="25"/>
<rectangle x1="7.99" y1="1.45" x2="8.39" y2="1.55" layer="25"/>
<rectangle x1="9.09" y1="1.45" x2="9.59" y2="1.55" layer="25"/>
<rectangle x1="10.49" y1="1.45" x2="10.99" y2="1.55" layer="25"/>
<rectangle x1="11.99" y1="1.45" x2="12.39" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.59" y2="1.65" layer="25"/>
<rectangle x1="1.29" y1="1.55" x2="1.69" y2="1.65" layer="25"/>
<rectangle x1="2.39" y1="1.55" x2="2.89" y2="1.65" layer="25"/>
<rectangle x1="3.09" y1="1.45" x2="3.49" y2="1.55" layer="25"/>
<rectangle x1="4.99" y1="1.55" x2="5.39" y2="1.65" layer="25"/>
<rectangle x1="6.49" y1="1.55" x2="7.69" y2="1.65" layer="25"/>
<rectangle x1="7.99" y1="1.55" x2="8.39" y2="1.65" layer="25"/>
<rectangle x1="9.09" y1="1.55" x2="9.59" y2="1.65" layer="25"/>
<rectangle x1="10.49" y1="1.55" x2="10.99" y2="1.65" layer="25"/>
<rectangle x1="11.99" y1="1.55" x2="12.39" y2="1.65" layer="25"/>
<rectangle x1="0.09" y1="1.65" x2="0.59" y2="1.75" layer="25"/>
<rectangle x1="1.29" y1="1.65" x2="1.69" y2="1.75" layer="25"/>
<rectangle x1="2.39" y1="1.65" x2="2.89" y2="1.75" layer="25"/>
<rectangle x1="3.09" y1="1.55" x2="3.49" y2="1.65" layer="25"/>
<rectangle x1="4.99" y1="1.65" x2="5.39" y2="1.75" layer="25"/>
<rectangle x1="7.19" y1="1.65" x2="7.69" y2="1.75" layer="25"/>
<rectangle x1="7.99" y1="1.65" x2="8.49" y2="1.75" layer="25"/>
<rectangle x1="9.09" y1="1.65" x2="9.59" y2="1.75" layer="25"/>
<rectangle x1="10.49" y1="1.65" x2="10.99" y2="1.75" layer="25"/>
<rectangle x1="11.99" y1="1.65" x2="12.39" y2="1.75" layer="25"/>
<rectangle x1="0.09" y1="1.75" x2="0.59" y2="1.85" layer="25"/>
<rectangle x1="1.19" y1="1.75" x2="1.79" y2="1.85" layer="25"/>
<rectangle x1="2.39" y1="1.75" x2="2.79" y2="1.85" layer="25"/>
<rectangle x1="3.09" y1="1.65" x2="3.49" y2="1.75" layer="25"/>
<rectangle x1="4.99" y1="1.75" x2="5.39" y2="1.85" layer="25"/>
<rectangle x1="7.19" y1="1.75" x2="7.69" y2="1.85" layer="25"/>
<rectangle x1="7.99" y1="1.75" x2="8.49" y2="1.85" layer="25"/>
<rectangle x1="9.09" y1="1.75" x2="9.59" y2="1.85" layer="25"/>
<rectangle x1="10.49" y1="1.75" x2="10.99" y2="1.85" layer="25"/>
<rectangle x1="11.99" y1="1.75" x2="12.39" y2="1.85" layer="25"/>
<rectangle x1="0.19" y1="1.85" x2="2.79" y2="1.95" layer="25"/>
<rectangle x1="3.09" y1="1.75" x2="3.49" y2="1.85" layer="25"/>
<rectangle x1="4.69" y1="1.85" x2="6.09" y2="1.95" layer="25"/>
<rectangle x1="6.39" y1="1.85" x2="7.69" y2="1.95" layer="25"/>
<rectangle x1="7.99" y1="1.85" x2="9.59" y2="1.95" layer="25"/>
<rectangle x1="9.79" y1="1.85" x2="11.59" y2="1.95" layer="25"/>
<rectangle x1="11.99" y1="1.85" x2="12.39" y2="1.95" layer="25"/>
<rectangle x1="0.19" y1="1.95" x2="2.79" y2="2.05" layer="25"/>
<rectangle x1="3.09" y1="1.85" x2="3.49" y2="1.95" layer="25"/>
<rectangle x1="6.49" y1="1.95" x2="7.59" y2="2.05" layer="25"/>
<rectangle x1="8.09" y1="1.95" x2="9.49" y2="2.05" layer="25"/>
<rectangle x1="9.79" y1="1.95" x2="11.69" y2="2.05" layer="25"/>
<rectangle x1="11.99" y1="1.95" x2="12.39" y2="2.05" layer="25"/>
<rectangle x1="13.39" y1="1.95" x2="13.59" y2="2.05" layer="25"/>
<rectangle x1="0.29" y1="2.05" x2="2.69" y2="2.15" layer="25"/>
<rectangle x1="3.09" y1="1.95" x2="3.49" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="2.05" x2="6.19" y2="2.15" layer="25"/>
<rectangle x1="6.49" y1="2.05" x2="7.59" y2="2.15" layer="25"/>
<rectangle x1="8.09" y1="2.05" x2="9.49" y2="2.15" layer="25"/>
<rectangle x1="9.79" y1="2.05" x2="11.69" y2="2.15" layer="25"/>
<rectangle x1="11.99" y1="2.05" x2="12.39" y2="2.15" layer="25"/>
<rectangle x1="0.39" y1="2.15" x2="1.39" y2="2.25" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="2.59" y2="2.25" layer="25"/>
<rectangle x1="4.69" y1="2.15" x2="6.19" y2="2.25" layer="25"/>
<rectangle x1="6.59" y1="2.15" x2="7.49" y2="2.25" layer="25"/>
<rectangle x1="8.19" y1="2.15" x2="9.29" y2="2.25" layer="25"/>
<rectangle x1="9.89" y1="2.15" x2="11.69" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.15" x2="12.39" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.25" x2="12.39" y2="2.35" layer="25"/>
<rectangle x1="11.99" y1="2.35" x2="12.39" y2="2.45" layer="25"/>
<rectangle x1="11.99" y1="2.45" x2="12.39" y2="2.55" layer="25"/>
<rectangle x1="13.39" y1="2.45" x2="13.79" y2="2.55" layer="25"/>
<rectangle x1="11.99" y1="2.55" x2="13.79" y2="2.65" layer="25"/>
<rectangle x1="11.99" y1="2.65" x2="13.79" y2="2.75" layer="25"/>
<rectangle x1="12.09" y1="2.75" x2="13.69" y2="2.85" layer="25"/>
<rectangle x1="12.19" y1="2.85" x2="13.59" y2="2.95" layer="25"/>
<rectangle x1="12.49" y1="2.95" x2="13.29" y2="3.05" layer="25"/>
<rectangle x1="7.19" y1="0.05" x2="7.29" y2="0.15" layer="25"/>
<rectangle x1="7.99" y1="0.05" x2="8.09" y2="0.15" layer="25"/>
<rectangle x1="9.09" y1="0.05" x2="9.19" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.05" x2="0.19" y2="0.15" layer="25"/>
<rectangle x1="3.39" y1="2.15" x2="3.49" y2="2.25" layer="25"/>
<rectangle x1="6.19" y1="2.15" x2="6.29" y2="2.25" layer="25"/>
<rectangle x1="9.79" y1="2.15" x2="9.89" y2="2.25" layer="25"/>
<rectangle x1="11.69" y1="2.15" x2="11.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="1.85" x2="13.49" y2="1.95" layer="25"/>
<rectangle x1="13.69" y1="1.25" x2="13.79" y2="1.35" layer="25"/>
<rectangle x1="13.39" y1="2.35" x2="13.79" y2="2.45" layer="25"/>
<rectangle x1="13.39" y1="2.25" x2="13.79" y2="2.35" layer="25"/>
<rectangle x1="13.39" y1="2.15" x2="13.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="2.05" x2="13.79" y2="2.15" layer="25"/>
<rectangle x1="13.39" y1="0.75" x2="13.79" y2="0.85" layer="25"/>
<rectangle x1="13.39" y1="0.85" x2="13.79" y2="0.95" layer="25"/>
<rectangle x1="13.39" y1="0.95" x2="13.79" y2="1.05" layer="25"/>
<rectangle x1="13.39" y1="1.05" x2="13.79" y2="1.15" layer="25"/>
<rectangle x1="13.29" y1="0.65" x2="13.39" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.65" x2="12.49" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.75" x2="12.49" y2="0.85" layer="25"/>
<rectangle x1="12.49" y1="0.65" x2="12.59" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="2.45" x2="12.49" y2="2.55" layer="25"/>
<rectangle x1="12.39" y1="2.35" x2="12.49" y2="2.45" layer="25"/>
<rectangle x1="12.49" y1="2.45" x2="12.59" y2="2.55" layer="25"/>
<rectangle x1="13.29" y1="2.45" x2="13.39" y2="2.55" layer="25"/>
<rectangle x1="13.59" y1="1.95" x2="13.69" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="1.95" x2="6.19" y2="2.05" layer="25"/>
<rectangle x1="3.19" y1="2.05" x2="3.49" y2="2.15" layer="25"/>
<rectangle x1="7.99" y1="0.15" x2="8.29" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.15" x2="12.69" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.85" x2="12.49" y2="0.95" layer="25"/>
<rectangle x1="12.39" y1="2.27" x2="12.49" y2="2.37" layer="25"/>
</package>
<package name="MUTANTC_LOGO" library_version="120">
<rectangle x1="1.87" y1="0.05" x2="1.93" y2="0.07" layer="25"/>
<rectangle x1="1.61" y1="0.07" x2="2.17" y2="0.09" layer="25"/>
<rectangle x1="1.51" y1="0.09" x2="2.27" y2="0.11" layer="25"/>
<rectangle x1="1.43" y1="0.11" x2="2.35" y2="0.13" layer="25"/>
<rectangle x1="1.35" y1="0.13" x2="2.41" y2="0.15" layer="25"/>
<rectangle x1="1.29" y1="0.15" x2="2.47" y2="0.17" layer="25"/>
<rectangle x1="1.25" y1="0.17" x2="2.53" y2="0.19" layer="25"/>
<rectangle x1="1.19" y1="0.19" x2="2.59" y2="0.21" layer="25"/>
<rectangle x1="1.15" y1="0.21" x2="2.63" y2="0.23" layer="25"/>
<rectangle x1="1.11" y1="0.23" x2="2.67" y2="0.25" layer="25"/>
<rectangle x1="1.07" y1="0.25" x2="2.71" y2="0.27" layer="25"/>
<rectangle x1="1.03" y1="0.27" x2="1.31" y2="0.29" layer="25"/>
<rectangle x1="1.53" y1="0.27" x2="2.75" y2="0.29" layer="25"/>
<rectangle x1="0.99" y1="0.29" x2="1.21" y2="0.31" layer="25"/>
<rectangle x1="1.63" y1="0.29" x2="2.79" y2="0.31" layer="25"/>
<rectangle x1="0.95" y1="0.31" x2="1.15" y2="0.33" layer="25"/>
<rectangle x1="1.69" y1="0.31" x2="2.81" y2="0.33" layer="25"/>
<rectangle x1="0.93" y1="0.33" x2="1.11" y2="0.35" layer="25"/>
<rectangle x1="1.73" y1="0.33" x2="2.85" y2="0.35" layer="25"/>
<rectangle x1="0.89" y1="0.35" x2="1.05" y2="0.37" layer="25"/>
<rectangle x1="1.77" y1="0.35" x2="2.87" y2="0.37" layer="25"/>
<rectangle x1="0.87" y1="0.37" x2="1.03" y2="0.39" layer="25"/>
<rectangle x1="1.81" y1="0.37" x2="2.91" y2="0.39" layer="25"/>
<rectangle x1="0.83" y1="0.39" x2="0.99" y2="0.41" layer="25"/>
<rectangle x1="1.85" y1="0.39" x2="2.93" y2="0.41" layer="25"/>
<rectangle x1="0.81" y1="0.41" x2="0.95" y2="0.43" layer="25"/>
<rectangle x1="1.87" y1="0.41" x2="2.97" y2="0.43" layer="25"/>
<rectangle x1="0.79" y1="0.43" x2="0.93" y2="0.45" layer="25"/>
<rectangle x1="1.89" y1="0.43" x2="2.99" y2="0.45" layer="25"/>
<rectangle x1="0.75" y1="0.45" x2="0.91" y2="0.47" layer="25"/>
<rectangle x1="1.93" y1="0.45" x2="3.01" y2="0.47" layer="25"/>
<rectangle x1="0.73" y1="0.47" x2="0.89" y2="0.49" layer="25"/>
<rectangle x1="1.95" y1="0.47" x2="3.03" y2="0.49" layer="25"/>
<rectangle x1="0.71" y1="0.49" x2="0.87" y2="0.51" layer="25"/>
<rectangle x1="1.97" y1="0.49" x2="3.05" y2="0.51" layer="25"/>
<rectangle x1="0.69" y1="0.51" x2="0.85" y2="0.53" layer="25"/>
<rectangle x1="1.99" y1="0.51" x2="3.09" y2="0.53" layer="25"/>
<rectangle x1="0.67" y1="0.53" x2="0.83" y2="0.55" layer="25"/>
<rectangle x1="2.01" y1="0.53" x2="3.11" y2="0.55" layer="25"/>
<rectangle x1="0.65" y1="0.55" x2="0.81" y2="0.57" layer="25"/>
<rectangle x1="2.01" y1="0.55" x2="3.13" y2="0.57" layer="25"/>
<rectangle x1="0.63" y1="0.57" x2="0.79" y2="0.59" layer="25"/>
<rectangle x1="2.03" y1="0.57" x2="3.15" y2="0.59" layer="25"/>
<rectangle x1="0.61" y1="0.59" x2="0.77" y2="0.61" layer="25"/>
<rectangle x1="2.05" y1="0.59" x2="3.17" y2="0.61" layer="25"/>
<rectangle x1="0.59" y1="0.61" x2="0.77" y2="0.63" layer="25"/>
<rectangle x1="2.07" y1="0.61" x2="3.19" y2="0.63" layer="25"/>
<rectangle x1="0.57" y1="0.63" x2="0.75" y2="0.65" layer="25"/>
<rectangle x1="2.07" y1="0.63" x2="3.21" y2="0.65" layer="25"/>
<rectangle x1="0.55" y1="0.65" x2="0.73" y2="0.67" layer="25"/>
<rectangle x1="2.09" y1="0.65" x2="3.21" y2="0.67" layer="25"/>
<rectangle x1="0.53" y1="0.67" x2="0.73" y2="0.69" layer="25"/>
<rectangle x1="2.09" y1="0.67" x2="3.23" y2="0.69" layer="25"/>
<rectangle x1="0.51" y1="0.69" x2="0.71" y2="0.71" layer="25"/>
<rectangle x1="2.11" y1="0.69" x2="3.25" y2="0.71" layer="25"/>
<rectangle x1="0.49" y1="0.71" x2="0.71" y2="0.73" layer="25"/>
<rectangle x1="2.11" y1="0.71" x2="3.27" y2="0.73" layer="25"/>
<rectangle x1="0.47" y1="0.73" x2="0.69" y2="0.75" layer="25"/>
<rectangle x1="2.13" y1="0.73" x2="3.29" y2="0.75" layer="25"/>
<rectangle x1="0.45" y1="0.75" x2="0.69" y2="0.77" layer="25"/>
<rectangle x1="2.13" y1="0.75" x2="3.31" y2="0.77" layer="25"/>
<rectangle x1="0.45" y1="0.77" x2="0.67" y2="0.79" layer="25"/>
<rectangle x1="2.13" y1="0.77" x2="3.31" y2="0.79" layer="25"/>
<rectangle x1="0.43" y1="0.79" x2="0.67" y2="0.81" layer="25"/>
<rectangle x1="2.15" y1="0.79" x2="3.33" y2="0.81" layer="25"/>
<rectangle x1="0.41" y1="0.81" x2="0.65" y2="0.83" layer="25"/>
<rectangle x1="2.15" y1="0.81" x2="2.67" y2="0.83" layer="25"/>
<rectangle x1="2.71" y1="0.81" x2="3.35" y2="0.83" layer="25"/>
<rectangle x1="0.41" y1="0.83" x2="0.65" y2="0.85" layer="25"/>
<rectangle x1="2.15" y1="0.83" x2="2.65" y2="0.85" layer="25"/>
<rectangle x1="2.75" y1="0.83" x2="3.37" y2="0.85" layer="25"/>
<rectangle x1="0.39" y1="0.85" x2="0.65" y2="0.87" layer="25"/>
<rectangle x1="2.17" y1="0.85" x2="2.63" y2="0.87" layer="25"/>
<rectangle x1="2.77" y1="0.85" x2="3.37" y2="0.87" layer="25"/>
<rectangle x1="0.37" y1="0.87" x2="0.65" y2="0.89" layer="25"/>
<rectangle x1="2.17" y1="0.87" x2="2.61" y2="0.89" layer="25"/>
<rectangle x1="2.81" y1="0.87" x2="3.39" y2="0.89" layer="25"/>
<rectangle x1="0.35" y1="0.89" x2="0.63" y2="0.91" layer="25"/>
<rectangle x1="2.17" y1="0.89" x2="2.61" y2="0.91" layer="25"/>
<rectangle x1="2.83" y1="0.89" x2="3.39" y2="0.91" layer="25"/>
<rectangle x1="0.35" y1="0.91" x2="0.63" y2="0.93" layer="25"/>
<rectangle x1="2.17" y1="0.91" x2="2.57" y2="0.93" layer="25"/>
<rectangle x1="2.85" y1="0.91" x2="3.41" y2="0.93" layer="25"/>
<rectangle x1="0.33" y1="0.93" x2="0.63" y2="0.95" layer="25"/>
<rectangle x1="2.17" y1="0.93" x2="2.47" y2="0.95" layer="25"/>
<rectangle x1="2.89" y1="0.93" x2="3.43" y2="0.95" layer="25"/>
<rectangle x1="0.33" y1="0.95" x2="0.63" y2="0.97" layer="25"/>
<rectangle x1="2.17" y1="0.95" x2="2.35" y2="0.97" layer="25"/>
<rectangle x1="2.91" y1="0.95" x2="3.43" y2="0.97" layer="25"/>
<rectangle x1="0.31" y1="0.97" x2="0.63" y2="0.99" layer="25"/>
<rectangle x1="2.19" y1="0.97" x2="2.25" y2="0.99" layer="25"/>
<rectangle x1="2.93" y1="0.97" x2="3.45" y2="0.99" layer="25"/>
<rectangle x1="0.29" y1="0.99" x2="0.61" y2="1.01" layer="25"/>
<rectangle x1="2.91" y1="0.99" x2="3.45" y2="1.01" layer="25"/>
<rectangle x1="0.29" y1="1.01" x2="0.61" y2="1.03" layer="25"/>
<rectangle x1="2.89" y1="1.01" x2="3.47" y2="1.03" layer="25"/>
<rectangle x1="0.27" y1="1.03" x2="0.61" y2="1.05" layer="25"/>
<rectangle x1="2.87" y1="1.03" x2="3.47" y2="1.05" layer="25"/>
<rectangle x1="0.27" y1="1.05" x2="0.61" y2="1.07" layer="25"/>
<rectangle x1="2.87" y1="1.05" x2="3.49" y2="1.07" layer="25"/>
<rectangle x1="0.25" y1="1.07" x2="0.61" y2="1.09" layer="25"/>
<rectangle x1="2.85" y1="1.07" x2="3.49" y2="1.09" layer="25"/>
<rectangle x1="0.25" y1="1.09" x2="0.61" y2="1.11" layer="25"/>
<rectangle x1="2.83" y1="1.09" x2="3.51" y2="1.11" layer="25"/>
<rectangle x1="0.23" y1="1.11" x2="0.61" y2="1.13" layer="25"/>
<rectangle x1="2.81" y1="1.11" x2="3.51" y2="1.13" layer="25"/>
<rectangle x1="0.23" y1="1.13" x2="0.61" y2="1.15" layer="25"/>
<rectangle x1="2.81" y1="1.13" x2="3.53" y2="1.15" layer="25"/>
<rectangle x1="0.21" y1="1.15" x2="0.61" y2="1.17" layer="25"/>
<rectangle x1="2.79" y1="1.15" x2="3.53" y2="1.17" layer="25"/>
<rectangle x1="0.21" y1="1.17" x2="0.63" y2="1.19" layer="25"/>
<rectangle x1="2.77" y1="1.17" x2="3.55" y2="1.19" layer="25"/>
<rectangle x1="0.21" y1="1.19" x2="0.63" y2="1.21" layer="25"/>
<rectangle x1="2.75" y1="1.19" x2="3.55" y2="1.21" layer="25"/>
<rectangle x1="0.19" y1="1.21" x2="0.63" y2="1.23" layer="25"/>
<rectangle x1="2.75" y1="1.21" x2="3.55" y2="1.23" layer="25"/>
<rectangle x1="0.19" y1="1.23" x2="0.63" y2="1.25" layer="25"/>
<rectangle x1="2.31" y1="1.23" x2="2.35" y2="1.25" layer="25"/>
<rectangle x1="2.73" y1="1.23" x2="3.57" y2="1.25" layer="25"/>
<rectangle x1="0.17" y1="1.25" x2="0.63" y2="1.27" layer="25"/>
<rectangle x1="2.21" y1="1.25" x2="2.33" y2="1.27" layer="25"/>
<rectangle x1="2.71" y1="1.25" x2="3.57" y2="1.27" layer="25"/>
<rectangle x1="0.17" y1="1.27" x2="0.63" y2="1.29" layer="25"/>
<rectangle x1="2.15" y1="1.27" x2="2.31" y2="1.29" layer="25"/>
<rectangle x1="2.69" y1="1.27" x2="3.59" y2="1.29" layer="25"/>
<rectangle x1="0.17" y1="1.29" x2="0.65" y2="1.31" layer="25"/>
<rectangle x1="2.15" y1="1.29" x2="2.29" y2="1.31" layer="25"/>
<rectangle x1="2.69" y1="1.29" x2="3.59" y2="1.31" layer="25"/>
<rectangle x1="0.15" y1="1.31" x2="0.65" y2="1.33" layer="25"/>
<rectangle x1="2.13" y1="1.31" x2="2.29" y2="1.33" layer="25"/>
<rectangle x1="2.69" y1="1.31" x2="3.59" y2="1.33" layer="25"/>
<rectangle x1="0.15" y1="1.33" x2="0.65" y2="1.35" layer="25"/>
<rectangle x1="2.13" y1="1.33" x2="2.27" y2="1.35" layer="25"/>
<rectangle x1="2.69" y1="1.33" x2="3.61" y2="1.35" layer="25"/>
<rectangle x1="0.15" y1="1.35" x2="0.67" y2="1.37" layer="25"/>
<rectangle x1="2.13" y1="1.35" x2="2.25" y2="1.37" layer="25"/>
<rectangle x1="2.69" y1="1.35" x2="3.61" y2="1.37" layer="25"/>
<rectangle x1="0.13" y1="1.37" x2="0.67" y2="1.39" layer="25"/>
<rectangle x1="2.11" y1="1.37" x2="2.23" y2="1.39" layer="25"/>
<rectangle x1="2.69" y1="1.37" x2="3.61" y2="1.39" layer="25"/>
<rectangle x1="0.13" y1="1.39" x2="0.67" y2="1.41" layer="25"/>
<rectangle x1="2.11" y1="1.39" x2="2.23" y2="1.41" layer="25"/>
<rectangle x1="2.69" y1="1.39" x2="3.61" y2="1.41" layer="25"/>
<rectangle x1="0.13" y1="1.41" x2="0.69" y2="1.43" layer="25"/>
<rectangle x1="2.09" y1="1.41" x2="2.21" y2="1.43" layer="25"/>
<rectangle x1="2.69" y1="1.41" x2="3.63" y2="1.43" layer="25"/>
<rectangle x1="0.13" y1="1.43" x2="0.69" y2="1.45" layer="25"/>
<rectangle x1="2.09" y1="1.43" x2="2.19" y2="1.45" layer="25"/>
<rectangle x1="2.69" y1="1.43" x2="3.63" y2="1.45" layer="25"/>
<rectangle x1="0.11" y1="1.45" x2="0.71" y2="1.47" layer="25"/>
<rectangle x1="2.07" y1="1.45" x2="2.17" y2="1.47" layer="25"/>
<rectangle x1="2.69" y1="1.45" x2="3.63" y2="1.47" layer="25"/>
<rectangle x1="0.11" y1="1.47" x2="0.71" y2="1.49" layer="25"/>
<rectangle x1="2.07" y1="1.47" x2="2.15" y2="1.49" layer="25"/>
<rectangle x1="2.69" y1="1.47" x2="3.63" y2="1.49" layer="25"/>
<rectangle x1="0.11" y1="1.49" x2="0.73" y2="1.51" layer="25"/>
<rectangle x1="2.05" y1="1.49" x2="2.15" y2="1.51" layer="25"/>
<rectangle x1="2.69" y1="1.49" x2="3.65" y2="1.51" layer="25"/>
<rectangle x1="0.11" y1="1.51" x2="0.75" y2="1.53" layer="25"/>
<rectangle x1="2.03" y1="1.51" x2="2.13" y2="1.53" layer="25"/>
<rectangle x1="2.69" y1="1.51" x2="3.65" y2="1.53" layer="25"/>
<rectangle x1="0.09" y1="1.53" x2="0.75" y2="1.55" layer="25"/>
<rectangle x1="2.01" y1="1.53" x2="2.11" y2="1.55" layer="25"/>
<rectangle x1="2.69" y1="1.53" x2="3.65" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.77" y2="1.57" layer="25"/>
<rectangle x1="2.01" y1="1.55" x2="2.09" y2="1.57" layer="25"/>
<rectangle x1="2.69" y1="1.55" x2="3.65" y2="1.57" layer="25"/>
<rectangle x1="0.09" y1="1.57" x2="0.79" y2="1.59" layer="25"/>
<rectangle x1="1.99" y1="1.57" x2="2.09" y2="1.59" layer="25"/>
<rectangle x1="2.69" y1="1.57" x2="3.65" y2="1.59" layer="25"/>
<rectangle x1="0.09" y1="1.59" x2="0.81" y2="1.61" layer="25"/>
<rectangle x1="1.97" y1="1.59" x2="2.07" y2="1.61" layer="25"/>
<rectangle x1="2.69" y1="1.59" x2="3.65" y2="1.61" layer="25"/>
<rectangle x1="0.09" y1="1.61" x2="0.83" y2="1.63" layer="25"/>
<rectangle x1="1.95" y1="1.61" x2="2.05" y2="1.63" layer="25"/>
<rectangle x1="2.43" y1="1.61" x2="2.45" y2="1.63" layer="25"/>
<rectangle x1="2.69" y1="1.61" x2="3.67" y2="1.63" layer="25"/>
<rectangle x1="0.07" y1="1.63" x2="0.85" y2="1.65" layer="25"/>
<rectangle x1="1.93" y1="1.63" x2="2.03" y2="1.65" layer="25"/>
<rectangle x1="2.41" y1="1.63" x2="2.45" y2="1.65" layer="25"/>
<rectangle x1="2.69" y1="1.63" x2="3.67" y2="1.65" layer="25"/>
<rectangle x1="0.07" y1="1.65" x2="0.87" y2="1.67" layer="25"/>
<rectangle x1="1.91" y1="1.65" x2="2.03" y2="1.67" layer="25"/>
<rectangle x1="2.41" y1="1.65" x2="2.45" y2="1.67" layer="25"/>
<rectangle x1="2.69" y1="1.65" x2="3.67" y2="1.67" layer="25"/>
<rectangle x1="0.07" y1="1.67" x2="0.89" y2="1.69" layer="25"/>
<rectangle x1="1.89" y1="1.67" x2="2.01" y2="1.69" layer="25"/>
<rectangle x1="2.39" y1="1.67" x2="2.45" y2="1.69" layer="25"/>
<rectangle x1="2.69" y1="1.67" x2="3.67" y2="1.69" layer="25"/>
<rectangle x1="0.07" y1="1.69" x2="0.91" y2="1.71" layer="25"/>
<rectangle x1="1.85" y1="1.69" x2="1.99" y2="1.71" layer="25"/>
<rectangle x1="2.37" y1="1.69" x2="2.45" y2="1.71" layer="25"/>
<rectangle x1="2.69" y1="1.69" x2="3.67" y2="1.71" layer="25"/>
<rectangle x1="0.07" y1="1.71" x2="0.93" y2="1.73" layer="25"/>
<rectangle x1="1.83" y1="1.71" x2="1.97" y2="1.73" layer="25"/>
<rectangle x1="2.35" y1="1.71" x2="2.45" y2="1.73" layer="25"/>
<rectangle x1="2.69" y1="1.71" x2="3.67" y2="1.73" layer="25"/>
<rectangle x1="0.07" y1="1.73" x2="0.97" y2="1.75" layer="25"/>
<rectangle x1="1.81" y1="1.73" x2="1.97" y2="1.75" layer="25"/>
<rectangle x1="2.35" y1="1.73" x2="2.45" y2="1.75" layer="25"/>
<rectangle x1="2.69" y1="1.73" x2="3.67" y2="1.75" layer="25"/>
<rectangle x1="0.07" y1="1.75" x2="0.99" y2="1.77" layer="25"/>
<rectangle x1="1.77" y1="1.75" x2="1.95" y2="1.77" layer="25"/>
<rectangle x1="2.33" y1="1.75" x2="2.45" y2="1.77" layer="25"/>
<rectangle x1="2.69" y1="1.75" x2="3.67" y2="1.77" layer="25"/>
<rectangle x1="0.07" y1="1.77" x2="1.03" y2="1.79" layer="25"/>
<rectangle x1="1.73" y1="1.77" x2="1.93" y2="1.79" layer="25"/>
<rectangle x1="2.31" y1="1.77" x2="2.45" y2="1.79" layer="25"/>
<rectangle x1="2.69" y1="1.77" x2="3.67" y2="1.79" layer="25"/>
<rectangle x1="0.07" y1="1.79" x2="1.09" y2="1.81" layer="25"/>
<rectangle x1="1.69" y1="1.79" x2="1.91" y2="1.81" layer="25"/>
<rectangle x1="2.29" y1="1.79" x2="2.45" y2="1.81" layer="25"/>
<rectangle x1="2.69" y1="1.79" x2="3.67" y2="1.81" layer="25"/>
<rectangle x1="0.07" y1="1.81" x2="1.13" y2="1.83" layer="25"/>
<rectangle x1="1.63" y1="1.81" x2="1.89" y2="1.83" layer="25"/>
<rectangle x1="2.29" y1="1.81" x2="2.45" y2="1.83" layer="25"/>
<rectangle x1="2.69" y1="1.81" x2="3.69" y2="1.83" layer="25"/>
<rectangle x1="0.07" y1="1.83" x2="1.21" y2="1.85" layer="25"/>
<rectangle x1="1.55" y1="1.83" x2="1.89" y2="1.85" layer="25"/>
<rectangle x1="2.27" y1="1.83" x2="2.45" y2="1.85" layer="25"/>
<rectangle x1="2.69" y1="1.83" x2="3.69" y2="1.85" layer="25"/>
<rectangle x1="0.05" y1="1.85" x2="1.35" y2="1.87" layer="25"/>
<rectangle x1="1.39" y1="1.85" x2="1.87" y2="1.87" layer="25"/>
<rectangle x1="2.25" y1="1.85" x2="2.45" y2="1.87" layer="25"/>
<rectangle x1="2.69" y1="1.85" x2="3.69" y2="1.87" layer="25"/>
<rectangle x1="0.05" y1="1.87" x2="1.85" y2="1.89" layer="25"/>
<rectangle x1="2.23" y1="1.87" x2="2.45" y2="1.89" layer="25"/>
<rectangle x1="2.69" y1="1.87" x2="3.69" y2="1.89" layer="25"/>
<rectangle x1="0.05" y1="1.89" x2="1.83" y2="1.91" layer="25"/>
<rectangle x1="2.23" y1="1.89" x2="2.45" y2="1.91" layer="25"/>
<rectangle x1="2.69" y1="1.89" x2="3.69" y2="1.91" layer="25"/>
<rectangle x1="0.05" y1="1.91" x2="1.83" y2="1.93" layer="25"/>
<rectangle x1="2.21" y1="1.91" x2="2.45" y2="1.93" layer="25"/>
<rectangle x1="2.69" y1="1.91" x2="3.69" y2="1.93" layer="25"/>
<rectangle x1="0.05" y1="1.93" x2="1.81" y2="1.95" layer="25"/>
<rectangle x1="2.19" y1="1.93" x2="2.45" y2="1.95" layer="25"/>
<rectangle x1="2.69" y1="1.93" x2="3.69" y2="1.95" layer="25"/>
<rectangle x1="0.05" y1="1.95" x2="1.79" y2="1.97" layer="25"/>
<rectangle x1="2.17" y1="1.95" x2="2.45" y2="1.97" layer="25"/>
<rectangle x1="2.69" y1="1.95" x2="3.67" y2="1.97" layer="25"/>
<rectangle x1="0.05" y1="1.97" x2="1.77" y2="1.99" layer="25"/>
<rectangle x1="2.15" y1="1.97" x2="2.45" y2="1.99" layer="25"/>
<rectangle x1="2.69" y1="1.97" x2="3.67" y2="1.99" layer="25"/>
<rectangle x1="0.07" y1="1.99" x2="1.77" y2="2.01" layer="25"/>
<rectangle x1="2.15" y1="1.99" x2="2.45" y2="2.01" layer="25"/>
<rectangle x1="2.69" y1="1.99" x2="3.67" y2="2.01" layer="25"/>
<rectangle x1="0.07" y1="2.01" x2="1.75" y2="2.03" layer="25"/>
<rectangle x1="2.13" y1="2.01" x2="2.45" y2="2.03" layer="25"/>
<rectangle x1="2.69" y1="2.01" x2="3.67" y2="2.03" layer="25"/>
<rectangle x1="0.07" y1="2.03" x2="1.73" y2="2.05" layer="25"/>
<rectangle x1="2.11" y1="2.03" x2="2.43" y2="2.05" layer="25"/>
<rectangle x1="2.77" y1="2.03" x2="3.67" y2="2.05" layer="25"/>
<rectangle x1="0.07" y1="2.05" x2="1.71" y2="2.07" layer="25"/>
<rectangle x1="2.09" y1="2.05" x2="2.39" y2="2.07" layer="25"/>
<rectangle x1="2.83" y1="2.05" x2="3.67" y2="2.07" layer="25"/>
<rectangle x1="0.07" y1="2.07" x2="1.69" y2="2.09" layer="25"/>
<rectangle x1="2.09" y1="2.07" x2="2.35" y2="2.09" layer="25"/>
<rectangle x1="2.87" y1="2.07" x2="3.67" y2="2.09" layer="25"/>
<rectangle x1="0.07" y1="2.09" x2="1.69" y2="2.11" layer="25"/>
<rectangle x1="2.07" y1="2.09" x2="2.31" y2="2.11" layer="25"/>
<rectangle x1="2.91" y1="2.09" x2="3.67" y2="2.11" layer="25"/>
<rectangle x1="0.07" y1="2.11" x2="1.21" y2="2.13" layer="25"/>
<rectangle x1="1.49" y1="2.11" x2="1.67" y2="2.13" layer="25"/>
<rectangle x1="2.05" y1="2.11" x2="2.27" y2="2.13" layer="25"/>
<rectangle x1="2.95" y1="2.11" x2="3.67" y2="2.13" layer="25"/>
<rectangle x1="0.07" y1="2.13" x2="1.15" y2="2.15" layer="25"/>
<rectangle x1="1.55" y1="2.13" x2="1.65" y2="2.15" layer="25"/>
<rectangle x1="2.03" y1="2.13" x2="2.25" y2="2.15" layer="25"/>
<rectangle x1="2.97" y1="2.13" x2="3.67" y2="2.15" layer="25"/>
<rectangle x1="0.07" y1="2.15" x2="1.11" y2="2.17" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="1.63" y2="2.17" layer="25"/>
<rectangle x1="2.03" y1="2.15" x2="2.23" y2="2.17" layer="25"/>
<rectangle x1="2.99" y1="2.15" x2="3.67" y2="2.17" layer="25"/>
<rectangle x1="0.07" y1="2.17" x2="1.07" y2="2.19" layer="25"/>
<rectangle x1="2.01" y1="2.17" x2="2.21" y2="2.19" layer="25"/>
<rectangle x1="3.01" y1="2.17" x2="3.65" y2="2.19" layer="25"/>
<rectangle x1="0.09" y1="2.19" x2="1.03" y2="2.21" layer="25"/>
<rectangle x1="1.99" y1="2.19" x2="2.19" y2="2.21" layer="25"/>
<rectangle x1="3.03" y1="2.19" x2="3.65" y2="2.21" layer="25"/>
<rectangle x1="0.09" y1="2.21" x2="1.01" y2="2.23" layer="25"/>
<rectangle x1="1.97" y1="2.21" x2="2.17" y2="2.23" layer="25"/>
<rectangle x1="3.05" y1="2.21" x2="3.65" y2="2.23" layer="25"/>
<rectangle x1="0.09" y1="2.23" x2="0.97" y2="2.25" layer="25"/>
<rectangle x1="1.97" y1="2.23" x2="2.15" y2="2.25" layer="25"/>
<rectangle x1="3.07" y1="2.23" x2="3.65" y2="2.25" layer="25"/>
<rectangle x1="0.09" y1="2.25" x2="0.95" y2="2.27" layer="25"/>
<rectangle x1="1.95" y1="2.25" x2="2.13" y2="2.27" layer="25"/>
<rectangle x1="3.09" y1="2.25" x2="3.65" y2="2.27" layer="25"/>
<rectangle x1="0.09" y1="2.27" x2="0.93" y2="2.29" layer="25"/>
<rectangle x1="1.93" y1="2.27" x2="2.13" y2="2.29" layer="25"/>
<rectangle x1="3.11" y1="2.27" x2="3.63" y2="2.29" layer="25"/>
<rectangle x1="0.09" y1="2.29" x2="0.91" y2="2.31" layer="25"/>
<rectangle x1="1.91" y1="2.29" x2="2.11" y2="2.31" layer="25"/>
<rectangle x1="3.11" y1="2.29" x2="3.63" y2="2.31" layer="25"/>
<rectangle x1="0.11" y1="2.31" x2="0.89" y2="2.33" layer="25"/>
<rectangle x1="1.89" y1="2.31" x2="2.11" y2="2.33" layer="25"/>
<rectangle x1="3.13" y1="2.31" x2="3.63" y2="2.33" layer="25"/>
<rectangle x1="0.11" y1="2.33" x2="0.89" y2="2.35" layer="25"/>
<rectangle x1="1.89" y1="2.33" x2="2.09" y2="2.35" layer="25"/>
<rectangle x1="3.13" y1="2.33" x2="3.63" y2="2.35" layer="25"/>
<rectangle x1="0.11" y1="2.35" x2="0.87" y2="2.37" layer="25"/>
<rectangle x1="1.87" y1="2.35" x2="2.09" y2="2.37" layer="25"/>
<rectangle x1="3.15" y1="2.35" x2="3.61" y2="2.37" layer="25"/>
<rectangle x1="0.11" y1="2.37" x2="0.85" y2="2.39" layer="25"/>
<rectangle x1="1.85" y1="2.37" x2="2.07" y2="2.39" layer="25"/>
<rectangle x1="3.15" y1="2.37" x2="3.61" y2="2.39" layer="25"/>
<rectangle x1="0.13" y1="2.39" x2="0.85" y2="2.41" layer="25"/>
<rectangle x1="1.85" y1="2.39" x2="2.07" y2="2.41" layer="25"/>
<rectangle x1="3.17" y1="2.39" x2="3.61" y2="2.41" layer="25"/>
<rectangle x1="0.13" y1="2.41" x2="0.83" y2="2.43" layer="25"/>
<rectangle x1="1.87" y1="2.41" x2="2.07" y2="2.43" layer="25"/>
<rectangle x1="3.17" y1="2.41" x2="3.61" y2="2.43" layer="25"/>
<rectangle x1="0.13" y1="2.43" x2="0.83" y2="2.45" layer="25"/>
<rectangle x1="1.87" y1="2.43" x2="2.07" y2="2.45" layer="25"/>
<rectangle x1="3.17" y1="2.43" x2="3.59" y2="2.45" layer="25"/>
<rectangle x1="0.13" y1="2.45" x2="0.81" y2="2.47" layer="25"/>
<rectangle x1="1.87" y1="2.45" x2="2.05" y2="2.47" layer="25"/>
<rectangle x1="3.19" y1="2.45" x2="3.59" y2="2.47" layer="25"/>
<rectangle x1="0.15" y1="2.47" x2="0.81" y2="2.49" layer="25"/>
<rectangle x1="1.89" y1="2.47" x2="2.05" y2="2.49" layer="25"/>
<rectangle x1="3.19" y1="2.47" x2="3.59" y2="2.49" layer="25"/>
<rectangle x1="0.15" y1="2.49" x2="0.79" y2="2.51" layer="25"/>
<rectangle x1="1.89" y1="2.49" x2="2.05" y2="2.51" layer="25"/>
<rectangle x1="3.19" y1="2.49" x2="3.57" y2="2.51" layer="25"/>
<rectangle x1="0.15" y1="2.51" x2="0.79" y2="2.53" layer="25"/>
<rectangle x1="1.89" y1="2.51" x2="2.05" y2="2.53" layer="25"/>
<rectangle x1="3.19" y1="2.51" x2="3.57" y2="2.53" layer="25"/>
<rectangle x1="0.17" y1="2.53" x2="0.79" y2="2.55" layer="25"/>
<rectangle x1="1.91" y1="2.53" x2="2.05" y2="2.55" layer="25"/>
<rectangle x1="3.19" y1="2.53" x2="3.57" y2="2.55" layer="25"/>
<rectangle x1="0.17" y1="2.55" x2="0.79" y2="2.57" layer="25"/>
<rectangle x1="1.91" y1="2.55" x2="2.05" y2="2.57" layer="25"/>
<rectangle x1="3.19" y1="2.55" x2="3.55" y2="2.57" layer="25"/>
<rectangle x1="0.19" y1="2.57" x2="0.77" y2="2.59" layer="25"/>
<rectangle x1="1.91" y1="2.57" x2="2.05" y2="2.59" layer="25"/>
<rectangle x1="3.21" y1="2.57" x2="3.55" y2="2.59" layer="25"/>
<rectangle x1="0.19" y1="2.59" x2="0.77" y2="2.61" layer="25"/>
<rectangle x1="1.91" y1="2.59" x2="2.05" y2="2.61" layer="25"/>
<rectangle x1="3.21" y1="2.59" x2="3.53" y2="2.61" layer="25"/>
<rectangle x1="0.19" y1="2.61" x2="0.77" y2="2.63" layer="25"/>
<rectangle x1="1.91" y1="2.61" x2="2.05" y2="2.63" layer="25"/>
<rectangle x1="3.21" y1="2.61" x2="3.53" y2="2.63" layer="25"/>
<rectangle x1="0.21" y1="2.63" x2="0.77" y2="2.65" layer="25"/>
<rectangle x1="1.91" y1="2.63" x2="2.05" y2="2.65" layer="25"/>
<rectangle x1="3.21" y1="2.63" x2="3.53" y2="2.65" layer="25"/>
<rectangle x1="0.21" y1="2.65" x2="0.77" y2="2.67" layer="25"/>
<rectangle x1="1.93" y1="2.65" x2="2.05" y2="2.67" layer="25"/>
<rectangle x1="3.19" y1="2.65" x2="3.51" y2="2.67" layer="25"/>
<rectangle x1="0.23" y1="2.67" x2="0.77" y2="2.69" layer="25"/>
<rectangle x1="1.93" y1="2.67" x2="2.05" y2="2.69" layer="25"/>
<rectangle x1="3.19" y1="2.67" x2="3.51" y2="2.69" layer="25"/>
<rectangle x1="0.23" y1="2.69" x2="0.77" y2="2.71" layer="25"/>
<rectangle x1="1.93" y1="2.69" x2="2.05" y2="2.71" layer="25"/>
<rectangle x1="3.19" y1="2.69" x2="3.49" y2="2.71" layer="25"/>
<rectangle x1="0.25" y1="2.71" x2="0.77" y2="2.73" layer="25"/>
<rectangle x1="1.91" y1="2.71" x2="2.07" y2="2.73" layer="25"/>
<rectangle x1="3.19" y1="2.71" x2="3.49" y2="2.73" layer="25"/>
<rectangle x1="0.25" y1="2.73" x2="0.77" y2="2.75" layer="25"/>
<rectangle x1="1.91" y1="2.73" x2="2.07" y2="2.75" layer="25"/>
<rectangle x1="3.19" y1="2.73" x2="3.47" y2="2.75" layer="25"/>
<rectangle x1="0.27" y1="2.75" x2="0.77" y2="2.77" layer="25"/>
<rectangle x1="1.91" y1="2.75" x2="2.07" y2="2.77" layer="25"/>
<rectangle x1="3.19" y1="2.75" x2="3.47" y2="2.77" layer="25"/>
<rectangle x1="0.27" y1="2.77" x2="0.77" y2="2.79" layer="25"/>
<rectangle x1="1.91" y1="2.77" x2="2.09" y2="2.79" layer="25"/>
<rectangle x1="3.17" y1="2.77" x2="3.45" y2="2.79" layer="25"/>
<rectangle x1="0.29" y1="2.79" x2="0.77" y2="2.81" layer="25"/>
<rectangle x1="1.91" y1="2.79" x2="2.09" y2="2.81" layer="25"/>
<rectangle x1="3.17" y1="2.79" x2="3.45" y2="2.81" layer="25"/>
<rectangle x1="0.29" y1="2.81" x2="0.79" y2="2.83" layer="25"/>
<rectangle x1="1.91" y1="2.81" x2="2.11" y2="2.83" layer="25"/>
<rectangle x1="3.17" y1="2.81" x2="3.43" y2="2.83" layer="25"/>
<rectangle x1="0.31" y1="2.83" x2="0.79" y2="2.85" layer="25"/>
<rectangle x1="1.89" y1="2.83" x2="2.11" y2="2.85" layer="25"/>
<rectangle x1="3.15" y1="2.83" x2="3.41" y2="2.85" layer="25"/>
<rectangle x1="0.31" y1="2.85" x2="0.79" y2="2.87" layer="25"/>
<rectangle x1="1.89" y1="2.85" x2="2.13" y2="2.87" layer="25"/>
<rectangle x1="3.15" y1="2.85" x2="3.41" y2="2.87" layer="25"/>
<rectangle x1="0.33" y1="2.87" x2="0.79" y2="2.89" layer="25"/>
<rectangle x1="1.89" y1="2.87" x2="2.13" y2="2.89" layer="25"/>
<rectangle x1="3.13" y1="2.87" x2="3.39" y2="2.89" layer="25"/>
<rectangle x1="0.35" y1="2.89" x2="0.81" y2="2.91" layer="25"/>
<rectangle x1="1.87" y1="2.89" x2="2.15" y2="2.91" layer="25"/>
<rectangle x1="3.13" y1="2.89" x2="3.39" y2="2.91" layer="25"/>
<rectangle x1="0.35" y1="2.91" x2="0.81" y2="2.93" layer="25"/>
<rectangle x1="1.87" y1="2.91" x2="2.15" y2="2.93" layer="25"/>
<rectangle x1="3.11" y1="2.91" x2="3.37" y2="2.93" layer="25"/>
<rectangle x1="0.37" y1="2.93" x2="0.83" y2="2.95" layer="25"/>
<rectangle x1="1.85" y1="2.93" x2="2.17" y2="2.95" layer="25"/>
<rectangle x1="3.09" y1="2.93" x2="3.35" y2="2.95" layer="25"/>
<rectangle x1="0.37" y1="2.95" x2="0.83" y2="2.97" layer="25"/>
<rectangle x1="1.85" y1="2.95" x2="2.19" y2="2.97" layer="25"/>
<rectangle x1="3.07" y1="2.95" x2="3.35" y2="2.97" layer="25"/>
<rectangle x1="0.39" y1="2.97" x2="0.85" y2="2.99" layer="25"/>
<rectangle x1="1.83" y1="2.97" x2="2.21" y2="2.99" layer="25"/>
<rectangle x1="3.07" y1="2.97" x2="3.33" y2="2.99" layer="25"/>
<rectangle x1="0.41" y1="2.99" x2="0.85" y2="3.01" layer="25"/>
<rectangle x1="1.83" y1="2.99" x2="2.23" y2="3.01" layer="25"/>
<rectangle x1="3.05" y1="2.99" x2="3.31" y2="3.01" layer="25"/>
<rectangle x1="0.43" y1="3.01" x2="0.87" y2="3.03" layer="25"/>
<rectangle x1="1.81" y1="3.01" x2="2.25" y2="3.03" layer="25"/>
<rectangle x1="3.03" y1="3.01" x2="3.29" y2="3.03" layer="25"/>
<rectangle x1="0.43" y1="3.03" x2="0.89" y2="3.05" layer="25"/>
<rectangle x1="1.79" y1="3.03" x2="2.27" y2="3.05" layer="25"/>
<rectangle x1="2.99" y1="3.03" x2="3.29" y2="3.05" layer="25"/>
<rectangle x1="0.45" y1="3.05" x2="0.89" y2="3.07" layer="25"/>
<rectangle x1="1.79" y1="3.05" x2="2.31" y2="3.07" layer="25"/>
<rectangle x1="2.97" y1="3.05" x2="3.27" y2="3.07" layer="25"/>
<rectangle x1="0.47" y1="3.07" x2="0.91" y2="3.09" layer="25"/>
<rectangle x1="1.77" y1="3.07" x2="2.33" y2="3.09" layer="25"/>
<rectangle x1="2.95" y1="3.07" x2="3.25" y2="3.09" layer="25"/>
<rectangle x1="0.49" y1="3.09" x2="0.93" y2="3.11" layer="25"/>
<rectangle x1="1.75" y1="3.09" x2="2.37" y2="3.11" layer="25"/>
<rectangle x1="2.91" y1="3.09" x2="3.23" y2="3.11" layer="25"/>
<rectangle x1="0.51" y1="3.11" x2="0.95" y2="3.13" layer="25"/>
<rectangle x1="1.73" y1="3.11" x2="2.41" y2="3.13" layer="25"/>
<rectangle x1="2.87" y1="3.11" x2="3.21" y2="3.13" layer="25"/>
<rectangle x1="0.53" y1="3.13" x2="0.97" y2="3.15" layer="25"/>
<rectangle x1="1.69" y1="3.13" x2="2.47" y2="3.15" layer="25"/>
<rectangle x1="2.81" y1="3.13" x2="3.19" y2="3.15" layer="25"/>
<rectangle x1="0.53" y1="3.15" x2="1.01" y2="3.17" layer="25"/>
<rectangle x1="1.67" y1="3.15" x2="2.57" y2="3.17" layer="25"/>
<rectangle x1="2.71" y1="3.15" x2="3.17" y2="3.17" layer="25"/>
<rectangle x1="0.55" y1="3.17" x2="1.03" y2="3.19" layer="25"/>
<rectangle x1="1.65" y1="3.17" x2="3.15" y2="3.19" layer="25"/>
<rectangle x1="0.57" y1="3.19" x2="1.07" y2="3.21" layer="25"/>
<rectangle x1="1.61" y1="3.19" x2="3.15" y2="3.21" layer="25"/>
<rectangle x1="0.59" y1="3.21" x2="1.11" y2="3.23" layer="25"/>
<rectangle x1="1.57" y1="3.21" x2="3.13" y2="3.23" layer="25"/>
<rectangle x1="0.61" y1="3.23" x2="1.17" y2="3.25" layer="25"/>
<rectangle x1="1.51" y1="3.23" x2="3.09" y2="3.25" layer="25"/>
<rectangle x1="0.63" y1="3.25" x2="1.25" y2="3.27" layer="25"/>
<rectangle x1="1.41" y1="3.25" x2="3.07" y2="3.27" layer="25"/>
<rectangle x1="0.65" y1="3.27" x2="3.05" y2="3.29" layer="25"/>
<rectangle x1="0.69" y1="3.29" x2="3.03" y2="3.31" layer="25"/>
<rectangle x1="0.71" y1="3.31" x2="3.01" y2="3.33" layer="25"/>
<rectangle x1="0.73" y1="3.33" x2="2.99" y2="3.35" layer="25"/>
<rectangle x1="0.75" y1="3.35" x2="2.95" y2="3.37" layer="25"/>
<rectangle x1="0.77" y1="3.37" x2="2.93" y2="3.39" layer="25"/>
<rectangle x1="0.81" y1="3.39" x2="2.91" y2="3.41" layer="25"/>
<rectangle x1="0.83" y1="3.41" x2="2.87" y2="3.43" layer="25"/>
<rectangle x1="0.87" y1="3.43" x2="2.85" y2="3.45" layer="25"/>
<rectangle x1="0.89" y1="3.45" x2="2.81" y2="3.47" layer="25"/>
<rectangle x1="0.93" y1="3.47" x2="2.79" y2="3.49" layer="25"/>
<rectangle x1="0.95" y1="3.49" x2="2.75" y2="3.51" layer="25"/>
<rectangle x1="0.99" y1="3.51" x2="2.71" y2="3.53" layer="25"/>
<rectangle x1="1.03" y1="3.53" x2="2.67" y2="3.55" layer="25"/>
<rectangle x1="1.07" y1="3.55" x2="2.63" y2="3.57" layer="25"/>
<rectangle x1="1.11" y1="3.57" x2="2.59" y2="3.59" layer="25"/>
<rectangle x1="1.15" y1="3.59" x2="2.55" y2="3.61" layer="25"/>
<rectangle x1="1.21" y1="3.61" x2="2.49" y2="3.63" layer="25"/>
<rectangle x1="1.27" y1="3.63" x2="2.45" y2="3.65" layer="25"/>
<rectangle x1="1.33" y1="3.65" x2="2.39" y2="3.67" layer="25"/>
<rectangle x1="1.39" y1="3.67" x2="2.31" y2="3.69" layer="25"/>
<rectangle x1="1.47" y1="3.69" x2="2.23" y2="3.71" layer="25"/>
<rectangle x1="1.57" y1="3.71" x2="2.13" y2="3.73" layer="25"/>
<rectangle x1="1.79" y1="3.73" x2="1.91" y2="3.75" layer="25"/>
</package>
<package name="MUTANTC_TEXT+LOGO" library_version="120">
<rectangle x1="9.13" y1="0.06" x2="9.23" y2="0.16" layer="25"/>
<rectangle x1="14.63" y1="0.06" x2="14.73" y2="0.16" layer="25"/>
<rectangle x1="9.13" y1="0.16" x2="9.43" y2="0.26" layer="25"/>
<rectangle x1="14.63" y1="0.16" x2="14.93" y2="0.26" layer="25"/>
<rectangle x1="9.13" y1="0.26" x2="9.53" y2="0.36" layer="25"/>
<rectangle x1="14.63" y1="0.26" x2="15.13" y2="0.36" layer="25"/>
<rectangle x1="9.13" y1="0.36" x2="9.53" y2="0.46" layer="25"/>
<rectangle x1="14.63" y1="0.36" x2="15.13" y2="0.46" layer="25"/>
<rectangle x1="9.13" y1="0.46" x2="9.53" y2="0.56" layer="25"/>
<rectangle x1="14.63" y1="0.46" x2="15.13" y2="0.56" layer="25"/>
<rectangle x1="9.13" y1="0.56" x2="9.53" y2="0.66" layer="25"/>
<rectangle x1="14.63" y1="0.56" x2="15.13" y2="0.66" layer="25"/>
<rectangle x1="9.13" y1="0.66" x2="9.53" y2="0.76" layer="25"/>
<rectangle x1="14.63" y1="0.66" x2="15.13" y2="0.76" layer="25"/>
<rectangle x1="9.13" y1="0.76" x2="9.53" y2="0.86" layer="25"/>
<rectangle x1="14.63" y1="0.76" x2="15.13" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.86" x2="4.53" y2="0.96" layer="25"/>
<rectangle x1="7.43" y1="0.76" x2="8.53" y2="0.86" layer="25"/>
<rectangle x1="9.13" y1="0.86" x2="9.53" y2="0.96" layer="25"/>
<rectangle x1="11.33" y1="0.86" x2="11.63" y2="0.96" layer="25"/>
<rectangle x1="13.23" y1="0.86" x2="13.53" y2="0.96" layer="25"/>
<rectangle x1="14.63" y1="0.86" x2="15.13" y2="0.96" layer="25"/>
<rectangle x1="16.73" y1="0.86" x2="17.33" y2="0.96" layer="25"/>
<rectangle x1="4.23" y1="0.96" x2="4.73" y2="1.06" layer="25"/>
<rectangle x1="7.33" y1="0.86" x2="8.63" y2="0.96" layer="25"/>
<rectangle x1="9.13" y1="0.96" x2="9.53" y2="1.06" layer="25"/>
<rectangle x1="10.53" y1="0.96" x2="11.83" y2="1.06" layer="25"/>
<rectangle x1="12.13" y1="0.96" x2="12.53" y2="1.06" layer="25"/>
<rectangle x1="13.23" y1="0.96" x2="13.73" y2="1.06" layer="25"/>
<rectangle x1="14.63" y1="0.96" x2="15.13" y2="1.06" layer="25"/>
<rectangle x1="16.33" y1="0.96" x2="17.73" y2="1.06" layer="25"/>
<rectangle x1="4.23" y1="1.06" x2="4.73" y2="1.16" layer="25"/>
<rectangle x1="7.23" y1="0.96" x2="8.73" y2="1.06" layer="25"/>
<rectangle x1="9.13" y1="1.06" x2="9.53" y2="1.16" layer="25"/>
<rectangle x1="10.43" y1="1.06" x2="11.83" y2="1.16" layer="25"/>
<rectangle x1="12.13" y1="1.06" x2="12.53" y2="1.16" layer="25"/>
<rectangle x1="13.23" y1="1.06" x2="13.73" y2="1.16" layer="25"/>
<rectangle x1="14.63" y1="1.06" x2="15.13" y2="1.16" layer="25"/>
<rectangle x1="16.23" y1="1.06" x2="17.83" y2="1.16" layer="25"/>
<rectangle x1="4.23" y1="1.16" x2="4.73" y2="1.26" layer="25"/>
<rectangle x1="7.23" y1="1.06" x2="8.73" y2="1.16" layer="25"/>
<rectangle x1="9.13" y1="1.16" x2="9.53" y2="1.26" layer="25"/>
<rectangle x1="10.33" y1="1.16" x2="11.83" y2="1.26" layer="25"/>
<rectangle x1="12.13" y1="1.16" x2="12.53" y2="1.26" layer="25"/>
<rectangle x1="13.23" y1="1.16" x2="13.73" y2="1.26" layer="25"/>
<rectangle x1="14.63" y1="1.16" x2="15.13" y2="1.26" layer="25"/>
<rectangle x1="16.13" y1="1.16" x2="17.93" y2="1.26" layer="25"/>
<rectangle x1="4.23" y1="1.26" x2="4.73" y2="1.36" layer="25"/>
<rectangle x1="7.23" y1="1.16" x2="7.63" y2="1.26" layer="25"/>
<rectangle x1="8.33" y1="1.16" x2="8.73" y2="1.26" layer="25"/>
<rectangle x1="9.13" y1="1.26" x2="9.53" y2="1.36" layer="25"/>
<rectangle x1="10.33" y1="1.26" x2="11.83" y2="1.36" layer="25"/>
<rectangle x1="12.13" y1="1.26" x2="12.53" y2="1.36" layer="25"/>
<rectangle x1="13.23" y1="1.26" x2="13.73" y2="1.36" layer="25"/>
<rectangle x1="14.63" y1="1.26" x2="15.13" y2="1.36" layer="25"/>
<rectangle x1="16.13" y1="1.26" x2="17.93" y2="1.36" layer="25"/>
<rectangle x1="4.23" y1="1.36" x2="4.73" y2="1.46" layer="25"/>
<rectangle x1="7.23" y1="1.26" x2="7.63" y2="1.36" layer="25"/>
<rectangle x1="8.33" y1="1.26" x2="8.73" y2="1.36" layer="25"/>
<rectangle x1="9.13" y1="1.36" x2="9.53" y2="1.46" layer="25"/>
<rectangle x1="10.33" y1="1.36" x2="10.73" y2="1.46" layer="25"/>
<rectangle x1="11.33" y1="1.36" x2="11.83" y2="1.46" layer="25"/>
<rectangle x1="12.13" y1="1.36" x2="12.53" y2="1.46" layer="25"/>
<rectangle x1="13.23" y1="1.36" x2="13.73" y2="1.46" layer="25"/>
<rectangle x1="14.63" y1="1.36" x2="15.13" y2="1.46" layer="25"/>
<rectangle x1="16.13" y1="1.36" x2="16.53" y2="1.46" layer="25"/>
<rectangle x1="17.53" y1="1.36" x2="17.93" y2="1.46" layer="25"/>
<rectangle x1="4.23" y1="1.46" x2="4.73" y2="1.56" layer="25"/>
<rectangle x1="7.23" y1="1.36" x2="7.63" y2="1.46" layer="25"/>
<rectangle x1="8.33" y1="1.36" x2="8.73" y2="1.46" layer="25"/>
<rectangle x1="9.13" y1="1.46" x2="9.53" y2="1.56" layer="25"/>
<rectangle x1="10.33" y1="1.46" x2="10.73" y2="1.56" layer="25"/>
<rectangle x1="11.33" y1="1.46" x2="11.83" y2="1.56" layer="25"/>
<rectangle x1="12.13" y1="1.46" x2="12.53" y2="1.56" layer="25"/>
<rectangle x1="13.23" y1="1.46" x2="13.73" y2="1.56" layer="25"/>
<rectangle x1="14.63" y1="1.46" x2="15.13" y2="1.56" layer="25"/>
<rectangle x1="16.13" y1="1.46" x2="16.53" y2="1.56" layer="25"/>
<rectangle x1="4.23" y1="1.56" x2="4.73" y2="1.66" layer="25"/>
<rectangle x1="7.23" y1="1.46" x2="7.63" y2="1.56" layer="25"/>
<rectangle x1="8.33" y1="1.46" x2="8.73" y2="1.56" layer="25"/>
<rectangle x1="9.13" y1="1.56" x2="9.53" y2="1.66" layer="25"/>
<rectangle x1="10.33" y1="1.56" x2="10.73" y2="1.66" layer="25"/>
<rectangle x1="11.33" y1="1.56" x2="11.83" y2="1.66" layer="25"/>
<rectangle x1="12.13" y1="1.56" x2="12.53" y2="1.66" layer="25"/>
<rectangle x1="13.23" y1="1.56" x2="13.73" y2="1.66" layer="25"/>
<rectangle x1="14.63" y1="1.56" x2="15.13" y2="1.66" layer="25"/>
<rectangle x1="16.13" y1="1.56" x2="16.53" y2="1.66" layer="25"/>
<rectangle x1="4.23" y1="1.66" x2="4.73" y2="1.76" layer="25"/>
<rectangle x1="7.23" y1="1.56" x2="7.63" y2="1.66" layer="25"/>
<rectangle x1="8.33" y1="1.56" x2="8.73" y2="1.66" layer="25"/>
<rectangle x1="9.13" y1="1.66" x2="9.53" y2="1.76" layer="25"/>
<rectangle x1="10.33" y1="1.66" x2="10.73" y2="1.76" layer="25"/>
<rectangle x1="11.33" y1="1.66" x2="11.83" y2="1.76" layer="25"/>
<rectangle x1="12.13" y1="1.66" x2="12.53" y2="1.76" layer="25"/>
<rectangle x1="16.13" y1="1.66" x2="16.53" y2="1.76" layer="25"/>
<rectangle x1="4.23" y1="1.76" x2="4.73" y2="1.86" layer="25"/>
<rectangle x1="5.43" y1="1.76" x2="5.83" y2="1.86" layer="25"/>
<rectangle x1="6.53" y1="1.76" x2="7.03" y2="1.86" layer="25"/>
<rectangle x1="7.23" y1="1.66" x2="7.63" y2="1.76" layer="25"/>
<rectangle x1="8.33" y1="1.66" x2="8.73" y2="1.76" layer="25"/>
<rectangle x1="9.13" y1="1.76" x2="9.53" y2="1.86" layer="25"/>
<rectangle x1="10.33" y1="1.76" x2="10.73" y2="1.86" layer="25"/>
<rectangle x1="11.33" y1="1.76" x2="11.83" y2="1.86" layer="25"/>
<rectangle x1="12.13" y1="1.76" x2="12.53" y2="1.86" layer="25"/>
<rectangle x1="16.13" y1="1.76" x2="16.53" y2="1.86" layer="25"/>
<rectangle x1="4.23" y1="1.86" x2="4.73" y2="1.96" layer="25"/>
<rectangle x1="5.43" y1="1.86" x2="5.83" y2="1.96" layer="25"/>
<rectangle x1="6.53" y1="1.86" x2="7.03" y2="1.96" layer="25"/>
<rectangle x1="7.23" y1="1.76" x2="7.63" y2="1.86" layer="25"/>
<rectangle x1="8.33" y1="1.76" x2="8.73" y2="1.86" layer="25"/>
<rectangle x1="9.13" y1="1.86" x2="9.53" y2="1.96" layer="25"/>
<rectangle x1="10.33" y1="1.86" x2="10.83" y2="1.96" layer="25"/>
<rectangle x1="11.23" y1="1.86" x2="11.83" y2="1.96" layer="25"/>
<rectangle x1="12.13" y1="1.86" x2="12.53" y2="1.96" layer="25"/>
<rectangle x1="16.13" y1="1.86" x2="16.53" y2="1.96" layer="25"/>
<rectangle x1="17.63" y1="1.86" x2="17.93" y2="1.96" layer="25"/>
<rectangle x1="4.23" y1="1.96" x2="4.73" y2="2.06" layer="25"/>
<rectangle x1="5.43" y1="1.96" x2="5.83" y2="2.06" layer="25"/>
<rectangle x1="6.53" y1="1.96" x2="7.03" y2="2.06" layer="25"/>
<rectangle x1="7.23" y1="1.86" x2="7.63" y2="1.96" layer="25"/>
<rectangle x1="9.13" y1="1.96" x2="9.53" y2="2.06" layer="25"/>
<rectangle x1="10.33" y1="1.96" x2="11.83" y2="2.06" layer="25"/>
<rectangle x1="12.13" y1="1.96" x2="12.53" y2="2.06" layer="25"/>
<rectangle x1="16.13" y1="1.96" x2="16.53" y2="2.06" layer="25"/>
<rectangle x1="4.23" y1="2.06" x2="4.73" y2="2.16" layer="25"/>
<rectangle x1="5.43" y1="2.06" x2="5.83" y2="2.16" layer="25"/>
<rectangle x1="6.53" y1="2.06" x2="7.03" y2="2.16" layer="25"/>
<rectangle x1="7.23" y1="1.96" x2="7.63" y2="2.06" layer="25"/>
<rectangle x1="9.13" y1="2.06" x2="9.53" y2="2.16" layer="25"/>
<rectangle x1="10.33" y1="2.06" x2="11.83" y2="2.16" layer="25"/>
<rectangle x1="12.13" y1="2.06" x2="12.53" y2="2.16" layer="25"/>
<rectangle x1="13.23" y1="2.06" x2="13.73" y2="2.16" layer="25"/>
<rectangle x1="14.63" y1="2.06" x2="15.13" y2="2.16" layer="25"/>
<rectangle x1="16.13" y1="2.06" x2="16.53" y2="2.16" layer="25"/>
<rectangle x1="4.23" y1="2.16" x2="4.73" y2="2.26" layer="25"/>
<rectangle x1="5.43" y1="2.16" x2="5.83" y2="2.26" layer="25"/>
<rectangle x1="6.53" y1="2.16" x2="7.03" y2="2.26" layer="25"/>
<rectangle x1="7.23" y1="2.06" x2="7.63" y2="2.16" layer="25"/>
<rectangle x1="9.13" y1="2.16" x2="9.53" y2="2.26" layer="25"/>
<rectangle x1="10.43" y1="2.16" x2="11.83" y2="2.26" layer="25"/>
<rectangle x1="12.13" y1="2.16" x2="12.53" y2="2.26" layer="25"/>
<rectangle x1="13.23" y1="2.16" x2="13.73" y2="2.26" layer="25"/>
<rectangle x1="14.63" y1="2.16" x2="15.13" y2="2.26" layer="25"/>
<rectangle x1="16.13" y1="2.16" x2="16.53" y2="2.26" layer="25"/>
<rectangle x1="4.23" y1="2.26" x2="4.73" y2="2.36" layer="25"/>
<rectangle x1="5.43" y1="2.26" x2="5.83" y2="2.36" layer="25"/>
<rectangle x1="6.53" y1="2.26" x2="7.03" y2="2.36" layer="25"/>
<rectangle x1="7.23" y1="2.16" x2="7.63" y2="2.26" layer="25"/>
<rectangle x1="9.13" y1="2.26" x2="9.53" y2="2.36" layer="25"/>
<rectangle x1="10.63" y1="2.26" x2="11.83" y2="2.36" layer="25"/>
<rectangle x1="12.13" y1="2.26" x2="12.53" y2="2.36" layer="25"/>
<rectangle x1="13.23" y1="2.26" x2="13.73" y2="2.36" layer="25"/>
<rectangle x1="14.63" y1="2.26" x2="15.13" y2="2.36" layer="25"/>
<rectangle x1="16.13" y1="2.26" x2="16.53" y2="2.36" layer="25"/>
<rectangle x1="4.23" y1="2.36" x2="4.73" y2="2.46" layer="25"/>
<rectangle x1="5.43" y1="2.36" x2="5.83" y2="2.46" layer="25"/>
<rectangle x1="6.53" y1="2.36" x2="7.03" y2="2.46" layer="25"/>
<rectangle x1="7.23" y1="2.26" x2="7.63" y2="2.36" layer="25"/>
<rectangle x1="9.13" y1="2.36" x2="9.53" y2="2.46" layer="25"/>
<rectangle x1="11.33" y1="2.36" x2="11.83" y2="2.46" layer="25"/>
<rectangle x1="12.13" y1="2.36" x2="12.63" y2="2.46" layer="25"/>
<rectangle x1="13.23" y1="2.36" x2="13.73" y2="2.46" layer="25"/>
<rectangle x1="14.63" y1="2.36" x2="15.13" y2="2.46" layer="25"/>
<rectangle x1="16.13" y1="2.36" x2="16.53" y2="2.46" layer="25"/>
<rectangle x1="4.23" y1="2.46" x2="4.73" y2="2.56" layer="25"/>
<rectangle x1="5.33" y1="2.46" x2="5.93" y2="2.56" layer="25"/>
<rectangle x1="6.53" y1="2.46" x2="6.93" y2="2.56" layer="25"/>
<rectangle x1="7.23" y1="2.36" x2="7.63" y2="2.46" layer="25"/>
<rectangle x1="9.13" y1="2.46" x2="9.53" y2="2.56" layer="25"/>
<rectangle x1="11.33" y1="2.46" x2="11.83" y2="2.56" layer="25"/>
<rectangle x1="12.13" y1="2.46" x2="12.63" y2="2.56" layer="25"/>
<rectangle x1="13.23" y1="2.46" x2="13.73" y2="2.56" layer="25"/>
<rectangle x1="14.63" y1="2.46" x2="15.13" y2="2.56" layer="25"/>
<rectangle x1="16.13" y1="2.46" x2="16.53" y2="2.56" layer="25"/>
<rectangle x1="4.33" y1="2.56" x2="6.93" y2="2.66" layer="25"/>
<rectangle x1="7.23" y1="2.46" x2="7.63" y2="2.56" layer="25"/>
<rectangle x1="8.83" y1="2.56" x2="10.23" y2="2.66" layer="25"/>
<rectangle x1="10.53" y1="2.56" x2="11.83" y2="2.66" layer="25"/>
<rectangle x1="12.13" y1="2.56" x2="13.73" y2="2.66" layer="25"/>
<rectangle x1="13.93" y1="2.56" x2="15.73" y2="2.66" layer="25"/>
<rectangle x1="16.13" y1="2.56" x2="16.53" y2="2.66" layer="25"/>
<rectangle x1="4.33" y1="2.66" x2="6.93" y2="2.76" layer="25"/>
<rectangle x1="7.23" y1="2.56" x2="7.63" y2="2.66" layer="25"/>
<rectangle x1="10.63" y1="2.66" x2="11.73" y2="2.76" layer="25"/>
<rectangle x1="12.23" y1="2.66" x2="13.63" y2="2.76" layer="25"/>
<rectangle x1="13.93" y1="2.66" x2="15.83" y2="2.76" layer="25"/>
<rectangle x1="16.13" y1="2.66" x2="16.53" y2="2.76" layer="25"/>
<rectangle x1="17.53" y1="2.66" x2="17.73" y2="2.76" layer="25"/>
<rectangle x1="4.43" y1="2.76" x2="6.83" y2="2.86" layer="25"/>
<rectangle x1="7.23" y1="2.66" x2="7.63" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.76" x2="10.33" y2="2.86" layer="25"/>
<rectangle x1="10.63" y1="2.76" x2="11.73" y2="2.86" layer="25"/>
<rectangle x1="12.23" y1="2.76" x2="13.63" y2="2.86" layer="25"/>
<rectangle x1="13.93" y1="2.76" x2="15.83" y2="2.86" layer="25"/>
<rectangle x1="16.13" y1="2.76" x2="16.53" y2="2.86" layer="25"/>
<rectangle x1="4.53" y1="2.86" x2="5.53" y2="2.96" layer="25"/>
<rectangle x1="5.73" y1="2.86" x2="6.73" y2="2.96" layer="25"/>
<rectangle x1="8.83" y1="2.86" x2="10.33" y2="2.96" layer="25"/>
<rectangle x1="10.73" y1="2.86" x2="11.63" y2="2.96" layer="25"/>
<rectangle x1="12.33" y1="2.86" x2="13.43" y2="2.96" layer="25"/>
<rectangle x1="14.03" y1="2.86" x2="15.83" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.86" x2="16.53" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.96" x2="16.53" y2="3.06" layer="25"/>
<rectangle x1="16.13" y1="3.06" x2="16.53" y2="3.16" layer="25"/>
<rectangle x1="16.13" y1="3.16" x2="16.53" y2="3.26" layer="25"/>
<rectangle x1="17.53" y1="3.16" x2="17.93" y2="3.26" layer="25"/>
<rectangle x1="16.13" y1="3.26" x2="17.93" y2="3.36" layer="25"/>
<rectangle x1="16.13" y1="3.36" x2="17.93" y2="3.46" layer="25"/>
<rectangle x1="16.23" y1="3.46" x2="17.83" y2="3.56" layer="25"/>
<rectangle x1="16.33" y1="3.56" x2="17.73" y2="3.66" layer="25"/>
<rectangle x1="16.63" y1="3.66" x2="17.43" y2="3.76" layer="25"/>
<rectangle x1="11.33" y1="0.76" x2="11.43" y2="0.86" layer="25"/>
<rectangle x1="12.13" y1="0.76" x2="12.23" y2="0.86" layer="25"/>
<rectangle x1="13.23" y1="0.76" x2="13.33" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.76" x2="4.33" y2="0.86" layer="25"/>
<rectangle x1="7.53" y1="2.86" x2="7.63" y2="2.96" layer="25"/>
<rectangle x1="10.33" y1="2.86" x2="10.43" y2="2.96" layer="25"/>
<rectangle x1="13.93" y1="2.86" x2="14.03" y2="2.96" layer="25"/>
<rectangle x1="15.83" y1="2.86" x2="15.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.56" x2="17.63" y2="2.66" layer="25"/>
<rectangle x1="17.83" y1="1.96" x2="17.93" y2="2.06" layer="25"/>
<rectangle x1="17.53" y1="3.06" x2="17.93" y2="3.16" layer="25"/>
<rectangle x1="17.53" y1="2.96" x2="17.93" y2="3.06" layer="25"/>
<rectangle x1="17.53" y1="2.86" x2="17.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.76" x2="17.93" y2="2.86" layer="25"/>
<rectangle x1="17.53" y1="1.46" x2="17.93" y2="1.56" layer="25"/>
<rectangle x1="17.53" y1="1.56" x2="17.93" y2="1.66" layer="25"/>
<rectangle x1="17.53" y1="1.66" x2="17.93" y2="1.76" layer="25"/>
<rectangle x1="17.53" y1="1.76" x2="17.93" y2="1.86" layer="25"/>
<rectangle x1="17.43" y1="1.36" x2="17.53" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.36" x2="16.63" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.46" x2="16.63" y2="1.56" layer="25"/>
<rectangle x1="16.63" y1="1.36" x2="16.73" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="3.16" x2="16.63" y2="3.26" layer="25"/>
<rectangle x1="16.53" y1="3.06" x2="16.63" y2="3.16" layer="25"/>
<rectangle x1="16.63" y1="3.16" x2="16.73" y2="3.26" layer="25"/>
<rectangle x1="17.43" y1="3.16" x2="17.53" y2="3.26" layer="25"/>
<rectangle x1="17.73" y1="2.66" x2="17.83" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.66" x2="10.33" y2="2.76" layer="25"/>
<rectangle x1="7.33" y1="2.76" x2="7.63" y2="2.86" layer="25"/>
<rectangle x1="12.13" y1="0.86" x2="12.43" y2="0.96" layer="25"/>
<rectangle x1="16.53" y1="0.86" x2="16.83" y2="0.96" layer="25"/>
<rectangle x1="2.07" y1="0.05" x2="2.13" y2="0.07" layer="25"/>
<rectangle x1="1.81" y1="0.07" x2="2.37" y2="0.09" layer="25"/>
<rectangle x1="1.71" y1="0.09" x2="2.47" y2="0.11" layer="25"/>
<rectangle x1="1.63" y1="0.11" x2="2.55" y2="0.13" layer="25"/>
<rectangle x1="1.55" y1="0.13" x2="2.61" y2="0.15" layer="25"/>
<rectangle x1="1.49" y1="0.15" x2="2.67" y2="0.17" layer="25"/>
<rectangle x1="1.45" y1="0.17" x2="2.73" y2="0.19" layer="25"/>
<rectangle x1="1.39" y1="0.19" x2="2.79" y2="0.21" layer="25"/>
<rectangle x1="1.35" y1="0.21" x2="2.83" y2="0.23" layer="25"/>
<rectangle x1="1.31" y1="0.23" x2="2.87" y2="0.25" layer="25"/>
<rectangle x1="1.27" y1="0.25" x2="2.91" y2="0.27" layer="25"/>
<rectangle x1="1.23" y1="0.27" x2="1.51" y2="0.29" layer="25"/>
<rectangle x1="1.73" y1="0.27" x2="2.95" y2="0.29" layer="25"/>
<rectangle x1="1.19" y1="0.29" x2="1.41" y2="0.31" layer="25"/>
<rectangle x1="1.83" y1="0.29" x2="2.99" y2="0.31" layer="25"/>
<rectangle x1="1.15" y1="0.31" x2="1.35" y2="0.33" layer="25"/>
<rectangle x1="1.89" y1="0.31" x2="3.01" y2="0.33" layer="25"/>
<rectangle x1="1.13" y1="0.33" x2="1.31" y2="0.35" layer="25"/>
<rectangle x1="1.93" y1="0.33" x2="3.05" y2="0.35" layer="25"/>
<rectangle x1="1.09" y1="0.35" x2="1.25" y2="0.37" layer="25"/>
<rectangle x1="1.97" y1="0.35" x2="3.07" y2="0.37" layer="25"/>
<rectangle x1="1.07" y1="0.37" x2="1.23" y2="0.39" layer="25"/>
<rectangle x1="2.01" y1="0.37" x2="3.11" y2="0.39" layer="25"/>
<rectangle x1="1.03" y1="0.39" x2="1.19" y2="0.41" layer="25"/>
<rectangle x1="2.05" y1="0.39" x2="3.13" y2="0.41" layer="25"/>
<rectangle x1="1.01" y1="0.41" x2="1.15" y2="0.43" layer="25"/>
<rectangle x1="2.07" y1="0.41" x2="3.17" y2="0.43" layer="25"/>
<rectangle x1="0.99" y1="0.43" x2="1.13" y2="0.45" layer="25"/>
<rectangle x1="2.09" y1="0.43" x2="3.19" y2="0.45" layer="25"/>
<rectangle x1="0.95" y1="0.45" x2="1.11" y2="0.47" layer="25"/>
<rectangle x1="2.13" y1="0.45" x2="3.21" y2="0.47" layer="25"/>
<rectangle x1="0.93" y1="0.47" x2="1.09" y2="0.49" layer="25"/>
<rectangle x1="2.15" y1="0.47" x2="3.23" y2="0.49" layer="25"/>
<rectangle x1="0.91" y1="0.49" x2="1.07" y2="0.51" layer="25"/>
<rectangle x1="2.17" y1="0.49" x2="3.25" y2="0.51" layer="25"/>
<rectangle x1="0.89" y1="0.51" x2="1.05" y2="0.53" layer="25"/>
<rectangle x1="2.19" y1="0.51" x2="3.29" y2="0.53" layer="25"/>
<rectangle x1="0.87" y1="0.53" x2="1.03" y2="0.55" layer="25"/>
<rectangle x1="2.21" y1="0.53" x2="3.31" y2="0.55" layer="25"/>
<rectangle x1="0.85" y1="0.55" x2="1.01" y2="0.57" layer="25"/>
<rectangle x1="2.21" y1="0.55" x2="3.33" y2="0.57" layer="25"/>
<rectangle x1="0.83" y1="0.57" x2="0.99" y2="0.59" layer="25"/>
<rectangle x1="2.23" y1="0.57" x2="3.35" y2="0.59" layer="25"/>
<rectangle x1="0.81" y1="0.59" x2="0.97" y2="0.61" layer="25"/>
<rectangle x1="2.25" y1="0.59" x2="3.37" y2="0.61" layer="25"/>
<rectangle x1="0.79" y1="0.61" x2="0.97" y2="0.63" layer="25"/>
<rectangle x1="2.27" y1="0.61" x2="3.39" y2="0.63" layer="25"/>
<rectangle x1="0.77" y1="0.63" x2="0.95" y2="0.65" layer="25"/>
<rectangle x1="2.27" y1="0.63" x2="3.41" y2="0.65" layer="25"/>
<rectangle x1="0.75" y1="0.65" x2="0.93" y2="0.67" layer="25"/>
<rectangle x1="2.29" y1="0.65" x2="3.41" y2="0.67" layer="25"/>
<rectangle x1="0.73" y1="0.67" x2="0.93" y2="0.69" layer="25"/>
<rectangle x1="2.29" y1="0.67" x2="3.43" y2="0.69" layer="25"/>
<rectangle x1="0.71" y1="0.69" x2="0.91" y2="0.71" layer="25"/>
<rectangle x1="2.31" y1="0.69" x2="3.45" y2="0.71" layer="25"/>
<rectangle x1="0.69" y1="0.71" x2="0.91" y2="0.73" layer="25"/>
<rectangle x1="2.31" y1="0.71" x2="3.47" y2="0.73" layer="25"/>
<rectangle x1="0.67" y1="0.73" x2="0.89" y2="0.75" layer="25"/>
<rectangle x1="2.33" y1="0.73" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="0.65" y1="0.75" x2="0.89" y2="0.77" layer="25"/>
<rectangle x1="2.33" y1="0.75" x2="3.51" y2="0.77" layer="25"/>
<rectangle x1="0.65" y1="0.77" x2="0.87" y2="0.79" layer="25"/>
<rectangle x1="2.33" y1="0.77" x2="3.51" y2="0.79" layer="25"/>
<rectangle x1="0.63" y1="0.79" x2="0.87" y2="0.81" layer="25"/>
<rectangle x1="2.35" y1="0.79" x2="3.53" y2="0.81" layer="25"/>
<rectangle x1="0.61" y1="0.81" x2="0.85" y2="0.83" layer="25"/>
<rectangle x1="2.35" y1="0.81" x2="2.87" y2="0.83" layer="25"/>
<rectangle x1="2.91" y1="0.81" x2="3.55" y2="0.83" layer="25"/>
<rectangle x1="0.61" y1="0.83" x2="0.85" y2="0.85" layer="25"/>
<rectangle x1="2.35" y1="0.83" x2="2.85" y2="0.85" layer="25"/>
<rectangle x1="2.95" y1="0.83" x2="3.57" y2="0.85" layer="25"/>
<rectangle x1="0.59" y1="0.85" x2="0.85" y2="0.87" layer="25"/>
<rectangle x1="2.37" y1="0.85" x2="2.83" y2="0.87" layer="25"/>
<rectangle x1="2.97" y1="0.85" x2="3.57" y2="0.87" layer="25"/>
<rectangle x1="0.57" y1="0.87" x2="0.85" y2="0.89" layer="25"/>
<rectangle x1="2.37" y1="0.87" x2="2.81" y2="0.89" layer="25"/>
<rectangle x1="3.01" y1="0.87" x2="3.59" y2="0.89" layer="25"/>
<rectangle x1="0.55" y1="0.89" x2="0.83" y2="0.91" layer="25"/>
<rectangle x1="2.37" y1="0.89" x2="2.81" y2="0.91" layer="25"/>
<rectangle x1="3.03" y1="0.89" x2="3.59" y2="0.91" layer="25"/>
<rectangle x1="0.55" y1="0.91" x2="0.83" y2="0.93" layer="25"/>
<rectangle x1="2.37" y1="0.91" x2="2.77" y2="0.93" layer="25"/>
<rectangle x1="3.05" y1="0.91" x2="3.61" y2="0.93" layer="25"/>
<rectangle x1="0.53" y1="0.93" x2="0.83" y2="0.95" layer="25"/>
<rectangle x1="2.37" y1="0.93" x2="2.67" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.93" x2="3.63" y2="0.95" layer="25"/>
<rectangle x1="0.53" y1="0.95" x2="0.83" y2="0.97" layer="25"/>
<rectangle x1="2.37" y1="0.95" x2="2.55" y2="0.97" layer="25"/>
<rectangle x1="3.11" y1="0.95" x2="3.63" y2="0.97" layer="25"/>
<rectangle x1="0.51" y1="0.97" x2="0.83" y2="0.99" layer="25"/>
<rectangle x1="2.39" y1="0.97" x2="2.45" y2="0.99" layer="25"/>
<rectangle x1="3.13" y1="0.97" x2="3.65" y2="0.99" layer="25"/>
<rectangle x1="0.49" y1="0.99" x2="0.81" y2="1.01" layer="25"/>
<rectangle x1="3.11" y1="0.99" x2="3.65" y2="1.01" layer="25"/>
<rectangle x1="0.49" y1="1.01" x2="0.81" y2="1.03" layer="25"/>
<rectangle x1="3.09" y1="1.01" x2="3.67" y2="1.03" layer="25"/>
<rectangle x1="0.47" y1="1.03" x2="0.81" y2="1.05" layer="25"/>
<rectangle x1="3.07" y1="1.03" x2="3.67" y2="1.05" layer="25"/>
<rectangle x1="0.47" y1="1.05" x2="0.81" y2="1.07" layer="25"/>
<rectangle x1="3.07" y1="1.05" x2="3.69" y2="1.07" layer="25"/>
<rectangle x1="0.45" y1="1.07" x2="0.81" y2="1.09" layer="25"/>
<rectangle x1="3.05" y1="1.07" x2="3.69" y2="1.09" layer="25"/>
<rectangle x1="0.45" y1="1.09" x2="0.81" y2="1.11" layer="25"/>
<rectangle x1="3.03" y1="1.09" x2="3.71" y2="1.11" layer="25"/>
<rectangle x1="0.43" y1="1.11" x2="0.81" y2="1.13" layer="25"/>
<rectangle x1="3.01" y1="1.11" x2="3.71" y2="1.13" layer="25"/>
<rectangle x1="0.43" y1="1.13" x2="0.81" y2="1.15" layer="25"/>
<rectangle x1="3.01" y1="1.13" x2="3.73" y2="1.15" layer="25"/>
<rectangle x1="0.41" y1="1.15" x2="0.81" y2="1.17" layer="25"/>
<rectangle x1="2.99" y1="1.15" x2="3.73" y2="1.17" layer="25"/>
<rectangle x1="0.41" y1="1.17" x2="0.83" y2="1.19" layer="25"/>
<rectangle x1="2.97" y1="1.17" x2="3.75" y2="1.19" layer="25"/>
<rectangle x1="0.41" y1="1.19" x2="0.83" y2="1.21" layer="25"/>
<rectangle x1="2.95" y1="1.19" x2="3.75" y2="1.21" layer="25"/>
<rectangle x1="0.39" y1="1.21" x2="0.83" y2="1.23" layer="25"/>
<rectangle x1="2.95" y1="1.21" x2="3.75" y2="1.23" layer="25"/>
<rectangle x1="0.39" y1="1.23" x2="0.83" y2="1.25" layer="25"/>
<rectangle x1="2.51" y1="1.23" x2="2.55" y2="1.25" layer="25"/>
<rectangle x1="2.93" y1="1.23" x2="3.77" y2="1.25" layer="25"/>
<rectangle x1="0.37" y1="1.25" x2="0.83" y2="1.27" layer="25"/>
<rectangle x1="2.41" y1="1.25" x2="2.53" y2="1.27" layer="25"/>
<rectangle x1="2.91" y1="1.25" x2="3.77" y2="1.27" layer="25"/>
<rectangle x1="0.37" y1="1.27" x2="0.83" y2="1.29" layer="25"/>
<rectangle x1="2.35" y1="1.27" x2="2.51" y2="1.29" layer="25"/>
<rectangle x1="2.89" y1="1.27" x2="3.79" y2="1.29" layer="25"/>
<rectangle x1="0.37" y1="1.29" x2="0.85" y2="1.31" layer="25"/>
<rectangle x1="2.35" y1="1.29" x2="2.49" y2="1.31" layer="25"/>
<rectangle x1="2.89" y1="1.29" x2="3.79" y2="1.31" layer="25"/>
<rectangle x1="0.35" y1="1.31" x2="0.85" y2="1.33" layer="25"/>
<rectangle x1="2.33" y1="1.31" x2="2.49" y2="1.33" layer="25"/>
<rectangle x1="2.89" y1="1.31" x2="3.79" y2="1.33" layer="25"/>
<rectangle x1="0.35" y1="1.33" x2="0.85" y2="1.35" layer="25"/>
<rectangle x1="2.33" y1="1.33" x2="2.47" y2="1.35" layer="25"/>
<rectangle x1="2.89" y1="1.33" x2="3.81" y2="1.35" layer="25"/>
<rectangle x1="0.35" y1="1.35" x2="0.87" y2="1.37" layer="25"/>
<rectangle x1="2.33" y1="1.35" x2="2.45" y2="1.37" layer="25"/>
<rectangle x1="2.89" y1="1.35" x2="3.81" y2="1.37" layer="25"/>
<rectangle x1="0.33" y1="1.37" x2="0.87" y2="1.39" layer="25"/>
<rectangle x1="2.31" y1="1.37" x2="2.43" y2="1.39" layer="25"/>
<rectangle x1="2.89" y1="1.37" x2="3.81" y2="1.39" layer="25"/>
<rectangle x1="0.33" y1="1.39" x2="0.87" y2="1.41" layer="25"/>
<rectangle x1="2.31" y1="1.39" x2="2.43" y2="1.41" layer="25"/>
<rectangle x1="2.89" y1="1.39" x2="3.81" y2="1.41" layer="25"/>
<rectangle x1="0.33" y1="1.41" x2="0.89" y2="1.43" layer="25"/>
<rectangle x1="2.29" y1="1.41" x2="2.41" y2="1.43" layer="25"/>
<rectangle x1="2.89" y1="1.41" x2="3.83" y2="1.43" layer="25"/>
<rectangle x1="0.33" y1="1.43" x2="0.89" y2="1.45" layer="25"/>
<rectangle x1="2.29" y1="1.43" x2="2.39" y2="1.45" layer="25"/>
<rectangle x1="2.89" y1="1.43" x2="3.83" y2="1.45" layer="25"/>
<rectangle x1="0.31" y1="1.45" x2="0.91" y2="1.47" layer="25"/>
<rectangle x1="2.27" y1="1.45" x2="2.37" y2="1.47" layer="25"/>
<rectangle x1="2.89" y1="1.45" x2="3.83" y2="1.47" layer="25"/>
<rectangle x1="0.31" y1="1.47" x2="0.91" y2="1.49" layer="25"/>
<rectangle x1="2.27" y1="1.47" x2="2.35" y2="1.49" layer="25"/>
<rectangle x1="2.89" y1="1.47" x2="3.83" y2="1.49" layer="25"/>
<rectangle x1="0.31" y1="1.49" x2="0.93" y2="1.51" layer="25"/>
<rectangle x1="2.25" y1="1.49" x2="2.35" y2="1.51" layer="25"/>
<rectangle x1="2.89" y1="1.49" x2="3.85" y2="1.51" layer="25"/>
<rectangle x1="0.31" y1="1.51" x2="0.95" y2="1.53" layer="25"/>
<rectangle x1="2.23" y1="1.51" x2="2.33" y2="1.53" layer="25"/>
<rectangle x1="2.89" y1="1.51" x2="3.85" y2="1.53" layer="25"/>
<rectangle x1="0.29" y1="1.53" x2="0.95" y2="1.55" layer="25"/>
<rectangle x1="2.21" y1="1.53" x2="2.31" y2="1.55" layer="25"/>
<rectangle x1="2.89" y1="1.53" x2="3.85" y2="1.55" layer="25"/>
<rectangle x1="0.29" y1="1.55" x2="0.97" y2="1.57" layer="25"/>
<rectangle x1="2.21" y1="1.55" x2="2.29" y2="1.57" layer="25"/>
<rectangle x1="2.89" y1="1.55" x2="3.85" y2="1.57" layer="25"/>
<rectangle x1="0.29" y1="1.57" x2="0.99" y2="1.59" layer="25"/>
<rectangle x1="2.19" y1="1.57" x2="2.29" y2="1.59" layer="25"/>
<rectangle x1="2.89" y1="1.57" x2="3.85" y2="1.59" layer="25"/>
<rectangle x1="0.29" y1="1.59" x2="1.01" y2="1.61" layer="25"/>
<rectangle x1="2.17" y1="1.59" x2="2.27" y2="1.61" layer="25"/>
<rectangle x1="2.89" y1="1.59" x2="3.85" y2="1.61" layer="25"/>
<rectangle x1="0.29" y1="1.61" x2="1.03" y2="1.63" layer="25"/>
<rectangle x1="2.15" y1="1.61" x2="2.25" y2="1.63" layer="25"/>
<rectangle x1="2.63" y1="1.61" x2="2.65" y2="1.63" layer="25"/>
<rectangle x1="2.89" y1="1.61" x2="3.87" y2="1.63" layer="25"/>
<rectangle x1="0.27" y1="1.63" x2="1.05" y2="1.65" layer="25"/>
<rectangle x1="2.13" y1="1.63" x2="2.23" y2="1.65" layer="25"/>
<rectangle x1="2.61" y1="1.63" x2="2.65" y2="1.65" layer="25"/>
<rectangle x1="2.89" y1="1.63" x2="3.87" y2="1.65" layer="25"/>
<rectangle x1="0.27" y1="1.65" x2="1.07" y2="1.67" layer="25"/>
<rectangle x1="2.11" y1="1.65" x2="2.23" y2="1.67" layer="25"/>
<rectangle x1="2.61" y1="1.65" x2="2.65" y2="1.67" layer="25"/>
<rectangle x1="2.89" y1="1.65" x2="3.87" y2="1.67" layer="25"/>
<rectangle x1="0.27" y1="1.67" x2="1.09" y2="1.69" layer="25"/>
<rectangle x1="2.09" y1="1.67" x2="2.21" y2="1.69" layer="25"/>
<rectangle x1="2.59" y1="1.67" x2="2.65" y2="1.69" layer="25"/>
<rectangle x1="2.89" y1="1.67" x2="3.87" y2="1.69" layer="25"/>
<rectangle x1="0.27" y1="1.69" x2="1.11" y2="1.71" layer="25"/>
<rectangle x1="2.05" y1="1.69" x2="2.19" y2="1.71" layer="25"/>
<rectangle x1="2.57" y1="1.69" x2="2.65" y2="1.71" layer="25"/>
<rectangle x1="2.89" y1="1.69" x2="3.87" y2="1.71" layer="25"/>
<rectangle x1="0.27" y1="1.71" x2="1.13" y2="1.73" layer="25"/>
<rectangle x1="2.03" y1="1.71" x2="2.17" y2="1.73" layer="25"/>
<rectangle x1="2.55" y1="1.71" x2="2.65" y2="1.73" layer="25"/>
<rectangle x1="2.89" y1="1.71" x2="3.87" y2="1.73" layer="25"/>
<rectangle x1="0.27" y1="1.73" x2="1.17" y2="1.75" layer="25"/>
<rectangle x1="2.01" y1="1.73" x2="2.17" y2="1.75" layer="25"/>
<rectangle x1="2.55" y1="1.73" x2="2.65" y2="1.75" layer="25"/>
<rectangle x1="2.89" y1="1.73" x2="3.87" y2="1.75" layer="25"/>
<rectangle x1="0.27" y1="1.75" x2="1.19" y2="1.77" layer="25"/>
<rectangle x1="1.97" y1="1.75" x2="2.15" y2="1.77" layer="25"/>
<rectangle x1="2.53" y1="1.75" x2="2.65" y2="1.77" layer="25"/>
<rectangle x1="2.89" y1="1.75" x2="3.87" y2="1.77" layer="25"/>
<rectangle x1="0.27" y1="1.77" x2="1.23" y2="1.79" layer="25"/>
<rectangle x1="1.93" y1="1.77" x2="2.13" y2="1.79" layer="25"/>
<rectangle x1="2.51" y1="1.77" x2="2.65" y2="1.79" layer="25"/>
<rectangle x1="2.89" y1="1.77" x2="3.87" y2="1.79" layer="25"/>
<rectangle x1="0.27" y1="1.79" x2="1.29" y2="1.81" layer="25"/>
<rectangle x1="1.89" y1="1.79" x2="2.11" y2="1.81" layer="25"/>
<rectangle x1="2.49" y1="1.79" x2="2.65" y2="1.81" layer="25"/>
<rectangle x1="2.89" y1="1.79" x2="3.87" y2="1.81" layer="25"/>
<rectangle x1="0.27" y1="1.81" x2="1.33" y2="1.83" layer="25"/>
<rectangle x1="1.83" y1="1.81" x2="2.09" y2="1.83" layer="25"/>
<rectangle x1="2.49" y1="1.81" x2="2.65" y2="1.83" layer="25"/>
<rectangle x1="2.89" y1="1.81" x2="3.89" y2="1.83" layer="25"/>
<rectangle x1="0.27" y1="1.83" x2="1.41" y2="1.85" layer="25"/>
<rectangle x1="1.75" y1="1.83" x2="2.09" y2="1.85" layer="25"/>
<rectangle x1="2.47" y1="1.83" x2="2.65" y2="1.85" layer="25"/>
<rectangle x1="2.89" y1="1.83" x2="3.89" y2="1.85" layer="25"/>
<rectangle x1="0.25" y1="1.85" x2="1.55" y2="1.87" layer="25"/>
<rectangle x1="1.59" y1="1.85" x2="2.07" y2="1.87" layer="25"/>
<rectangle x1="2.45" y1="1.85" x2="2.65" y2="1.87" layer="25"/>
<rectangle x1="2.89" y1="1.85" x2="3.89" y2="1.87" layer="25"/>
<rectangle x1="0.25" y1="1.87" x2="2.05" y2="1.89" layer="25"/>
<rectangle x1="2.43" y1="1.87" x2="2.65" y2="1.89" layer="25"/>
<rectangle x1="2.89" y1="1.87" x2="3.89" y2="1.89" layer="25"/>
<rectangle x1="0.25" y1="1.89" x2="2.03" y2="1.91" layer="25"/>
<rectangle x1="2.43" y1="1.89" x2="2.65" y2="1.91" layer="25"/>
<rectangle x1="2.89" y1="1.89" x2="3.89" y2="1.91" layer="25"/>
<rectangle x1="0.25" y1="1.91" x2="2.03" y2="1.93" layer="25"/>
<rectangle x1="2.41" y1="1.91" x2="2.65" y2="1.93" layer="25"/>
<rectangle x1="2.89" y1="1.91" x2="3.89" y2="1.93" layer="25"/>
<rectangle x1="0.25" y1="1.93" x2="2.01" y2="1.95" layer="25"/>
<rectangle x1="2.39" y1="1.93" x2="2.65" y2="1.95" layer="25"/>
<rectangle x1="2.89" y1="1.93" x2="3.89" y2="1.95" layer="25"/>
<rectangle x1="0.25" y1="1.95" x2="1.99" y2="1.97" layer="25"/>
<rectangle x1="2.37" y1="1.95" x2="2.65" y2="1.97" layer="25"/>
<rectangle x1="2.89" y1="1.95" x2="3.87" y2="1.97" layer="25"/>
<rectangle x1="0.25" y1="1.97" x2="1.97" y2="1.99" layer="25"/>
<rectangle x1="2.35" y1="1.97" x2="2.65" y2="1.99" layer="25"/>
<rectangle x1="2.89" y1="1.97" x2="3.87" y2="1.99" layer="25"/>
<rectangle x1="0.27" y1="1.99" x2="1.97" y2="2.01" layer="25"/>
<rectangle x1="2.35" y1="1.99" x2="2.65" y2="2.01" layer="25"/>
<rectangle x1="2.89" y1="1.99" x2="3.87" y2="2.01" layer="25"/>
<rectangle x1="0.27" y1="2.01" x2="1.95" y2="2.03" layer="25"/>
<rectangle x1="2.33" y1="2.01" x2="2.65" y2="2.03" layer="25"/>
<rectangle x1="2.89" y1="2.01" x2="3.87" y2="2.03" layer="25"/>
<rectangle x1="0.27" y1="2.03" x2="1.93" y2="2.05" layer="25"/>
<rectangle x1="2.31" y1="2.03" x2="2.63" y2="2.05" layer="25"/>
<rectangle x1="2.97" y1="2.03" x2="3.87" y2="2.05" layer="25"/>
<rectangle x1="0.27" y1="2.05" x2="1.91" y2="2.07" layer="25"/>
<rectangle x1="2.29" y1="2.05" x2="2.59" y2="2.07" layer="25"/>
<rectangle x1="3.03" y1="2.05" x2="3.87" y2="2.07" layer="25"/>
<rectangle x1="0.27" y1="2.07" x2="1.89" y2="2.09" layer="25"/>
<rectangle x1="2.29" y1="2.07" x2="2.55" y2="2.09" layer="25"/>
<rectangle x1="3.07" y1="2.07" x2="3.87" y2="2.09" layer="25"/>
<rectangle x1="0.27" y1="2.09" x2="1.89" y2="2.11" layer="25"/>
<rectangle x1="2.27" y1="2.09" x2="2.51" y2="2.11" layer="25"/>
<rectangle x1="3.11" y1="2.09" x2="3.87" y2="2.11" layer="25"/>
<rectangle x1="0.27" y1="2.11" x2="1.41" y2="2.13" layer="25"/>
<rectangle x1="1.69" y1="2.11" x2="1.87" y2="2.13" layer="25"/>
<rectangle x1="2.25" y1="2.11" x2="2.47" y2="2.13" layer="25"/>
<rectangle x1="3.15" y1="2.11" x2="3.87" y2="2.13" layer="25"/>
<rectangle x1="0.27" y1="2.13" x2="1.35" y2="2.15" layer="25"/>
<rectangle x1="1.75" y1="2.13" x2="1.85" y2="2.15" layer="25"/>
<rectangle x1="2.23" y1="2.13" x2="2.45" y2="2.15" layer="25"/>
<rectangle x1="3.17" y1="2.13" x2="3.87" y2="2.15" layer="25"/>
<rectangle x1="0.27" y1="2.15" x2="1.31" y2="2.17" layer="25"/>
<rectangle x1="1.79" y1="2.15" x2="1.83" y2="2.17" layer="25"/>
<rectangle x1="2.23" y1="2.15" x2="2.43" y2="2.17" layer="25"/>
<rectangle x1="3.19" y1="2.15" x2="3.87" y2="2.17" layer="25"/>
<rectangle x1="0.27" y1="2.17" x2="1.27" y2="2.19" layer="25"/>
<rectangle x1="2.21" y1="2.17" x2="2.41" y2="2.19" layer="25"/>
<rectangle x1="3.21" y1="2.17" x2="3.85" y2="2.19" layer="25"/>
<rectangle x1="0.29" y1="2.19" x2="1.23" y2="2.21" layer="25"/>
<rectangle x1="2.19" y1="2.19" x2="2.39" y2="2.21" layer="25"/>
<rectangle x1="3.23" y1="2.19" x2="3.85" y2="2.21" layer="25"/>
<rectangle x1="0.29" y1="2.21" x2="1.21" y2="2.23" layer="25"/>
<rectangle x1="2.17" y1="2.21" x2="2.37" y2="2.23" layer="25"/>
<rectangle x1="3.25" y1="2.21" x2="3.85" y2="2.23" layer="25"/>
<rectangle x1="0.29" y1="2.23" x2="1.17" y2="2.25" layer="25"/>
<rectangle x1="2.17" y1="2.23" x2="2.35" y2="2.25" layer="25"/>
<rectangle x1="3.27" y1="2.23" x2="3.85" y2="2.25" layer="25"/>
<rectangle x1="0.29" y1="2.25" x2="1.15" y2="2.27" layer="25"/>
<rectangle x1="2.15" y1="2.25" x2="2.33" y2="2.27" layer="25"/>
<rectangle x1="3.29" y1="2.25" x2="3.85" y2="2.27" layer="25"/>
<rectangle x1="0.29" y1="2.27" x2="1.13" y2="2.29" layer="25"/>
<rectangle x1="2.13" y1="2.27" x2="2.33" y2="2.29" layer="25"/>
<rectangle x1="3.31" y1="2.27" x2="3.83" y2="2.29" layer="25"/>
<rectangle x1="0.29" y1="2.29" x2="1.11" y2="2.31" layer="25"/>
<rectangle x1="2.11" y1="2.29" x2="2.31" y2="2.31" layer="25"/>
<rectangle x1="3.31" y1="2.29" x2="3.83" y2="2.31" layer="25"/>
<rectangle x1="0.31" y1="2.31" x2="1.09" y2="2.33" layer="25"/>
<rectangle x1="2.09" y1="2.31" x2="2.31" y2="2.33" layer="25"/>
<rectangle x1="3.33" y1="2.31" x2="3.83" y2="2.33" layer="25"/>
<rectangle x1="0.31" y1="2.33" x2="1.09" y2="2.35" layer="25"/>
<rectangle x1="2.09" y1="2.33" x2="2.29" y2="2.35" layer="25"/>
<rectangle x1="3.33" y1="2.33" x2="3.83" y2="2.35" layer="25"/>
<rectangle x1="0.31" y1="2.35" x2="1.07" y2="2.37" layer="25"/>
<rectangle x1="2.07" y1="2.35" x2="2.29" y2="2.37" layer="25"/>
<rectangle x1="3.35" y1="2.35" x2="3.81" y2="2.37" layer="25"/>
<rectangle x1="0.31" y1="2.37" x2="1.05" y2="2.39" layer="25"/>
<rectangle x1="2.05" y1="2.37" x2="2.27" y2="2.39" layer="25"/>
<rectangle x1="3.35" y1="2.37" x2="3.81" y2="2.39" layer="25"/>
<rectangle x1="0.33" y1="2.39" x2="1.05" y2="2.41" layer="25"/>
<rectangle x1="2.05" y1="2.39" x2="2.27" y2="2.41" layer="25"/>
<rectangle x1="3.37" y1="2.39" x2="3.81" y2="2.41" layer="25"/>
<rectangle x1="0.33" y1="2.41" x2="1.03" y2="2.43" layer="25"/>
<rectangle x1="2.07" y1="2.41" x2="2.27" y2="2.43" layer="25"/>
<rectangle x1="3.37" y1="2.41" x2="3.81" y2="2.43" layer="25"/>
<rectangle x1="0.33" y1="2.43" x2="1.03" y2="2.45" layer="25"/>
<rectangle x1="2.07" y1="2.43" x2="2.27" y2="2.45" layer="25"/>
<rectangle x1="3.37" y1="2.43" x2="3.79" y2="2.45" layer="25"/>
<rectangle x1="0.33" y1="2.45" x2="1.01" y2="2.47" layer="25"/>
<rectangle x1="2.07" y1="2.45" x2="2.25" y2="2.47" layer="25"/>
<rectangle x1="3.39" y1="2.45" x2="3.79" y2="2.47" layer="25"/>
<rectangle x1="0.35" y1="2.47" x2="1.01" y2="2.49" layer="25"/>
<rectangle x1="2.09" y1="2.47" x2="2.25" y2="2.49" layer="25"/>
<rectangle x1="3.39" y1="2.47" x2="3.79" y2="2.49" layer="25"/>
<rectangle x1="0.35" y1="2.49" x2="0.99" y2="2.51" layer="25"/>
<rectangle x1="2.09" y1="2.49" x2="2.25" y2="2.51" layer="25"/>
<rectangle x1="3.39" y1="2.49" x2="3.77" y2="2.51" layer="25"/>
<rectangle x1="0.35" y1="2.51" x2="0.99" y2="2.53" layer="25"/>
<rectangle x1="2.09" y1="2.51" x2="2.25" y2="2.53" layer="25"/>
<rectangle x1="3.39" y1="2.51" x2="3.77" y2="2.53" layer="25"/>
<rectangle x1="0.37" y1="2.53" x2="0.99" y2="2.55" layer="25"/>
<rectangle x1="2.11" y1="2.53" x2="2.25" y2="2.55" layer="25"/>
<rectangle x1="3.39" y1="2.53" x2="3.77" y2="2.55" layer="25"/>
<rectangle x1="0.37" y1="2.55" x2="0.99" y2="2.57" layer="25"/>
<rectangle x1="2.11" y1="2.55" x2="2.25" y2="2.57" layer="25"/>
<rectangle x1="3.39" y1="2.55" x2="3.75" y2="2.57" layer="25"/>
<rectangle x1="0.39" y1="2.57" x2="0.97" y2="2.59" layer="25"/>
<rectangle x1="2.11" y1="2.57" x2="2.25" y2="2.59" layer="25"/>
<rectangle x1="3.41" y1="2.57" x2="3.75" y2="2.59" layer="25"/>
<rectangle x1="0.39" y1="2.59" x2="0.97" y2="2.61" layer="25"/>
<rectangle x1="2.11" y1="2.59" x2="2.25" y2="2.61" layer="25"/>
<rectangle x1="3.41" y1="2.59" x2="3.73" y2="2.61" layer="25"/>
<rectangle x1="0.39" y1="2.61" x2="0.97" y2="2.63" layer="25"/>
<rectangle x1="2.11" y1="2.61" x2="2.25" y2="2.63" layer="25"/>
<rectangle x1="3.41" y1="2.61" x2="3.73" y2="2.63" layer="25"/>
<rectangle x1="0.41" y1="2.63" x2="0.97" y2="2.65" layer="25"/>
<rectangle x1="2.11" y1="2.63" x2="2.25" y2="2.65" layer="25"/>
<rectangle x1="3.41" y1="2.63" x2="3.73" y2="2.65" layer="25"/>
<rectangle x1="0.41" y1="2.65" x2="0.97" y2="2.67" layer="25"/>
<rectangle x1="2.13" y1="2.65" x2="2.25" y2="2.67" layer="25"/>
<rectangle x1="3.39" y1="2.65" x2="3.71" y2="2.67" layer="25"/>
<rectangle x1="0.43" y1="2.67" x2="0.97" y2="2.69" layer="25"/>
<rectangle x1="2.13" y1="2.67" x2="2.25" y2="2.69" layer="25"/>
<rectangle x1="3.39" y1="2.67" x2="3.71" y2="2.69" layer="25"/>
<rectangle x1="0.43" y1="2.69" x2="0.97" y2="2.71" layer="25"/>
<rectangle x1="2.13" y1="2.69" x2="2.25" y2="2.71" layer="25"/>
<rectangle x1="3.39" y1="2.69" x2="3.69" y2="2.71" layer="25"/>
<rectangle x1="0.45" y1="2.71" x2="0.97" y2="2.73" layer="25"/>
<rectangle x1="2.11" y1="2.71" x2="2.27" y2="2.73" layer="25"/>
<rectangle x1="3.39" y1="2.71" x2="3.69" y2="2.73" layer="25"/>
<rectangle x1="0.45" y1="2.73" x2="0.97" y2="2.75" layer="25"/>
<rectangle x1="2.11" y1="2.73" x2="2.27" y2="2.75" layer="25"/>
<rectangle x1="3.39" y1="2.73" x2="3.67" y2="2.75" layer="25"/>
<rectangle x1="0.47" y1="2.75" x2="0.97" y2="2.77" layer="25"/>
<rectangle x1="2.11" y1="2.75" x2="2.27" y2="2.77" layer="25"/>
<rectangle x1="3.39" y1="2.75" x2="3.67" y2="2.77" layer="25"/>
<rectangle x1="0.47" y1="2.77" x2="0.97" y2="2.79" layer="25"/>
<rectangle x1="2.11" y1="2.77" x2="2.29" y2="2.79" layer="25"/>
<rectangle x1="3.37" y1="2.77" x2="3.65" y2="2.79" layer="25"/>
<rectangle x1="0.49" y1="2.79" x2="0.97" y2="2.81" layer="25"/>
<rectangle x1="2.11" y1="2.79" x2="2.29" y2="2.81" layer="25"/>
<rectangle x1="3.37" y1="2.79" x2="3.65" y2="2.81" layer="25"/>
<rectangle x1="0.49" y1="2.81" x2="0.99" y2="2.83" layer="25"/>
<rectangle x1="2.11" y1="2.81" x2="2.31" y2="2.83" layer="25"/>
<rectangle x1="3.37" y1="2.81" x2="3.63" y2="2.83" layer="25"/>
<rectangle x1="0.51" y1="2.83" x2="0.99" y2="2.85" layer="25"/>
<rectangle x1="2.09" y1="2.83" x2="2.31" y2="2.85" layer="25"/>
<rectangle x1="3.35" y1="2.83" x2="3.61" y2="2.85" layer="25"/>
<rectangle x1="0.51" y1="2.85" x2="0.99" y2="2.87" layer="25"/>
<rectangle x1="2.09" y1="2.85" x2="2.33" y2="2.87" layer="25"/>
<rectangle x1="3.35" y1="2.85" x2="3.61" y2="2.87" layer="25"/>
<rectangle x1="0.53" y1="2.87" x2="0.99" y2="2.89" layer="25"/>
<rectangle x1="2.09" y1="2.87" x2="2.33" y2="2.89" layer="25"/>
<rectangle x1="3.33" y1="2.87" x2="3.59" y2="2.89" layer="25"/>
<rectangle x1="0.55" y1="2.89" x2="1.01" y2="2.91" layer="25"/>
<rectangle x1="2.07" y1="2.89" x2="2.35" y2="2.91" layer="25"/>
<rectangle x1="3.33" y1="2.89" x2="3.59" y2="2.91" layer="25"/>
<rectangle x1="0.55" y1="2.91" x2="1.01" y2="2.93" layer="25"/>
<rectangle x1="2.07" y1="2.91" x2="2.35" y2="2.93" layer="25"/>
<rectangle x1="3.31" y1="2.91" x2="3.57" y2="2.93" layer="25"/>
<rectangle x1="0.57" y1="2.93" x2="1.03" y2="2.95" layer="25"/>
<rectangle x1="2.05" y1="2.93" x2="2.37" y2="2.95" layer="25"/>
<rectangle x1="3.29" y1="2.93" x2="3.55" y2="2.95" layer="25"/>
<rectangle x1="0.57" y1="2.95" x2="1.03" y2="2.97" layer="25"/>
<rectangle x1="2.05" y1="2.95" x2="2.39" y2="2.97" layer="25"/>
<rectangle x1="3.27" y1="2.95" x2="3.55" y2="2.97" layer="25"/>
<rectangle x1="0.59" y1="2.97" x2="1.05" y2="2.99" layer="25"/>
<rectangle x1="2.03" y1="2.97" x2="2.41" y2="2.99" layer="25"/>
<rectangle x1="3.27" y1="2.97" x2="3.53" y2="2.99" layer="25"/>
<rectangle x1="0.61" y1="2.99" x2="1.05" y2="3.01" layer="25"/>
<rectangle x1="2.03" y1="2.99" x2="2.43" y2="3.01" layer="25"/>
<rectangle x1="3.25" y1="2.99" x2="3.51" y2="3.01" layer="25"/>
<rectangle x1="0.63" y1="3.01" x2="1.07" y2="3.03" layer="25"/>
<rectangle x1="2.01" y1="3.01" x2="2.45" y2="3.03" layer="25"/>
<rectangle x1="3.23" y1="3.01" x2="3.49" y2="3.03" layer="25"/>
<rectangle x1="0.63" y1="3.03" x2="1.09" y2="3.05" layer="25"/>
<rectangle x1="1.99" y1="3.03" x2="2.47" y2="3.05" layer="25"/>
<rectangle x1="3.19" y1="3.03" x2="3.49" y2="3.05" layer="25"/>
<rectangle x1="0.65" y1="3.05" x2="1.09" y2="3.07" layer="25"/>
<rectangle x1="1.99" y1="3.05" x2="2.51" y2="3.07" layer="25"/>
<rectangle x1="3.17" y1="3.05" x2="3.47" y2="3.07" layer="25"/>
<rectangle x1="0.67" y1="3.07" x2="1.11" y2="3.09" layer="25"/>
<rectangle x1="1.97" y1="3.07" x2="2.53" y2="3.09" layer="25"/>
<rectangle x1="3.15" y1="3.07" x2="3.45" y2="3.09" layer="25"/>
<rectangle x1="0.69" y1="3.09" x2="1.13" y2="3.11" layer="25"/>
<rectangle x1="1.95" y1="3.09" x2="2.57" y2="3.11" layer="25"/>
<rectangle x1="3.11" y1="3.09" x2="3.43" y2="3.11" layer="25"/>
<rectangle x1="0.71" y1="3.11" x2="1.15" y2="3.13" layer="25"/>
<rectangle x1="1.93" y1="3.11" x2="2.61" y2="3.13" layer="25"/>
<rectangle x1="3.07" y1="3.11" x2="3.41" y2="3.13" layer="25"/>
<rectangle x1="0.73" y1="3.13" x2="1.17" y2="3.15" layer="25"/>
<rectangle x1="1.89" y1="3.13" x2="2.67" y2="3.15" layer="25"/>
<rectangle x1="3.01" y1="3.13" x2="3.39" y2="3.15" layer="25"/>
<rectangle x1="0.73" y1="3.15" x2="1.21" y2="3.17" layer="25"/>
<rectangle x1="1.87" y1="3.15" x2="2.77" y2="3.17" layer="25"/>
<rectangle x1="2.91" y1="3.15" x2="3.37" y2="3.17" layer="25"/>
<rectangle x1="0.75" y1="3.17" x2="1.23" y2="3.19" layer="25"/>
<rectangle x1="1.85" y1="3.17" x2="3.35" y2="3.19" layer="25"/>
<rectangle x1="0.77" y1="3.19" x2="1.27" y2="3.21" layer="25"/>
<rectangle x1="1.81" y1="3.19" x2="3.35" y2="3.21" layer="25"/>
<rectangle x1="0.79" y1="3.21" x2="1.31" y2="3.23" layer="25"/>
<rectangle x1="1.77" y1="3.21" x2="3.33" y2="3.23" layer="25"/>
<rectangle x1="0.81" y1="3.23" x2="1.37" y2="3.25" layer="25"/>
<rectangle x1="1.71" y1="3.23" x2="3.29" y2="3.25" layer="25"/>
<rectangle x1="0.83" y1="3.25" x2="1.45" y2="3.27" layer="25"/>
<rectangle x1="1.61" y1="3.25" x2="3.27" y2="3.27" layer="25"/>
<rectangle x1="0.85" y1="3.27" x2="3.25" y2="3.29" layer="25"/>
<rectangle x1="0.89" y1="3.29" x2="3.23" y2="3.31" layer="25"/>
<rectangle x1="0.91" y1="3.31" x2="3.21" y2="3.33" layer="25"/>
<rectangle x1="0.93" y1="3.33" x2="3.19" y2="3.35" layer="25"/>
<rectangle x1="0.95" y1="3.35" x2="3.15" y2="3.37" layer="25"/>
<rectangle x1="0.97" y1="3.37" x2="3.13" y2="3.39" layer="25"/>
<rectangle x1="1.01" y1="3.39" x2="3.11" y2="3.41" layer="25"/>
<rectangle x1="1.03" y1="3.41" x2="3.07" y2="3.43" layer="25"/>
<rectangle x1="1.07" y1="3.43" x2="3.05" y2="3.45" layer="25"/>
<rectangle x1="1.09" y1="3.45" x2="3.01" y2="3.47" layer="25"/>
<rectangle x1="1.13" y1="3.47" x2="2.99" y2="3.49" layer="25"/>
<rectangle x1="1.15" y1="3.49" x2="2.95" y2="3.51" layer="25"/>
<rectangle x1="1.19" y1="3.51" x2="2.91" y2="3.53" layer="25"/>
<rectangle x1="1.23" y1="3.53" x2="2.87" y2="3.55" layer="25"/>
<rectangle x1="1.27" y1="3.55" x2="2.83" y2="3.57" layer="25"/>
<rectangle x1="1.31" y1="3.57" x2="2.79" y2="3.59" layer="25"/>
<rectangle x1="1.35" y1="3.59" x2="2.75" y2="3.61" layer="25"/>
<rectangle x1="1.41" y1="3.61" x2="2.69" y2="3.63" layer="25"/>
<rectangle x1="1.47" y1="3.63" x2="2.65" y2="3.65" layer="25"/>
<rectangle x1="1.53" y1="3.65" x2="2.59" y2="3.67" layer="25"/>
<rectangle x1="1.59" y1="3.67" x2="2.51" y2="3.69" layer="25"/>
<rectangle x1="1.67" y1="3.69" x2="2.43" y2="3.71" layer="25"/>
<rectangle x1="1.77" y1="3.71" x2="2.33" y2="3.73" layer="25"/>
<rectangle x1="1.99" y1="3.73" x2="2.11" y2="3.75" layer="25"/>
<rectangle x1="16.53" y1="1.56" x2="16.63" y2="1.66" layer="25"/>
<rectangle x1="16.53" y1="2.98" x2="16.63" y2="3.08" layer="25"/>
</package>
<package name="PI-2X20" urn="urn:adsk.eagle:footprint:22315/1" locally_modified="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-24.13" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="MHCD42-THROUGHHOLE">
<pad name="OUT-" x="5.08" y="2.54" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="BAT+" x="5.08" y="0" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="BAT-" x="5.08" y="-2.54" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="VIN-" x="5.08" y="-5.08" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="VIN+" x="5.08" y="-7.62" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="OUT+" x="5.08" y="5.08" drill="0.9" diameter="1.6764" shape="octagon"/>
<rectangle x1="-18.1" y1="-9.23" x2="6.9" y2="6.75" layer="25"/>
</package>
<package name="EXPANSION-FEMALE-2X10">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<text x="-12.7" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51" rot="R90"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
</package>
<package name="OSHW_8MM">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="1.20990625" y="2.59544375"/>
<vertex x="1.016003125" y="2.6924"/>
<vertex x="0.848315625" y="2.740309375"/>
<vertex x="0.681434375" y="2.78799375"/>
<vertex x="0.676425" y="2.787259375"/>
<vertex x="0.671709375" y="2.790771875"/>
<vertex x="0.6660625" y="2.792384375"/>
<vertex x="0.66360625" y="2.79680625"/>
<vertex x="0.659546875" y="2.799828125"/>
<vertex x="0.658696875" y="2.80564375"/>
<vertex x="0.65584375" y="2.810778125"/>
<vertex x="0.657234375" y="2.815640625"/>
<vertex x="0.508" y="3.8354"/>
<vertex x="-0.4572" y="3.8354"/>
<vertex x="-0.65624375" y="2.815309375"/>
<vertex x="-0.6550375" y="2.8108875"/>
<vertex x="-0.658178125" y="2.8053875"/>
<vertex x="-0.65939375" y="2.7991625"/>
<vertex x="-0.6632" y="2.7966"/>
<vertex x="-0.665475" y="2.792615625"/>
<vertex x="-0.67159375" y="2.790946875"/>
<vertex x="-0.676846875" y="2.787409375"/>
<vertex x="-0.681346875" y="2.7882875"/>
<vertex x="-0.9398" y="2.7178"/>
<vertex x="-1.4285" y="2.473446875"/>
<vertex x="-1.430884375" y="2.47006875"/>
<vertex x="-1.43755625" y="2.46891875"/>
<vertex x="-1.44361875" y="2.4658875"/>
<vertex x="-1.44754375" y="2.467196875"/>
<vertex x="-1.451621875" y="2.46649375"/>
<vertex x="-1.457159375" y="2.470403125"/>
<vertex x="-1.46358125" y="2.47254375"/>
<vertex x="-1.46543125" y="2.476240625"/>
<vertex x="-2.3114" y="3.0734"/>
<vertex x="-2.48918125" y="2.92101875"/>
<vertex x="-2.6744375" y="2.735759375"/>
<vertex x="-2.84481875" y="2.56538125"/>
<vertex x="-2.9972" y="2.387603125"/>
<vertex x="-2.4251375" y="1.541946875"/>
<vertex x="-2.42125" y="1.5397875"/>
<vertex x="-2.419475" y="1.533575"/>
<vertex x="-2.415846875" y="1.5282125"/>
<vertex x="-2.416690625" y="1.523834375"/>
<vertex x="-2.41546875" y="1.51955625"/>
<vertex x="-2.41860625" y="1.51390625"/>
<vertex x="-2.419834375" y="1.50755"/>
<vertex x="-2.423528125" y="1.505053125"/>
<vertex x="-2.539990625" y="1.29541875"/>
<vertex x="-2.6416" y="1.066803125"/>
<vertex x="-2.71410625" y="0.873453125"/>
<vertex x="-2.714103125" y="0.87345"/>
<vertex x="-2.78644375" y="0.680553125"/>
<vertex x="-2.785859375" y="0.676665625"/>
<vertex x="-2.790003125" y="0.671059375"/>
<vertex x="-2.79245" y="0.664534375"/>
<vertex x="-2.796028125" y="0.662909375"/>
<vertex x="-2.798365625" y="0.65974375"/>
<vertex x="-2.8052625" y="0.658709375"/>
<vertex x="-2.81160625" y="0.655825"/>
<vertex x="-2.8152875" y="0.65720625"/>
<vertex x="-3.81" y="0.508003125"/>
<vertex x="-3.8354" y="0.2032125"/>
<vertex x="-3.8354" y="-0.4572"/>
<vertex x="-2.79048125" y="-0.631353125"/>
<vertex x="-2.784734375" y="-0.630203125"/>
<vertex x="-2.78051875" y="-0.6330125"/>
<vertex x="-2.775515625" y="-0.633846875"/>
<vertex x="-2.772103125" y="-0.638621875"/>
<vertex x="-2.767225" y="-0.641875"/>
<vertex x="-2.76623125" y="-0.64684375"/>
<vertex x="-2.763284375" y="-0.65096875"/>
<vertex x="-2.76425" y="-0.65675625"/>
<vertex x="-2.717803125" y="-0.888996875"/>
<vertex x="-2.6162" y="-1.142996875"/>
<vertex x="-2.51458125" y="-1.3716375"/>
<vertex x="-2.422696875" y="-1.5554"/>
<vertex x="-2.41908125" y="-1.5581125"/>
<vertex x="-2.418178125" y="-1.5644375"/>
<vertex x="-2.4153125" y="-1.57016875"/>
<vertex x="-2.416746875" y="-1.574465625"/>
<vertex x="-2.41610625" y="-1.578940625"/>
<vertex x="-2.41994375" y="-1.58405625"/>
<vertex x="-2.42196875" y="-1.59013125"/>
<vertex x="-2.42601875" y="-1.59215625"/>
<vertex x="-3.0226" y="-2.3876"/>
<vertex x="-2.87021875" y="-2.56538125"/>
<vertex x="-2.684959375" y="-2.7506375"/>
<vertex x="-2.5145875" y="-2.9210125"/>
<vertex x="-2.3622" y="-3.048"/>
<vertex x="-1.51659375" y="-2.47596875"/>
<vertex x="-1.51430625" y="-2.4719625"/>
<vertex x="-1.50821875" y="-2.470303125"/>
<vertex x="-1.502990625" y="-2.466765625"/>
<vertex x="-1.49845625" y="-2.467640625"/>
<vertex x="-1.49400625" y="-2.466428125"/>
<vertex x="-1.48853125" y="-2.46955625"/>
<vertex x="-1.482328125" y="-2.470753125"/>
<vertex x="-1.4797375" y="-2.47458125"/>
<vertex x="-1.320790625" y="-2.56540625"/>
<vertex x="-1.15255625" y="-2.63750625"/>
<vertex x="-1.151734375" y="-2.63723125"/>
<vertex x="-1.14298125" y="-2.641609375"/>
<vertex x="-1.134009375" y="-2.645453125"/>
<vertex x="-1.1336875" y="-2.646253125"/>
<vertex x="-0.9906" y="-2.717803125"/>
<vertex x="-0.381" y="-0.9906"/>
<vertex x="-0.550484375" y="-0.90961875"/>
<vertex x="-0.553140625" y="-0.910028125"/>
<vertex x="-0.559715625" y="-0.90520625"/>
<vertex x="-0.56705" y="-0.901703125"/>
<vertex x="-0.56794375" y="-0.899175"/>
<vertex x="-0.71119375" y="-0.79415"/>
<vertex x="-0.71388125" y="-0.794059375"/>
<vertex x="-0.719446875" y="-0.7881"/>
<vertex x="-0.726003125" y="-0.78329375"/>
<vertex x="-0.7264125" y="-0.78064375"/>
<vertex x="-0.847659375" y="-0.65083125"/>
<vertex x="-0.850275" y="-0.65024375"/>
<vertex x="-0.854621875" y="-0.643378125"/>
<vertex x="-0.8601875" y="-0.63741875"/>
<vertex x="-0.860096875" y="-0.63473125"/>
<vertex x="-0.9551" y="-0.484671875"/>
<vertex x="-0.95756875" y="-0.48360625"/>
<vertex x="-0.960575" y="-0.476025"/>
<vertex x="-0.964921875" y="-0.469159375"/>
<vertex x="-0.964334375" y="-0.46654375"/>
<vertex x="-1.02981875" y="-0.301415625"/>
<vertex x="-1.032040625" y="-0.2999125"/>
<vertex x="-1.033584375" y="-0.291921875"/>
<vertex x="-1.036584375" y="-0.284353125"/>
<vertex x="-1.03551875" y="-0.2818875"/>
<vertex x="-1.06918125" y="-0.10748125"/>
<vertex x="-1.071084375" y="-0.105590625"/>
<vertex x="-1.071115625" y="-0.097459375"/>
<vertex x="-1.072659375" y="-0.089459375"/>
<vertex x="-1.071153125" y="-0.087234375"/>
<vertex x="-1.071825" y="0.09039375"/>
<vertex x="-1.07334375" y="0.092603125"/>
<vertex x="-1.0718625" y="0.1006"/>
<vertex x="-1.07189375" y="0.108746875"/>
<vertex x="-1.07" y="0.110653125"/>
<vertex x="-1.037659375" y="0.285303125"/>
<vertex x="-1.038740625" y="0.287759375"/>
<vertex x="-1.0358" y="0.29534375"/>
<vertex x="-1.034315625" y="0.303353125"/>
<vertex x="-1.032103125" y="0.304875"/>
<vertex x="-0.967875" y="0.470478125"/>
<vertex x="-0.96848125" y="0.473090625"/>
<vertex x="-0.964184375" y="0.479990625"/>
<vertex x="-0.9612375" y="0.487590625"/>
<vertex x="-0.958778125" y="0.488675"/>
<vertex x="-0.864896875" y="0.639465625"/>
<vertex x="-0.865009375" y="0.64214375"/>
<vertex x="-0.85950625" y="0.648125"/>
<vertex x="-0.855196875" y="0.655046875"/>
<vertex x="-0.852578125" y="0.65565625"/>
<vertex x="-0.73231875" y="0.786375"/>
<vertex x="-0.73193125" y="0.78903125"/>
<vertex x="-0.7254" y="0.79389375"/>
<vertex x="-0.719890625" y="0.799884375"/>
<vertex x="-0.717209375" y="0.799996875"/>
<vertex x="-0.57474375" y="0.9061"/>
<vertex x="-0.57386875" y="0.9086375"/>
<vertex x="-0.566553125" y="0.9122"/>
<vertex x="-0.560021875" y="0.917065625"/>
<vertex x="-0.557365625" y="0.916678125"/>
<vertex x="-0.397690625" y="0.99445625"/>
<vertex x="-0.396359375" y="0.996784375"/>
<vertex x="-0.3885125" y="0.998925"/>
<vertex x="-0.3811875" y="1.00249375"/>
<vertex x="-0.378646875" y="1.00161875"/>
<vertex x="-0.2072875" y="1.04838125"/>
<vertex x="-0.205546875" y="1.050421875"/>
<vertex x="-0.197434375" y="1.05106875"/>
<vertex x="-0.189578125" y="1.0532125"/>
<vertex x="-0.187246875" y="1.05188125"/>
<vertex x="-0.010290625" y="1.06598125"/>
<vertex x="-0.009109375" y="1.067090625"/>
<vertex x="-0.000028125" y="1.0668"/>
<vertex x="0.0090625" y="1.067525"/>
<vertex x="0.0103" y="1.06646875"/>
<vertex x="0.190590625" y="1.06070625"/>
<vertex x="0.19288125" y="1.062159375"/>
<vertex x="0.200815625" y="1.06038125"/>
<vertex x="0.20891875" y="1.060121875"/>
<vertex x="0.21076875" y="1.05815"/>
<vertex x="0.38686875" y="1.018671875"/>
<vertex x="0.389384375" y="1.019665625"/>
<vertex x="0.396821875" y="1.016440625"/>
<vertex x="0.404753125" y="1.0146625"/>
<vertex x="0.406203125" y="1.012371875"/>
<vertex x="0.57176875" y="0.94058125"/>
<vertex x="0.574428125" y="0.9410875"/>
<vertex x="0.58113125" y="0.936521875"/>
<vertex x="0.5885875" y="0.933290625"/>
<vertex x="0.589584375" y="0.93076875"/>
<vertex x="0.7387625" y="0.82920625"/>
<vertex x="0.741471875" y="0.829203125"/>
<vertex x="0.747209375" y="0.82345625"/>
<vertex x="0.75391875" y="0.8188875"/>
<vertex x="0.754425" y="0.816228125"/>
<vertex x="0.88191875" y="0.688490625"/>
<vertex x="0.88458125" y="0.68798125"/>
<vertex x="0.889140625" y="0.681259375"/>
<vertex x="0.894871875" y="0.675515625"/>
<vertex x="0.89486875" y="0.672809375"/>
<vertex x="0.996153125" y="0.523440625"/>
<vertex x="0.998671875" y="0.522440625"/>
<vertex x="1.001890625" y="0.51498125"/>
<vertex x="1.00644375" y="0.508265625"/>
<vertex x="1.005934375" y="0.50560625"/>
<vertex x="1.077415625" y="0.339903125"/>
<vertex x="1.0797" y="0.33845"/>
<vertex x="1.081459375" y="0.330528125"/>
<vertex x="1.084678125" y="0.32306875"/>
<vertex x="1.083678125" y="0.32055"/>
<vertex x="1.122825" y="0.144384375"/>
<vertex x="1.124796875" y="0.142528125"/>
<vertex x="1.125040625" y="0.13441875"/>
<vertex x="1.126803125" y="0.1264875"/>
<vertex x="1.125346875" y="0.1242"/>
<vertex x="1.130778125" y="-0.056184375"/>
<vertex x="1.132365625" y="-0.058378125"/>
<vertex x="1.131084375" y="-0.06639375"/>
<vertex x="1.131328125" y="-0.074509375"/>
<vertex x="1.129471875" y="-0.07648125"/>
<vertex x="1.100978125" y="-0.25469375"/>
<vertex x="1.102128125" y="-0.25714375"/>
<vertex x="1.099365625" y="-0.264778125"/>
<vertex x="1.098084375" y="-0.272796875"/>
<vertex x="1.095890625" y="-0.274384375"/>
<vertex x="1.034496875" y="-0.4440875"/>
<vertex x="1.035165625" y="-0.4467125"/>
<vertex x="1.031021875" y="-0.45369375"/>
<vertex x="1.028259375" y="-0.461328125"/>
<vertex x="1.02580625" y="-0.462478125"/>
<vertex x="0.933684375" y="-0.617665625"/>
<vertex x="0.93385" y="-0.620365625"/>
<vertex x="0.928478125" y="-0.6264375"/>
<vertex x="0.924328125" y="-0.633428125"/>
<vertex x="0.9217" y="-0.634096875"/>
<vertex x="0.802128125" y="-0.76925"/>
<vertex x="0.801784375" y="-0.7719375"/>
<vertex x="0.795359375" y="-0.7769"/>
<vertex x="0.78998125" y="-0.78298125"/>
<vertex x="0.787278125" y="-0.783146875"/>
<vertex x="0.644459375" y="-0.893503125"/>
<vertex x="0.643615625" y="-0.896078125"/>
<vertex x="0.63636875" y="-0.899753125"/>
<vertex x="0.62995" y="-0.9047125"/>
<vertex x="0.627265625" y="-0.90436875"/>
<vertex x="0.4572" y="-0.9906"/>
<vertex x="1.117596875" y="-2.6924"/>
<vertex x="1.286265625" y="-2.596021875"/>
<vertex x="1.286265625" y="-2.59601875"/>
<vertex x="1.63200625" y="-2.39845625"/>
<vertex x="1.634990625" y="-2.39435625"/>
<vertex x="1.6407875" y="-2.393440625"/>
<vertex x="1.645878125" y="-2.39053125"/>
<vertex x="1.650765625" y="-2.3918625"/>
<vertex x="1.655775" y="-2.391071875"/>
<vertex x="1.660521875" y="-2.394521875"/>
<vertex x="1.666178125" y="-2.396065625"/>
<vertex x="1.668690625" y="-2.400465625"/>
<vertex x="2.489196875" y="-2.9972"/>
<vertex x="2.659559375" y="-2.8268375"/>
<vertex x="2.81941875" y="-2.66698125"/>
<vertex x="2.978646875" y="-2.4812125"/>
<vertex x="2.97865" y="-2.4812125"/>
<vertex x="3.1242" y="-2.3114"/>
<vertex x="2.526734375" y="-1.4152"/>
<vertex x="2.522446875" y="-1.41251875"/>
<vertex x="2.521121875" y="-1.40678125"/>
<vertex x="2.5178625" y="-1.401890625"/>
<vertex x="2.518853125" y="-1.396940625"/>
<vertex x="2.517715625" y="-1.392015625"/>
<vertex x="2.520834375" y="-1.387025"/>
<vertex x="2.5219875" y="-1.381259375"/>
<vertex x="2.5261875" y="-1.378459375"/>
<vertex x="2.641596875" y="-1.193803125"/>
<vertex x="2.714471875" y="-0.975184375"/>
<vertex x="2.714471875" y="-0.97518125"/>
<vertex x="2.793996875" y="-0.736615625"/>
<vertex x="2.838825" y="-0.5797125"/>
<vertex x="2.838015625" y="-0.575090625"/>
<vertex x="2.84160625" y="-0.569978125"/>
<vertex x="2.84331875" y="-0.56398125"/>
<vertex x="2.847415625" y="-0.56170625"/>
<vertex x="2.850109375" y="-0.55786875"/>
<vertex x="2.856259375" y="-0.55679375"/>
<vertex x="2.8617125" y="-0.5537625"/>
<vertex x="2.86621875" y="-0.55505"/>
<vertex x="3.8608" y="-0.381"/>
<vertex x="3.8862" y="-0.10159375"/>
<vertex x="3.8862" y="0.126990625"/>
<vertex x="3.86210625" y="0.319734375"/>
<vertex x="3.861821875" y="0.319965625"/>
<vertex x="3.860803125" y="0.3301375"/>
<vertex x="3.859540625" y="0.34025"/>
<vertex x="3.859765625" y="0.3405375"/>
<vertex x="3.8354" y="0.5842"/>
<vertex x="2.8151875" y="0.758384375"/>
<vertex x="2.811621875" y="0.7570875"/>
<vertex x="2.80518125" y="0.76009375"/>
<vertex x="2.79818125" y="0.7612875"/>
<vertex x="2.795990625" y="0.76438125"/>
<vertex x="2.792553125" y="0.765984375"/>
<vertex x="2.790125" y="0.772659375"/>
<vertex x="2.78601875" y="0.77845625"/>
<vertex x="2.78665625" y="0.782196875"/>
<vertex x="2.695959375" y="1.031615625"/>
<vertex x="2.695603125" y="1.03179375"/>
<vertex x="2.692390625" y="1.041428125"/>
<vertex x="2.68890625" y="1.0510125"/>
<vertex x="2.689075" y="1.051375"/>
<vertex x="2.641596875" y="1.1938"/>
<vertex x="2.540021875" y="1.371565625"/>
<vertex x="2.424796875" y="1.555925"/>
<vertex x="2.4200375" y="1.55964375"/>
<vertex x="2.419440625" y="1.56449375"/>
<vertex x="2.41685" y="1.568640625"/>
<vertex x="2.418209375" y="1.574528125"/>
<vertex x="2.417471875" y="1.580528125"/>
<vertex x="2.42048125" y="1.58438125"/>
<vertex x="2.42158125" y="1.58914375"/>
<vertex x="2.42670625" y="1.592346875"/>
<vertex x="3.048" y="2.3876"/>
<vertex x="2.89561875" y="2.56538125"/>
<vertex x="2.717803125" y="2.743196875"/>
<vertex x="2.522515625" y="2.914071875"/>
<vertex x="2.3114" y="3.0988"/>
<vertex x="1.465303125" y="2.50155"/>
<vertex x="1.463871875" y="2.498334375"/>
<vertex x="1.457003125" y="2.49569375"/>
<vertex x="1.4509875" y="2.491446875"/>
<vertex x="1.44751875" y="2.49204375"/>
<vertex x="1.44423125" y="2.49078125"/>
<vertex x="1.437503125" y="2.493771875"/>
<vertex x="1.43025" y="2.495021875"/>
<vertex x="1.42821875" y="2.497896875"/>
<vertex x="1.228765625" y="2.586546875"/>
<vertex x="1.228128125" y="2.586334375"/>
<vertex x="1.21920625" y="2.59079375"/>
<vertex x="1.210146875" y="2.594821875"/>
</polygon>
</package>
<package name="USB_C_FEMALE">
<smd name="B6" x="0.75" y="0" dx="0.3" dy="1.45" layer="1"/>
<smd name="A7" x="0.25" y="0" dx="0.3" dy="1.45" layer="1"/>
<smd name="GND2" x="3.225" y="0" dx="0.6" dy="1.45" layer="1"/>
<smd name="VBUS2" x="2.45" y="0" dx="0.55" dy="1.45" layer="1"/>
<smd name="B5" x="1.75" y="0" dx="0.3" dy="1.45" layer="1"/>
<smd name="A8" x="1.25" y="0" dx="0.3" dy="1.45" layer="1"/>
<smd name="B7" x="-0.75" y="0" dx="0.3" dy="1.45" layer="1" rot="R180"/>
<smd name="A6" x="-0.25" y="0" dx="0.3" dy="1.45" layer="1" rot="R180"/>
<smd name="GND" x="-3.225" y="0" dx="0.6" dy="1.45" layer="1" rot="R180"/>
<smd name="VBUS1" x="-2.45" y="0" dx="0.55" dy="1.45" layer="1" rot="R180"/>
<smd name="B8" x="-1.75" y="0" dx="0.3" dy="1.45" layer="1" rot="R180"/>
<smd name="A5" x="-1.25" y="0" dx="0.3" dy="1.45" layer="1" rot="R180"/>
<hole x="2.89" y="-1.445" drill="0.65"/>
<wire x1="-4.62" y1="-5.4" x2="-4.62" y2="-4.78" width="0.01" layer="46"/>
<wire x1="-4.02" y1="-5.4" x2="-4.02" y2="-4.78" width="0.01" layer="46"/>
<wire x1="-4.62" y1="-4.78" x2="-4.02" y2="-4.78" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.02" y1="-5.4" x2="-4.62" y2="-5.4" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.32" y1="-4.295" x2="-4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="-4.82" y1="-5.095" x2="-3.82" y2="-5.095" width="0.01" layer="51"/>
<wire x1="-4.62" y1="-5.095" x2="-4.02" y2="-5.095" width="0.01" layer="52"/>
<wire x1="-4.32" y1="-4.495" x2="-4.32" y2="-5.695" width="0.01" layer="52"/>
<wire x1="-4.82" y1="-5.395" x2="-3.82" y2="-5.395" width="0.01" layer="51"/>
<wire x1="-4.82" y1="-4.795" x2="-3.82" y2="-4.795" width="0.01" layer="51"/>
<wire x1="-4.62" y1="-1.465" x2="-4.62" y2="-0.365" width="0.01" layer="46"/>
<wire x1="-4.02" y1="-1.465" x2="-4.02" y2="-0.365" width="0.01" layer="46"/>
<wire x1="-4.02" y1="-1.465" x2="-4.62" y2="-1.465" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.82" y1="-0.915" x2="-3.82" y2="-0.915" width="0.01" layer="51"/>
<wire x1="-4.62" y1="-0.915" x2="-4.02" y2="-0.915" width="0.01" layer="52"/>
<wire x1="-4.82" y1="-1.515" x2="-3.82" y2="-1.515" width="0.01" layer="51"/>
<wire x1="-4.82" y1="-0.315" x2="-3.82" y2="-0.315" width="0.01" layer="51"/>
<wire x1="-4.62" y1="-0.365" x2="-4.02" y2="-0.365" width="0.01" layer="46" curve="-180"/>
<wire x1="4.32" y1="-0.065" x2="4.32" y2="-1.765" width="0.01" layer="52"/>
<wire x1="4.02" y1="-5.4" x2="4.02" y2="-4.78" width="0.01" layer="46"/>
<wire x1="4.62" y1="-5.4" x2="4.62" y2="-4.78" width="0.01" layer="46"/>
<wire x1="4.02" y1="-4.78" x2="4.62" y2="-4.78" width="0.01" layer="46" curve="-180"/>
<wire x1="4.62" y1="-5.4" x2="4.02" y2="-5.4" width="0.01" layer="46" curve="-180"/>
<wire x1="4.32" y1="-4.295" x2="4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="3.82" y1="-5.095" x2="4.82" y2="-5.095" width="0.01" layer="51"/>
<wire x1="4.02" y1="-5.095" x2="4.62" y2="-5.095" width="0.01" layer="52"/>
<wire x1="4.32" y1="-4.495" x2="4.32" y2="-5.695" width="0.01" layer="52"/>
<wire x1="3.82" y1="-5.395" x2="4.82" y2="-5.395" width="0.01" layer="51"/>
<wire x1="3.82" y1="-4.795" x2="4.82" y2="-4.795" width="0.01" layer="51"/>
<wire x1="4.02" y1="-1.465" x2="4.02" y2="-0.365" width="0.01" layer="46"/>
<wire x1="4.62" y1="-1.465" x2="4.62" y2="-0.365" width="0.01" layer="46"/>
<wire x1="4.62" y1="-1.465" x2="4.02" y2="-1.465" width="0.01" layer="46" curve="-180"/>
<wire x1="3.82" y1="-0.915" x2="4.82" y2="-0.915" width="0.01" layer="51"/>
<wire x1="4.02" y1="-0.915" x2="4.62" y2="-0.915" width="0.01" layer="52"/>
<wire x1="3.82" y1="-1.515" x2="4.82" y2="-1.515" width="0.01" layer="51"/>
<wire x1="3.82" y1="-0.315" x2="4.82" y2="-0.315" width="0.01" layer="51"/>
<wire x1="4.02" y1="-0.365" x2="4.62" y2="-0.365" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.32" y1="0.135" x2="-4.32" y2="0.125" width="0.01" layer="51"/>
<wire x1="-4.32" y1="0.125" x2="-4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-0.065" x2="-4.32" y2="-1.765" width="0.01" layer="52"/>
<wire x1="4.32" y1="0.135" x2="4.32" y2="0.125" width="0.01" layer="51"/>
<wire x1="4.32" y1="0.125" x2="4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="-4.32" y2="0.125" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="0.125" x2="4.32" y2="0.125" width="0.1524" layer="51"/>
<wire x1="4.32" y1="0.125" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.7" x2="4.32" y2="-7.7" width="0.1524" layer="51" curve="-21.282614"/>
<wire x1="-4.32" y1="-2.2" x2="-4.32" y2="-4" width="0.1524" layer="21"/>
<wire x1="4.32" y1="-2.2" x2="4.32" y2="-4" width="0.1524" layer="21"/>
<polygon width="0.01" layer="29" pour="solid">
<vertex x="-4.44808125" y="-2.056765625"/>
<vertex x="-4.32" y="-2.065"/>
<vertex x="-4.19191875" y="-2.056765625"/>
<vertex x="-4.0688875" y="-2.02024375"/>
<vertex x="-3.95705625" y="-1.957246875"/>
<vertex x="-3.862046875" y="-1.870946875"/>
<vertex x="-3.78863125" y="-1.765678125"/>
<vertex x="-3.740471875" y="-1.646703125"/>
<vertex x="-3.72" y="-1.519996875"/>
<vertex x="-3.72" y="-0.319990625"/>
<vertex x="-3.735490625" y="-0.2077625"/>
<vertex x="-3.77256875" y="-0.100709375"/>
<vertex x="-3.829834375" y="-0.002940625"/>
<vertex x="-3.905046875" y="0.08175625"/>
<vertex x="-3.9953625" y="0.150175"/>
<vertex x="-4.097284375" y="0.199646875"/>
<vertex x="-4.206903125" y="0.228296875"/>
<vertex x="-4.32" y="0.235"/>
<vertex x="-4.433096875" y="0.228296875"/>
<vertex x="-4.542715625" y="0.199646875"/>
<vertex x="-4.6446375" y="0.150175"/>
<vertex x="-4.734953125" y="0.08175625"/>
<vertex x="-4.810165625" y="-0.002940625"/>
<vertex x="-4.86743125" y="-0.100709375"/>
<vertex x="-4.904509375" y="-0.2077625"/>
<vertex x="-4.92" y="-0.319990625"/>
<vertex x="-4.92" y="-1.519996875"/>
<vertex x="-4.899528125" y="-1.646703125"/>
<vertex x="-4.85136875" y="-1.765678125"/>
<vertex x="-4.777953125" y="-1.870946875"/>
<vertex x="-4.68294375" y="-1.957246875"/>
<vertex x="-4.5711125" y="-2.02024375"/>
</polygon>
<polygon width="0.01" layer="31" pour="solid">
<vertex x="-4.44808125" y="-2.056765625"/>
<vertex x="-4.32" y="-2.065"/>
<vertex x="-4.19191875" y="-2.056765625"/>
<vertex x="-4.0688875" y="-2.02024375"/>
<vertex x="-3.95705625" y="-1.957246875"/>
<vertex x="-3.862046875" y="-1.870946875"/>
<vertex x="-3.78863125" y="-1.765678125"/>
<vertex x="-3.740471875" y="-1.646703125"/>
<vertex x="-3.72" y="-1.519996875"/>
<vertex x="-3.72" y="-0.319990625"/>
<vertex x="-3.735490625" y="-0.2077625"/>
<vertex x="-3.77256875" y="-0.100709375"/>
<vertex x="-3.829834375" y="-0.002940625"/>
<vertex x="-3.905046875" y="0.08175625"/>
<vertex x="-3.9953625" y="0.150175"/>
<vertex x="-4.097284375" y="0.199646875"/>
<vertex x="-4.206903125" y="0.228296875"/>
<vertex x="-4.32" y="0.235"/>
<vertex x="-4.433096875" y="0.228296875"/>
<vertex x="-4.542715625" y="0.199646875"/>
<vertex x="-4.6446375" y="0.150175"/>
<vertex x="-4.734953125" y="0.08175625"/>
<vertex x="-4.810165625" y="-0.002940625"/>
<vertex x="-4.86743125" y="-0.100709375"/>
<vertex x="-4.904509375" y="-0.2077625"/>
<vertex x="-4.92" y="-0.319990625"/>
<vertex x="-4.92" y="-1.519996875"/>
<vertex x="-4.899528125" y="-1.646703125"/>
<vertex x="-4.85136875" y="-1.765678125"/>
<vertex x="-4.777953125" y="-1.870946875"/>
<vertex x="-4.68294375" y="-1.957246875"/>
<vertex x="-4.5711125" y="-2.02024375"/>
</polygon>
<polygon width="0.01" layer="29" pour="solid">
<vertex x="4.19191875" y="-2.056765625"/>
<vertex x="4.32" y="-2.065"/>
<vertex x="4.44808125" y="-2.056765625"/>
<vertex x="4.5711125" y="-2.02024375"/>
<vertex x="4.68294375" y="-1.957246875"/>
<vertex x="4.777953125" y="-1.870946875"/>
<vertex x="4.85136875" y="-1.765678125"/>
<vertex x="4.899528125" y="-1.646703125"/>
<vertex x="4.92" y="-1.519996875"/>
<vertex x="4.92" y="-0.319990625"/>
<vertex x="4.904509375" y="-0.2077625"/>
<vertex x="4.86743125" y="-0.100709375"/>
<vertex x="4.810165625" y="-0.002940625"/>
<vertex x="4.734953125" y="0.08175625"/>
<vertex x="4.6446375" y="0.150175"/>
<vertex x="4.542715625" y="0.199646875"/>
<vertex x="4.433096875" y="0.228296875"/>
<vertex x="4.32" y="0.235"/>
<vertex x="4.206903125" y="0.228296875"/>
<vertex x="4.097284375" y="0.199646875"/>
<vertex x="3.9953625" y="0.150175"/>
<vertex x="3.905046875" y="0.08175625"/>
<vertex x="3.829834375" y="-0.002940625"/>
<vertex x="3.77256875" y="-0.100709375"/>
<vertex x="3.735490625" y="-0.2077625"/>
<vertex x="3.72" y="-0.319990625"/>
<vertex x="3.72" y="-1.519996875"/>
<vertex x="3.740471875" y="-1.646703125"/>
<vertex x="3.78863125" y="-1.765678125"/>
<vertex x="3.862046875" y="-1.870946875"/>
<vertex x="3.95705625" y="-1.957246875"/>
<vertex x="4.0688875" y="-2.02024375"/>
</polygon>
<polygon width="0.01" layer="29" pour="solid">
<vertex x="4.18698125" y="-5.980575"/>
<vertex x="4.31999375" y="-5.995"/>
<vertex x="4.43661875" y="-5.98400625"/>
<vertex x="4.54885" y="-5.950471875"/>
<vertex x="4.652375" y="-5.89569375"/>
<vertex x="4.743225" y="-5.82176875"/>
<vertex x="4.817915625" y="-5.73153125"/>
<vertex x="4.873565625" y="-5.628459375"/>
<vertex x="4.908034375" y="-5.516515625"/>
<vertex x="4.92" y="-5.399996875"/>
<vertex x="4.92" y="-4.800009375"/>
<vertex x="4.908909375" y="-4.682409375"/>
<vertex x="4.875090625" y="-4.5692375"/>
<vertex x="4.81985" y="-4.46485"/>
<vertex x="4.745303125" y="-4.3732375"/>
<vertex x="4.6543125" y="-4.29793125"/>
<vertex x="4.550371875" y="-4.241815625"/>
<vertex x="4.437490625" y="-4.2070625"/>
<vertex x="4.319996875" y="-4.195"/>
<vertex x="4.185996875" y="-4.2106625"/>
<vertex x="4.058834375" y="-4.255746875"/>
<vertex x="3.944896875" y="-4.328003125"/>
<vertex x="3.84989375" y="-4.42379375"/>
<vertex x="3.778584375" y="-4.53833125"/>
<vertex x="3.73455" y="-4.66586875"/>
<vertex x="3.72" y="-4.799990625"/>
<vertex x="3.72" y="-5.40000625"/>
<vertex x="3.7355375" y="-5.5329"/>
<vertex x="3.780253125" y="-5.658990625"/>
<vertex x="3.8519125" y="-5.7719875"/>
<vertex x="3.946909375" y="-5.866196875"/>
<vertex x="4.060496875" y="-5.936909375"/>
</polygon>
<polygon width="0.01" layer="29" pour="solid">
<vertex x="-4.45301875" y="-5.980575"/>
<vertex x="-4.32000625" y="-5.995"/>
<vertex x="-4.20338125" y="-5.98400625"/>
<vertex x="-4.09115" y="-5.950471875"/>
<vertex x="-3.987625" y="-5.89569375"/>
<vertex x="-3.896775" y="-5.82176875"/>
<vertex x="-3.822084375" y="-5.73153125"/>
<vertex x="-3.766434375" y="-5.628459375"/>
<vertex x="-3.731965625" y="-5.516515625"/>
<vertex x="-3.72" y="-5.399996875"/>
<vertex x="-3.72" y="-4.800009375"/>
<vertex x="-3.731090625" y="-4.682409375"/>
<vertex x="-3.764909375" y="-4.5692375"/>
<vertex x="-3.82015" y="-4.46485"/>
<vertex x="-3.894696875" y="-4.3732375"/>
<vertex x="-3.9856875" y="-4.29793125"/>
<vertex x="-4.089628125" y="-4.241815625"/>
<vertex x="-4.202509375" y="-4.2070625"/>
<vertex x="-4.320003125" y="-4.195"/>
<vertex x="-4.454003125" y="-4.2106625"/>
<vertex x="-4.581165625" y="-4.255746875"/>
<vertex x="-4.695103125" y="-4.328003125"/>
<vertex x="-4.79010625" y="-4.42379375"/>
<vertex x="-4.861415625" y="-4.53833125"/>
<vertex x="-4.90545" y="-4.66586875"/>
<vertex x="-4.92" y="-4.799990625"/>
<vertex x="-4.92" y="-5.40000625"/>
<vertex x="-4.9044625" y="-5.5329"/>
<vertex x="-4.859746875" y="-5.658990625"/>
<vertex x="-4.7880875" y="-5.7719875"/>
<vertex x="-4.693090625" y="-5.866196875"/>
<vertex x="-4.579503125" y="-5.936909375"/>
</polygon>
<polygon width="0.01" layer="31" pour="solid">
<vertex x="4.19191875" y="-2.056765625"/>
<vertex x="4.32" y="-2.065"/>
<vertex x="4.44808125" y="-2.056765625"/>
<vertex x="4.5711125" y="-2.02024375"/>
<vertex x="4.68294375" y="-1.957246875"/>
<vertex x="4.777953125" y="-1.870946875"/>
<vertex x="4.85136875" y="-1.765678125"/>
<vertex x="4.899528125" y="-1.646703125"/>
<vertex x="4.92" y="-1.519996875"/>
<vertex x="4.92" y="-0.319990625"/>
<vertex x="4.904509375" y="-0.2077625"/>
<vertex x="4.86743125" y="-0.100709375"/>
<vertex x="4.810165625" y="-0.002940625"/>
<vertex x="4.734953125" y="0.08175625"/>
<vertex x="4.6446375" y="0.150175"/>
<vertex x="4.542715625" y="0.199646875"/>
<vertex x="4.433096875" y="0.228296875"/>
<vertex x="4.32" y="0.235"/>
<vertex x="4.206903125" y="0.228296875"/>
<vertex x="4.097284375" y="0.199646875"/>
<vertex x="3.9953625" y="0.150175"/>
<vertex x="3.905046875" y="0.08175625"/>
<vertex x="3.829834375" y="-0.002940625"/>
<vertex x="3.77256875" y="-0.100709375"/>
<vertex x="3.735490625" y="-0.2077625"/>
<vertex x="3.72" y="-0.319990625"/>
<vertex x="3.72" y="-1.519996875"/>
<vertex x="3.740471875" y="-1.646703125"/>
<vertex x="3.78863125" y="-1.765678125"/>
<vertex x="3.862046875" y="-1.870946875"/>
<vertex x="3.95705625" y="-1.957246875"/>
<vertex x="4.0688875" y="-2.02024375"/>
</polygon>
<polygon width="0.01" layer="31" pour="solid">
<vertex x="4.18698125" y="-5.980575"/>
<vertex x="4.31999375" y="-5.995"/>
<vertex x="4.43661875" y="-5.98400625"/>
<vertex x="4.54885" y="-5.950471875"/>
<vertex x="4.652375" y="-5.89569375"/>
<vertex x="4.743225" y="-5.82176875"/>
<vertex x="4.817915625" y="-5.73153125"/>
<vertex x="4.873565625" y="-5.628459375"/>
<vertex x="4.908034375" y="-5.516515625"/>
<vertex x="4.92" y="-5.399996875"/>
<vertex x="4.92" y="-4.800009375"/>
<vertex x="4.908909375" y="-4.682409375"/>
<vertex x="4.875090625" y="-4.5692375"/>
<vertex x="4.81985" y="-4.46485"/>
<vertex x="4.745303125" y="-4.3732375"/>
<vertex x="4.6543125" y="-4.29793125"/>
<vertex x="4.550371875" y="-4.241815625"/>
<vertex x="4.437490625" y="-4.2070625"/>
<vertex x="4.319996875" y="-4.195"/>
<vertex x="4.185996875" y="-4.2106625"/>
<vertex x="4.058834375" y="-4.255746875"/>
<vertex x="3.944896875" y="-4.328003125"/>
<vertex x="3.84989375" y="-4.42379375"/>
<vertex x="3.778584375" y="-4.53833125"/>
<vertex x="3.73455" y="-4.66586875"/>
<vertex x="3.72" y="-4.799990625"/>
<vertex x="3.72" y="-5.40000625"/>
<vertex x="3.7355375" y="-5.5329"/>
<vertex x="3.780253125" y="-5.658990625"/>
<vertex x="3.8519125" y="-5.7719875"/>
<vertex x="3.946909375" y="-5.866196875"/>
<vertex x="4.060496875" y="-5.936909375"/>
</polygon>
<polygon width="0.01" layer="31" pour="solid">
<vertex x="-4.45301875" y="-5.980575"/>
<vertex x="-4.32000625" y="-5.995"/>
<vertex x="-4.20338125" y="-5.98400625"/>
<vertex x="-4.09115" y="-5.950471875"/>
<vertex x="-3.987625" y="-5.89569375"/>
<vertex x="-3.896775" y="-5.82176875"/>
<vertex x="-3.822084375" y="-5.73153125"/>
<vertex x="-3.766434375" y="-5.628459375"/>
<vertex x="-3.731965625" y="-5.516515625"/>
<vertex x="-3.72" y="-5.399996875"/>
<vertex x="-3.72" y="-4.800009375"/>
<vertex x="-3.731090625" y="-4.682409375"/>
<vertex x="-3.764909375" y="-4.5692375"/>
<vertex x="-3.82015" y="-4.46485"/>
<vertex x="-3.894696875" y="-4.3732375"/>
<vertex x="-3.9856875" y="-4.29793125"/>
<vertex x="-4.089628125" y="-4.241815625"/>
<vertex x="-4.202509375" y="-4.2070625"/>
<vertex x="-4.320003125" y="-4.195"/>
<vertex x="-4.454003125" y="-4.2106625"/>
<vertex x="-4.581165625" y="-4.255746875"/>
<vertex x="-4.695103125" y="-4.328003125"/>
<vertex x="-4.79010625" y="-4.42379375"/>
<vertex x="-4.861415625" y="-4.53833125"/>
<vertex x="-4.90545" y="-4.66586875"/>
<vertex x="-4.92" y="-4.799990625"/>
<vertex x="-4.92" y="-5.40000625"/>
<vertex x="-4.9044625" y="-5.5329"/>
<vertex x="-4.859746875" y="-5.658990625"/>
<vertex x="-4.7880875" y="-5.7719875"/>
<vertex x="-4.693090625" y="-5.866196875"/>
<vertex x="-4.579503125" y="-5.936909375"/>
</polygon>
<text x="0" y="-2.54" size="0.762" layer="25" align="center">&gt;Name</text>
<text x="0" y="-3.81" size="0.762" layer="27" align="center">&gt;Value</text>
<smd name="SHLD1" x="-4.32" y="-0.915" dx="1" dy="2.1" layer="1" roundness="100" rot="R180"/>
<smd name="SHLD2" x="4.32" y="-0.915" dx="1" dy="2.1" layer="1" roundness="100" rot="R180"/>
<smd name="SHLD3" x="-4.32" y="-0.915" dx="1" dy="2.1" layer="16" roundness="100"/>
<smd name="SHLD4" x="4.32" y="-0.915" dx="1" dy="2.1" layer="16" roundness="100"/>
<smd name="SHLD5" x="-4.32" y="-5.095" dx="1" dy="1.6" layer="1" roundness="100" rot="R180"/>
<smd name="SHLD6" x="-4.32" y="-5.095" dx="1" dy="1.6" layer="16" roundness="100"/>
<smd name="SHLD7" x="4.32" y="-5.095" dx="1" dy="1.6" layer="16" roundness="100"/>
<smd name="SHLD8" x="4.32" y="-5.095" dx="1" dy="1.6" layer="1" roundness="100" rot="R180"/>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="-4.8" y="-2"/>
<vertex x="-3.8" y="-2"/>
<vertex x="-3.8" y="0.1"/>
<vertex x="-4.8" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="-4.8" y="-5.9"/>
<vertex x="-3.8" y="-5.9"/>
<vertex x="-3.8" y="-4.3"/>
<vertex x="-4.8" y="-4.3"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="3.8" y="-2"/>
<vertex x="4.8" y="-2"/>
<vertex x="4.8" y="0.1"/>
<vertex x="3.8" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="3.8" y="-5.9"/>
<vertex x="4.8" y="-5.9"/>
<vertex x="4.8" y="-4.3"/>
<vertex x="3.8" y="-4.3"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="-4.8" y="-2"/>
<vertex x="-3.8" y="-2"/>
<vertex x="-3.8" y="0.1"/>
<vertex x="-4.8" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="-4.8" y="-5.9"/>
<vertex x="-3.8" y="-5.9"/>
<vertex x="-3.8" y="-4.3"/>
<vertex x="-4.8" y="-4.3"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="3.8" y="-2"/>
<vertex x="4.8" y="-2"/>
<vertex x="4.8" y="0.1"/>
<vertex x="3.8" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="3.8" y="-5.9"/>
<vertex x="4.8" y="-5.9"/>
<vertex x="4.8" y="-4.3"/>
<vertex x="3.8" y="-4.3"/>
</polygon>
<hole x="-2.89" y="-1.445" drill="0.65"/>
</package>
<package name="MODULE_ESP32-S2-WROVER">
<wire x1="-9" y1="15.5" x2="9" y2="15.5" width="0.127" layer="51"/>
<wire x1="9" y1="15.5" x2="9" y2="9.2" width="0.127" layer="51"/>
<wire x1="9" y1="9.2" x2="9" y2="-15.5" width="0.127" layer="51"/>
<wire x1="9" y1="-15.5" x2="-9" y2="-15.5" width="0.127" layer="51"/>
<wire x1="-9" y1="-15.5" x2="-9" y2="9.2" width="0.127" layer="51"/>
<wire x1="-9" y1="15.5" x2="-9" y2="9.2" width="0.127" layer="51"/>
<wire x1="-9" y1="9.2" x2="9" y2="9.2" width="0.127" layer="51"/>
<text x="-6" y="11.75" size="1.778" layer="51">ANTENNA</text>
<wire x1="-9.25" y1="15.75" x2="9.25" y2="15.75" width="0.05" layer="39"/>
<wire x1="9.25" y1="15.75" x2="9.25" y2="8.7" width="0.05" layer="39"/>
<wire x1="9.25" y1="8.7" x2="9.75" y2="8.7" width="0.05" layer="39"/>
<wire x1="9.75" y1="8.7" x2="9.75" y2="-16.25" width="0.05" layer="39"/>
<wire x1="9.75" y1="-16.25" x2="-9.75" y2="-16.25" width="0.05" layer="39"/>
<wire x1="-9.75" y1="-16.25" x2="-9.75" y2="8.7" width="0.05" layer="39"/>
<wire x1="-9.75" y1="8.7" x2="-9.25" y2="8.7" width="0.05" layer="39"/>
<wire x1="-9.25" y1="8.7" x2="-9.25" y2="15.75" width="0.05" layer="39"/>
<circle x="-10.2" y="8" radius="0.1" width="0.2" layer="51"/>
<circle x="-10.2" y="8" radius="0.1" width="0.2" layer="21"/>
<text x="-9" y="16" size="1.27" layer="25">&gt;NAME</text>
<text x="-9" y="-16.5" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<wire x1="-9" y1="-15.3" x2="-9" y2="-15.5" width="0.127" layer="21"/>
<wire x1="-9" y1="-15.5" x2="-7.55" y2="-15.5" width="0.127" layer="21"/>
<wire x1="7.55" y1="-15.5" x2="9" y2="-15.5" width="0.127" layer="21"/>
<wire x1="9" y1="-15.5" x2="9" y2="-15.3" width="0.127" layer="21"/>
<wire x1="-9" y1="9" x2="-9" y2="15.5" width="0.127" layer="21"/>
<wire x1="-9" y1="15.5" x2="9" y2="15.5" width="0.127" layer="21"/>
<wire x1="9" y1="15.5" x2="9" y2="9" width="0.127" layer="21"/>
<smd name="1" x="-8.95" y="8" dx="1.8" dy="0.9" layer="1"/>
<smd name="43_5" x="-1.19" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="-8.95" y="6.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="3" x="-8.95" y="5" dx="1.8" dy="0.9" layer="1"/>
<smd name="4" x="-8.95" y="3.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="5" x="-8.95" y="2" dx="1.8" dy="0.9" layer="1"/>
<smd name="6" x="-8.95" y="0.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="7" x="-8.95" y="-1" dx="1.8" dy="0.9" layer="1"/>
<smd name="8" x="-8.95" y="-2.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="9" x="-8.95" y="-4" dx="1.8" dy="0.9" layer="1"/>
<smd name="10" x="-8.95" y="-5.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="11" x="-8.95" y="-7" dx="1.8" dy="0.9" layer="1"/>
<smd name="12" x="-8.95" y="-8.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="13" x="-8.95" y="-10" dx="1.8" dy="0.9" layer="1"/>
<smd name="14" x="-8.95" y="-11.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="15" x="-8.95" y="-13" dx="1.8" dy="0.9" layer="1"/>
<smd name="16" x="-8.95" y="-14.5" dx="1.8" dy="0.9" layer="1"/>
<smd name="17" x="-6.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="18" x="-5.25" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="19" x="-3.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="20" x="-2.25" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="21" x="-0.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="22" x="0.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="23" x="2.25" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="24" x="3.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="25" x="5.25" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="26" x="6.75" y="-15.45" dx="1.8" dy="0.9" layer="1" rot="R90"/>
<smd name="27" x="8.95" y="-14.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="28" x="8.95" y="-13" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="29" x="8.95" y="-11.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="30" x="8.95" y="-10" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="31" x="8.95" y="-8.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="32" x="8.95" y="-7" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="33" x="8.95" y="-5.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="34" x="8.95" y="-4" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="35" x="8.95" y="-2.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="36" x="8.95" y="-1" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="37" x="8.95" y="0.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="38" x="8.95" y="2" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="39" x="8.95" y="3.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="40" x="8.95" y="5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="41" x="8.95" y="6.5" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="42" x="8.95" y="8" dx="1.8" dy="0.9" layer="1" rot="R180"/>
<smd name="43_1" x="-2.69" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_2" x="-1.19" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_3" x="0.31" y="1.45" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_4" x="-2.69" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_6" x="0.31" y="-0.05" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_7" x="-2.69" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_8" x="-1.19" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
<smd name="43_9" x="0.31" y="-1.55" dx="1.1" dy="1.1" layer="1"/>
</package>
<package name="3.3V_BATTERY" library_version="200">
<smd name="GND" x="0" y="4.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="VCC" x="0" y="0" dx="2.54" dy="1.27" layer="1"/>
<circle x="8" y="2" radius="2" width="6" layer="21"/>
</package>
<package name="IRF7319" urn="urn:adsk.eagle:footprint:30958/1" locally_modified="yes" library_version="5">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt; SOP-8L&lt;p&gt;
Source: http://www.diodes.com/datasheets/ds31262.pdf</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-3.175" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1501" y1="-3.1001" x2="-1.6599" y2="-2" layer="51"/>
<rectangle x1="-0.8801" y1="-3.1001" x2="-0.3899" y2="-2" layer="51"/>
<rectangle x1="0.3899" y1="-3.1001" x2="0.8801" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="-3.1001" x2="2.1501" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="2" x2="2.1501" y2="3.1001" layer="51"/>
<rectangle x1="0.3899" y1="2" x2="0.8801" y2="3.1001" layer="51"/>
<rectangle x1="-0.8801" y1="2" x2="-0.3899" y2="3.1001" layer="51"/>
<rectangle x1="-2.1501" y1="2" x2="-1.6599" y2="3.1001" layer="51"/>
<wire x1="-1.91" y1="-0.86" x2="-1.9" y2="-0.86" width="0.6096" layer="25"/>
</package>
<package name="TP4056" library_version="207">
<wire x1="-2.527" y1="-2.003" x2="2.197" y2="-2.003" width="0.1524" layer="51"/>
<wire x1="2.197" y1="-2.003" x2="2.197" y2="1.603" width="0.1524" layer="21"/>
<wire x1="2.197" y1="1.603" x2="-2.527" y2="1.603" width="0.1524" layer="51"/>
<wire x1="-2.527" y1="1.603" x2="-2.527" y2="-2.003" width="0.1524" layer="21"/>
<circle x="-1.9684" y="-1.1906" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-2.07" y="-2.8924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="2" x="-0.8" y="-2.8924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="3" x="0.47" y="-2.8924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="4" x="1.74" y="-2.8924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="5" x="1.74" y="2.4924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="6" x="0.47" y="2.4924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="7" x="-0.8" y="2.4924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="8" x="-2.07" y="2.4924" dx="0.6096" dy="2.0574" layer="1"/>
<smd name="EPAD" x="-0.165" y="-0.2" dx="2.286" dy="2.286" layer="1" rot="R90"/>
<text x="-1.435" y="-0.835" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.435" y="-0.2" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.2478" y1="-3.0702" x2="-1.8922" y2="-2.0542" layer="51"/>
<rectangle x1="-0.9778" y1="-3.0702" x2="-0.6222" y2="-2.0542" layer="51"/>
<rectangle x1="0.2922" y1="-3.0702" x2="0.6478" y2="-2.0542" layer="51"/>
<rectangle x1="1.5622" y1="-3.0702" x2="1.9178" y2="-2.0542" layer="51"/>
<rectangle x1="-2.2478" y1="1.6542" x2="-1.8922" y2="2.6702" layer="51"/>
<rectangle x1="-0.9778" y1="1.6542" x2="-0.6222" y2="2.6702" layer="51"/>
<rectangle x1="0.2922" y1="1.6542" x2="0.6478" y2="2.6702" layer="51"/>
<rectangle x1="1.5622" y1="1.6542" x2="1.9178" y2="2.6702" layer="51"/>
</package>
<package name="MHCD42-SMD">
<smd name="OUT-" x="5.08" y="2.54" dx="3.81" dy="1.9304" layer="1"/>
<smd name="BAT+" x="5.08" y="0" dx="3.81" dy="1.9304" layer="1"/>
<smd name="BAT-" x="5.08" y="-2.54" dx="3.81" dy="1.9304" layer="1"/>
<smd name="VIN-" x="5.08" y="-5.08" dx="3.81" dy="1.9304" layer="1"/>
<smd name="VIN+" x="5.08" y="-7.62" dx="3.81" dy="1.9304" layer="1"/>
<smd name="OUT+" x="5.08" y="5.08" dx="3.81" dy="1.9304" layer="1"/>
<rectangle x1="-18" y1="-9.25" x2="7" y2="6.75" layer="25"/>
</package>
<package name="DS3231" urn="urn:adsk.eagle:footprint:6240108/1" locally_modified="yes" library_version="2">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<wire x1="-5.395" y1="5.9" x2="5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.395" y1="-5.9" x2="-5.395" y2="-5.9" width="0.1998" layer="39"/>
<wire x1="-5.395" y1="-5.9" x2="-5.395" y2="5.9" width="0.1998" layer="39"/>
<wire x1="5.19" y1="-3.7" x2="-5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.7" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.2" x2="-5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="3.7" x2="5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="3.7" x2="5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="5.395" y1="5.9" x2="5.395" y2="-5.9" width="0.1998" layer="39"/>
<smd name="2" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<rectangle x1="-4.6901" y1="-5.32" x2="-4.1999" y2="-3.8001" layer="51"/>
<rectangle x1="-3.4201" y1="-5.32" x2="-2.9299" y2="-3.8001" layer="51"/>
<rectangle x1="-2.1501" y1="-5.32" x2="-1.6599" y2="-3.8001" layer="51"/>
<rectangle x1="-0.8801" y1="-5.32" x2="-0.3899" y2="-3.8001" layer="51"/>
<rectangle x1="0.3899" y1="-5.32" x2="0.8801" y2="-3.8001" layer="51"/>
<rectangle x1="1.6599" y1="-5.32" x2="2.1501" y2="-3.8001" layer="51"/>
<rectangle x1="2.9299" y1="-5.32" x2="3.4201" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="-5.32" x2="4.6901" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="3.8001" x2="4.6901" y2="5.32" layer="51"/>
<rectangle x1="2.9299" y1="3.8001" x2="3.4201" y2="5.32" layer="51"/>
<rectangle x1="1.6599" y1="3.8001" x2="2.1501" y2="5.32" layer="51"/>
<rectangle x1="0.3899" y1="3.8001" x2="0.8801" y2="5.32" layer="51"/>
<rectangle x1="-0.8801" y1="3.8001" x2="-0.3899" y2="5.32" layer="51"/>
<rectangle x1="-2.1501" y1="3.8001" x2="-1.6599" y2="5.32" layer="51"/>
<rectangle x1="-3.4201" y1="3.8001" x2="-2.9299" y2="5.32" layer="51"/>
<rectangle x1="-4.6901" y1="3.8001" x2="-4.1999" y2="5.32" layer="51"/>
<wire x1="-4.36" y1="-2.33" x2="-4.36" y2="-2.4" width="1.016" layer="25"/>
</package>
<package name="SOLDERJUMPER_ARROW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<smd name="2" x="1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.651" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.0508" layer="1" pour="solid">
<vertex x="-1.143" y="-0.7366"/>
<vertex x="-0.5715" y="-0.7366"/>
<vertex x="0.1651" y="0"/>
<vertex x="-0.5715" y="0.7366"/>
<vertex x="-1.143" y="0.7366"/>
</polygon>
<polygon width="0.0508" layer="1" pour="solid">
<vertex x="-0.127" y="-0.7366"/>
<vertex x="1.143" y="-0.7366"/>
<vertex x="1.143" y="0.7366"/>
<vertex x="-0.127" y="0.7366"/>
<vertex x="0.566859375" y="0.0179625"/>
<vertex x="0.57410625" y="0.010965625"/>
<vertex x="0.574115625" y="0.010446875"/>
<vertex x="0.574475" y="0.010075"/>
<vertex x="0.574296875" y="0"/>
<vertex x="0.574475" y="-0.010075"/>
<vertex x="0.574115625" y="-0.010446875"/>
<vertex x="0.57410625" y="-0.010965625"/>
<vertex x="0.566859375" y="-0.0179625"/>
</polygon>
<rectangle x1="-1.4605" y1="-0.8255" x2="1.4605" y2="0.8255" layer="29"/>
</package>
<package name="SOLDERJUMPER_REFLOW_NOPASTE" library_version="216">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="OSHW_16MM">
<polygon width="0.127" layer="25" pour="solid">
<vertex x="-5.00380625" y="-5.89279375"/>
<vertex x="-4.673596875" y="-6.1468"/>
<vertex x="-3.03129375" y="-4.977275"/>
<vertex x="-3.022340625" y="-4.970340625"/>
<vertex x="-3.02106875" y="-4.96999375"/>
<vertex x="-3.01999375" y="-4.969228125"/>
<vertex x="-3.00889375" y="-4.966675"/>
<vertex x="-2.99796875" y="-4.963696875"/>
<vertex x="-2.996665625" y="-4.9638625"/>
<vertex x="-2.995375" y="-4.963565625"/>
<vertex x="-2.9841375" y="-4.965453125"/>
<vertex x="-2.972909375" y="-4.96688125"/>
<vertex x="-2.97176875" y="-4.967534375"/>
<vertex x="-2.9704625" y="-4.967753125"/>
<vertex x="-2.960834375" y="-4.97378125"/>
<vertex x="-2.64159375" y="-5.15620625"/>
<vertex x="-2.285971875" y="-5.3340125"/>
<vertex x="-1.9812" y="-5.461003125"/>
<vertex x="-0.7874" y="-2.032"/>
<vertex x="-1.0807625" y="-1.893765625"/>
<vertex x="-1.085696875" y="-1.892753125"/>
<vertex x="-1.092078125" y="-1.888434375"/>
<vertex x="-1.099053125" y="-1.885146875"/>
<vertex x="-1.10244375" y="-1.88141875"/>
<vertex x="-1.360640625" y="-1.7066375"/>
<vertex x="-1.3653625" y="-1.704875"/>
<vertex x="-1.37100625" y="-1.69961875"/>
<vertex x="-1.3773875" y="-1.6953"/>
<vertex x="-1.380159375" y="-1.69109375"/>
<vertex x="-1.60834375" y="-1.478609375"/>
<vertex x="-1.61274375" y="-1.4761375"/>
<vertex x="-1.617515625" y="-1.47006875"/>
<vertex x="-1.62315" y="-1.464821875"/>
<vertex x="-1.625240625" y="-1.46024375"/>
<vertex x="-1.81795" y="-1.215140625"/>
<vertex x="-1.8219125" y="-1.212025"/>
<vertex x="-1.8256875" y="-1.2053"/>
<vertex x="-1.83045" y="-1.19924375"/>
<vertex x="-1.831809375" y="-1.194396875"/>
<vertex x="-1.984446875" y="-0.9225375"/>
<vertex x="-1.98788125" y="-0.918846875"/>
<vertex x="-1.990575" y="-0.91161875"/>
<vertex x="-1.994346875" y="-0.904903125"/>
<vertex x="-1.99494375" y="-0.899903125"/>
<vertex x="-2.103871875" y="-0.6077375"/>
<vertex x="-2.1067" y="-0.603559375"/>
<vertex x="-2.10825" y="-0.596"/>
<vertex x="-2.110940625" y="-0.58878125"/>
<vertex x="-2.110759375" y="-0.58375"/>
<vertex x="-2.1733625" y="-0.27830625"/>
<vertex x="-2.17550625" y="-0.27375625"/>
<vertex x="-2.175871875" y="-0.26606875"/>
<vertex x="-2.177421875" y="-0.258503125"/>
<vertex x="-2.176465625" y="-0.253546875"/>
<vertex x="-2.19125625" y="0.0578875"/>
<vertex x="-2.192675" y="0.062721875"/>
<vertex x="-2.19185" y="0.070384375"/>
<vertex x="-2.192215625" y="0.0780875"/>
<vertex x="-2.190509375" y="0.082828125"/>
<vertex x="-2.15713125" y="0.392821875"/>
<vertex x="-2.1577875" y="0.39780625"/>
<vertex x="-2.155796875" y="0.405234375"/>
<vertex x="-2.15496875" y="0.412915625"/>
<vertex x="-2.152546875" y="0.417346875"/>
<vertex x="-2.07179375" y="0.718525"/>
<vertex x="-2.071671875" y="0.72355625"/>
<vertex x="-2.06855625" y="0.730596875"/>
<vertex x="-2.06655625" y="0.738053125"/>
<vertex x="-2.063484375" y="0.74205625"/>
<vertex x="-1.937303125" y="1.027134375"/>
<vertex x="-1.93640625" y="1.032103125"/>
<vertex x="-1.932228125" y="1.0386"/>
<vertex x="-1.929115625" y="1.045634375"/>
<vertex x="-1.925475" y="1.04910625"/>
<vertex x="-1.756853125" y="1.3113625"/>
<vertex x="-1.755203125" y="1.316121875"/>
<vertex x="-1.7500875" y="1.321884375"/>
<vertex x="-1.745915625" y="1.328371875"/>
<vertex x="-1.741775" y="1.331246875"/>
<vertex x="-1.53474375" y="1.5644125"/>
<vertex x="-1.53238125" y="1.568859375"/>
<vertex x="-1.52644375" y="1.573759375"/>
<vertex x="-1.52131875" y="1.57953125"/>
<vertex x="-1.516778125" y="1.581734375"/>
<vertex x="-1.2763125" y="1.7801875"/>
<vertex x="-1.27329375" y="1.78421875"/>
<vertex x="-1.266665625" y="1.78815"/>
<vertex x="-1.260715625" y="1.793059375"/>
<vertex x="-1.25589375" y="1.7945375"/>
<vertex x="-0.9877125" y="1.953571875"/>
<vertex x="-0.984109375" y="1.9570875"/>
<vertex x="-0.9769625" y="1.959946875"/>
<vertex x="-0.970321875" y="1.963884375"/>
<vertex x="-0.965328125" y="1.9646"/>
<vertex x="-0.67585625" y="2.0804"/>
<vertex x="-0.671753125" y="2.08331875"/>
<vertex x="-0.66424375" y="2.08504375"/>
<vertex x="-0.65708125" y="2.087909375"/>
<vertex x="-0.652040625" y="2.087846875"/>
<vertex x="-0.34816875" y="2.1576625"/>
<vertex x="-0.343665625" y="2.159915625"/>
<vertex x="-0.33598125" y="2.1604625"/>
<vertex x="-0.3284625" y="2.162190625"/>
<vertex x="-0.323490625" y="2.161353125"/>
<vertex x="-0.01250625" y="2.183509375"/>
<vertex x="-0.009640625" y="2.18461875"/>
<vertex x="-0.000009375" y="2.184396875"/>
<vertex x="0.009621875" y="2.185084375"/>
<vertex x="0.01254375" y="2.1841125"/>
<vertex x="0.329975" y="2.176846875"/>
<vertex x="0.334971875" y="2.177925"/>
<vertex x="0.342484375" y="2.176559375"/>
<vertex x="0.35011875" y="2.176384375"/>
<vertex x="0.354796875" y="2.17431875"/>
<vertex x="0.667184375" y="2.11751875"/>
<vertex x="0.672290625" y="2.117803125"/>
<vertex x="0.6795" y="2.115278125"/>
<vertex x="0.6870125" y="2.1139125"/>
<vertex x="0.69130625" y="2.11114375"/>
<vertex x="0.991003125" y="2.006190625"/>
<vertex x="0.996090625" y="2.005675"/>
<vertex x="1.002815625" y="2.00205625"/>
<vertex x="1.010021875" y="1.99953125"/>
<vertex x="1.01383125" y="1.996125"/>
<vertex x="1.293421875" y="1.845615625"/>
<vertex x="1.29836875" y="1.844309375"/>
<vertex x="1.304446875" y="1.83968125"/>
<vertex x="1.31116875" y="1.8360625"/>
<vertex x="1.3144" y="1.8321"/>
<vertex x="1.56701875" y="1.639728125"/>
<vertex x="1.571696875" y="1.637665625"/>
<vertex x="1.576978125" y="1.63214375"/>
<vertex x="1.58305" y="1.62751875"/>
<vertex x="1.585621875" y="1.623103125"/>
<vertex x="1.8050875" y="1.39358125"/>
<vertex x="1.80938125" y="1.390815625"/>
<vertex x="1.813721875" y="1.38455"/>
<vertex x="1.819009375" y="1.379021875"/>
<vertex x="1.8208625" y="1.37425"/>
<vertex x="2.001709375" y="1.11329375"/>
<vertex x="2.00551875" y="1.109890625"/>
<vertex x="2.00883125" y="1.103015625"/>
<vertex x="2.013184375" y="1.096734375"/>
<vertex x="2.014265625" y="1.091734375"/>
<vertex x="2.1521125" y="0.80566875"/>
<vertex x="2.155346875" y="0.801709375"/>
<vertex x="2.157546875" y="0.79439375"/>
<vertex x="2.160859375" y="0.78751875"/>
<vertex x="2.161146875" y="0.78241875"/>
<vertex x="2.252575" y="0.47831875"/>
<vertex x="2.255153125" y="0.473896875"/>
<vertex x="2.25618125" y="0.466321875"/>
<vertex x="2.258378125" y="0.45901875"/>
<vertex x="2.257865625" y="0.4539375"/>
<vertex x="2.30061875" y="0.139303125"/>
<vertex x="2.302475" y="0.13453125"/>
<vertex x="2.30230625" y="0.126884375"/>
<vertex x="2.303334375" y="0.119328125"/>
<vertex x="2.302034375" y="0.114390625"/>
<vertex x="2.295075" y="-0.2030625"/>
<vertex x="2.296159375" y="-0.208053125"/>
<vertex x="2.294803125" y="-0.215559375"/>
<vertex x="2.294634375" y="-0.223203125"/>
<vertex x="2.292571875" y="-0.2278875"/>
<vertex x="2.23606875" y="-0.54035"/>
<vertex x="2.236359375" y="-0.5454625"/>
<vertex x="2.2338375" y="-0.552684375"/>
<vertex x="2.23248125" y="-0.5601875"/>
<vertex x="2.22971875" y="-0.56448125"/>
<vertex x="2.1250625" y="-0.864271875"/>
<vertex x="2.12455" y="-0.869365625"/>
<vertex x="2.12093125" y="-0.876103125"/>
<vertex x="2.11841875" y="-0.8833"/>
<vertex x="2.115021875" y="-0.887109375"/>
<vertex x="1.964771875" y="-1.166859375"/>
<vertex x="1.963471875" y="-1.171809375"/>
<vertex x="1.958846875" y="-1.17789375"/>
<vertex x="1.9552375" y="-1.184615625"/>
<vertex x="1.951284375" y="-1.187846875"/>
<vertex x="1.75915625" y="-1.440659375"/>
<vertex x="1.7571" y="-1.4453375"/>
<vertex x="1.7515875" y="-1.45061875"/>
<vertex x="1.746965625" y="-1.4567"/>
<vertex x="1.74255" y="-1.459275"/>
<vertex x="1.5132625" y="-1.67894375"/>
<vertex x="1.5105" y="-1.683240625"/>
<vertex x="1.50423125" y="-1.68759375"/>
<vertex x="1.498715625" y="-1.692878125"/>
<vertex x="1.49395" y="-1.694734375"/>
<vertex x="1.233128125" y="-1.8758625"/>
<vertex x="1.22973125" y="-1.879675"/>
<vertex x="1.2228625" y="-1.88299375"/>
<vertex x="1.216584375" y="-1.887353125"/>
<vertex x="1.211584375" y="-1.888440625"/>
<vertex x="0.914396875" y="-2.032003125"/>
<vertex x="2.2352" y="-5.3848"/>
<vertex x="2.565403125" y="-5.232396875"/>
<vertex x="2.97179375" y="-5.00380625"/>
<vertex x="3.23046875" y="-4.83135625"/>
<vertex x="3.231015625" y="-4.8305875"/>
<vertex x="3.240975" y="-4.82435"/>
<vertex x="3.2507" y="-4.81786875"/>
<vertex x="3.251621875" y="-4.8176875"/>
<vertex x="3.252428125" y="-4.81718125"/>
<vertex x="3.2640375" y="-4.81523125"/>
<vertex x="3.27548125" y="-4.812965625"/>
<vertex x="3.27640625" y="-4.81315"/>
<vertex x="3.277340625" y="-4.81299375"/>
<vertex x="3.288784375" y="-4.815625"/>
<vertex x="3.30025" y="-4.81791875"/>
<vertex x="3.301034375" y="-4.81844375"/>
<vertex x="3.301959375" y="-4.81865625"/>
<vertex x="3.31154375" y="-4.82548125"/>
<vertex x="3.321240625" y="-4.831975"/>
<vertex x="3.3217625" y="-4.832759375"/>
<vertex x="4.953" y="-5.994403125"/>
<vertex x="5.384803125" y="-5.613396875"/>
<vertex x="5.791203125" y="-5.181596875"/>
<vertex x="6.070590625" y="-4.8514125"/>
<vertex x="6.248396875" y="-4.622803125"/>
<vertex x="5.077796875" y="-2.92915625"/>
<vertex x="5.072153125" y="-2.92295625"/>
<vertex x="5.070678125" y="-2.918859375"/>
<vertex x="5.068203125" y="-2.915278125"/>
<vertex x="5.0664375" y="-2.907078125"/>
<vertex x="5.063596875" y="-2.8991875"/>
<vertex x="5.063803125" y="-2.894840625"/>
<vertex x="5.062884375" y="-2.89058125"/>
<vertex x="5.06439375" y="-2.882328125"/>
<vertex x="5.0647875" y="-2.873953125"/>
<vertex x="5.06664375" y="-2.870009375"/>
<vertex x="5.067425" y="-2.86573125"/>
<vertex x="5.071971875" y="-2.8586875"/>
<vertex x="5.25780625" y="-2.4637875"/>
<vertex x="5.435603125" y="-1.9811875"/>
<vertex x="5.588" y="-1.47320625"/>
<vertex x="5.65403125" y="-1.209071875"/>
<vertex x="5.656421875" y="-1.1978375"/>
<vertex x="5.65708125" y="-1.19688125"/>
<vertex x="5.6573625" y="-1.19575"/>
<vertex x="5.6642125" y="-1.1865"/>
<vertex x="5.67073125" y="-1.17701875"/>
<vertex x="5.67170625" y="-1.176384375"/>
<vertex x="5.6724" y="-1.17545"/>
<vertex x="5.682303125" y="-1.169509375"/>
<vertex x="5.691921875" y="-1.163265625"/>
<vertex x="5.693059375" y="-1.16305625"/>
<vertex x="5.6940625" y="-1.162453125"/>
<vertex x="5.70550625" y="-1.16075"/>
<vertex x="7.7216" y="-0.787403125"/>
<vertex x="7.7216" y="0.0507875"/>
<vertex x="7.696203125" y="0.558784375"/>
<vertex x="7.6454" y="1.0922"/>
<vertex x="5.57788125" y="1.49075625"/>
<vertex x="5.5677375" y="1.491921875"/>
<vertex x="5.565559375" y="1.49313125"/>
<vertex x="5.5631125" y="1.493603125"/>
<vertex x="5.554590625" y="1.499225"/>
<vertex x="5.545653125" y="1.504190625"/>
<vertex x="5.5441" y="1.506146875"/>
<vertex x="5.542025" y="1.507515625"/>
<vertex x="5.53630625" y="1.515965625"/>
<vertex x="5.529946875" y="1.523975"/>
<vertex x="5.5292625" y="1.526371875"/>
<vertex x="5.527865625" y="1.528434375"/>
<vertex x="5.5258125" y="1.53844375"/>
<vertex x="5.384796875" y="2.032"/>
<vertex x="5.1562" y="2.5146"/>
<vertex x="5.003828125" y="2.79395"/>
<vertex x="4.873053125" y="3.011909375"/>
<vertex x="4.866709375" y="3.022040625"/>
<vertex x="4.86660625" y="3.022653125"/>
<vertex x="4.8662875" y="3.023184375"/>
<vertex x="4.8645125" y="3.035103125"/>
<vertex x="4.862521875" y="3.046953125"/>
<vertex x="4.862659375" y="3.04755625"/>
<vertex x="4.86256875" y="3.048171875"/>
<vertex x="4.865478125" y="3.0598125"/>
<vertex x="4.868184375" y="3.071571875"/>
<vertex x="4.868546875" y="3.07208125"/>
<vertex x="4.868696875" y="3.072678125"/>
<vertex x="4.875815625" y="3.082290625"/>
<vertex x="6.0452" y="4.724396875"/>
<vertex x="5.74874375" y="5.070265625"/>
<vertex x="5.748559375" y="5.070359375"/>
<vertex x="5.74065" y="5.079709375"/>
<vertex x="5.732440625" y="5.089284375"/>
<vertex x="5.732375" y="5.0894875"/>
<vertex x="5.461" y="5.410196875"/>
<vertex x="5.089521875" y="5.73215"/>
<vertex x="5.089246875" y="5.73224375"/>
<vertex x="5.08001875" y="5.7403875"/>
<vertex x="5.070653125" y="5.748503125"/>
<vertex x="5.070521875" y="5.748765625"/>
<vertex x="4.6482" y="6.1214"/>
<vertex x="2.904653125" y="4.925825"/>
<vertex x="2.895834375" y="4.919121875"/>
<vertex x="2.894278125" y="4.9187125"/>
<vertex x="2.892953125" y="4.917803125"/>
<vertex x="2.882125" y="4.9155125"/>
<vertex x="2.87140625" y="4.912690625"/>
<vertex x="2.8698125" y="4.912909375"/>
<vertex x="2.8682375" y="4.912575"/>
<vertex x="2.857353125" y="4.914603125"/>
<vertex x="2.846375" y="4.9161"/>
<vertex x="2.844984375" y="4.9169125"/>
<vertex x="2.843403125" y="4.91720625"/>
<vertex x="2.834121875" y="4.923246875"/>
<vertex x="2.565396875" y="5.080003125"/>
<vertex x="2.1336125" y="5.2578"/>
<vertex x="1.727178125" y="5.410209375"/>
<vertex x="1.3853625" y="5.52414375"/>
<vertex x="1.37585" y="5.5263875"/>
<vertex x="1.373453125" y="5.528115625"/>
<vertex x="1.37065625" y="5.529046875"/>
<vertex x="1.363271875" y="5.53545"/>
<vertex x="1.355353125" y="5.54115625"/>
<vertex x="1.353803125" y="5.5436625"/>
<vertex x="1.351571875" y="5.545596875"/>
<vertex x="1.34720625" y="5.554328125"/>
<vertex x="1.342065625" y="5.562640625"/>
<vertex x="1.341590625" y="5.5655625"/>
<vertex x="1.340275" y="5.56819375"/>
<vertex x="1.339584375" y="5.577909375"/>
<vertex x="1.016" y="7.5692"/>
<vertex x="0.533409375" y="7.62"/>
<vertex x="-0.507990625" y="7.62"/>
<vertex x="-0.9398" y="7.5692"/>
<vertex x="-1.03926875" y="6.997275"/>
<vertex x="-1.039165625" y="6.9967875"/>
<vertex x="-1.0414125" y="6.98493125"/>
<vertex x="-1.043509375" y="6.972884375"/>
<vertex x="-1.04378125" y="6.97245625"/>
<vertex x="-1.31313125" y="5.5522375"/>
<vertex x="-1.31395" y="5.542834375"/>
<vertex x="-1.31546875" y="5.5399125"/>
<vertex x="-1.31608125" y="5.536684375"/>
<vertex x="-1.32124375" y="5.52880625"/>
<vertex x="-1.325603125" y="5.520421875"/>
<vertex x="-1.328125" y="5.518303125"/>
<vertex x="-1.329928125" y="5.515553125"/>
<vertex x="-1.337728125" y="5.510240625"/>
<vertex x="-1.34495" y="5.504175"/>
<vertex x="-1.348084375" y="5.503184375"/>
<vertex x="-1.35080625" y="5.50133125"/>
<vertex x="-1.360040625" y="5.499409375"/>
<vertex x="-1.803415625" y="5.35939375"/>
<vertex x="-2.209784375" y="5.20700625"/>
<vertex x="-2.8841125" y="4.869846875"/>
<vertex x="-2.8923375" y="4.86473125"/>
<vertex x="-2.895334375" y="4.8642375"/>
<vertex x="-2.89805" y="4.862878125"/>
<vertex x="-2.907703125" y="4.862190625"/>
<vertex x="-2.9172625" y="4.8606125"/>
<vertex x="-2.920221875" y="4.861303125"/>
<vertex x="-2.923246875" y="4.8610875"/>
<vertex x="-2.93243125" y="4.864146875"/>
<vertex x="-2.9418625" y="4.866346875"/>
<vertex x="-2.944328125" y="4.8681125"/>
<vertex x="-2.9472125" y="4.869075"/>
<vertex x="-2.9545375" y="4.875428125"/>
<vertex x="-4.6228" y="6.0706"/>
<vertex x="-5.0037875" y="5.740409375"/>
<vertex x="-5.334003125" y="5.43559375"/>
<vertex x="-5.67279375" y="5.07074375"/>
<vertex x="-5.9944" y="4.724403125"/>
<vertex x="-4.832015625" y="3.042653125"/>
<vertex x="-4.8316125" y="3.042384375"/>
<vertex x="-4.824865625" y="3.032309375"/>
<vertex x="-4.817928125" y="3.022271875"/>
<vertex x="-4.817825" y="3.021796875"/>
<vertex x="-4.81755625" y="3.02139375"/>
<vertex x="-4.81518125" y="3.009515625"/>
<vertex x="-4.812609375" y="2.997575"/>
<vertex x="-4.812696875" y="2.99709375"/>
<vertex x="-4.812603125" y="2.996625"/>
<vertex x="-4.814953125" y="2.98474375"/>
<vertex x="-4.81715" y="2.972725"/>
<vertex x="-4.8174125" y="2.97231875"/>
<vertex x="-4.81750625" y="2.97184375"/>
<vertex x="-4.824190625" y="2.961815625"/>
<vertex x="-4.83085" y="2.9515"/>
<vertex x="-4.83125625" y="2.95121875"/>
<vertex x="-5.0546" y="2.6162"/>
<vertex x="-5.25780625" y="2.184384375"/>
<vertex x="-5.410190625" y="1.778021875"/>
<vertex x="-5.551075" y="1.284928125"/>
<vertex x="-5.553884375" y="1.273721875"/>
<vertex x="-5.554521875" y="1.2728625"/>
<vertex x="-5.55481875" y="1.271825"/>
<vertex x="-5.56203125" y="1.2627375"/>
<vertex x="-5.568934375" y="1.253434375"/>
<vertex x="-5.56985625" y="1.25288125"/>
<vertex x="-5.570525" y="1.252040625"/>
<vertex x="-5.580675" y="1.246403125"/>
<vertex x="-5.59060625" y="1.240453125"/>
<vertex x="-5.591665625" y="1.240296875"/>
<vertex x="-5.592609375" y="1.239771875"/>
<vertex x="-5.604184375" y="1.238440625"/>
<vertex x="-7.620003125" y="0.9398"/>
<vertex x="-7.6454" y="0.60960625"/>
<vertex x="-7.6454" y="-0.533409375"/>
<vertex x="-7.619996875" y="-0.9398"/>
<vertex x="-5.6043" y="-1.313078125"/>
<vertex x="-5.592040625" y="-1.315234375"/>
<vertex x="-5.5917875" y="-1.315396875"/>
<vertex x="-5.59149375" y="-1.31545"/>
<vertex x="-5.581034375" y="-1.322240625"/>
<vertex x="-5.570728125" y="-1.328796875"/>
<vertex x="-5.570559375" y="-1.3290375"/>
<vertex x="-5.570303125" y="-1.329203125"/>
<vertex x="-5.563259375" y="-1.339453125"/>
<vertex x="-5.556228125" y="-1.34948125"/>
<vertex x="-5.5561625" y="-1.349775"/>
<vertex x="-5.55599375" y="-1.350021875"/>
<vertex x="-5.553434375" y="-1.362046875"/>
<vertex x="-5.461" y="-1.77800625"/>
<vertex x="-5.308584375" y="-2.159034375"/>
<vertex x="-5.1054125" y="-2.616175"/>
<vertex x="-4.845175" y="-3.112990625"/>
<vertex x="-4.839834375" y="-3.1213875"/>
<vertex x="-4.839359375" y="-3.1240875"/>
<vertex x="-4.838084375" y="-3.126525"/>
<vertex x="-4.837190625" y="-3.136459375"/>
<vertex x="-4.83546875" y="-3.14626875"/>
<vertex x="-4.836065625" y="-3.148946875"/>
<vertex x="-4.83581875" y="-3.151684375"/>
<vertex x="-4.83879375" y="-3.16120625"/>
<vertex x="-4.840959375" y="-3.170925"/>
<vertex x="-4.842534375" y="-3.173171875"/>
<vertex x="-4.84335625" y="-3.175796875"/>
<vertex x="-4.849753125" y="-3.183459375"/>
<vertex x="-6.0198" y="-4.8514"/>
<vertex x="-5.715015625" y="-5.20698125"/>
<vertex x="-5.410190625" y="-5.53720625"/>
</polygon>
</package>
<package name="OSHW_5MM">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="-1.498575" y="-1.752621875"/>
<vertex x="-1.4097" y="-1.8288"/>
<vertex x="-0.906828125" y="-1.485371875"/>
<vertex x="-0.904903125" y="-1.481684375"/>
<vertex x="-0.898465625" y="-1.4796625"/>
<vertex x="-0.8928875" y="-1.475853125"/>
<vertex x="-0.88879375" y="-1.476625"/>
<vertex x="-0.884828125" y="-1.475378125"/>
<vertex x="-0.878846875" y="-1.4785"/>
<vertex x="-0.872209375" y="-1.47975"/>
<vertex x="-0.8698625" y="-1.4831875"/>
<vertex x="-0.5969" y="-1.625596875"/>
<vertex x="-0.2413" y="-0.609596875"/>
<vertex x="-0.358453125" y="-0.55098125"/>
<vertex x="-0.361521875" y="-0.55130625"/>
<vertex x="-0.36756875" y="-0.54641875"/>
<vertex x="-0.374515625" y="-0.54294375"/>
<vertex x="-0.375490625" y="-0.54001875"/>
<vertex x="-0.4694625" y="-0.464075"/>
<vertex x="-0.472528125" y="-0.4637375"/>
<vertex x="-0.477384375" y="-0.457675"/>
<vertex x="-0.483428125" y="-0.452790625"/>
<vertex x="-0.483753125" y="-0.449721875"/>
<vertex x="-0.559275" y="-0.355434375"/>
<vertex x="-0.5622" y="-0.354446875"/>
<vertex x="-0.56565" y="-0.347475"/>
<vertex x="-0.570503125" y="-0.341415625"/>
<vertex x="-0.570165625" y="-0.338353125"/>
<vertex x="-0.623753125" y="-0.2300625"/>
<vertex x="-0.62639375" y="-0.228475"/>
<vertex x="-0.62826875" y="-0.2209375"/>
<vertex x="-0.631715625" y="-0.213971875"/>
<vertex x="-0.630728125" y="-0.21105"/>
<vertex x="-0.65989375" y="-0.09380625"/>
<vertex x="-0.6621375" y="-0.091684375"/>
<vertex x="-0.66235625" y="-0.08390625"/>
<vertex x="-0.664228125" y="-0.076378125"/>
<vertex x="-0.662640625" y="-0.0737375"/>
<vertex x="-0.666021875" y="0.047034375"/>
<vertex x="-0.667759375" y="0.0495875"/>
<vertex x="-0.66630625" y="0.057228125"/>
<vertex x="-0.666525" y="0.0649875"/>
<vertex x="-0.664409375" y="0.067228125"/>
<vertex x="-0.641859375" y="0.185915625"/>
<vertex x="-0.64300625" y="0.188775"/>
<vertex x="-0.639959375" y="0.1959125"/>
<vertex x="-0.63850625" y="0.20355625"/>
<vertex x="-0.635953125" y="0.20529375"/>
<vertex x="-0.5885125" y="0.316409375"/>
<vertex x="-0.589021875" y="0.31945625"/>
<vertex x="-0.58450625" y="0.3257875"/>
<vertex x="-0.581459375" y="0.332928125"/>
<vertex x="-0.5786" y="0.334078125"/>
<vertex x="-0.508475" y="0.43244375"/>
<vertex x="-0.508321875" y="0.435521875"/>
<vertex x="-0.5025625" y="0.440734375"/>
<vertex x="-0.49805" y="0.447065625"/>
<vertex x="-0.49500625" y="0.447575"/>
<vertex x="-0.405428125" y="0.528665625"/>
<vertex x="-0.40461875" y="0.531640625"/>
<vertex x="-0.397875" y="0.5355"/>
<vertex x="-0.392115625" y="0.540715625"/>
<vertex x="-0.389034375" y="0.5405625"/>
<vertex x="-0.284190625" y="0.600590625"/>
<vertex x="-0.2827625" y="0.603325"/>
<vertex x="-0.27535625" y="0.60565"/>
<vertex x="-0.268609375" y="0.6095125"/>
<vertex x="-0.26563125" y="0.608703125"/>
<vertex x="-0.150359375" y="0.644884375"/>
<vertex x="-0.148378125" y="0.64725"/>
<vertex x="-0.1406375" y="0.6479375"/>
<vertex x="-0.133225" y="0.6502625"/>
<vertex x="-0.130490625" y="0.648834375"/>
<vertex x="-0.010153125" y="0.6595"/>
<vertex x="-0.007775" y="0.66135625"/>
<vertex x="0.0000125" y="0.6604"/>
<vertex x="0.007809375" y="0.661090625"/>
<vertex x="0.010115625" y="0.659159375"/>
<vertex x="0.125928125" y="0.6449375"/>
<vertex x="0.128565625" y="0.6462375"/>
<vertex x="0.13605" y="0.64369375"/>
<vertex x="0.14389375" y="0.64273125"/>
<vertex x="0.145703125" y="0.640415625"/>
<vertex x="0.256196875" y="0.602875"/>
<vertex x="0.25904375" y="0.603609375"/>
<vertex x="0.265846875" y="0.599596875"/>
<vertex x="0.273334375" y="0.597053125"/>
<vertex x="0.274634375" y="0.594415625"/>
<vertex x="0.375140625" y="0.53514375"/>
<vertex x="0.378078125" y="0.53528125"/>
<vertex x="0.383928125" y="0.529959375"/>
<vertex x="0.39073125" y="0.525946875"/>
<vertex x="0.391465625" y="0.5231"/>
<vertex x="0.477775" y="0.44456875"/>
<vertex x="0.48068125" y="0.44410625"/>
<vertex x="0.485321875" y="0.437703125"/>
<vertex x="0.491165625" y="0.4323875"/>
<vertex x="0.491303125" y="0.429453125"/>
<vertex x="0.559790625" y="0.334978125"/>
<vertex x="0.562540625" y="0.33393125"/>
<vertex x="0.565778125" y="0.32671875"/>
<vertex x="0.570415625" y="0.320321875"/>
<vertex x="0.569953125" y="0.31741875"/>
<vertex x="0.61773125" y="0.21096875"/>
<vertex x="0.620209375" y="0.209384375"/>
<vertex x="0.62190625" y="0.20166875"/>
<vertex x="0.62514375" y="0.19445625"/>
<vertex x="0.624096875" y="0.19170625"/>
<vertex x="0.64916875" y="0.07774375"/>
<vertex x="0.651271875" y="0.0756875"/>
<vertex x="0.651359375" y="0.067784375"/>
<vertex x="0.653059375" y="0.0600625"/>
<vertex x="0.651475" y="0.057584375"/>
<vertex x="0.65278125" y="-0.05908125"/>
<vertex x="0.654421875" y="-0.061525"/>
<vertex x="0.652896875" y="-0.0692875"/>
<vertex x="0.652984375" y="-0.077184375"/>
<vertex x="0.65093125" y="-0.079284375"/>
<vertex x="0.62841875" y="-0.193784375"/>
<vertex x="0.629525" y="-0.19650625"/>
<vertex x="0.626453125" y="-0.20378125"/>
<vertex x="0.624928125" y="-0.211540625"/>
<vertex x="0.622484375" y="-0.21318125"/>
<vertex x="0.577096875" y="-0.320678125"/>
<vertex x="0.577625" y="-0.32356875"/>
<vertex x="0.57313125" y="-0.33006875"/>
<vertex x="0.57005625" y="-0.337353125"/>
<vertex x="0.56733125" y="-0.338459375"/>
<vertex x="0.500978125" y="-0.434453125"/>
<vertex x="0.50090625" y="-0.437390625"/>
<vertex x="0.49518125" y="-0.4428375"/>
<vertex x="0.4906875" y="-0.449340625"/>
<vertex x="0.48779375" y="-0.44986875"/>
<vertex x="0.403278125" y="-0.5303"/>
<vertex x="0.40260625" y="-0.533165625"/>
<vertex x="0.395884375" y="-0.537334375"/>
<vertex x="0.3901625" y="-0.54278125"/>
<vertex x="0.387225" y="-0.542709375"/>
<vertex x="0.2794" y="-0.6096"/>
<vertex x="0.6731" y="-1.6002"/>
<vertex x="0.82550625" y="-1.536696875"/>
<vertex x="0.95948125" y="-1.44738125"/>
<vertex x="0.962775" y="-1.442675"/>
<vertex x="0.96789375" y="-1.441771875"/>
<vertex x="0.972215625" y="-1.438890625"/>
<vertex x="0.97784375" y="-1.440015625"/>
<vertex x="0.983496875" y="-1.43901875"/>
<vertex x="0.98775" y="-1.441996875"/>
<vertex x="0.992846875" y="-1.443015625"/>
<vertex x="0.99603125" y="-1.44779375"/>
<vertex x="1.4859" y="-1.7907"/>
<vertex x="1.5875" y="-1.7018"/>
<vertex x="1.676390625" y="-1.612909375"/>
<vertex x="1.77798125" y="-1.498625"/>
<vertex x="1.8669" y="-1.3843"/>
<vertex x="1.510990625" y="-0.868840625"/>
<vertex x="1.5067875" y="-0.866390625"/>
<vertex x="1.50524375" y="-0.86051875"/>
<vertex x="1.501796875" y="-0.855528125"/>
<vertex x="1.502671875" y="-0.850746875"/>
<vertex x="1.501434375" y="-0.84604375"/>
<vertex x="1.504490625" y="-0.840803125"/>
<vertex x="1.505584375" y="-0.834828125"/>
<vertex x="1.509590625" y="-0.8320625"/>
<vertex x="1.587496875" y="-0.69850625"/>
<vertex x="1.638296875" y="-0.546115625"/>
<vertex x="1.6838" y="-0.3641"/>
<vertex x="1.68289375" y="-0.35916875"/>
<vertex x="1.686246875" y="-0.354303125"/>
<vertex x="1.687684375" y="-0.348559375"/>
<vertex x="1.691990625" y="-0.345975"/>
<vertex x="1.6948375" y="-0.341846875"/>
<vertex x="1.700653125" y="-0.340778125"/>
<vertex x="1.705728125" y="-0.337734375"/>
<vertex x="1.7106" y="-0.338953125"/>
<vertex x="2.3114" y="-0.2286"/>
<vertex x="2.3114" y="0.06349375"/>
<vertex x="2.298721875" y="0.20295"/>
<vertex x="2.286" y="0.3302"/>
<vertex x="1.684975" y="0.440590625"/>
<vertex x="1.6808625" y="0.439325"/>
<vertex x="1.675015625" y="0.442421875"/>
<vertex x="1.668509375" y="0.443615625"/>
<vertex x="1.666065625" y="0.447159375"/>
<vertex x="1.662265625" y="0.449171875"/>
<vertex x="1.660321875" y="0.455490625"/>
<vertex x="1.656565625" y="0.4609375"/>
<vertex x="1.65734375" y="0.46516875"/>
<vertex x="1.61289375" y="0.60961875"/>
<vertex x="1.549396875" y="0.74930625"/>
<vertex x="1.46574375" y="0.892715625"/>
<vertex x="1.464840625" y="0.893015625"/>
<vertex x="1.460490625" y="0.901721875"/>
<vertex x="1.45659375" y="0.908396875"/>
<vertex x="1.45401875" y="0.910246875"/>
<vertex x="1.4529375" y="0.916825"/>
<vertex x="1.449953125" y="0.922796875"/>
<vertex x="1.451296875" y="0.926825"/>
<vertex x="1.450609375" y="0.931009375"/>
<vertex x="1.454496875" y="0.936425"/>
<vertex x="1.456609375" y="0.942759375"/>
<vertex x="1.460409375" y="0.944659375"/>
<vertex x="1.8034" y="1.4224"/>
<vertex x="1.7018" y="1.5494"/>
<vertex x="1.574803125" y="1.676396875"/>
<vertex x="1.47320625" y="1.76529375"/>
<vertex x="1.3843" y="1.8288"/>
<vertex x="0.8813625" y="1.4730625"/>
<vertex x="0.879065625" y="1.468928125"/>
<vertex x="0.873109375" y="1.467225"/>
<vertex x="0.86805" y="1.463646875"/>
<vertex x="0.863384375" y="1.464446875"/>
<vertex x="0.858834375" y="1.463146875"/>
<vertex x="0.85341875" y="1.46615625"/>
<vertex x="0.847309375" y="1.467203125"/>
<vertex x="0.844575" y="1.47106875"/>
<vertex x="0.74930625" y="1.523996875"/>
<vertex x="0.644615625" y="1.570525"/>
<vertex x="0.6446125" y="1.570525"/>
<vertex x="0.520703125" y="1.6256"/>
<vertex x="0.427859375" y="1.64623125"/>
<vertex x="0.42258125" y="1.6452625"/>
<vertex x="0.417996875" y="1.648421875"/>
<vertex x="0.412559375" y="1.64963125"/>
<vertex x="0.409678125" y="1.654159375"/>
<vertex x="0.405259375" y="1.65720625"/>
<vertex x="0.404253125" y="1.662684375"/>
<vertex x="0.4012625" y="1.667384375"/>
<vertex x="0.402428125" y="1.672625"/>
<vertex x="0.2921" y="2.273303125"/>
<vertex x="0.1397125" y="2.286"/>
<vertex x="-0.1523875" y="2.286"/>
<vertex x="-0.2794" y="2.2733"/>
<vertex x="-0.377446875" y="1.685009375"/>
<vertex x="-0.3761125" y="1.6810125"/>
<vertex x="-0.3791125" y="1.675015625"/>
<vertex x="-0.3802125" y="1.668409375"/>
<vertex x="-0.383640625" y="1.665959375"/>
<vertex x="-0.385525" y="1.66219375"/>
<vertex x="-0.391878125" y="1.660075"/>
<vertex x="-0.397334375" y="1.656178125"/>
<vertex x="-0.401490625" y="1.656871875"/>
<vertex x="-0.57151875" y="1.600190625"/>
<vertex x="-0.698496875" y="1.5494"/>
<vertex x="-0.857309375" y="1.45865625"/>
<vertex x="-0.860278125" y="1.4545625"/>
<vertex x="-0.866096875" y="1.453634375"/>
<vertex x="-0.8712" y="1.45071875"/>
<vertex x="-0.876065625" y="1.452046875"/>
<vertex x="-0.88105625" y="1.45125"/>
<vertex x="-0.885821875" y="1.45470625"/>
<vertex x="-0.8915" y="1.456253125"/>
<vertex x="-0.894003125" y="1.4606375"/>
<vertex x="-1.384296875" y="1.8161"/>
<vertex x="-1.498603125" y="1.727196875"/>
<vertex x="-1.625590625" y="1.60020625"/>
<vertex x="-1.720309375" y="1.493653125"/>
<vertex x="-1.720309375" y="1.4927875"/>
<vertex x="-1.727234375" y="1.4858625"/>
<vertex x="-1.733715625" y="1.478571875"/>
<vertex x="-1.734575" y="1.478521875"/>
<vertex x="-1.7907" y="1.4224"/>
<vertex x="-1.447415625" y="0.919734375"/>
<vertex x="-1.4431625" y="0.917184375"/>
<vertex x="-1.4417125" y="0.9113875"/>
<vertex x="-1.43834375" y="0.906453125"/>
<vertex x="-1.4392625" y="0.90158125"/>
<vertex x="-1.438059375" y="0.89676875"/>
<vertex x="-1.441134375" y="0.89164375"/>
<vertex x="-1.442240625" y="0.885775"/>
<vertex x="-1.44633125" y="0.88298125"/>
<vertex x="-1.51130625" y="0.774690625"/>
<vertex x="-1.587503125" y="0.609596875"/>
<vertex x="-1.622896875" y="0.479809375"/>
<vertex x="-1.622153125" y="0.477946875"/>
<vertex x="-1.6256" y="0.469903125"/>
<vertex x="-1.62790625" y="0.461446875"/>
<vertex x="-1.62965" y="0.46045"/>
<vertex x="-1.65529375" y="0.4006125"/>
<vertex x="-1.654790625" y="0.39753125"/>
<vertex x="-1.6593" y="0.391259375"/>
<vertex x="-1.66235" y="0.384146875"/>
<vertex x="-1.665253125" y="0.382984375"/>
<vertex x="-1.667078125" y="0.38045"/>
<vertex x="-1.674703125" y="0.37920625"/>
<vertex x="-1.6818875" y="0.37633125"/>
<vertex x="-1.6847625" y="0.3775625"/>
<vertex x="-2.286" y="0.279403125"/>
<vertex x="-2.286" y="-0.279403125"/>
<vertex x="-1.685209375" y="-0.377490625"/>
<vertex x="-1.68055625" y="-0.376159375"/>
<vertex x="-1.675228125" y="-0.37911875"/>
<vertex x="-1.66921875" y="-0.3801"/>
<vertex x="-1.66639375" y="-0.384028125"/>
<vertex x="-1.6621625" y="-0.386378125"/>
<vertex x="-1.6604875" y="-0.392234375"/>
<vertex x="-1.65693125" y="-0.39718125"/>
<vertex x="-1.6577125" y="-0.401959375"/>
<vertex x="-1.612915625" y="-0.558753125"/>
<vertex x="-1.574796875" y="-0.685809375"/>
<vertex x="-1.5239875" y="-0.787425"/>
<vertex x="-1.44614375" y="-0.92086875"/>
<vertex x="-1.44195625" y="-0.92390625"/>
<vertex x="-1.441046875" y="-0.92960625"/>
<vertex x="-1.43814375" y="-0.934584375"/>
<vertex x="-1.43945625" y="-0.939578125"/>
<vertex x="-1.43864375" y="-0.944684375"/>
<vertex x="-1.44203125" y="-0.94935625"/>
<vertex x="-1.443496875" y="-0.95493125"/>
<vertex x="-1.447959375" y="-0.957534375"/>
<vertex x="-1.8034" y="-1.4478"/>
<vertex x="-1.701790625" y="-1.56210625"/>
<vertex x="-1.61290625" y="-1.650990625"/>
</polygon>
</package>
<package name="SOLDERJUMPER_CLOSEDWIRE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="WIRE" x="0" y="0" dx="0.635" dy="0.2032" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.254" layer="29" pour="solid">
<vertex x="-1.27" y="-0.762"/>
<vertex x="1.27" y="-0.762"/>
<vertex x="1.27" y="0.762"/>
<vertex x="-1.27" y="0.762"/>
</polygon>
</package>
<package name="GY-87" library_version="314">
<pad name="SDA" x="-5.08" y="-2.54" drill="0.9" diameter="1.778" shape="octagon"/>
<pad name="SCL" x="-5.08" y="0" drill="0.9" diameter="1.778" shape="octagon"/>
<pad name="GND" x="-5.08" y="2.54" drill="0.9" diameter="1.778" shape="octagon"/>
<pad name="VCC" x="-5.08" y="5.08" drill="0.9" diameter="1.778" shape="octagon"/>
<rectangle x1="-6.8" y1="-12" x2="10.2" y2="9.4" layer="25"/>
</package>
<package name="HEADER-2X4" urn="urn:adsk.eagle:footprint:22351/1" locally_modified="yes" library_version="8">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
<package name="DIODE_SS34(SMA)" urn="urn:adsk.eagle:footprint:16378179/1" locally_modified="yes" library_version="273">
<description>Molded Body, 5.20 X 2.60 X 2.90 mm body
&lt;p&gt;Molded Body package with body size 5.20 X 2.60 X 2.90 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.475" x2="-3.6179" y2="1.475" width="0.12" layer="21"/>
<wire x1="-3.6179" y1="1.475" x2="-3.6179" y2="-1.475" width="0.12" layer="21"/>
<wire x1="-3.6179" y1="-1.475" x2="2.8" y2="-1.475" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.475" x2="-2.8" y2="-1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.475" x2="-2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.475" x2="2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.475" x2="2.8" y2="-1.475" width="0.12" layer="51"/>
<smd name="1" x="-2.1079" y="0" dx="2.392" dy="1.5653" layer="1"/>
<smd name="2" x="2.1079" y="0" dx="2.392" dy="1.5653" layer="1"/>
<text x="0" y="2.11" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.11" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-3.6" y1="0.8" x2="-1.6" y2="1.5" layer="21"/>
<rectangle x1="-3.6" y1="-0.8" x2="-3.3" y2="0.8" layer="21"/>
<rectangle x1="-3.6" y1="-1.5" x2="-1.6" y2="-0.8" layer="21"/>
</package>
<package name="BUZZER-12MM-NS" urn="urn:adsk.eagle:footprint:38537/1" locally_modified="yes" library_version="1">
<description>&lt;h3&gt;12mm Buzzer - PTH (No silkscreent variant)&lt;/h3&gt;
&lt;p&gt;This is a small 12mm round speaker that operates around the audible 2kHz range. You can use these speakers to create simple music or user interfaces.&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/General/cem-1203-42-.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;Devices Using&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;BUZZER&lt;/li&gt;&lt;/ul&gt;</description>
<pad name="-" x="-3.8" y="0" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="+" x="3.8" y="0" drill="0.9" diameter="1.5" shape="octagon"/>
<text x="0" y="6.096" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="5.588" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<circle x="0" y="0" radius="2.594221875" width="7" layer="25"/>
</package>
<package name="JSTPH2_BATT">
<description>2-Pin JST PH Series Right-Angle Connector (+/- for batteries)</description>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.2032" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="3" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2" x2="-3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-2" x2="-3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-4.5" x2="3.2" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-4.5" x2="3.2" y2="-2" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="3" x2="2.25" y2="3" width="0.2032" layer="21"/>
<wire x1="4" y1="-0.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-4.5" x2="3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-2" x2="1.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2" x2="-3.15" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-2" x2="-3.15" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-4.5" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-4" y2="-0.5" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-2.2225" y="1.9685" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.2225" y="1.27" size="0.4064" layer="27" ratio="10">&gt;Value</text>
</package>
<package name="SOLDERJUMPER_ARROW_REFLOW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<smd name="2" x="1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.651" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.1524" layer="1" pour="solid">
<vertex x="-1.0922" y="-0.6858"/>
<vertex x="-0.592540625" y="-0.6858"/>
<vertex x="0.093259375" y="0"/>
<vertex x="-0.592540625" y="0.6858"/>
<vertex x="-1.0922" y="0.6858"/>
</polygon>
<polygon width="0.1524" layer="1" pour="solid">
<vertex x="-0.0073375" y="-0.6858"/>
<vertex x="1.0922" y="-0.6858"/>
<vertex x="1.0922" y="0.6858"/>
<vertex x="-0.0073375" y="0.6858"/>
<vertex x="0.602796875" y="0.053875"/>
<vertex x="0.612725" y="0.044290625"/>
<vertex x="0.613275" y="0.043025"/>
<vertex x="0.614240625" y="0.042025"/>
<vertex x="0.6193125" y="0.02914375"/>
<vertex x="0.624815625" y="0.016490625"/>
<vertex x="0.624840625" y="0.01510625"/>
<vertex x="0.625346875" y="0.01381875"/>
<vertex x="0.625103125" y="0"/>
<vertex x="0.625346875" y="-0.01381875"/>
<vertex x="0.624840625" y="-0.01510625"/>
<vertex x="0.624815625" y="-0.016490625"/>
<vertex x="0.6193125" y="-0.02914375"/>
<vertex x="0.614240625" y="-0.042025"/>
<vertex x="0.613275" y="-0.043025"/>
<vertex x="0.612725" y="-0.044290625"/>
<vertex x="0.602796875" y="-0.053875"/>
</polygon>
<rectangle x1="-1.4605" y1="-0.8255" x2="1.4605" y2="0.8255" layer="29"/>
<smd name="WIRE" x="0.31" y="0" dx="0.635" dy="0.2032" layer="1" cream="no"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" locally_modified="yes" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="-0.6" y1="-0.9" x2="0.6" y2="0.9" layer="21"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" locally_modified="yes" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="-0.6" y1="-0.9" x2="0.6" y2="0.9" layer="21"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="FPC-10" urn="urn:adsk.eagle:footprint:15743557/1" locally_modified="yes" library_version="3">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt;1.00 mm SMT ZIF Horizontal Bottom Contact, 10 Pins</description>
<smd name="5" x="-0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="Z2" x="7.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="Z1" x="-7.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="2" x="-3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<wire x1="-7.5" y1="2.7" x2="7.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.7" x2="7.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.1" x2="7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="-1.6" x2="-7.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="-7.5" y1="2.1" x2="-7.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="-8.5" y1="-1.6" x2="-7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.73" y1="-1.6" x2="-7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="-1.6" x2="7.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.5" y1="-1.6" x2="7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.73" y1="-1.6" x2="8.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="8.5" y1="-1.6" x2="8.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="8.5" y1="-2.7" x2="-8.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="-8.5" y1="-2.7" x2="-8.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-7.5" y1="2.1" x2="-7.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="-7.73" y1="2.1" x2="-7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="7.5" y1="2.1" x2="7.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="7.73" y1="2.1" x2="7.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-5.1" y1="2.8" x2="-7.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="-7.6" y1="2.8" x2="-7.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="5.1" y1="2.8" x2="7.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="7.6" y1="2.8" x2="7.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="-7.83" y1="-0.7" x2="-7.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-7.83" y1="-1.5" x2="-8.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-8.6" y1="-1.5" x2="-8.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-8.6" y1="-2.8" x2="8.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="8.6" y1="-2.8" x2="8.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="8.6" y1="-1.5" x2="7.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="7.83" y1="-1.5" x2="7.83" y2="-0.7" width="0.2" layer="21"/>
<text x="-8.6616" y="1.1185" size="0.8128" layer="25" align="bottom-right">&gt;NAME</text>
<text x="-8.6616" y="-0.6534" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<circle x="-5.3" y="2.23" radius="0.1" width="0.2" layer="21"/>
<polygon width="0.1" layer="39" pour="solid">
<vertex x="-8.03" y="-0.599290625"/>
<vertex x="-8.03" y="-1.270709375"/>
<vertex x="-8.059290625" y="-1.3"/>
<vertex x="-8.8" y="-1.3"/>
<vertex x="-8.8" y="-3"/>
<vertex x="8.8" y="-3"/>
<vertex x="8.8" y="-1.3"/>
<vertex x="8.029290625" y="-1.3"/>
<vertex x="8" y="-1.270709375"/>
<vertex x="8" y="-0.599290625"/>
<vertex x="8.029290625" y="-0.57"/>
<vertex x="8.2" y="-0.57"/>
<vertex x="8.2" y="2.43"/>
<vertex x="7.829290625" y="2.43"/>
<vertex x="7.8" y="2.459290625"/>
<vertex x="7.8" y="3"/>
<vertex x="5.029290625" y="3"/>
<vertex x="5" y="3.029290625"/>
<vertex x="5" y="4.63"/>
<vertex x="-5" y="4.63"/>
<vertex x="-5" y="3.029290625"/>
<vertex x="-5.029290625" y="3"/>
<vertex x="-7.8" y="3"/>
<vertex x="-7.8" y="2.459290625"/>
<vertex x="-7.829290625" y="2.43"/>
<vertex x="-8.2" y="2.43"/>
<vertex x="-8.2" y="-0.57"/>
<vertex x="-8.059290625" y="-0.57"/>
</polygon>
</package>
<package name="FPC-16" urn="urn:adsk.eagle:footprint:15743560/1" locally_modified="yes" library_version="3">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt; 1.00 mm SMT ZIF Horizontal Bottom Contact,  16 Pins</description>
<smd name="5" x="-3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="-2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-5.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="Z2" x="10.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="Z1" x="-10.1" y="0.93" dx="1.8" dy="2.6" layer="1"/>
<smd name="2" x="-6.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-7.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="0.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="1.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="2.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="3.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="4.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="5.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="6.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="7.5" y="3.33" dx="0.6" dy="2.2" layer="1"/>
<wire x1="-10.5" y1="2.7" x2="10.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.7" x2="10.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.1" x2="10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="-1.6" x2="-10.5" y2="2.1" width="0.1" layer="51"/>
<wire x1="-10.5" y1="2.1" x2="-10.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="-11.5" y1="-1.6" x2="-10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.73" y1="-1.6" x2="-10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="-1.6" x2="10.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.5" y1="-1.6" x2="10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.73" y1="-1.6" x2="11.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="11.5" y1="-1.6" x2="11.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="11.5" y1="-2.7" x2="-11.5" y2="-2.7" width="0.1" layer="51"/>
<wire x1="-11.5" y1="-2.7" x2="-11.5" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-10.5" y1="2.1" x2="-10.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="-10.73" y1="2.1" x2="-10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="10.5" y1="2.1" x2="10.73" y2="2.1" width="0.1" layer="51"/>
<wire x1="10.73" y1="2.1" x2="10.73" y2="-1.6" width="0.1" layer="51"/>
<wire x1="-8.1" y1="2.8" x2="-10.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="-10.6" y1="2.8" x2="-10.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="8.1" y1="2.8" x2="10.6" y2="2.8" width="0.2" layer="21"/>
<wire x1="10.6" y1="2.8" x2="10.6" y2="2.6" width="0.2" layer="21"/>
<wire x1="-10.83" y1="-0.7" x2="-10.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-10.83" y1="-1.5" x2="-11.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-11.6" y1="-1.5" x2="-11.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-11.6" y1="-2.8" x2="11.6" y2="-2.8" width="0.2" layer="21"/>
<wire x1="11.6" y1="-2.8" x2="11.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="11.6" y1="-1.5" x2="10.83" y2="-1.5" width="0.2" layer="21"/>
<wire x1="10.83" y1="-1.5" x2="10.83" y2="-0.7" width="0.2" layer="21"/>
<text x="-11.6616" y="1.1185" size="0.8128" layer="25" align="bottom-right">&gt;NAME</text>
<text x="-11.6616" y="-0.6534" size="0.8128" layer="27" align="bottom-right">&gt;VALUE</text>
<circle x="-8.3" y="2.23" radius="0.1" width="0.2" layer="21"/>
<polygon width="0.1" layer="39" pour="solid">
<vertex x="-11.03" y="-0.599290625"/>
<vertex x="-11.03" y="-1.270709375"/>
<vertex x="-11.059290625" y="-1.3"/>
<vertex x="-11.8" y="-1.3"/>
<vertex x="-11.8" y="-3"/>
<vertex x="11.8" y="-3"/>
<vertex x="11.8" y="-1.3"/>
<vertex x="11.029290625" y="-1.3"/>
<vertex x="11" y="-1.270709375"/>
<vertex x="11" y="-0.599290625"/>
<vertex x="11.029290625" y="-0.57"/>
<vertex x="11.2" y="-0.57"/>
<vertex x="11.2" y="2.43"/>
<vertex x="10.829290625" y="2.43"/>
<vertex x="10.8" y="2.459290625"/>
<vertex x="10.8" y="3"/>
<vertex x="8.029290625" y="3"/>
<vertex x="8" y="3.029290625"/>
<vertex x="8" y="4.63"/>
<vertex x="-8" y="4.63"/>
<vertex x="-8" y="3.029290625"/>
<vertex x="-8.029290625" y="3"/>
<vertex x="-10.8" y="3"/>
<vertex x="-10.8" y="2.459290625"/>
<vertex x="-10.829290625" y="2.43"/>
<vertex x="-11.2" y="2.43"/>
<vertex x="-11.2" y="-0.57"/>
<vertex x="-11.059290625" y="-0.57"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="SW_M" urn="urn:adsk.eagle:package:3097791/4" locally_modified="yes" type="box" library_version="6">
<description>WS-TSW-6x6 mm washable J-Bend SMD Tact Switch,4 pins</description>
<packageinstances>
<packageinstance name="SW_M"/>
</packageinstances>
</package3d>
<package3d name="PI-2X20" urn="urn:adsk.eagle:package:22443/2" locally_modified="yes" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="PI-2X20"/>
</packageinstances>
</package3d>
<package3d name="IRF7319" urn="urn:adsk.eagle:package:30987/2" locally_modified="yes" type="model" library_version="5">
<description>SMALL OUTLINE INTEGRATED CIRCUIT SOP-8L
Source: http://www.diodes.com/datasheets/ds31262.pdf</description>
<packageinstances>
<packageinstance name="IRF7319"/>
</packageinstances>
</package3d>
<package3d name="DS3231" urn="urn:adsk.eagle:package:6240754/1" locally_modified="yes" type="box" library_version="2">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<packageinstances>
<packageinstance name="DS3231"/>
</packageinstances>
</package3d>
<package3d name="HEADER-2X4" urn="urn:adsk.eagle:package:22461/2" locally_modified="yes" type="model" library_version="8">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="HEADER-2X4"/>
</packageinstances>
</package3d>
<package3d name="DIODE_SS34(SMA)" urn="urn:adsk.eagle:package:16378188/1" locally_modified="yes" type="model" library_version="273">
<description>Molded Body, 5.20 X 2.60 X 2.90 mm body
&lt;p&gt;Molded Body package with body size 5.20 X 2.60 X 2.90 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIODE_SS34(SMA)"/>
</packageinstances>
</package3d>
<package3d name="BUZZER-12MM-NS" urn="urn:adsk.eagle:package:38553/1" locally_modified="yes" type="box" library_version="1">
<description>12mm Buzzer - PTH (No silkscreent variant)
This is a small 12mm round speaker that operates around the audible 2kHz range. You can use these speakers to create simple music or user interfaces.
Datasheet
Devices Using
BUZZER</description>
<packageinstances>
<packageinstance name="BUZZER-12MM-NS"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" locally_modified="yes" type="model" library_version="11" footprint_synced="no">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C0805" urn="urn:adsk.eagle:package:23617/2" locally_modified="yes" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0805"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" locally_modified="yes" type="model" library_version="11" footprint_synced="no">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" locally_modified="yes" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="FPC-10" urn="urn:adsk.eagle:package:15743822/2" locally_modified="yes" type="model" library_version="3">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt;1.00 mm SMT ZIF Horizontal Bottom Contact, 10 Pins</description>
<packageinstances>
<packageinstance name="FPC-10"/>
</packageinstances>
</package3d>
<package3d name="FPC-16" urn="urn:adsk.eagle:package:15743819/2" locally_modified="yes" type="model" library_version="3">
<description>&lt;b&gt;WR-FPC&lt;/b&gt; &lt;br &gt; 1.00 mm SMT ZIF Horizontal Bottom Contact,  16 Pins</description>
<packageinstances>
<packageinstance name="FPC-16"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SW_M" library_version="27">
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-0.37" y1="-1.4" x2="-1.64" y2="-0.13" width="0.254" layer="94"/>
<circle x="0" y="1.87" radius="0.5" width="0.254" layer="94"/>
<circle x="0" y="-1.87" radius="0.5" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="4" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="6.35" y="1.27" size="1.778" layer="95">&gt;Name</text>
<text x="6.35" y="-2.54" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="GY-87" library_version="314">
<wire x1="-15.24" y1="17.78" x2="-15.24" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="-15.24" y1="-22.86" x2="12.7" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="17.78" width="0.6096" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-15.24" y2="17.78" width="0.6096" layer="94"/>
<text x="8.636" y="-9.398" size="1.778" layer="94" rot="R270">GY-87</text>
<text x="10.16" y="-7.62" size="1.778" layer="94" rot="R90">IMU</text>
<pin name="VCC" x="-20.32" y="15.24" length="middle"/>
<pin name="GND" x="-20.32" y="10.16" length="middle"/>
<pin name="SCL" x="-20.32" y="5.08" length="middle"/>
<pin name="SDA" x="-20.32" y="0" length="middle"/>
<wire x1="-2.54" y1="-20.32" x2="2.54" y2="-20.32" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-19.05" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-21.59" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-20.32" x2="-2.54" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-3.81" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-1.27" y2="-16.51" width="0.3048" layer="94"/>
<text x="3.81" y="-21.082" size="1.778" layer="94">x</text>
<text x="0" y="-15.24" size="1.778" layer="94">y</text>
<text x="-6.604" y="14.732" size="1.778" layer="94">3.3V </text>
</symbol>
<symbol name="MUTANTC" library_version="120">
<text x="0" y="2.54" size="3.81" layer="94" font="vector" ratio="20">mutantc</text>
<rectangle x1="0" y1="0" x2="20.32" y2="2.54" layer="94"/>
</symbol>
<symbol name="PI-2X20" library_version="154">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="MHCD42">
<pin name="OUT+" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="OUT-" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="BAT+" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="BAT-" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="VIN-" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="VIN+" x="25.4" y="-7.62" length="middle" rot="R180"/>
<wire x1="-5.08" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="94"/>
<wire x1="20.32" y1="7.62" x2="20.32" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="-5.08" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="7.62" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="94">MHCD42</text>
</symbol>
<symbol name="EXPANSION-2X10" library_version="154">
<wire x1="-6.35" y1="-15.24" x2="8.89" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-15.24" x2="8.89" y2="12.7" width="0.4064" layer="94"/>
<wire x1="8.89" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="OSHWLOGO" library_version="158">
<wire x1="0" y1="0" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="94">OSHW Logo</text>
</symbol>
<symbol name="USB_C_FEMALE" library_version="271">
<description>&lt;h3&gt;USB - C 16 Pin&lt;/h3&gt;
Exposes the minimal pins needed to implement a USB 2.x legacy device.</description>
<wire x1="-2.54" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="0" size="2.54" layer="94" rot="R270" align="center">USB-C</text>
<text x="-2.54" y="-17.526" size="1.778" layer="96" font="vector" rot="MR180" align="top-left">&gt;VALUE</text>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="10.414" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="GND" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="VBUS" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="CC1" x="12.7" y="0" length="short" rot="R180"/>
<pin name="D+" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="D-" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="CC2" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="SHLD" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="SBU1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="SBU2" x="12.7" y="-7.62" length="short" rot="R180"/>
</symbol>
<symbol name="ESP32-S2-WROVER" library_version="188">
<wire x1="-22.86" y1="33.02" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="-35.56" width="0.254" layer="94"/>
<wire x1="22.86" y1="-35.56" x2="-22.86" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-35.56" x2="-22.86" y2="33.02" width="0.254" layer="94"/>
<text x="-22.6559" y="34.1345" size="1.778" layer="95">&gt;NAME</text>
<text x="-22.9067" y="-38.2081" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="27.94" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="27.94" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-27.94" y="25.4" length="middle" direction="in"/>
<pin name="IO34" x="27.94" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="IO35" x="27.94" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="IO33" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="IO26" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="IO14" x="-27.94" y="-25.4" length="middle"/>
<pin name="IO12" x="-27.94" y="-20.32" length="middle"/>
<pin name="IO13" x="-27.94" y="-22.86" length="middle"/>
<pin name="IO15" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="IO2" x="-27.94" y="5.08" length="middle"/>
<pin name="IO0_BOOT0" x="-27.94" y="12.7" length="middle"/>
<pin name="IO4" x="-27.94" y="0" length="middle"/>
<pin name="IO16" x="27.94" y="15.24" length="middle" rot="R180"/>
<pin name="IO17/DAC1" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="IO5" x="-27.94" y="-2.54" length="middle"/>
<pin name="IO18/DAC2" x="27.94" y="10.16" length="middle" rot="R180"/>
<pin name="IO19/D-" x="27.94" y="7.62" length="middle" rot="R180"/>
<pin name="IO21" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="RXD0" x="27.94" y="22.86" length="middle" rot="R180"/>
<pin name="TXD0" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="IO1" x="-27.94" y="7.62" length="middle"/>
<pin name="IO3" x="-27.94" y="2.54" length="middle"/>
<pin name="IO6" x="-27.94" y="-5.08" length="middle"/>
<pin name="IO7" x="-27.94" y="-7.62" length="middle"/>
<pin name="IO8" x="-27.94" y="-10.16" length="middle"/>
<pin name="IO9" x="-27.94" y="-12.7" length="middle"/>
<pin name="IO10" x="-27.94" y="-15.24" length="middle"/>
<pin name="IO11" x="-27.94" y="-17.78" length="middle"/>
<pin name="IO20/D+" x="27.94" y="5.08" length="middle" rot="R180"/>
<pin name="IO36" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="IO37" x="27.94" y="-12.7" length="middle" rot="R180"/>
<pin name="IO38" x="27.94" y="-15.24" length="middle" rot="R180"/>
<pin name="IO39/TCK" x="27.94" y="-17.78" length="middle" rot="R180"/>
<pin name="IO40/TDO" x="27.94" y="-20.32" length="middle" rot="R180"/>
<pin name="IO41/TDI" x="27.94" y="-22.86" length="middle" rot="R180"/>
<pin name="IO42/TMS" x="27.94" y="-25.4" length="middle" rot="R180"/>
<pin name="IO45_SPIV" x="-27.94" y="15.24" length="middle"/>
<pin name="IO46_DEBUGOUT" x="-27.94" y="17.78" length="middle" direction="in"/>
<pin name="EXP" x="27.94" y="-30.48" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="3.3V_BATTERY" library_version="200">
<pin name="GND" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="10.16" y="-5.08" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="P_MOSFET">
<wire x1="-1.651" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.508" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.381" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="-0.635" x2="2.032" y2="-0.508" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-1.1176" y="0.589171875"/>
<vertex x="-1.1176" y="-0.589171875"/>
<vertex x="-0.174921875" y="0"/>
</polygon>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="0.807828125" y="0.4826"/>
<vertex x="1.397" y="-0.460078125"/>
<vertex x="1.986171875" y="0.4826"/>
</polygon>
</symbol>
<symbol name="N_MOSFET">
<wire x1="-0.508" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="0.762" x2="2.032" y2="0.889" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-1.476078125" y="0"/>
<vertex x="-0.5334" y="-0.589171875"/>
<vertex x="-0.5334" y="0.589171875"/>
</polygon>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="0.807828125" y="-0.2286"/>
<vertex x="1.986171875" y="-0.2286"/>
<vertex x="1.397" y="0.714078125"/>
</polygon>
</symbol>
<symbol name="TP4056" library_version="207">
<pin name="TEMP" x="-12.7" y="7.62" length="middle"/>
<pin name="ISET" x="-12.7" y="2.54" length="middle"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle"/>
<pin name="VIN" x="-12.7" y="-7.62" length="middle"/>
<pin name="BAT" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="DONE" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="CHRG" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="CE" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="3.302" y="11.43" size="1.27" layer="94">&gt;NAME</text>
<text x="2.032" y="-12.446" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="DS3231" library_version="211">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-10.16" y="-12.7" size="1.778" layer="96" font="vector">DS3231</text>
<text x="10.16" y="15.24" size="1.778" layer="95" font="vector" rot="R180">&gt;NAME</text>
<pin name="SCL" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="/RST" x="-15.24" y="2.54" length="middle" direction="pas" function="dot"/>
<pin name="VBAT" x="15.24" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="32KHZ" x="-15.24" y="10.16" length="middle" direction="out"/>
<pin name="SQW/INT" x="-15.24" y="5.08" length="middle" direction="out"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle" direction="pwr"/>
<pin name="GND" x="15.24" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="NC" x="-15.24" y="0" length="middle" direction="nc"/>
<pin name="NC1" x="-15.24" y="-2.54" length="middle" direction="nc"/>
<pin name="NC2" x="-15.24" y="-5.08" length="middle" direction="nc"/>
<pin name="NC3" x="-15.24" y="-7.62" length="middle" direction="nc"/>
<pin name="NC4" x="15.24" y="-7.62" length="middle" direction="nc" rot="R180"/>
<pin name="NC5" x="15.24" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="NC6" x="15.24" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC7" x="15.24" y="0" length="middle" direction="nc" rot="R180"/>
</symbol>
<symbol name="SOLDERJUMPER" library_version="216">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="HEADER-2X4" library_version="276">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="DIODE_SS34(SMA)" library_version="273">
<description>Diode</description>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="0" y="5.08" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="95" align="center">&gt;VALUE</text>
<text x="0" y="-7.62" size="1.778" layer="95" align="center">&gt;SPICEMODEL</text>
<text x="0" y="-10.16" size="1.778" layer="95" align="center">&gt;SPICEEXTRA</text>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="BUZZER" library_version="276">
<description>&lt;h3&gt;Buzzer&lt;/h3&gt;</description>
<wire x1="-1.27" y1="1.905" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.635" y1="3.175" x2="0.635" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0.635" x2="1.905" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="3.175" width="0.1524" layer="94"/>
<wire x1="1.905" y1="3.175" x2="0.635" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.715" y1="3.81" x2="5.715" y2="4.445" width="0.254" layer="94"/>
<wire x1="5.715" y1="4.445" x2="-3.175" y2="4.445" width="0.254" layer="94"/>
<wire x1="-3.175" y1="4.445" x2="-3.175" y2="3.81" width="0.254" layer="94"/>
<wire x1="-3.175" y1="3.81" x2="-2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="4.826" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.334" y="0" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="JST_PH_2PIN">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="NC2" x="-2.54" y="-7.62" length="middle" rot="R90"/>
<pin name="NC1" x="-2.54" y="10.16" length="middle" rot="R270"/>
</symbol>
<symbol name="CAPASITOR" library_version="314">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR" library_version="314">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="FPC-10" library_version="317">
<pin name="1" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="7" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="9" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="10" x="-12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="Z1" x="15.24" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="Z2" x="-17.78" y="0" visible="pad" length="short" direction="pas"/>
<text x="-1.27" y="3.048" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="11.13" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-2.54" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="-12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-12.7" y2="0" width="0.254" layer="94"/>
<wire x1="-12.7" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FPC-16" library_version="317">
<pin name="1" x="17.78" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="15.24" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="6" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="7" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="8" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="10" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="11" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="12" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="13" x="-12.7" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="14" x="-15.24" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="15" x="-17.78" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="16" x="-20.32" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="Z1" x="22.86" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="Z2" x="-25.4" y="0" visible="pad" length="short" direction="pas"/>
<text x="-1.27" y="3.048" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="18.75" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="-22.86" y1="2.54" x2="-22.86" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-2.54" x2="20.32" y2="-2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="-20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="-20.32" y1="2.54" x2="-22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="-20.32" y1="2.54" x2="-20.32" y2="0" width="0.254" layer="94"/>
<wire x1="-20.32" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SW_M" prefix="S" uservalue="yes">
<description>&lt;b&gt;6x6 mm SMD J-Bend washable WS-TASV&lt;/b&gt;&lt;br&gt;
&lt;BR&gt;
&lt;BR&gt;
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/v2/Family_WS-TASV_6x6_J-Bend_SMD_Tact_Switch_4304830xx8xx.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/images/v2/Family_WS-TASV_6x6_J-Bend_SMD_Tact_Switch_4304830xx8xx.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/em/datasheet/4304x3xxx8x6.pdf"&gt;http://katalog.we-online.de/em/datasheet/4304x3xxx8x6.pdf&lt;/a&gt;&lt;p&gt;

2015 (C) Wurth Elektronik</description>
<gates>
<gate name="G$1" symbol="SW_M" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW_M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3097791/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GY-87" library_version="314">
<gates>
<gate name="G$1" symbol="GY-87" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="GY-87">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MUTANTC" library_version="120">
<gates>
<gate name="G$1" symbol="MUTANTC" x="22.86" y="2.54"/>
</gates>
<devices>
<device name="" package="MUTANTC_TEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_LOGO" package="MUTANTC_LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_TEXT+LOGO" package="MUTANTC_TEXT+LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PI-2X20" prefix="JP" uservalue="yes" library_version="154">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PI-2X20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PI-2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22443/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="12" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHCD42">
<gates>
<gate name="G$1" symbol="MHCD42" x="-17.78" y="-10.16"/>
</gates>
<devices>
<device name="SMD" package="MHCD42-SMD">
<connects>
<connect gate="G$1" pin="BAT+" pad="BAT+"/>
<connect gate="G$1" pin="BAT-" pad="BAT-"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
<connect gate="G$1" pin="OUT-" pad="OUT-"/>
<connect gate="G$1" pin="VIN+" pad="VIN+"/>
<connect gate="G$1" pin="VIN-" pad="VIN-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THROWHOLE" package="MHCD42-THROUGHHOLE">
<connects>
<connect gate="G$1" pin="BAT+" pad="BAT+"/>
<connect gate="G$1" pin="BAT-" pad="BAT-"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
<connect gate="G$1" pin="OUT-" pad="OUT-"/>
<connect gate="G$1" pin="VIN+" pad="VIN+"/>
<connect gate="G$1" pin="VIN-" pad="VIN-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EXPANSION-FEMALE-2X10" prefix="JP">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="EXPANSION-2X10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXPANSION-FEMALE-2X10">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OSHWLOGO" prefix="GOLD_ORB_SM">
<description>&lt;p&gt;The OSHW logo for PCB layout</description>
<gates>
<gate name="G$1" symbol="OSHWLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LOGO16MM" package="OSHW_16MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO8MM" package="OSHW_8MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO5MM" package="OSHW_5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_C_FEMALE" prefix="J" uservalue="yes">
<description>&lt;h3&gt;USB Type C 16Pin Connector&lt;/h3&gt;

Super Speed pins not available on the 16-pin purely SMD connector so this part is best for USB 2.0 implementations. D1 and D2 are tied together enabling D+/- no matter which way the cable is plugged into the connector. The two channel configuration pins (CC1/2) are exposed. These are normally connected to ground via 5.1k resistors but can be reconfigured for high current/high power applications.</description>
<gates>
<gate name="J1" symbol="USB_C_FEMALE" x="0" y="0"/>
</gates>
<devices>
<device name="16PIN" package="USB_C_FEMALE">
<connects>
<connect gate="J1" pin="CC1" pad="A5"/>
<connect gate="J1" pin="CC2" pad="B5"/>
<connect gate="J1" pin="D+" pad="A6 B6" route="any"/>
<connect gate="J1" pin="D-" pad="A7 B7"/>
<connect gate="J1" pin="GND" pad="GND GND2"/>
<connect gate="J1" pin="SBU1" pad="A8"/>
<connect gate="J1" pin="SBU2" pad="B8"/>
<connect gate="J1" pin="SHLD" pad="SHLD1 SHLD2 SHLD3 SHLD4 SHLD5 SHLD6 SHLD7 SHLD8" route="any"/>
<connect gate="J1" pin="VBUS" pad="VBUS1 VBUS2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14122" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP32-S2-WROVER" prefix="U">
<description>WiFi 802.11b/g/n Transceiver Module 2.4GHz Antenna Not Included Surface Mount &lt;a href="https://snapeda.com/parts/ESP32-S2-WROVER/Espressif%20Systems/view-part/?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESP32-S2-WROVER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP32-S2-WROVER">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="41"/>
<connect gate="G$1" pin="EXP" pad="43_1 43_2 43_3 43_4 43_5 43_6 43_7 43_8 43_9"/>
<connect gate="G$1" pin="GND" pad="1 26 42" route="any"/>
<connect gate="G$1" pin="IO0_BOOT0" pad="3"/>
<connect gate="G$1" pin="IO1" pad="4"/>
<connect gate="G$1" pin="IO10" pad="13"/>
<connect gate="G$1" pin="IO11" pad="14"/>
<connect gate="G$1" pin="IO12" pad="15"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="17"/>
<connect gate="G$1" pin="IO15" pad="18"/>
<connect gate="G$1" pin="IO16" pad="19"/>
<connect gate="G$1" pin="IO17/DAC1" pad="20"/>
<connect gate="G$1" pin="IO18/DAC2" pad="21"/>
<connect gate="G$1" pin="IO19/D-" pad="22"/>
<connect gate="G$1" pin="IO2" pad="5"/>
<connect gate="G$1" pin="IO20/D+" pad="23"/>
<connect gate="G$1" pin="IO21" pad="24"/>
<connect gate="G$1" pin="IO26" pad="25"/>
<connect gate="G$1" pin="IO3" pad="6"/>
<connect gate="G$1" pin="IO33" pad="27"/>
<connect gate="G$1" pin="IO34" pad="28"/>
<connect gate="G$1" pin="IO35" pad="29"/>
<connect gate="G$1" pin="IO36" pad="30"/>
<connect gate="G$1" pin="IO37" pad="31"/>
<connect gate="G$1" pin="IO38" pad="32"/>
<connect gate="G$1" pin="IO39/TCK" pad="33"/>
<connect gate="G$1" pin="IO4" pad="7"/>
<connect gate="G$1" pin="IO40/TDO" pad="34"/>
<connect gate="G$1" pin="IO41/TDI" pad="35"/>
<connect gate="G$1" pin="IO42/TMS" pad="36"/>
<connect gate="G$1" pin="IO45_SPIV" pad="39"/>
<connect gate="G$1" pin="IO46_DEBUGOUT" pad="40"/>
<connect gate="G$1" pin="IO5" pad="8"/>
<connect gate="G$1" pin="IO6" pad="9"/>
<connect gate="G$1" pin="IO7" pad="10"/>
<connect gate="G$1" pin="IO8" pad="11"/>
<connect gate="G$1" pin="IO9" pad="12"/>
<connect gate="G$1" pin="RXD0" pad="38"/>
<connect gate="G$1" pin="TXD0" pad="37"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" WiFi 802.11b/g/n Transceiver Module 2.4GHz Antenna Not Included Surface Mount "/>
<attribute name="DIGIKEY-PURCHASE-URL" value="https://snapeda.com/shop?store=DigiKey&amp;id=4614060"/>
<attribute name="MF" value="Espressif Systems"/>
<attribute name="MP" value="ESP32-S2-WROVER"/>
<attribute name="PACKAGE" value="VFQFN-56 Espressif Systems"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V_BATTERY">
<gates>
<gate name="G$1" symbol="3.3V_BATTERY" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="3.3V_BATTERY">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IRF7319" prefix="T" uservalue="yes">
<description>&lt;b&gt;DUAL N AND P-CHANNEL ENHANCEMENT MODE MOSFET&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.farnell.com/datasheets/1693880.pdf"&gt;Data sheet&lt;/a&gt;</description>
<gates>
<gate name="2" symbol="P_MOSFET" x="0" y="-7.62"/>
<gate name="1" symbol="N_MOSFET" x="0" y="7.62"/>
</gates>
<devices>
<device name="SO8" package="IRF7319">
<connects>
<connect gate="1" pin="D" pad="7 8" route="any"/>
<connect gate="1" pin="G" pad="2"/>
<connect gate="1" pin="S" pad="1"/>
<connect gate="2" pin="D" pad="5 6"/>
<connect gate="2" pin="G" pad="4"/>
<connect gate="2" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30987/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP4056" library_version="207">
<gates>
<gate name="G$1" symbol="TP4056" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TP4056">
<connects>
<connect gate="G$1" pin="BAT" pad="5"/>
<connect gate="G$1" pin="CE" pad="8"/>
<connect gate="G$1" pin="CHRG" pad="7"/>
<connect gate="G$1" pin="DONE" pad="6"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="ISET" pad="2"/>
<connect gate="G$1" pin="TEMP" pad="1"/>
<connect gate="G$1" pin="VIN" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DS3231" prefix="IC">
<gates>
<gate name="G$1" symbol="DS3231" x="0" y="0"/>
</gates>
<devices>
<device name="/SO" package="DS3231">
<connects>
<connect gate="G$1" pin="/RST" pad="4"/>
<connect gate="G$1" pin="32KHZ" pad="1"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="NC" pad="5"/>
<connect gate="G$1" pin="NC1" pad="6"/>
<connect gate="G$1" pin="NC2" pad="7"/>
<connect gate="G$1" pin="NC3" pad="8"/>
<connect gate="G$1" pin="NC4" pad="9"/>
<connect gate="G$1" pin="NC5" pad="10"/>
<connect gate="G$1" pin="NC6" pad="11"/>
<connect gate="G$1" pin="NC7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="16"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="SQW/INT" pad="3"/>
<connect gate="G$1" pin="VBAT" pad="14"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240754/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDERJUMPER" prefix="SJ" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;SMD Solder JUMPER&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;Solder the two pads together to create a connection, or remove the solder to break it.&lt;/p&gt;
&lt;b&gt;REFLOW&lt;/b&gt; - Use this footprint for solder paste and reflow ovens.&lt;br/&gt;
&lt;b&gt;WAVE&lt;/b&gt; - Use this footprint for hand-soldering (larger pads).
&lt;p&gt;&lt;b&gt;CLOSED&lt;/b&gt; - Has a trace between the two pads to ensure it is closed by default.  The trace needs to be cut to disable the jumper, and can be closed again by creating a solder bridge between the two pads.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="SOLDERJUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="CLOSED" package="SOLDERJUMPER_CLOSEDWIRE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW_NOPASTE" package="SOLDERJUMPER_REFLOW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SOLDERJUMPER_ARROW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="ARROW_CLOSED" package="SOLDERJUMPER_ARROW_REFLOW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-2X4" prefix="JP" library_version="276">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="HEADER-2X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER-2X4">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22461/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SS34(SMA)" prefix="D" library_version="273">
<description>&lt;B&gt;Diode Rectifier - Generic</description>
<gates>
<gate name="G$1" symbol="DIODE_SS34(SMA)" x="0" y="0"/>
</gates>
<devices>
<device name="DO-214AC" package="DIODE_SS34(SMA)">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16378188/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Diode" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="FORWARD_CURRENT" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="REVERSE_VOLTAGE" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Rectifier" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="DIODE" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="D">
<pinmap gate="G$1" pin="A" pinorder="1"/>
<pinmap gate="G$1" pin="C" pinorder="2"/>
</pinmapping>
<model name="DMOD">
**********************
* Autodesk EAGLE - Spice Model File
* Date: 9/17/17
* basic diode intrinsic model
**********************
.MODEL DMOD D</model>
</spice>
</deviceset>
<deviceset name="BUZZER" prefix="LS">
<description>&lt;h3&gt;Buzzer&lt;/h3&gt;
&lt;p&gt;Small round buzzers that operate around the audible 2kHz range. You can use these as speakers to create simple music or user interfaces.&lt;/p&gt;
&lt;p&gt;Variant Overview:
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;PTH - &lt;/b&gt; &lt;a href="https://www.sparkfun.com/products/7950"&gt;Mini Speaker - PC Mount 12mm 2.048kHz&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;b&gt;PTH_KIT - &lt;/b&gt; &lt;a href="https://www.sparkfun.com/products/7950"&gt;Mini Speaker - PC Mount 12mm 2.048kHz&lt;/a&gt;&lt;/li&gt;&lt;ul&gt;&lt;li&gt; KIT simplifies soldering by adding stop mask to one side of the pads.&lt;/li&gt;&lt;/ul&gt;
&lt;li&gt;&lt;b&gt;PTH_NO_SILK - &lt;/b&gt; &lt;a href="https://www.sparkfun.com/products/7950"&gt;Mini Speaker - PC Mount 12mm 2.048kHz&lt;/a&gt;&lt;/li&gt;&lt;ul&gt;&lt;li&gt; No silkscreen indicator of buzzer outline.&lt;/li&gt;&lt;/ul&gt;
&lt;li&gt;&lt;b&gt;PTH_KIT_NO_SILK - &lt;/b&gt; &lt;a href="https://www.sparkfun.com/products/7950"&gt;Mini Speaker - PC Mount 12mm 2.048kHz&lt;/a&gt;&lt;/li&gt;&lt;ul&gt;&lt;li&gt; KIT package w/o silkscreen&lt;/li&gt;&lt;/ul&gt;
&lt;li&gt;&lt;b&gt;SMD - &lt;/b&gt; Buzzer Audio Magnetic (CCV-084B16)&lt;/li&gt; &lt;ul&gt;&lt;li&gt;SMD buzzer used on, e.g. the &lt;a href="https://www.sparkfun.com/products/8463"&gt;LilyPad Buzzer&lt;/a&gt; and &lt;a href="https://www.sparkfun.com/products/9963"&gt;RFID USB Reader&lt;/a&gt;.&lt;/li&gt;&lt;/ul&gt;&lt;li&gt;&lt;b&gt;SMD_KIT - &lt;/b&gt; Buzzer Audio Magnetic (CCV-084B16)&lt;/li&gt;&lt;ul&gt;&lt;li&gt;SMD Buzzer with elongated pins to make soldering easier. Used on the &lt;a href="https://www.sparkfun.com/products/10935"&gt;SparkFun Simon Says - Surface Mount Soldering Kit&lt;/a&gt;.&lt;/li&gt;&lt;/ul&gt;
&lt;/ul&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BUZZER" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH_NO_SILK" package="BUZZER-12MM-NS">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38553/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="COMP-08253"/>
<attribute name="SF_SKU" value="COM-07950"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_PH_2PIN" prefix="X" uservalue="yes">
<description>JST 2-Pin Right-Angle Connector
&lt;ul&gt;
&lt;li&gt;PH-Series - 4UConnector: 17311&lt;/li&gt;
&lt;li&gt;SH-Series - 4UConnector: 07278&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="JST_PH_2PIN" x="2.54" y="0"/>
</gates>
<devices>
<device name="_BATT" package="JSTPH2_BATT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="NC1" pad="NC1"/>
<connect gate="G$1" pin="NC2" pad="NC2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPASITOR" prefix="C" uservalue="yes" library_version="314">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="CAPASITOR" x="0" y="0"/>
</gates>
<devices>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="54" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23617/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes" library_version="314">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="41" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0850" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FPC-10" prefix="J" library_version="317">
<description>&lt;b&gt;WR-FPC 1.00 mm SMT ZIF Horizontal Bottom Contact&lt;br&gt; &lt;/b&gt;
&lt;br&gt;
&lt;b&gt;KIND PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Pitch 1 mm
&lt;br&gt;&lt;br&gt;
&lt;b&gt;MATERIAL PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Insulator Material PPS
&lt;br&gt;Contact Material Phosphor Bronze
&lt;br&gt;Contact Plating 100 (µ") Tin over 50 (µ") Nickel
&lt;br&gt;Contact Type Stamped 
&lt;br&gt;
&lt;br&gt;&lt;a href="https://www.we-online.com/catalog/media/o33099v209%20Family_WR-FPC_68611214422.jpg" title="Enlarge picture"&gt;
&lt;img src="https://www.we-online.com/catalog/media/o33099v209%20Family_WR-FPC_68611214422.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="https://www.we-online.com/catalog/en/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422/"&gt;https://www.we-online.com/catalog/en/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422/&lt;/a&gt;&lt;p&gt;

&lt;/b&gt;Updated by Ella Wu  2020-08-26&lt;br&gt;
&lt;/b&gt;2020(C) Wurth Elektronik</description>
<gates>
<gate name="G$1" symbol="FPC-10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FPC-10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="Z1" pad="Z1"/>
<connect gate="G$1" pin="Z2" pad="Z2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15743822/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CONTACT-RESISTANCE" value="20Ohm"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/68611014422.pdf"/>
<attribute name="IR" value="1A"/>
<attribute name="L" value="17mm"/>
<attribute name="MATE-WITH" value="WR-FFC 1.00 mm Flat Flexible Cable Type 1 (Contacts on same side)/ Type 2 (Contacts opposite)"/>
<attribute name="PACKAGING" value="Tape and Reel"/>
<attribute name="PART-NUMBER" value=" 68611014422 "/>
<attribute name="PINS" value=" 10 "/>
<attribute name="PITCH" value="1mm"/>
<attribute name="TYPE" value="Horizontal"/>
<attribute name="VALUE" value=" 68611014422 "/>
<attribute name="WORKING-VOLTAGE" value="125V(AC)"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FPC-16" prefix="J" library_version="317">
<description>&lt;b&gt;WR-FPC 1.00 mm SMT ZIF Horizontal Bottom Contact&lt;br&gt; &lt;/b&gt;
&lt;br&gt;
&lt;b&gt;KIND PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Pitch 1 mm
&lt;br&gt;&lt;br&gt;
&lt;b&gt;MATERIAL PROPERTIES 
&lt;br&gt;&lt;br&gt;
&lt;/b&gt;Insulator Material PPS
&lt;br&gt;Contact Material Phosphor Bronze
&lt;br&gt;Contact Plating 100 (µ") Tin over 50 (µ") Nickel
&lt;br&gt;Contact Type Stamped 
&lt;br&gt;
&lt;br&gt;&lt;a href="https://www.we-online.com/catalog/media/o33099v209%20Family_WR-FPC_68611214422.jpg" title="Enlarge picture"&gt;
&lt;img src="https://www.we-online.com/catalog/media/o33099v209%20Family_WR-FPC_68611214422.jpg"  width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="https://www.we-online.com/catalog/en/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422/"&gt;https://www.we-online.com/catalog/en/FPC_1_00_SMT_ZIF_HORIZONTAL_BOTTOM_CONTACT_6861XX14422/&lt;/a&gt;&lt;p&gt;

&lt;/b&gt;Updated by Ella Wu  2020-08-26&lt;br&gt;
&lt;/b&gt;2020(C) Wurth Elektronik</description>
<gates>
<gate name="G$1" symbol="FPC-16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FPC-16">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="Z1" pad="Z1"/>
<connect gate="G$1" pin="Z2" pad="Z2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15743819/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CONTACT-RESISTANCE" value="20Ohm"/>
<attribute name="DATASHEET-URL" value="https://www.we-online.com/catalog/datasheet/68611614422.pdf"/>
<attribute name="IR" value="1A"/>
<attribute name="L" value="23mm"/>
<attribute name="MATE-WITH" value="WR-FFC 1.00 mm Flat Flexible Cable Type 1 (Contacts on same side)/ Type 2 (Contacts opposite)"/>
<attribute name="PACKAGING" value="Tape and Reel"/>
<attribute name="PART-NUMBER" value=" 68611614422 "/>
<attribute name="PINS" value=" 16 "/>
<attribute name="PITCH" value="1mm"/>
<attribute name="TYPE" value="Horizontal"/>
<attribute name="VALUE" value=" 68611614422 "/>
<attribute name="WORKING-VOLTAGE" value="125V(AC)"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue_R3" urn="urn:adsk.eagle:library:5828899">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:footprint:5829391/2" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="-3" y="2" size="0.762" layer="25">&gt;NAME</text>
<text x="-3" y="-2.7" size="0.762" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SPARKFUN-ELECTROMECHANICAL_TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:package:5829833/3" type="model" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<packageinstances>
<packageinstance name="TACTILE-SWITCH-1101NE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" urn="urn:adsk.eagle:symbol:5828967/1" library_version="49">
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS032G05A3SM-SPST-MOMENTARY-SW" urn="urn:adsk.eagle:component:5829909/3" prefix="S" library_version="49">
<description>6x3.5mm 2-pin Momentary Switch (Push-button)
&lt;p&gt;
&lt;a href="https://www.digikey.com/products/en?keywords=CKN10388TR-ND"&gt;Digikey Link&lt;/a&gt;
&lt;br&gt;
&lt;a href="https://media.digikey.com/pdf/Data%20Sheets/C&amp;K/RS-032G05_-SM_RT.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-1101NE" package="TACTILE-SWITCH-1101NE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829833/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Switches" urn="urn:adsk.eagle:library:14854419">
<description>This library contains various switch types such as DIP,Tactile,Toggle Push Button etc.

&lt;br&gt;
&lt;br&gt;If you would like to purchase any of our product please visit, &lt;a href="https://www.te.com/usa-en/home.html?te_bu=Cor&amp;te_type=disp&amp;te_campaign=ult_glo_cor-ult-global-disp-autodesk-models_sma-299_1&amp;elqCampaignId=84950"&gt;TE.com&lt;/a&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Terms and Conditions Disclaimer:&lt;/b&gt;
&lt;br&gt;
By using any of these models, you agree that this information has been provided to you free of charge for your use but remains the sole property of TE Connectivity Corporation (''TE'') or SnapEDA,  Inc. or Ultra Librarian/EMA Design Automation, Inc. (collectively, "Company"). While Company has used reasonable efforts to ensure its accuracy, Company does not guarantee that it is error-free, not makes any other representation, warranty, or guarantee that the information is completely accurate or up-to-date. In many cases, the CAD data has been simplified to remove proprietary detail while maintaining critical interface geometric detail for use by customers. Company expressly disclaims all implied warranties regarding this information, including but not limited to any implied warranties or merchantability or fitness for a particular purpose.</description>
<packages>
<package name="SW2_1825027-E" urn="urn:adsk.eagle:footprint:14854489/1" library_version="5">
<pad name="1" x="0" y="0" drill="0.9906" diameter="1.4986"/>
<pad name="2" x="4.4958" y="0" drill="0.9906" diameter="1.4986"/>
<pad name="3" x="-1.2446" y="2.4892" drill="1.2954" diameter="1.8034"/>
<pad name="4" x="5.7658" y="2.4892" drill="1.2954" diameter="1.8034"/>
<wire x1="4.0132" y1="-3.3528" x2="4.0132" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-3.3528" x2="0.508" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-6.9088" x2="4.0132" y2="-6.9088" width="0.1524" layer="51"/>
<wire x1="-1.2954" y1="-3.3528" x2="5.8166" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="5.8166" y1="-3.3528" x2="5.8166" y2="4.0132" width="0.1524" layer="51"/>
<wire x1="5.8166" y1="4.0132" x2="-1.2954" y2="4.0132" width="0.1524" layer="51"/>
<wire x1="-1.2954" y1="4.0132" x2="-1.2954" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-3.4798" x2="5.9436" y2="-3.4798" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="-3.4798" x2="5.9436" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="4.1402" x2="-1.4224" y2="4.1402" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="4.1402" x2="-1.4224" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="1.143" x2="-1.4224" y2="-3.4798" width="0.1524" layer="21"/>
<wire x1="5.9436" y1="3.683" x2="5.9436" y2="4.1402" width="0.1524" layer="21"/>
<text x="0.6858" y="-0.635" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-2.3622" y="-0.635" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="0.5334" y="2.6162" size="1.27" layer="21" ratio="6" rot="SR0">&gt;Value</text>
<text x="-1.016" y="2.6162" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<packages3d>
<package3d name="SW2_1825027-E" urn="urn:adsk.eagle:package:14854601/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="SW2_1825027-E"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SW2_1825027-A" urn="urn:adsk.eagle:symbol:14854440/2" library_version="5">
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="-3.175" y1="0" x2="3.81" y2="1.905" width="0.2032" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="-3.81" x2="0.889" y2="-3.81" width="0.2032" layer="94"/>
<wire x1="-0.635" y1="-4.191" x2="0.635" y2="-4.191" width="0.2032" layer="94"/>
<wire x1="-0.381" y1="-4.572" x2="0.381" y2="-4.572" width="0.2032" layer="94"/>
<wire x1="-0.127" y1="-4.953" x2="0.127" y2="-4.953" width="0.2032" layer="94"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.2032" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.2032" layer="94"/>
<text x="-7.8994" y="2.0828" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-7.8994" y="7.1628" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1-1825027-4" urn="urn:adsk.eagle:component:14854724/3" prefix="SW" library_version="5">
<gates>
<gate name="A" symbol="SW2_1825027-A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW2_1825027-E">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14854601/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="BUILT_BY" value="EMA_UL_Team" constant="no"/>
<attribute name="CATEGORY" value="Tactile Switches" constant="no"/>
<attribute name="CONFIGURATION__POLE-THROW_" value="Single Pole - Single Throw" constant="no"/>
<attribute name="CONTACT_CURRENT_RATING" value="50" constant="no"/>
<attribute name="COPYRIGHT" value="Copyright (C) 2016 Accelerated Designs. All rights reserved" constant="no"/>
<attribute name="DESCRIPTION" value="FSMRA5JH04=R/A,TACT PB SW,160G" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="MAX_SWITCHING_CURRENT" value="-" constant="no"/>
<attribute name="MAX_SWITCHING_VOLTAGE_AC_" value="-" constant="no"/>
<attribute name="PART_STATUS" value="active" constant="no"/>
<attribute name="PRODUCT_TYPE" value="Switch" constant="no"/>
<attribute name="SERIES" value="Unknown" constant="no"/>
<attribute name="SWITCH_TYPE" value="Tactile Switches" constant="no"/>
<attribute name="TE_INTERNAL_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="TE_PART_NUMBER" value="1-1825027-4" constant="no"/>
<attribute name="VENDOR" value="TE Connectivity" constant="no"/>
<attribute name="VOLTAGE_RATING" value="-" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad" urn="urn:adsk.eagle:library:412">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMD1,27-2,54" urn="urn:adsk.eagle:footprint:30822/1" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.8" y="-2.4" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="SMD1,27-2,54" urn="urn:adsk.eagle:package:30839/1" type="box" library_version="2">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD1,27-2,54"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PAD" urn="urn:adsk.eagle:symbol:30808/1" library_version="2">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD2" urn="urn:adsk.eagle:component:30857/2" prefix="PAD" uservalue="yes" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD1,27-2,54">
<connects>
<connect gate="1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30839/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23" urn="urn:adsk.eagle:footprint:43155/1" library_version="8">
<description>&lt;B&gt;DIODE&lt;/B&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOT23" urn="urn:adsk.eagle:package:43389/2" type="model" library_version="8">
<description>DIODE</description>
<packageinstances>
<packageinstance name="SOT23"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SDD_AKKA" urn="urn:adsk.eagle:symbol:43242/2" library_version="8">
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.016" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.016" x2="1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="0.762" y="2.0066" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.318" y="-3.9624" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="A2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="CC" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAT54C" urn="urn:adsk.eagle:component:43609/4" prefix="D" library_version="8">
<description>&lt;b&gt;Schottky Diodes&lt;/b&gt;&lt;p&gt;
Source: Fairchild .. BAT54.pdf</description>
<gates>
<gate name="G$1" symbol="SDD_AKKA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="CC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:43389/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LED" urn="urn:adsk.eagle:library:22900745">
<description>&lt;B&gt;LED parts CHIP-Flat Top, Round Top</description>
<packages>
<package name="LEDC1608X35N_FLAT-R" urn="urn:adsk.eagle:footprint:24294736/1" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.35 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.35 mm&lt;/p&gt;</description>
<smd name="C" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<wire x1="-1.3099" y1="0.7699" x2="0.8" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<text x="-0.127" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC1608X55N_FLAT-R" urn="urn:adsk.eagle:footprint:24294737/1" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.55 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.55 mm&lt;/p&gt;</description>
<smd name="C" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<wire x1="-1.3099" y1="0.7699" x2="0.8" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<text x="-0.127" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC1608X80N_FLAT-R" urn="urn:adsk.eagle:footprint:24294739/1" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.80 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.80 mm&lt;/p&gt;</description>
<smd name="C" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<wire x1="-1.3099" y1="0.7699" x2="0.8" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<text x="-0.127" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X110N_FLAT-R" urn="urn:adsk.eagle:footprint:24294742/1" library_version="7">
<description>Chip LED, 2.00 X 1.25 X 1.10 mm body
 &lt;p&gt;Chip LED package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<smd name="C" x="-1.025" y="0" dx="0.7618" dy="1.3618" layer="1"/>
<smd name="A" x="1.025" y="0" dx="0.7618" dy="1.3618" layer="1"/>
<wire x1="-1.6599" y1="0.9949" x2="1" y2="0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="0.9949" x2="-1.6599" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="-0.9949" x2="1" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.12" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.12" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.12" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.12" layer="51"/>
<text x="-0.127" y="1.6299" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.6299" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC3216X75N_FLAT-R" urn="urn:adsk.eagle:footprint:24294744/1" library_version="7">
<description>Chip LED, 3.20 X 1.60 X 0.75 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 1.60 X 0.75 mm&lt;/p&gt;</description>
<smd name="C" x="-1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<smd name="A" x="1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<wire x1="-2.2599" y1="1.1699" x2="1.6" y2="1.1699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="1.1699" x2="-2.2599" y2="-1.1699" width="0.12" layer="21"/>
<wire x1="-2.2599" y1="-1.1699" x2="1.6" y2="-1.1699" width="0.12" layer="21"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.12" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.12" layer="51"/>
<text x="-0.127" y="1.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC1005X25N_FLAT-R" urn="urn:adsk.eagle:footprint:24294731/1" library_version="7">
<description>Chip LED, 1.00 X 0.50 X 0.25 mm body
 &lt;p&gt;Chip LED package with body size 1.00 X 0.50 X 0.25 mm&lt;/p&gt;</description>
<smd name="C" x="-0.45" y="0" dx="0.7" dy="0.5" layer="1"/>
<smd name="A" x="0.45" y="0" dx="0.7" dy="0.5" layer="1"/>
<wire x1="-1.0099" y1="0.6199" x2="0.5" y2="0.6199" width="0.12" layer="21"/>
<wire x1="-1.0099" y1="0.6199" x2="-1.0099" y2="-0.6199" width="0.12" layer="21"/>
<wire x1="-1.0099" y1="-0.6199" x2="0.5" y2="-0.6199" width="0.12" layer="21"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.12" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.12" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.12" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.12" layer="51"/>
<text x="-0.127" y="1.2549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="-0.127" y="-1.2549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LEDC1608X35N_FLAT-R" urn="urn:adsk.eagle:package:24294797/1" type="model" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.35 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X35N_FLAT-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC1608X55N_FLAT-R" urn="urn:adsk.eagle:package:24294799/1" type="model" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.55 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X55N_FLAT-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC1608X80N_FLAT-R" urn="urn:adsk.eagle:package:24294802/1" type="model" library_version="7">
<description>Chip LED, 1.60 X 0.80 X 0.80 mm body
 &lt;p&gt;Chip LED package with body size 1.60 X 0.80 X 0.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X80N_FLAT-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X110N_FLAT-R" urn="urn:adsk.eagle:package:24294806/1" type="model" library_version="7">
<description>Chip LED, 2.00 X 1.25 X 1.10 mm body
 &lt;p&gt;Chip LED package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X110N_FLAT-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC3216X75N_FLAT-R" urn="urn:adsk.eagle:package:24294810/1" type="model" library_version="7">
<description>Chip LED, 3.20 X 1.60 X 0.75 mm body
 &lt;p&gt;Chip LED package with body size 3.20 X 1.60 X 0.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC3216X75N_FLAT-R"/>
</packageinstances>
</package3d>
<package3d name="LEDC1005X25N_FLAT-R" urn="urn:adsk.eagle:package:24294790/1" type="model" library_version="7">
<description>Chip LED, 1.00 X 0.50 X 0.25 mm body
 &lt;p&gt;Chip LED package with body size 1.00 X 0.50 X 0.25 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1005X25N_FLAT-R"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:22900757/3" library_version="7">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="-0.762" size="1.778" layer="95" rot="R180" align="top-right">&gt;NAME</text>
<text x="2.54" y="-3.302" size="1.778" layer="96" rot="R180" align="top-right">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-3.048" y="-1.27"/>
<vertex x="-3.429" y="-2.159"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-2.921" y="-2.413"/>
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="CHIP-FLAT-R" urn="urn:adsk.eagle:component:22900849/5" prefix="D" uservalue="yes" library_version="8">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_0603-0.35MM" package="LEDC1608X35N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294797/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603-0.55MM" package="LEDC1608X55N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294799/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0603-0.80MM" package="LEDC1608X80N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294802/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0805" package="LEDC2012X110N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294806/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="_1206" package="LEDC3216X75N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294810/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="_0402" package="LEDC1005X25N_FLAT-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:24294790/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="LED" constant="no"/>
<attribute name="COLOR" value="RED" constant="no"/>
<attribute name="DESCRIPTION" value="CHIP LED FLAT" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="CHIP" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="FLAT_TOP" constant="no"/>
<attribute name="VALUE" value="LED_RED" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi" urn="urn:adsk.eagle:library:514">
<description>&lt;h3&gt;SparkFun Discrete Semiconductors&lt;/h3&gt;
This library contains diodes, optoisolators, TRIACs, MOSFETs, transistors, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOT23-3" urn="urn:adsk.eagle:footprint:38398/1" library_version="1">
<description>SOT23-3</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.651" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.651" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="TO-92" urn="urn:adsk.eagle:footprint:38414/1" library_version="1">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-0.7863" y1="2.5485" x2="-2.0946" y2="-1.651" width="0.2032" layer="21" curve="111.098962"/>
<wire x1="2.0945" y1="-1.651" x2="0.7863" y2="2.548396875" width="0.2032" layer="21" curve="111.099507"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796"/>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.048" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="TO-92-EZ" urn="urn:adsk.eagle:footprint:38415/1" library_version="1">
<wire x1="-0.7863" y1="2.5485" x2="-2.0946" y2="-1.651" width="0.2032" layer="21" curve="111.098962"/>
<wire x1="2.0945" y1="-1.651" x2="0.7863" y2="2.548396875" width="0.2032" layer="21" curve="111.099507"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.048" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<circle x="0" y="1.905" radius="1.02390625" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="0" y="1.905" radius="0.508" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="0.508" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="1.27" y="0" radius="0.508" width="0" layer="29"/>
</package>
</packages>
<packages3d>
<package3d name="SOT23-3" urn="urn:adsk.eagle:package:38446/1" type="box" library_version="1">
<description>SOT23-3</description>
<packageinstances>
<packageinstance name="SOT23-3"/>
</packageinstances>
</package3d>
<package3d name="TO-92" urn="urn:adsk.eagle:package:38450/1" type="box" library_version="1">
<description>TO 92</description>
<packageinstances>
<packageinstance name="TO-92"/>
</packageinstances>
</package3d>
<package3d name="TO-92-EZ" urn="urn:adsk.eagle:package:38451/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="TO-92-EZ"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="NPN" urn="urn:adsk.eagle:symbol:38413/1" library_version="1">
<description>&lt;h3&gt; NPN Transistor&lt;/h3&gt;
Allows current flow when high potential at base.</description>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-2.286" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANS_NPN" urn="urn:adsk.eagle:component:38477/1" prefix="Q" library_version="1">
<description>&lt;h3&gt;NPN transistor&lt;/h3&gt;
&lt;p&gt;Current controlled devices typically used to amplify current or used as a switch.&lt;/p&gt;
&lt;ul&gt;
  &lt;li&gt;
    BC547 - 
    &lt;a href="http://www.sparkfun.com/products/8928"&gt;COM-08928&lt;/a&gt;
    (TO-92 45V 100mA) (1.Collector 2.Base 3.Emitter)
  &lt;/li&gt;
  &lt;li&gt;
    2N3904 - 
    &lt;a href="http://www.sparkfun.com/products/521"&gt;COM-00521&lt;/a&gt;
    (TO-92 40V 200mA) (1.Emitter 2.Base 3.Collector)
  &lt;/li&gt;
  &lt;li&gt;
    P2N2222A - 
    &lt;a href="http://www.sparkfun.com/products/12852"&gt;COM-12852&lt;/a&gt;
    (TO-92 40V 600mA) (1.Collector 2.Base 3.Emitter)
  &lt;/li&gt;

&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="-MMBT2222AL" package="SOT23-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38446/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08049"/>
<attribute name="VALUE" value="600mA/40V"/>
</technology>
</technologies>
</device>
<device name="-2N3904" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38450/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08447"/>
<attribute name="VALUE" value="200mA/40V"/>
</technology>
</technologies>
</device>
<device name="-P2N2222A" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38450/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09536"/>
<attribute name="VALUE" value="600mA/40V"/>
</technology>
</technologies>
</device>
<device name="-MMBTA42" package="SOT23-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38446/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09116"/>
<attribute name="VALUE" value="500mA/300V"/>
</technology>
</technologies>
</device>
<device name="-2N3904-EZ" package="TO-92-EZ">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38451/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08447"/>
<attribute name="VALUE" value="200mA/40V"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="2" name="lowpower" width="0.3048" drill="0.4826">
</class>
<class number="3" name="Power" width="0.3048" drill="0.4826">
</class>
</classes>
<parts>
<part name="C1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C5" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C7" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C8" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C9" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D5" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D7" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D8" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D9" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E5" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E7" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E8" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E9" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="A2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A3" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A4" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A6" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="A9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B11" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B3" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B4" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B6" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B0" library="Switches" library_urn="urn:adsk.eagle:library:14854419" deviceset="1-1825027-4" device="" package3d_urn="urn:adsk.eagle:package:14854601/2" value="s"/>
<part name="C10" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="D10" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E10" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="A10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="B10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="D11" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="E11" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SW_M" device="" package3d_urn="urn:adsk.eagle:package:3097791/4"/>
<part name="C0" library="Switches" library_urn="urn:adsk.eagle:library:14854419" deviceset="1-1825027-4" device="" package3d_urn="urn:adsk.eagle:package:14854601/2" value="s"/>
<part name="J1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="FPC-16" device="" package3d_urn="urn:adsk.eagle:package:15743819/2" value=" 68611614422 "/>
<part name="JP1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="EXPANSION-FEMALE-2X10" device=""/>
<part name="IRF7319" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="IRF7319" device="SO8" package3d_urn="urn:adsk.eagle:package:30987/2"/>
<part name="R0_100K" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R1_100K" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="POW" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="IC1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="DS3231" device="/SO" package3d_urn="urn:adsk.eagle:package:6240754/1"/>
<part name="R3_10K" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R4_10K" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="C1_100NF" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="C2_100NF" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="J5" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="FPC-10" device="" package3d_urn="urn:adsk.eagle:package:15743822/2" value=" 68611014422 "/>
<part name="D12" library="Diode" deviceset="BAT54C" device="" package3d_urn="urn:adsk.eagle:package:43389/2"/>
<part name="U$1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="MUTANTC" device="MUTANTC_TEXT+LOGO"/>
<part name="JP3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="PI-2X20" device="" package3d_urn="urn:adsk.eagle:package:22443/2"/>
<part name="U$2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="MHCD42" device="THROWHOLE"/>
<part name="GOLD_ORB_SM1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="OSHWLOGO" device="LOGO8MM"/>
<part name="J3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="USB_C_FEMALE" device="16PIN"/>
<part name="J4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="USB_C_FEMALE" device="16PIN"/>
<part name="R4_5.1K3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R4_5.1K4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="U2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="ESP32-S2-WROVER" device=""/>
<part name="JP6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="HEADER-2X4" device="" package3d_urn="urn:adsk.eagle:package:22461/2"/>
<part name="U$6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="3.3V_BATTERY" device=""/>
<part name="U$7" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="TP4056" device=""/>
<part name="R6_1.2K" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="C4_10UF" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="C8_10UF" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="ONE" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SOLDERJUMPER" device=""/>
<part name="IRF1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="IRF7319" device="SO8" package3d_urn="urn:adsk.eagle:package:30987/2"/>
<part name="R1_100K1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R0_100K3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="IRF4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="IRF7319" device="SO8" package3d_urn="urn:adsk.eagle:package:30987/2"/>
<part name="R1_100K4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R0_100K6" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R0_100K2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="R0_100K1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="U$3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="GY-87" device=""/>
<part name="ONE2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SOLDERJUMPER" device=""/>
<part name="ONE3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="SOLDERJUMPER" device=""/>
<part name="LED1" library="LED" library_urn="urn:adsk.eagle:library:22900745" deviceset="CHIP-FLAT-R" device="_1206" package3d_urn="urn:adsk.eagle:package:24294810/1" value="LED_RED"/>
<part name="R6_1.2K1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="LS1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="BUZZER" device="-PTH_NO_SILK" package3d_urn="urn:adsk.eagle:package:38553/1"/>
<part name="Q2" library="SparkFun-DiscreteSemi" library_urn="urn:adsk.eagle:library:514" deviceset="TRANS_NPN" device="-MMBT2222AL" package3d_urn="urn:adsk.eagle:package:38446/1" value="600mA/40V"/>
<part name="R5_10K1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="NEGATIVE" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="POSITIVE" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="D14" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="DIODE_SS34(SMA)" device="DO-214AC" package3d_urn="urn:adsk.eagle:package:16378188/1" value="DIODE"/>
<part name="Q1" library="SparkFun-DiscreteSemi" library_urn="urn:adsk.eagle:library:514" deviceset="TRANS_NPN" device="-MMBT2222AL" package3d_urn="urn:adsk.eagle:package:38446/1" value="600mA/40V"/>
<part name="R5_10K3" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="C2_10UF2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="R5_10K4" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="+" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="-" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="X1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="JST_PH_2PIN" device="_BATT"/>
<part name="D13" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="DIODE_SS34(SMA)" device="DO-214AC" package3d_urn="urn:adsk.eagle:package:16378188/1" value="DIODE"/>
<part name="R5_10K2" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="A1" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
<part name="C2_10UF1" library="aaaaaa" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="A11" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3" value="sm"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="C1" gate="G$1" x="114.3" y="-101.6" smashed="yes">
<attribute name="NAME" x="120.65" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="120.65" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="-45.72" y="-101.6" smashed="yes">
<attribute name="NAME" x="-39.37" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="-39.37" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="-27.94" y="-101.6" smashed="yes">
<attribute name="NAME" x="-21.59" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="-21.59" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="-10.16" y="-101.6" smashed="yes">
<attribute name="NAME" x="-3.81" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.81" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="7.62" y="-101.6" smashed="yes">
<attribute name="NAME" x="13.97" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="13.97" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="25.4" y="-101.6" smashed="yes">
<attribute name="NAME" x="31.75" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="31.75" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="43.18" y="-101.6" smashed="yes">
<attribute name="NAME" x="49.53" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="60.96" y="-101.6" smashed="yes">
<attribute name="NAME" x="67.31" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="78.74" y="-101.6" smashed="yes">
<attribute name="NAME" x="85.09" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="85.09" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="-60.96" y="-116.84" smashed="yes">
<attribute name="NAME" x="-54.61" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="-54.61" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="-45.72" y="-116.84" smashed="yes">
<attribute name="NAME" x="-39.37" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="-39.37" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="-27.94" y="-116.84" smashed="yes">
<attribute name="NAME" x="-21.59" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="-21.59" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="-10.16" y="-116.84" smashed="yes">
<attribute name="NAME" x="-3.81" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.81" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="7.62" y="-116.84" smashed="yes">
<attribute name="NAME" x="13.97" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="13.97" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="25.4" y="-116.84" smashed="yes">
<attribute name="NAME" x="31.75" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="31.75" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D7" gate="G$1" x="43.18" y="-116.84" smashed="yes">
<attribute name="NAME" x="49.53" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D8" gate="G$1" x="60.96" y="-116.84" smashed="yes">
<attribute name="NAME" x="67.31" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="78.74" y="-116.84" smashed="yes">
<attribute name="NAME" x="85.09" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="85.09" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="E1" gate="G$1" x="-60.96" y="-134.62" smashed="yes">
<attribute name="NAME" x="-54.61" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="-54.61" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E2" gate="G$1" x="-45.72" y="-134.62" smashed="yes">
<attribute name="NAME" x="-39.37" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="-39.37" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E3" gate="G$1" x="-27.94" y="-134.62" smashed="yes">
<attribute name="NAME" x="-21.59" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="-21.59" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E4" gate="G$1" x="-10.16" y="-134.62" smashed="yes">
<attribute name="NAME" x="-3.81" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.81" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E5" gate="G$1" x="7.62" y="-134.62" smashed="yes">
<attribute name="NAME" x="13.97" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="13.97" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E6" gate="G$1" x="25.4" y="-134.62" smashed="yes">
<attribute name="NAME" x="31.75" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="31.75" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E7" gate="G$1" x="43.18" y="-134.62" smashed="yes">
<attribute name="NAME" x="49.53" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E8" gate="G$1" x="60.96" y="-134.62" smashed="yes">
<attribute name="NAME" x="67.31" y="-133.35" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="E9" gate="G$1" x="76.2" y="-134.62" smashed="yes">
<attribute name="VALUE" x="82.55" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="A2" gate="G$1" x="-45.72" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-48.26" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-43.18" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A3" gate="G$1" x="-27.94" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-30.48" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-25.4" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A4" gate="G$1" x="-10.16" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-12.7" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-7.62" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A5" gate="G$1" x="7.62" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.16" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A6" gate="G$1" x="22.86" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="20.32" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="25.4" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A7" gate="G$1" x="40.64" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="38.1" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="43.18" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A8" gate="G$1" x="58.42" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.96" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A9" gate="G$1" x="76.2" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.74" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B11" gate="G$1" x="114.3" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="111.76" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="116.84" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B2" gate="G$1" x="-45.72" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-48.26" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-43.18" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B3" gate="G$1" x="-27.94" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-30.48" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-25.4" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B4" gate="G$1" x="-10.16" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-12.7" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-7.62" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B5" gate="G$1" x="7.62" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.16" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B6" gate="G$1" x="22.86" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="20.32" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="25.4" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B7" gate="G$1" x="40.64" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="38.1" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="43.18" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B8" gate="G$1" x="58.42" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.96" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B9" gate="G$1" x="76.2" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.74" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B0" gate="A" x="-63.5" y="-88.9" smashed="yes">
<attribute name="NAME" x="-63.5" y="-87.376" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-63.5" y="-89.408" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="C10" gate="G$1" x="96.52" y="-101.6" smashed="yes">
<attribute name="NAME" x="102.87" y="-100.33" size="1.778" layer="95"/>
<attribute name="VALUE" x="102.87" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="96.52" y="-116.84" smashed="yes">
<attribute name="NAME" x="102.87" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="102.87" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="E10" gate="G$1" x="96.52" y="-134.62" smashed="yes">
<attribute name="VALUE" x="102.87" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="A10" gate="G$1" x="93.98" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="91.44" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="96.52" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="B10" gate="G$1" x="93.98" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="91.44" y="-86.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="96.52" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="116.84" y="-116.84" smashed="yes">
<attribute name="NAME" x="123.19" y="-115.57" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.19" y="-119.38" size="1.778" layer="96"/>
</instance>
<instance part="E11" gate="G$1" x="116.84" y="-134.62" smashed="yes">
<attribute name="VALUE" x="123.19" y="-137.16" size="1.778" layer="96"/>
</instance>
<instance part="C0" gate="A" x="-63.5" y="-101.6" smashed="yes">
<attribute name="NAME" x="-63.5" y="-100.076" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-63.5" y="-102.108" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="J1" gate="G$1" x="-218.44" y="-73.66" smashed="yes" rot="MR270"/>
<instance part="JP1" gate="A" x="-129.54" y="-63.5" smashed="yes">
<attribute name="NAME" x="-135.89" y="-50.165" size="1.778" layer="95"/>
<attribute name="VALUE" x="-135.89" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="IRF7319" gate="2" x="134.62" y="-170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="137.16" y="-170.18" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="137.16" y="-170.18" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="IRF7319" gate="1" x="106.68" y="-177.8" smashed="yes">
<attribute name="VALUE" x="109.22" y="-177.8" size="1.778" layer="96"/>
<attribute name="NAME" x="93.98" y="-175.26" size="1.778" layer="95"/>
</instance>
<instance part="R0_100K" gate="G$1" x="99.06" y="-187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="97.5614" y="-191.77" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="102.362" y="-191.77" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1_100K" gate="G$1" x="121.92" y="-162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="120.4214" y="-166.37" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="125.222" y="-166.37" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="POW" gate="G$1" x="119.38" y="-185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="-187.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="121.92" y="-187.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC1" gate="G$1" x="-302.26" y="-81.28" smashed="yes">
<attribute name="NAME" x="-292.1" y="-66.04" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R3_10K" gate="G$1" x="-279.4" y="-63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="-280.8986" y="-67.31" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-276.098" y="-67.31" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4_10K" gate="G$1" x="-266.7" y="-63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="-268.1986" y="-67.31" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-263.398" y="-67.31" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C1_100NF" gate="G$1" x="-322.58" y="-66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="-324.104" y="-66.421" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-324.104" y="-61.341" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2_100NF" gate="G$1" x="-269.24" y="-83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="-270.764" y="-84.201" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-270.764" y="-79.121" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J5" gate="G$1" x="-218.44" y="-114.3" smashed="yes" rot="R90"/>
<instance part="D12" gate="G$1" x="119.38" y="-175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="118.618" y="-177.2666" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="123.698" y="-171.2976" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$1" gate="G$1" x="-228.6" y="-154.94" smashed="yes"/>
<instance part="JP3" gate="A" x="-129.54" y="-114.3" smashed="yes">
<attribute name="NAME" x="-135.89" y="-88.265" size="1.778" layer="95"/>
<attribute name="VALUE" x="-135.89" y="-144.78" size="1.778" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="66.04" y="-226.06" smashed="yes"/>
<instance part="GOLD_ORB_SM1" gate="G$1" x="-228.6" y="-167.64" smashed="yes"/>
<instance part="J3" gate="J1" x="-312.42" y="-149.86" smashed="yes">
<attribute name="VALUE" x="-314.96" y="-167.386" size="1.778" layer="96" font="vector" rot="MR180" align="top-left"/>
<attribute name="NAME" x="-314.96" y="-139.446" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="J4" gate="J1" x="-312.42" y="-185.42" smashed="yes">
<attribute name="VALUE" x="-314.96" y="-202.946" size="1.778" layer="96" font="vector" rot="MR180" align="top-left"/>
<attribute name="NAME" x="-314.96" y="-175.006" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="R4_5.1K3" gate="G$1" x="-287.02" y="-185.42" smashed="yes">
<attribute name="NAME" x="-290.83" y="-183.9214" size="1.778" layer="95"/>
<attribute name="VALUE" x="-290.83" y="-188.722" size="1.778" layer="96"/>
</instance>
<instance part="R4_5.1K4" gate="G$1" x="-266.7" y="-187.96" smashed="yes">
<attribute name="NAME" x="-270.51" y="-186.4614" size="1.778" layer="95"/>
<attribute name="VALUE" x="-270.51" y="-191.262" size="1.778" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="-185.42" y="-220.98" smashed="yes">
<attribute name="NAME" x="-208.0759" y="-186.8455" size="1.778" layer="95"/>
<attribute name="VALUE" x="-208.3267" y="-259.1881" size="1.778" layer="96"/>
</instance>
<instance part="JP6" gate="A" x="-129.54" y="-160.02" smashed="yes">
<attribute name="VALUE" x="-135.89" y="-172.72" size="1.778" layer="96"/>
</instance>
<instance part="U$6" gate="G$1" x="-302.26" y="-109.22" smashed="yes"/>
<instance part="U$7" gate="G$1" x="-10.16" y="-231.14" smashed="yes">
<attribute name="NAME" x="-6.858" y="-219.71" size="1.27" layer="94"/>
<attribute name="VALUE" x="-8.128" y="-243.586" size="1.27" layer="94"/>
</instance>
<instance part="R6_1.2K" gate="G$1" x="-43.18" y="-228.6" smashed="yes">
<attribute name="NAME" x="-46.99" y="-227.1014" size="1.778" layer="95"/>
<attribute name="VALUE" x="-46.99" y="-231.902" size="1.778" layer="96"/>
</instance>
<instance part="C4_10UF" gate="G$1" x="10.16" y="-248.92" smashed="yes" rot="R180">
<attribute name="NAME" x="13.081" y="-247.904" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="8.636" y="-244.221" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C8_10UF" gate="G$1" x="-30.48" y="-248.92" smashed="yes" rot="R180">
<attribute name="NAME" x="-27.559" y="-247.904" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-32.004" y="-244.221" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="ONE" gate="1" x="106.68" y="-236.22" smashed="yes">
<attribute name="NAME" x="109.22" y="-238.76" size="1.778" layer="95"/>
<attribute name="VALUE" x="104.14" y="-240.03" size="1.778" layer="96"/>
</instance>
<instance part="IRF1" gate="2" x="-25.4" y="-170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="-22.86" y="-170.18" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="-22.86" y="-170.18" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="IRF1" gate="1" x="-33.02" y="-175.26" smashed="yes">
<attribute name="VALUE" x="-30.48" y="-175.26" size="1.778" layer="96"/>
<attribute name="NAME" x="-45.72" y="-172.72" size="1.778" layer="95"/>
</instance>
<instance part="R1_100K1" gate="G$1" x="-33.02" y="-160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="-34.5186" y="-163.83" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-29.718" y="-163.83" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R0_100K3" gate="G$1" x="-40.64" y="-185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-42.1386" y="-189.23" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-37.338" y="-189.23" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IRF4" gate="2" x="40.64" y="-170.18" smashed="yes" rot="MR180">
<attribute name="VALUE" x="43.18" y="-170.18" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="43.18" y="-170.18" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="IRF4" gate="1" x="33.02" y="-175.26" smashed="yes">
<attribute name="VALUE" x="35.56" y="-175.26" size="1.778" layer="96"/>
<attribute name="NAME" x="20.32" y="-172.72" size="1.778" layer="95"/>
</instance>
<instance part="R1_100K4" gate="G$1" x="33.02" y="-160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="31.5214" y="-163.83" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.322" y="-163.83" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R0_100K6" gate="G$1" x="25.4" y="-185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="23.9014" y="-189.23" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="28.702" y="-189.23" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R0_100K2" gate="G$1" x="-96.52" y="-231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="-98.0186" y="-234.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-93.218" y="-234.95" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R0_100K1" gate="G$1" x="-96.52" y="-210.82" smashed="yes" rot="R270">
<attribute name="NAME" x="-95.0214" y="-207.01" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-99.822" y="-207.01" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$3" gate="G$1" x="-198.12" y="-309.88" smashed="yes" rot="R180"/>
<instance part="ONE2" gate="1" x="50.8" y="-165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="45.72" y="-160.02" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="46.99" y="-162.56" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="ONE3" gate="1" x="-15.24" y="-165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-12.7" y="-162.56" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-11.43" y="-167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED1" gate="G$1" x="30.48" y="-228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="25.908" y="-232.156" size="1.778" layer="95"/>
<attribute name="VALUE" x="25.908" y="-234.315" size="1.778" layer="96"/>
</instance>
<instance part="R6_1.2K1" gate="G$1" x="15.24" y="-228.6" smashed="yes" rot="R180">
<attribute name="NAME" x="19.05" y="-230.0986" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="19.05" y="-225.298" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LS1" gate="G$1" x="-27.94" y="-320.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-32.766" y="-322.58" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-27.94" y="-314.706" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="Q2" gate="G$1" x="-116.84" y="-307.34" smashed="yes">
<attribute name="NAME" x="-114.3" y="-307.34" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-114.3" y="-309.626" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R5_10K1" gate="G$1" x="-81.28" y="-294.64" smashed="yes" rot="R180">
<attribute name="NAME" x="-77.47" y="-296.1386" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-77.47" y="-291.338" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="NEGATIVE" gate="1" x="-101.6" y="-284.48" smashed="yes" rot="R270">
<attribute name="NAME" x="-99.7458" y="-283.337" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-104.902" y="-283.337" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="POSITIVE" gate="1" x="-96.52" y="-284.48" smashed="yes" rot="R270">
<attribute name="NAME" x="-94.6658" y="-283.337" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-99.822" y="-283.337" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D14" gate="G$1" x="-294.64" y="-297.18" smashed="yes" rot="R90">
<attribute name="NAME" x="-299.72" y="-297.18" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="-289.56" y="-297.18" size="1.778" layer="95" rot="R90" align="center"/>
</instance>
<instance part="Q1" gate="G$1" x="-297.18" y="-317.5" smashed="yes">
<attribute name="NAME" x="-294.64" y="-317.5" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-294.64" y="-319.786" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R5_10K3" gate="G$1" x="-309.88" y="-302.26" smashed="yes" rot="R90">
<attribute name="NAME" x="-311.3786" y="-306.07" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-306.578" y="-306.07" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C2_10UF2" gate="G$1" x="-281.94" y="-297.18" smashed="yes" rot="R180">
<attribute name="NAME" x="-279.019" y="-296.164" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-283.464" y="-292.481" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R5_10K4" gate="G$1" x="-259.08" y="-284.48" smashed="yes" rot="R180">
<attribute name="NAME" x="-255.27" y="-285.9786" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-255.27" y="-281.178" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+" gate="1" x="-259.08" y="-304.8" smashed="yes" rot="R180">
<attribute name="NAME" x="-257.937" y="-306.6542" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-257.937" y="-301.498" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="-" gate="1" x="-259.08" y="-307.34" smashed="yes" rot="R180">
<attribute name="NAME" x="-257.937" y="-309.1942" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-257.937" y="-304.038" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X1" gate="G$1" x="63.5" y="-302.26" smashed="yes" rot="R180">
<attribute name="NAME" x="69.85" y="-307.975" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="69.85" y="-297.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="-269.24" y="-142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-269.24" y="-147.32" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="-269.24" y="-137.16" size="1.778" layer="95" rot="R180" align="center"/>
</instance>
<instance part="R5_10K2" gate="G$1" x="-302.26" y="-223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-303.7586" y="-227.33" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-298.958" y="-227.33" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="A1" gate="G$1" x="-302.26" y="-243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="-304.8" y="-246.38" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-299.72" y="-246.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C2_10UF1" gate="G$1" x="-289.56" y="-246.38" smashed="yes" rot="R180">
<attribute name="NAME" x="-286.639" y="-245.364" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-291.084" y="-241.681" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="A11" gate="G$1" x="-266.7" y="-241.3" smashed="yes" rot="R90">
<attribute name="NAME" x="-269.24" y="-243.84" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-264.16" y="-243.84" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3V" class="0">
<segment>
<wire x1="-124.46" y1="-111.76" x2="-116.84" y2="-111.76" width="0.1524" layer="91"/>
<label x="-116.84" y="-111.76" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="18"/>
</segment>
<segment>
<wire x1="-124.46" y1="-91.44" x2="-116.84" y2="-91.44" width="0.1524" layer="91"/>
<label x="-116.84" y="-91.44" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="2"/>
</segment>
<segment>
<label x="-325.12" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="-317.5" y1="-73.66" x2="-322.58" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="-73.66" x2="-325.12" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="-68.58" x2="-322.58" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-322.58" y="-73.66"/>
<pinref part="C1_100NF" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-279.4" y="-55.88" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-279.4" y1="-58.42" x2="-279.4" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R3_10K" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="-266.7" y="-55.88" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-266.7" y1="-58.42" x2="-266.7" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R4_10K" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="53.34" y="-154.94" size="1.778" layer="95" xref="yes"/>
<wire x1="33.02" y1="-154.94" x2="40.64" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="IRF4" gate="2" pin="S"/>
<wire x1="40.64" y1="-154.94" x2="40.64" y2="-165.1" width="0.1524" layer="91"/>
<junction x="40.64" y="-154.94"/>
<wire x1="40.64" y1="-154.94" x2="50.8" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="ONE2" gate="1" pin="1"/>
<wire x1="50.8" y1="-154.94" x2="53.34" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-160.02" x2="50.8" y2="-154.94" width="0.1524" layer="91"/>
<junction x="50.8" y="-154.94"/>
<pinref part="R1_100K4" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-193.04" y1="-109.22" x2="-213.36" y2="-109.22" width="0.1524" layer="91"/>
<label x="-193.04" y="-109.22" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="-152.4" y1="-190.5" x2="-157.48" y2="-190.5" width="0.1524" layer="91"/>
<label x="-152.4" y="-190.5" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="3V3"/>
</segment>
<segment>
<wire x1="-132.08" y1="-160.02" x2="-147.32" y2="-160.02" width="0.1524" layer="91"/>
<label x="-147.32" y="-160.02" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP6" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="-213.36" y1="-78.74" x2="-208.28" y2="-78.74" width="0.1524" layer="91"/>
<label x="-208.28" y="-78.74" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="6"/>
</segment>
<segment>
<wire x1="-66.04" y1="-294.64" x2="-76.2" y2="-294.64" width="0.1524" layer="91"/>
<label x="-66.04" y="-294.64" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="R5_10K1" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-332.74" y="-312.42" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-254" y1="-284.48" x2="-248.92" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="-284.48" x2="-248.92" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="-332.74" x2="-332.74" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="-332.74" x2="-332.74" y2="-312.42" width="0.1524" layer="91"/>
<pinref part="R5_10K4" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-256.54" y="-142.24" size="1.778" layer="95" xref="yes"/>
<wire x1="-256.54" y1="-142.24" x2="-264.16" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="A"/>
</segment>
<segment>
<label x="-302.26" y="-215.9" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-302.26" y1="-218.44" x2="-302.26" y2="-215.9" width="0.1524" layer="91"/>
<pinref part="R5_10K2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="IRF7319" gate="2" pin="D"/>
<wire x1="134.62" y1="-175.26" x2="134.62" y2="-200.66" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-200.66" x2="93.98" y2="-200.66" width="0.1524" layer="91"/>
<label x="93.98" y="-200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-132.08" y1="-91.44" x2="-139.7" y2="-91.44" width="0.1524" layer="91"/>
<label x="-139.7" y="-91.44" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-132.08" y1="-93.98" x2="-152.4" y2="-93.98" width="0.1524" layer="91"/>
<label x="-152.4" y="-93.98" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="3"/>
</segment>
<segment>
<label x="-12.7" y="-154.94" size="1.778" layer="95" xref="yes"/>
<wire x1="-33.02" y1="-154.94" x2="-25.4" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="IRF1" gate="2" pin="S"/>
<wire x1="-25.4" y1="-154.94" x2="-25.4" y2="-165.1" width="0.1524" layer="91"/>
<junction x="-25.4" y="-154.94"/>
<wire x1="-25.4" y1="-154.94" x2="-15.24" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="ONE3" gate="1" pin="2"/>
<wire x1="-15.24" y1="-154.94" x2="-12.7" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-160.02" x2="-15.24" y2="-154.94" width="0.1524" layer="91"/>
<junction x="-15.24" y="-154.94"/>
<pinref part="R1_100K1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-213.36" y1="-91.44" x2="-187.96" y2="-91.44" width="0.1524" layer="91"/>
<label x="-187.96" y="-91.44" size="1.778" layer="95" xref="yes"/>
<junction x="-213.36" y="-91.44"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-132.08" y1="-60.96" x2="-139.7" y2="-60.96" width="0.1524" layer="91"/>
<label x="-139.7" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
<segment>
<label x="-114.3" y="-53.34" size="1.778" layer="95" xref="yes"/>
<wire x1="-124.46" y1="-53.34" x2="-114.3" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="99.06" y1="-193.04" x2="106.68" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="IRF7319" gate="1" pin="S"/>
<wire x1="106.68" y1="-193.04" x2="106.68" y2="-182.88" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-193.04" x2="93.98" y2="-193.04" width="0.1524" layer="91"/>
<junction x="99.06" y="-193.04"/>
<wire x1="142.24" y1="-193.04" x2="119.38" y2="-193.04" width="0.1524" layer="91"/>
<junction x="106.68" y="-193.04"/>
<pinref part="POW" gate="G$1" pin="1"/>
<wire x1="119.38" y1="-193.04" x2="106.68" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-190.5" x2="119.38" y2="-193.04" width="0.1524" layer="91"/>
<junction x="119.38" y="-193.04"/>
<label x="93.98" y="-193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<label x="142.24" y="-193.04" size="1.778" layer="95" xref="yes"/>
<pinref part="R0_100K" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-269.24" y="-91.44" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-269.24" y1="-86.36" x2="-269.24" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="C2_100NF" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-279.4" y="-78.74" size="1.778" layer="95" xref="yes"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="-287.02" y1="-78.74" x2="-279.4" y2="-78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-322.58" y="-55.88" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-322.58" y1="-60.96" x2="-322.58" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C1_100NF" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-276.86" y1="-106.68" x2="-292.1" y2="-106.68" width="0.1524" layer="91"/>
<label x="-276.86" y="-106.68" size="1.778" layer="95" xref="yes"/>
<pinref part="U$6" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-177.8" y1="-320.04" x2="-170.18" y2="-320.04" width="0.1524" layer="91"/>
<label x="-170.18" y="-320.04" size="1.778" layer="95" xref="yes"/>
<pinref part="U$3" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-132.08" y1="-106.68" x2="-139.7" y2="-106.68" width="0.1524" layer="91"/>
<label x="-139.7" y="-106.68" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="13"/>
</segment>
<segment>
<label x="-152.4" y="-114.3" size="1.778" layer="95" rot="MR0" xref="yes"/>
<wire x1="-132.08" y1="-114.3" x2="-152.4" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="19"/>
</segment>
<segment>
<wire x1="-132.08" y1="-127" x2="-139.7" y2="-127" width="0.1524" layer="91"/>
<label x="-139.7" y="-127" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="29"/>
</segment>
<segment>
<wire x1="-132.08" y1="-132.08" x2="-139.7" y2="-132.08" width="0.1524" layer="91"/>
<label x="-139.7" y="-132.08" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="33"/>
</segment>
<segment>
<wire x1="-132.08" y1="-96.52" x2="-139.7" y2="-96.52" width="0.1524" layer="91"/>
<label x="-139.7" y="-96.52" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="-124.46" y1="-121.92" x2="-116.84" y2="-121.92" width="0.1524" layer="91"/>
<label x="-116.84" y="-121.92" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="26"/>
</segment>
<segment>
<wire x1="-124.46" y1="-139.7" x2="-106.68" y2="-139.7" width="0.1524" layer="91"/>
<label x="-106.68" y="-139.7" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="40"/>
</segment>
<segment>
<wire x1="-124.46" y1="-101.6" x2="-116.84" y2="-101.6" width="0.1524" layer="91"/>
<label x="-116.84" y="-101.6" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="10"/>
</segment>
<segment>
<wire x1="114.3" y1="-223.52" x2="91.44" y2="-223.52" width="0.1524" layer="91"/>
<label x="114.3" y="-223.52" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="OUT-"/>
</segment>
<segment>
<wire x1="99.06" y1="-231.14" x2="91.44" y2="-231.14" width="0.1524" layer="91"/>
<label x="99.06" y="-231.14" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="VIN-"/>
</segment>
<segment>
<wire x1="114.3" y1="-228.6" x2="91.44" y2="-228.6" width="0.1524" layer="91"/>
<label x="114.3" y="-228.6" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="BAT-"/>
</segment>
<segment>
<label x="-279.4" y="-185.42" size="1.778" layer="95" xref="yes"/>
<wire x1="-281.94" y1="-185.42" x2="-279.4" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="R4_5.1K3" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="-259.08" y="-187.96" size="1.778" layer="95" xref="yes"/>
<wire x1="-261.62" y1="-187.96" x2="-259.08" y2="-187.96" width="0.1524" layer="91"/>
<pinref part="R4_5.1K4" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-289.56" y1="-195.58" x2="-299.72" y2="-195.58" width="0.1524" layer="91"/>
<label x="-289.56" y="-195.58" size="1.778" layer="95" xref="yes"/>
<pinref part="J4" gate="J1" pin="SHLD"/>
</segment>
<segment>
<wire x1="-157.48" y1="-254" x2="-152.4" y2="-254" width="0.1524" layer="91"/>
<label x="-152.4" y="-254" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-157.48" y1="-251.46" x2="-132.08" y2="-251.46" width="0.1524" layer="91"/>
<label x="-132.08" y="-251.46" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="EXP"/>
</segment>
<segment>
<wire x1="-213.36" y1="-127" x2="-208.28" y2="-127" width="0.1524" layer="91"/>
<label x="-208.28" y="-127" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="10"/>
</segment>
<segment>
<wire x1="-281.94" y1="-198.12" x2="-299.72" y2="-198.12" width="0.1524" layer="91"/>
<label x="-281.94" y="-198.12" size="1.778" layer="95" xref="yes"/>
<pinref part="J4" gate="J1" pin="GND"/>
</segment>
<segment>
<wire x1="-50.8" y1="-228.6" x2="-48.26" y2="-228.6" width="0.1524" layer="91"/>
<label x="-50.8" y="-228.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R6_1.2K" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-27.94" y1="-233.68" x2="-22.86" y2="-233.68" width="0.1524" layer="91"/>
<label x="-27.94" y="-233.68" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$7" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-27.94" y1="-223.52" x2="-22.86" y2="-223.52" width="0.1524" layer="91"/>
<label x="-27.94" y="-223.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U$7" gate="G$1" pin="TEMP"/>
</segment>
<segment>
<label x="10.16" y="-254" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="10.16" y1="-251.46" x2="10.16" y2="-254" width="0.1524" layer="91"/>
<pinref part="C4_10UF" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-30.48" y="-254" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-30.48" y1="-251.46" x2="-30.48" y2="-254" width="0.1524" layer="91"/>
<pinref part="C8_10UF" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-45.72" y1="-193.04" x2="-40.64" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="IRF1" gate="1" pin="S"/>
<wire x1="-40.64" y1="-193.04" x2="-33.02" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-193.04" x2="-33.02" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-193.04" x2="-33.02" y2="-193.04" width="0.1524" layer="91"/>
<junction x="-33.02" y="-193.04"/>
<label x="-45.72" y="-193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<label x="-15.24" y="-193.04" size="1.778" layer="95" xref="yes"/>
<wire x1="-40.64" y1="-190.5" x2="-40.64" y2="-193.04" width="0.1524" layer="91"/>
<junction x="-40.64" y="-193.04"/>
<pinref part="R0_100K3" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="20.32" y1="-193.04" x2="25.4" y2="-193.04" width="0.1524" layer="91"/>
<pinref part="IRF4" gate="1" pin="S"/>
<wire x1="25.4" y1="-193.04" x2="33.02" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-193.04" x2="33.02" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-193.04" x2="33.02" y2="-193.04" width="0.1524" layer="91"/>
<junction x="33.02" y="-193.04"/>
<label x="20.32" y="-193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<label x="50.8" y="-193.04" size="1.778" layer="95" xref="yes"/>
<wire x1="25.4" y1="-190.5" x2="25.4" y2="-193.04" width="0.1524" layer="91"/>
<junction x="25.4" y="-193.04"/>
<pinref part="R0_100K6" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-96.52" y1="-236.22" x2="-96.52" y2="-238.76" width="0.1524" layer="91"/>
<label x="-96.52" y="-238.76" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="R0_100K2" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-116.84" y="-160.02" size="1.778" layer="95" xref="yes"/>
<wire x1="-124.46" y1="-160.02" x2="-116.84" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="JP6" gate="A" pin="6"/>
</segment>
<segment>
<label x="-294.64" y="-160.02" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-160.02" x2="-294.64" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="SHLD"/>
</segment>
<segment>
<label x="-17.78" y="-317.5" size="1.778" layer="95" xref="yes"/>
<wire x1="-25.4" y1="-317.5" x2="-17.78" y2="-317.5" width="0.1524" layer="91"/>
<pinref part="LS1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-213.36" y1="-81.28" x2="-187.96" y2="-81.28" width="0.1524" layer="91"/>
<label x="-187.96" y="-81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="5"/>
</segment>
<segment>
<label x="-114.3" y="-322.58" size="1.778" layer="95" rot="MR270" xref="yes"/>
<wire x1="-114.3" y1="-312.42" x2="-114.3" y2="-322.58" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="E"/>
</segment>
<segment>
<label x="-322.58" y="-309.88" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-322.58" y1="-309.88" x2="-322.58" y2="-330.2" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="-330.2" x2="-294.64" y2="-330.2" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="E"/>
<wire x1="-294.64" y1="-330.2" x2="-294.64" y2="-322.58" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-109.22" y="-162.56" size="1.778" layer="95" xref="yes"/>
<wire x1="-124.46" y1="-162.56" x2="-109.22" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="JP6" gate="A" pin="8"/>
</segment>
<segment>
<label x="-281.94" y="-162.56" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-162.56" x2="-281.94" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="GND"/>
</segment>
<segment>
<wire x1="91.44" y1="-304.8" x2="66.04" y2="-304.8" width="0.1524" layer="91"/>
<label x="91.44" y="-304.8" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="66.04" y1="-289.56" x2="66.04" y2="-294.64" width="0.1524" layer="91"/>
<label x="66.04" y="-289.56" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="X1" gate="G$1" pin="NC2"/>
</segment>
<segment>
<wire x1="-302.26" y1="-248.92" x2="-302.26" y2="-254" width="0.1524" layer="91"/>
<label x="-302.26" y="-254" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="A1" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="-289.56" y="-254" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="-289.56" y1="-248.92" x2="-289.56" y2="-254" width="0.1524" layer="91"/>
<pinref part="C2_10UF1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-266.7" y1="-246.38" x2="-266.7" y2="-254" width="0.1524" layer="91"/>
<label x="-266.7" y="-254" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="A11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="LDR" class="0">
<segment>
<wire x1="-132.08" y1="-208.28" x2="-157.48" y2="-208.28" width="0.1524" layer="91"/>
<label x="-132.08" y="-208.28" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO17/DAC1"/>
</segment>
<segment>
<wire x1="-213.36" y1="-58.42" x2="-208.28" y2="-58.42" width="0.1524" layer="91"/>
<label x="-208.28" y="-58.42" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="J1" gate="G$1" pin="14"/>
</segment>
</net>
<net name="P_SCL" class="0">
<segment>
<wire x1="-114.3" y1="-58.42" x2="-124.46" y2="-58.42" width="0.1524" layer="91"/>
<label x="-114.3" y="-58.42" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<label x="-259.08" y="-71.12" size="1.778" layer="95" xref="yes"/>
<wire x1="-287.02" y1="-71.12" x2="-266.7" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-71.12" x2="-259.08" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-68.58" x2="-266.7" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-266.7" y="-71.12"/>
<pinref part="R4_10K" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-177.8" y1="-314.96" x2="-170.18" y2="-314.96" width="0.1524" layer="91"/>
<label x="-170.18" y="-314.96" size="1.778" layer="95" xref="yes"/>
<pinref part="U$3" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="-124.46" y1="-96.52" x2="-116.84" y2="-96.52" width="0.1524" layer="91"/>
<label x="-116.84" y="-96.52" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="6"/>
</segment>
<segment>
<label x="-294.64" y="-149.86" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-149.86" x2="-294.64" y2="-149.86" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="CC1"/>
</segment>
<segment>
<label x="-281.94" y="-152.4" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-152.4" x2="-281.94" y2="-152.4" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="CC2"/>
</segment>
</net>
<net name="PRX" class="0">
<segment>
<wire x1="-114.3" y1="-63.5" x2="-124.46" y2="-63.5" width="0.1524" layer="91"/>
<label x="-114.3" y="-63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
<segment>
<wire x1="-132.08" y1="-101.6" x2="-139.7" y2="-101.6" width="0.1524" layer="91"/>
<label x="-139.7" y="-101.6" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="9"/>
</segment>
<segment>
<label x="-294.64" y="-144.78" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-144.78" x2="-294.64" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="D+"/>
</segment>
</net>
<net name="GP27" class="0">
<segment>
<wire x1="-149.86" y1="-63.5" x2="-132.08" y2="-63.5" width="0.1524" layer="91"/>
<label x="-149.86" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
<segment>
<wire x1="-124.46" y1="-106.68" x2="-116.84" y2="-106.68" width="0.1524" layer="91"/>
<label x="-116.84" y="-106.68" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="14"/>
</segment>
</net>
<net name="GP22" class="0">
<segment>
<wire x1="-132.08" y1="-66.04" x2="-139.7" y2="-66.04" width="0.1524" layer="91"/>
<label x="-139.7" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="11"/>
</segment>
<segment>
<wire x1="-124.46" y1="-109.22" x2="-106.68" y2="-109.22" width="0.1524" layer="91"/>
<label x="-106.68" y="-109.22" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="16"/>
</segment>
</net>
<net name="ID_SD" class="0">
<segment>
<wire x1="-124.46" y1="-68.58" x2="-114.3" y2="-68.58" width="0.1524" layer="91"/>
<label x="-114.3" y="-68.58" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="14"/>
</segment>
<segment>
<wire x1="-124.46" y1="-124.46" x2="-106.68" y2="-124.46" width="0.1524" layer="91"/>
<label x="-106.68" y="-124.46" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="28"/>
</segment>
</net>
<net name="PTX" class="0">
<segment>
<wire x1="-104.14" y1="-60.96" x2="-124.46" y2="-60.96" width="0.1524" layer="91"/>
<label x="-104.14" y="-60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
<segment>
<wire x1="-152.4" y1="-99.06" x2="-132.08" y2="-99.06" width="0.1524" layer="91"/>
<label x="-152.4" y="-99.06" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="7"/>
</segment>
<segment>
<label x="-281.94" y="-147.32" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-147.32" x2="-281.94" y2="-147.32" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="D-"/>
</segment>
</net>
<net name="GP23" class="0">
<segment>
<wire x1="-104.14" y1="-66.04" x2="-124.46" y2="-66.04" width="0.1524" layer="91"/>
<label x="-104.14" y="-66.04" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="12"/>
</segment>
<segment>
<wire x1="-132.08" y1="-109.22" x2="-152.4" y2="-109.22" width="0.1524" layer="91"/>
<label x="-152.4" y="-109.22" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="15"/>
</segment>
</net>
<net name="ID_SC" class="0">
<segment>
<wire x1="-104.14" y1="-71.12" x2="-124.46" y2="-71.12" width="0.1524" layer="91"/>
<label x="-104.14" y="-71.12" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="16"/>
</segment>
<segment>
<wire x1="-132.08" y1="-124.46" x2="-152.4" y2="-124.46" width="0.1524" layer="91"/>
<label x="-152.4" y="-124.46" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="27"/>
</segment>
</net>
<net name="GP19" class="0">
<segment>
<wire x1="-124.46" y1="-134.62" x2="-106.68" y2="-134.62" width="0.1524" layer="91"/>
<label x="-106.68" y="-134.62" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="36"/>
</segment>
<segment>
<wire x1="-213.36" y1="-55.88" x2="-187.96" y2="-55.88" width="0.1524" layer="91"/>
<label x="-187.96" y="-55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="15"/>
</segment>
</net>
<net name="GP26" class="0">
<segment>
<wire x1="-116.84" y1="-137.16" x2="-124.46" y2="-137.16" width="0.1524" layer="91"/>
<label x="-116.84" y="-137.16" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="38"/>
</segment>
<segment>
<wire x1="-157.48" y1="-243.84" x2="-132.08" y2="-243.84" width="0.1524" layer="91"/>
<label x="-132.08" y="-243.84" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO41/TDI"/>
</segment>
</net>
<net name="GP6" class="0">
<segment>
<wire x1="-104.14" y1="-76.2" x2="-124.46" y2="-76.2" width="0.1524" layer="91"/>
<label x="-104.14" y="-76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="20"/>
</segment>
<segment>
<wire x1="-124.46" y1="-129.54" x2="-106.68" y2="-129.54" width="0.1524" layer="91"/>
<label x="-106.68" y="-129.54" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="32"/>
</segment>
</net>
<net name="GP16" class="0">
<segment>
<wire x1="-149.86" y1="-73.66" x2="-132.08" y2="-73.66" width="0.1524" layer="91"/>
<label x="-149.86" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="17"/>
</segment>
<segment>
<wire x1="-152.4" y1="-134.62" x2="-132.08" y2="-134.62" width="0.1524" layer="91"/>
<label x="-152.4" y="-134.62" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="35"/>
</segment>
</net>
<net name="GP13" class="0">
<segment>
<wire x1="-132.08" y1="-76.2" x2="-139.7" y2="-76.2" width="0.1524" layer="91"/>
<label x="-139.7" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="19"/>
</segment>
<segment>
<wire x1="-124.46" y1="-132.08" x2="-116.84" y2="-132.08" width="0.1524" layer="91"/>
<label x="-116.84" y="-132.08" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="34"/>
</segment>
</net>
<net name="GP4" class="0">
<segment>
<wire x1="-132.08" y1="-58.42" x2="-149.86" y2="-58.42" width="0.1524" layer="91"/>
<label x="-149.86" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="-124.46" y1="-99.06" x2="-106.68" y2="-99.06" width="0.1524" layer="91"/>
<label x="-106.68" y="-99.06" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="8"/>
</segment>
</net>
<net name="P_SDA" class="0">
<segment>
<wire x1="-139.7" y1="-55.88" x2="-132.08" y2="-55.88" width="0.1524" layer="91"/>
<label x="-139.7" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="-287.02" y1="-73.66" x2="-279.4" y2="-73.66" width="0.1524" layer="91"/>
<label x="-271.78" y="-73.66" size="1.778" layer="95" xref="yes"/>
<wire x1="-279.4" y1="-73.66" x2="-271.78" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="-73.66" x2="-279.4" y2="-68.58" width="0.1524" layer="91"/>
<junction x="-279.4" y="-73.66"/>
<pinref part="R3_10K" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-177.8" y1="-309.88" x2="-170.18" y2="-309.88" width="0.1524" layer="91"/>
<label x="-170.18" y="-309.88" size="1.778" layer="95" xref="yes"/>
<pinref part="U$3" gate="G$1" pin="SDA"/>
</segment>
<segment>
<wire x1="-124.46" y1="-93.98" x2="-106.68" y2="-93.98" width="0.1524" layer="91"/>
<label x="-106.68" y="-93.98" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="4"/>
</segment>
<segment>
<label x="-284.48" y="-157.48" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-157.48" x2="-284.48" y2="-157.48" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="SBU2"/>
</segment>
<segment>
<label x="-294.64" y="-154.94" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-154.94" x2="-294.64" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="J3" gate="J1" pin="SBU1"/>
</segment>
</net>
<net name="GP12" class="0">
<segment>
<wire x1="-139.7" y1="-71.12" x2="-132.08" y2="-71.12" width="0.1524" layer="91"/>
<label x="-139.7" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="15"/>
</segment>
<segment>
<wire x1="-132.08" y1="-129.54" x2="-152.4" y2="-129.54" width="0.1524" layer="91"/>
<label x="-152.4" y="-129.54" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="31"/>
</segment>
</net>
<net name="GP5" class="0">
<segment>
<wire x1="-114.3" y1="-73.66" x2="-124.46" y2="-73.66" width="0.1524" layer="91"/>
<label x="-114.3" y="-73.66" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="18"/>
</segment>
<segment>
<wire x1="-124.46" y1="-127" x2="-116.84" y2="-127" width="0.1524" layer="91"/>
<label x="-116.84" y="-127" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="30"/>
</segment>
</net>
<net name="GP21" class="0">
<segment>
<wire x1="-132.08" y1="-139.7" x2="-152.4" y2="-139.7" width="0.1524" layer="91"/>
<label x="-152.4" y="-139.7" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="39"/>
</segment>
<segment>
<wire x1="-119.38" y1="-307.34" x2="-127" y2="-307.34" width="0.1524" layer="91"/>
<label x="-127" y="-307.34" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="Q2" gate="G$1" pin="B"/>
</segment>
</net>
<net name="GP20" class="0">
<segment>
<wire x1="-132.08" y1="-137.16" x2="-139.7" y2="-137.16" width="0.1524" layer="91"/>
<label x="-139.7" y="-137.16" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="37"/>
</segment>
<segment>
<wire x1="-213.36" y1="-53.34" x2="-208.28" y2="-53.34" width="0.1524" layer="91"/>
<label x="-208.28" y="-53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="16"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="4"/>
<pinref part="C3" gate="G$1" pin="3"/>
<wire x1="-40.64" y1="-106.68" x2="-33.02" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="4"/>
<pinref part="C4" gate="G$1" pin="3"/>
<wire x1="-22.86" y1="-106.68" x2="-15.24" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="4"/>
<pinref part="C5" gate="G$1" pin="3"/>
<wire x1="-5.08" y1="-106.68" x2="2.54" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="4"/>
<pinref part="C6" gate="G$1" pin="3"/>
<wire x1="12.7" y1="-106.68" x2="20.32" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="4"/>
<pinref part="C7" gate="G$1" pin="3"/>
<wire x1="30.48" y1="-106.68" x2="38.1" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="4"/>
<pinref part="C8" gate="G$1" pin="3"/>
<wire x1="48.26" y1="-106.68" x2="55.88" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="4"/>
<pinref part="C9" gate="G$1" pin="3"/>
<wire x1="66.04" y1="-106.68" x2="73.66" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="4"/>
<pinref part="D2" gate="G$1" pin="3"/>
<wire x1="-55.88" y1="-121.92" x2="-50.8" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="4"/>
<pinref part="D3" gate="G$1" pin="3"/>
<wire x1="-40.64" y1="-121.92" x2="-33.02" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="4"/>
<pinref part="D4" gate="G$1" pin="3"/>
<wire x1="-22.86" y1="-121.92" x2="-15.24" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="4"/>
<pinref part="D5" gate="G$1" pin="3"/>
<wire x1="-5.08" y1="-121.92" x2="2.54" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="4"/>
<pinref part="D6" gate="G$1" pin="3"/>
<wire x1="12.7" y1="-121.92" x2="20.32" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="4"/>
<pinref part="D7" gate="G$1" pin="3"/>
<wire x1="30.48" y1="-121.92" x2="38.1" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="4"/>
<pinref part="D8" gate="G$1" pin="3"/>
<wire x1="48.26" y1="-121.92" x2="55.88" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="4"/>
<pinref part="D9" gate="G$1" pin="3"/>
<wire x1="66.04" y1="-121.92" x2="73.66" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="E1" gate="G$1" pin="4"/>
<pinref part="E2" gate="G$1" pin="3"/>
<wire x1="-55.88" y1="-139.7" x2="-50.8" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="E2" gate="G$1" pin="4"/>
<pinref part="E3" gate="G$1" pin="3"/>
<wire x1="-40.64" y1="-139.7" x2="-33.02" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="E3" gate="G$1" pin="4"/>
<pinref part="E4" gate="G$1" pin="3"/>
<wire x1="-22.86" y1="-139.7" x2="-15.24" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="E4" gate="G$1" pin="4"/>
<pinref part="E5" gate="G$1" pin="3"/>
<wire x1="-5.08" y1="-139.7" x2="2.54" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="E5" gate="G$1" pin="4"/>
<pinref part="E6" gate="G$1" pin="3"/>
<wire x1="12.7" y1="-139.7" x2="20.32" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="E6" gate="G$1" pin="4"/>
<pinref part="E7" gate="G$1" pin="3"/>
<wire x1="30.48" y1="-139.7" x2="38.1" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="E7" gate="G$1" pin="4"/>
<pinref part="E8" gate="G$1" pin="3"/>
<wire x1="48.26" y1="-139.7" x2="55.88" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="E8" gate="G$1" pin="4"/>
<pinref part="E9" gate="G$1" pin="3"/>
<wire x1="66.04" y1="-139.7" x2="71.12" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<label x="134.62" y="-78.74" size="1.778" layer="95" xref="yes"/>
<pinref part="B10" gate="G$1" pin="2"/>
<wire x1="134.62" y1="-78.74" x2="114.3" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-78.74" x2="93.98" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-78.74" x2="76.2" y2="-78.74" width="0.1524" layer="91"/>
<junction x="93.98" y="-78.74"/>
<pinref part="B9" gate="G$1" pin="2"/>
<pinref part="B8" gate="G$1" pin="2"/>
<wire x1="76.2" y1="-78.74" x2="58.42" y2="-78.74" width="0.1524" layer="91"/>
<junction x="76.2" y="-78.74"/>
<pinref part="B7" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-78.74" x2="40.64" y2="-78.74" width="0.1524" layer="91"/>
<junction x="58.42" y="-78.74"/>
<pinref part="B6" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-78.74" x2="22.86" y2="-78.74" width="0.1524" layer="91"/>
<junction x="40.64" y="-78.74"/>
<pinref part="B5" gate="G$1" pin="2"/>
<wire x1="22.86" y1="-78.74" x2="7.62" y2="-78.74" width="0.1524" layer="91"/>
<junction x="22.86" y="-78.74"/>
<wire x1="7.62" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="7.62" y="-78.74"/>
<pinref part="B4" gate="G$1" pin="2"/>
<pinref part="B3" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="-78.74" x2="-27.94" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-10.16" y="-78.74"/>
<pinref part="B2" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="-78.74" x2="-45.72" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-27.94" y="-78.74"/>
<pinref part="B11" gate="G$1" pin="2"/>
<junction x="114.3" y="-78.74"/>
<pinref part="B0" gate="A" pin="1"/>
<wire x1="-45.72" y1="-78.74" x2="-71.12" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-78.74" x2="-71.12" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-45.72" y="-78.74"/>
</segment>
<segment>
<wire x1="-231.14" y1="-215.9" x2="-213.36" y2="-215.9" width="0.1524" layer="91"/>
<label x="-231.14" y="-215.9" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO2"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="E11" gate="G$1" pin="4"/>
<wire x1="121.92" y1="-139.7" x2="134.62" y2="-139.7" width="0.1524" layer="91"/>
<label x="134.62" y="-139.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-243.84" x2="-218.44" y2="-243.84" width="0.1524" layer="91"/>
<label x="-218.44" y="-243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO13"/>
</segment>
</net>
<net name="1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="2"/>
<pinref part="E1" gate="G$1" pin="2"/>
<label x="-53.34" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="-53.34" y1="-111.76" x2="-53.34" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-101.6" x2="-53.34" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-88.9" x2="-53.34" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-111.76" x2="-53.34" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-129.54" x2="-53.34" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-111.76" x2="-53.34" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-53.34" y="-111.76"/>
<pinref part="B0" gate="A" pin="2"/>
<wire x1="-55.88" y1="-88.9" x2="-53.34" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-53.34" y="-88.9"/>
<pinref part="C0" gate="A" pin="2"/>
<wire x1="-55.88" y1="-101.6" x2="-53.34" y2="-101.6" width="0.1524" layer="91"/>
<junction x="-53.34" y="-101.6"/>
</segment>
<segment>
<wire x1="-218.44" y1="-228.6" x2="-213.36" y2="-228.6" width="0.1524" layer="91"/>
<label x="-218.44" y="-228.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO7"/>
</segment>
<segment>
<wire x1="-213.36" y1="-116.84" x2="-208.28" y2="-116.84" width="0.1524" layer="91"/>
<label x="-208.28" y="-116.84" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="6"/>
</segment>
</net>
<net name="4" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="2"/>
<pinref part="E4" gate="G$1" pin="2"/>
<pinref part="B4" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="-88.9" x2="-2.54" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A4" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="-71.12" x2="-2.54" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-71.12" x2="-2.54" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-2.54" y="-88.9"/>
<wire x1="-2.54" y1="-71.12" x2="-2.54" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-2.54" y="-71.12"/>
<label x="-2.54" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="-2.54" y1="-88.9" x2="-2.54" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-96.52" x2="-2.54" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-111.76" x2="-2.54" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-129.54" x2="-2.54" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-111.76" x2="-2.54" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-2.54" y="-111.76"/>
<wire x1="-5.08" y1="-96.52" x2="-2.54" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-2.54" y="-96.52"/>
</segment>
<segment>
<wire x1="-157.48" y1="-223.52" x2="-132.08" y2="-223.52" width="0.1524" layer="91"/>
<label x="-132.08" y="-223.52" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO33"/>
</segment>
</net>
<net name="5" class="0">
<segment>
<pinref part="E5" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="B5" gate="G$1" pin="1"/>
<wire x1="7.62" y1="-88.9" x2="15.24" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A5" gate="G$1" pin="1"/>
<wire x1="7.62" y1="-71.12" x2="15.24" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-71.12" x2="15.24" y2="-88.9" width="0.1524" layer="91"/>
<junction x="15.24" y="-88.9"/>
<wire x1="15.24" y1="-71.12" x2="15.24" y2="-50.8" width="0.1524" layer="91"/>
<junction x="15.24" y="-71.12"/>
<label x="15.24" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="15.24" y1="-88.9" x2="15.24" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-96.52" x2="15.24" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-111.76" x2="15.24" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-129.54" x2="15.24" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-111.76" x2="15.24" y2="-111.76" width="0.1524" layer="91"/>
<junction x="15.24" y="-111.76"/>
<wire x1="12.7" y1="-96.52" x2="15.24" y2="-96.52" width="0.1524" layer="91"/>
<junction x="15.24" y="-96.52"/>
</segment>
<segment>
<wire x1="-157.48" y1="-226.06" x2="-152.4" y2="-226.06" width="0.1524" layer="91"/>
<label x="-152.4" y="-226.06" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO34"/>
</segment>
</net>
<net name="6" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="2"/>
<pinref part="E6" gate="G$1" pin="2"/>
<pinref part="B6" gate="G$1" pin="1"/>
<wire x1="22.86" y1="-88.9" x2="33.02" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A6" gate="G$1" pin="1"/>
<wire x1="22.86" y1="-71.12" x2="33.02" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-71.12" x2="33.02" y2="-88.9" width="0.1524" layer="91"/>
<junction x="33.02" y="-88.9"/>
<wire x1="33.02" y1="-71.12" x2="33.02" y2="-50.8" width="0.1524" layer="91"/>
<junction x="33.02" y="-71.12"/>
<label x="33.02" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="33.02" y1="-88.9" x2="33.02" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-96.52" x2="33.02" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-111.76" x2="33.02" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-129.54" x2="33.02" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-111.76" x2="33.02" y2="-111.76" width="0.1524" layer="91"/>
<junction x="33.02" y="-111.76"/>
<wire x1="30.48" y1="-96.52" x2="33.02" y2="-96.52" width="0.1524" layer="91"/>
<junction x="33.02" y="-96.52"/>
</segment>
<segment>
<wire x1="-157.48" y1="-246.38" x2="-152.4" y2="-246.38" width="0.1524" layer="91"/>
<label x="-152.4" y="-246.38" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO42/TMS"/>
</segment>
</net>
<net name="7" class="0">
<segment>
<pinref part="E7" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="B7" gate="G$1" pin="1"/>
<wire x1="40.64" y1="-88.9" x2="50.8" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A7" gate="G$1" pin="1"/>
<wire x1="40.64" y1="-71.12" x2="50.8" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-71.12" x2="50.8" y2="-88.9" width="0.1524" layer="91"/>
<junction x="50.8" y="-88.9"/>
<wire x1="50.8" y1="-71.12" x2="50.8" y2="-50.8" width="0.1524" layer="91"/>
<junction x="50.8" y="-71.12"/>
<label x="50.8" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="50.8" y1="-88.9" x2="50.8" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-96.52" x2="50.8" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-111.76" x2="50.8" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-129.54" x2="50.8" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-111.76" x2="50.8" y2="-111.76" width="0.1524" layer="91"/>
<junction x="50.8" y="-111.76"/>
<wire x1="48.26" y1="-96.52" x2="50.8" y2="-96.52" width="0.1524" layer="91"/>
<junction x="50.8" y="-96.52"/>
</segment>
<segment>
<wire x1="-157.48" y1="-205.74" x2="-152.4" y2="-205.74" width="0.1524" layer="91"/>
<label x="-152.4" y="-205.74" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO16"/>
</segment>
</net>
<net name="9" class="0">
<segment>
<pinref part="E9" gate="G$1" pin="2"/>
<pinref part="D9" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="B9" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-88.9" x2="86.36" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A9" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-71.12" x2="86.36" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-71.12" x2="86.36" y2="-88.9" width="0.1524" layer="91"/>
<junction x="86.36" y="-88.9"/>
<wire x1="86.36" y1="-71.12" x2="86.36" y2="-50.8" width="0.1524" layer="91"/>
<junction x="86.36" y="-71.12"/>
<label x="86.36" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="81.28" y1="-129.54" x2="86.36" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-88.9" x2="86.36" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-96.52" x2="86.36" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-111.76" x2="86.36" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-111.76" x2="86.36" y2="-111.76" width="0.1524" layer="91"/>
<junction x="86.36" y="-111.76"/>
<wire x1="83.82" y1="-96.52" x2="86.36" y2="-96.52" width="0.1524" layer="91"/>
<junction x="86.36" y="-96.52"/>
</segment>
<segment>
<wire x1="-231.14" y1="-246.38" x2="-213.36" y2="-246.38" width="0.1524" layer="91"/>
<label x="-231.14" y="-246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO14"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="2"/>
<pinref part="E2" gate="G$1" pin="2"/>
<pinref part="B2" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="-88.9" x2="-38.1" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="-71.12" x2="-38.1" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-71.12" x2="-38.1" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-38.1" y="-88.9"/>
<wire x1="-38.1" y1="-71.12" x2="-38.1" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-38.1" y="-71.12"/>
<label x="-38.1" y="-50.8" size="1.778" layer="95" xref="yes"/>
<label x="-38.1" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="-38.1" y1="-88.9" x2="-38.1" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-96.52" x2="-38.1" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="-111.76" x2="-38.1" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-129.54" x2="-38.1" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-111.76" x2="-38.1" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-38.1" y="-111.76"/>
<wire x1="-40.64" y1="-96.52" x2="-38.1" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-38.1" y="-96.52"/>
</segment>
<segment>
<wire x1="-132.08" y1="-228.6" x2="-157.48" y2="-228.6" width="0.1524" layer="91"/>
<label x="-132.08" y="-228.6" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO35"/>
</segment>
</net>
<net name="3" class="0">
<segment>
<pinref part="E3" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="B3" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="-88.9" x2="-20.32" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A3" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="-71.12" x2="-20.32" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-71.12" x2="-20.32" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-20.32" y="-88.9"/>
<wire x1="-20.32" y1="-71.12" x2="-20.32" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-20.32" y="-71.12"/>
<label x="-20.32" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="-20.32" y1="-88.9" x2="-20.32" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-96.52" x2="-20.32" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-111.76" x2="-20.32" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-129.54" x2="-20.32" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-111.76" x2="-20.32" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-20.32" y="-111.76"/>
<wire x1="-22.86" y1="-96.52" x2="-20.32" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-20.32" y="-96.52"/>
</segment>
<segment>
<wire x1="-132.08" y1="-218.44" x2="-157.48" y2="-218.44" width="0.1524" layer="91"/>
<label x="-132.08" y="-218.44" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO21"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="A10" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-60.96" x2="134.62" y2="-60.96" width="0.1524" layer="91"/>
<junction x="93.98" y="-60.96"/>
<pinref part="A2" gate="G$1" pin="2"/>
<pinref part="A3" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="-60.96" x2="-45.72" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="A4" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="-60.96" x2="-10.16" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-27.94" y="-60.96"/>
<pinref part="A5" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="-60.96" x2="7.62" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-10.16" y="-60.96"/>
<pinref part="A6" gate="G$1" pin="2"/>
<wire x1="7.62" y1="-60.96" x2="22.86" y2="-60.96" width="0.1524" layer="91"/>
<junction x="7.62" y="-60.96"/>
<pinref part="A7" gate="G$1" pin="2"/>
<junction x="22.86" y="-60.96"/>
<pinref part="A8" gate="G$1" pin="2"/>
<wire x1="22.86" y1="-60.96" x2="40.64" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-60.96" x2="58.42" y2="-60.96" width="0.1524" layer="91"/>
<junction x="40.64" y="-60.96"/>
<pinref part="A9" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-60.96" x2="76.2" y2="-60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="-60.96"/>
<wire x1="76.2" y1="-60.96" x2="93.98" y2="-60.96" width="0.1524" layer="91"/>
<junction x="76.2" y="-60.96"/>
<label x="134.62" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-193.04" y1="-104.14" x2="-213.36" y2="-104.14" width="0.1524" layer="91"/>
<label x="-193.04" y="-104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-218.44" y1="-218.44" x2="-213.36" y2="-218.44" width="0.1524" layer="91"/>
<label x="-218.44" y="-218.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO3"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<label x="134.62" y="-106.68" size="1.778" layer="95" xref="yes"/>
<wire x1="119.38" y1="-106.68" x2="134.62" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="-213.36" y1="-238.76" x2="-218.44" y2="-238.76" width="0.1524" layer="91"/>
<label x="-218.44" y="-238.76" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO11"/>
</segment>
<segment>
<label x="-76.2" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-71.12" y1="-101.6" x2="-76.2" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="C0" gate="A" pin="1"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="4"/>
<wire x1="134.62" y1="-121.92" x2="121.92" y2="-121.92" width="0.1524" layer="91"/>
<label x="134.62" y="-121.92" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-213.36" y1="-241.3" x2="-231.14" y2="-241.3" width="0.1524" layer="91"/>
<label x="-231.14" y="-241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO12"/>
</segment>
</net>
<net name="8" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="D8" gate="G$1" pin="2"/>
<pinref part="E8" gate="G$1" pin="2"/>
<pinref part="B8" gate="G$1" pin="1"/>
<wire x1="58.42" y1="-88.9" x2="68.58" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A8" gate="G$1" pin="1"/>
<wire x1="68.58" y1="-88.9" x2="68.58" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-71.12" x2="58.42" y2="-71.12" width="0.1524" layer="91"/>
<junction x="68.58" y="-88.9"/>
<wire x1="68.58" y1="-71.12" x2="68.58" y2="-50.8" width="0.1524" layer="91"/>
<junction x="68.58" y="-71.12"/>
<label x="68.58" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="68.58" y1="-129.54" x2="68.58" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-111.76" x2="68.58" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-96.52" x2="68.58" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-96.52" x2="68.58" y2="-96.52" width="0.1524" layer="91"/>
<junction x="68.58" y="-96.52"/>
<wire x1="66.04" y1="-111.76" x2="68.58" y2="-111.76" width="0.1524" layer="91"/>
<junction x="68.58" y="-111.76"/>
<wire x1="66.04" y1="-129.54" x2="68.58" y2="-129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-152.4" y="-210.82" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO18/DAC2"/>
<wire x1="-152.4" y1="-210.82" x2="-157.48" y2="-210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="10" class="0">
<segment>
<pinref part="E10" gate="G$1" pin="2"/>
<pinref part="D10" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="B10" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-88.9" x2="104.14" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="A10" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-71.12" x2="104.14" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-71.12" x2="104.14" y2="-88.9" width="0.1524" layer="91"/>
<junction x="104.14" y="-88.9"/>
<wire x1="104.14" y1="-71.12" x2="104.14" y2="-50.8" width="0.1524" layer="91"/>
<junction x="104.14" y="-71.12"/>
<label x="104.14" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="101.6" y1="-129.54" x2="104.14" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-88.9" x2="104.14" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-96.52" x2="104.14" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-111.76" x2="104.14" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-111.76" x2="104.14" y2="-111.76" width="0.1524" layer="91"/>
<junction x="104.14" y="-111.76"/>
<wire x1="101.6" y1="-96.52" x2="104.14" y2="-96.52" width="0.1524" layer="91"/>
<junction x="104.14" y="-96.52"/>
</segment>
<segment>
<wire x1="-132.08" y1="-203.2" x2="-157.48" y2="-203.2" width="0.1524" layer="91"/>
<label x="-132.08" y="-203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO15"/>
</segment>
</net>
<net name="11" class="0">
<segment>
<pinref part="E11" gate="G$1" pin="2"/>
<pinref part="D11" gate="G$1" pin="2"/>
<label x="124.46" y="-50.8" size="1.778" layer="95" xref="yes"/>
<wire x1="124.46" y1="-111.76" x2="124.46" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-96.52" x2="124.46" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-88.9" x2="124.46" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-129.54" x2="124.46" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-111.76" x2="124.46" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-111.76" x2="124.46" y2="-111.76" width="0.1524" layer="91"/>
<junction x="124.46" y="-111.76"/>
<pinref part="B11" gate="G$1" pin="1"/>
<wire x1="114.3" y1="-88.9" x2="124.46" y2="-88.9" width="0.1524" layer="91"/>
<junction x="124.46" y="-88.9"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-96.52" x2="124.46" y2="-96.52" width="0.1524" layer="91"/>
<junction x="124.46" y="-96.52"/>
</segment>
<segment>
<wire x1="-231.14" y1="-226.06" x2="-213.36" y2="-226.06" width="0.1524" layer="91"/>
<label x="-231.14" y="-226.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO6"/>
</segment>
<segment>
<wire x1="-193.04" y1="-114.3" x2="-213.36" y2="-114.3" width="0.1524" layer="91"/>
<label x="-193.04" y="-114.3" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$256" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="4"/>
<wire x1="83.82" y1="-106.68" x2="91.44" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$26RE" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="4"/>
<wire x1="91.44" y1="-121.92" x2="83.82" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="WF" class="0">
<segment>
<pinref part="E9" gate="G$1" pin="4"/>
<pinref part="E10" gate="G$1" pin="3"/>
<wire x1="81.28" y1="-139.7" x2="91.44" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="WFEW" class="0">
<segment>
<pinref part="E10" gate="G$1" pin="4"/>
<pinref part="E11" gate="G$1" pin="3"/>
<wire x1="111.76" y1="-139.7" x2="101.6" y2="-139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EF2" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="4"/>
<pinref part="D11" gate="G$1" pin="3"/>
<wire x1="111.76" y1="-121.92" x2="101.6" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT+" class="0">
<segment>
<wire x1="99.06" y1="-226.06" x2="91.44" y2="-226.06" width="0.1524" layer="91"/>
<label x="99.06" y="-226.06" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="BAT+"/>
</segment>
<segment>
<wire x1="15.24" y1="-238.76" x2="10.16" y2="-238.76" width="0.1524" layer="91"/>
<label x="15.24" y="-238.76" size="1.778" layer="95" xref="yes"/>
<pinref part="U$7" gate="G$1" pin="BAT"/>
<wire x1="10.16" y1="-238.76" x2="5.08" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-243.84" x2="10.16" y2="-238.76" width="0.1524" layer="91"/>
<junction x="10.16" y="-238.76"/>
<pinref part="C4_10UF" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="-96.52" y="-198.12" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="-96.52" y1="-198.12" x2="-96.52" y2="-205.74" width="0.1524" layer="91"/>
<pinref part="R0_100K1" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="81.28" y="-302.26" size="1.778" layer="95" xref="yes"/>
<wire x1="66.04" y1="-302.26" x2="81.28" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="66.04" y1="-317.5" x2="66.04" y2="-312.42" width="0.1524" layer="91"/>
<label x="66.04" y="-317.5" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="X1" gate="G$1" pin="NC1"/>
</segment>
</net>
<net name="TP_IRQ" class="0">
<segment>
<wire x1="-124.46" y1="-104.14" x2="-106.68" y2="-104.14" width="0.1524" layer="91"/>
<label x="-106.68" y="-104.14" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="12"/>
</segment>
<segment>
<wire x1="-213.36" y1="-83.82" x2="-208.28" y2="-83.82" width="0.1524" layer="91"/>
<label x="-208.28" y="-83.82" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="LCD_RS" class="0">
<segment>
<wire x1="-132.08" y1="-111.76" x2="-139.7" y2="-111.76" width="0.1524" layer="91"/>
<label x="-139.7" y="-111.76" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="17"/>
</segment>
<segment>
<wire x1="-213.36" y1="-76.2" x2="-187.96" y2="-76.2" width="0.1524" layer="91"/>
<label x="-187.96" y="-76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="LCD_SI" class="0">
<segment>
<wire x1="-124.46" y1="-114.3" x2="-106.68" y2="-114.3" width="0.1524" layer="91"/>
<label x="-106.68" y="-114.3" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="20"/>
</segment>
<segment>
<wire x1="-213.36" y1="-73.66" x2="-208.28" y2="-73.66" width="0.1524" layer="91"/>
<label x="-208.28" y="-73.66" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="8"/>
</segment>
</net>
<net name="TP_SO" class="0">
<segment>
<wire x1="-124.46" y1="-116.84" x2="-116.84" y2="-116.84" width="0.1524" layer="91"/>
<label x="-116.84" y="-116.84" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="22"/>
</segment>
<segment>
<wire x1="-213.36" y1="-71.12" x2="-187.96" y2="-71.12" width="0.1524" layer="91"/>
<label x="-187.96" y="-71.12" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="9"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<wire x1="-132.08" y1="-116.84" x2="-139.7" y2="-116.84" width="0.1524" layer="91"/>
<label x="-139.7" y="-116.84" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="21"/>
</segment>
<segment>
<wire x1="-213.36" y1="-68.58" x2="-208.28" y2="-68.58" width="0.1524" layer="91"/>
<label x="-208.28" y="-68.58" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="10"/>
</segment>
</net>
<net name="LCD_SCK" class="0">
<segment>
<wire x1="-124.46" y1="-119.38" x2="-106.68" y2="-119.38" width="0.1524" layer="91"/>
<label x="-106.68" y="-119.38" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP3" gate="A" pin="24"/>
</segment>
<segment>
<wire x1="-213.36" y1="-66.04" x2="-187.96" y2="-66.04" width="0.1524" layer="91"/>
<label x="-187.96" y="-66.04" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="11"/>
</segment>
</net>
<net name="LCD_CS" class="0">
<segment>
<wire x1="-132.08" y1="-119.38" x2="-152.4" y2="-119.38" width="0.1524" layer="91"/>
<label x="-152.4" y="-119.38" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="23"/>
</segment>
<segment>
<wire x1="-213.36" y1="-63.5" x2="-208.28" y2="-63.5" width="0.1524" layer="91"/>
<label x="-208.28" y="-63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="12"/>
</segment>
</net>
<net name="TP_CS" class="0">
<segment>
<wire x1="-132.08" y1="-121.92" x2="-139.7" y2="-121.92" width="0.1524" layer="91"/>
<label x="-139.7" y="-121.92" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="25"/>
</segment>
<segment>
<wire x1="-213.36" y1="-60.96" x2="-187.96" y2="-60.96" width="0.1524" layer="91"/>
<label x="-187.96" y="-60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="13"/>
</segment>
</net>
<net name="GP18" class="0">
<segment>
<wire x1="-132.08" y1="-104.14" x2="-152.4" y2="-104.14" width="0.1524" layer="91"/>
<label x="-152.4" y="-104.14" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP3" gate="A" pin="11"/>
</segment>
<segment>
<wire x1="-213.36" y1="-86.36" x2="-187.96" y2="-86.36" width="0.1524" layer="91"/>
<label x="-187.96" y="-86.36" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="POWER_LCD" class="0">
<segment>
<wire x1="-132.08" y1="-238.76" x2="-157.48" y2="-238.76" width="0.1524" layer="91"/>
<label x="-132.08" y="-238.76" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO39/TCK"/>
</segment>
<segment>
<wire x1="-208.28" y1="-88.9" x2="-213.36" y2="-88.9" width="0.1524" layer="91"/>
<label x="-208.28" y="-88.9" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="J1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SQW" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SQW/INT"/>
<wire x1="-332.74" y1="-76.2" x2="-317.5" y2="-76.2" width="0.1524" layer="91"/>
<label x="-332.74" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IRF7319" gate="2" pin="G"/>
<pinref part="IRF7319" gate="1" pin="D"/>
<wire x1="121.92" y1="-167.64" x2="127" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="127" y1="-167.64" x2="129.54" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-172.72" x2="106.68" y2="-167.64" width="0.1524" layer="91"/>
<junction x="121.92" y="-167.64"/>
<wire x1="106.68" y1="-167.64" x2="121.92" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-167.64" x2="93.98" y2="-167.64" width="0.1524" layer="91"/>
<junction x="106.68" y="-167.64"/>
<label x="93.98" y="-167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="D12" gate="G$1" pin="A1"/>
<wire x1="124.46" y1="-175.26" x2="127" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="127" y1="-175.26" x2="127" y2="-167.64" width="0.1524" layer="91"/>
<junction x="127" y="-167.64"/>
<pinref part="R1_100K" gate="G$1" pin="1"/>
</segment>
</net>
<net name="BAT_M" class="0">
<segment>
<label x="-104.14" y="-220.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-96.52" y1="-220.98" x2="-104.14" y2="-220.98" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-215.9" x2="-96.52" y2="-220.98" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-220.98" x2="-96.52" y2="-226.06" width="0.1524" layer="91"/>
<junction x="-96.52" y="-220.98"/>
<pinref part="R0_100K2" gate="G$1" pin="2"/>
<pinref part="R0_100K1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-218.44" y1="-213.36" x2="-213.36" y2="-213.36" width="0.1524" layer="91"/>
<label x="-218.44" y="-213.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO1"/>
</segment>
</net>
<net name="LED_T" class="0">
<segment>
<wire x1="-213.36" y1="-111.76" x2="-208.28" y2="-111.76" width="0.1524" layer="91"/>
<label x="-208.28" y="-111.76" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="-213.36" y1="-223.52" x2="-218.44" y2="-223.52" width="0.1524" layer="91"/>
<label x="-218.44" y="-223.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO5"/>
</segment>
</net>
<net name="EX_5V" class="0">
<segment>
<wire x1="-149.86" y1="-53.34" x2="-132.08" y2="-53.34" width="0.1524" layer="91"/>
<label x="-149.86" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="-149.86" y1="-68.58" x2="-132.08" y2="-68.58" width="0.1524" layer="91"/>
<label x="-149.86" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="13"/>
</segment>
<segment>
<pinref part="IRF1" gate="2" pin="D"/>
<wire x1="-25.4" y1="-175.26" x2="-25.4" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-177.8" x2="-15.24" y2="-177.8" width="0.1524" layer="91"/>
<label x="-12.7" y="-177.8" size="1.778" layer="95" xref="yes"/>
<pinref part="ONE3" gate="1" pin="1"/>
<wire x1="-15.24" y1="-177.8" x2="-12.7" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-170.18" x2="-15.24" y2="-177.8" width="0.1524" layer="91"/>
<junction x="-15.24" y="-177.8"/>
</segment>
</net>
<net name="POWER_EX" class="0">
<segment>
<pinref part="IRF4" gate="1" pin="G"/>
<wire x1="27.94" y1="-177.8" x2="25.4" y2="-177.8" width="0.1524" layer="91"/>
<label x="20.32" y="-177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="25.4" y1="-177.8" x2="20.32" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-180.34" x2="25.4" y2="-177.8" width="0.1524" layer="91"/>
<junction x="25.4" y="-177.8"/>
<pinref part="R0_100K6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="IRF1" gate="1" pin="G"/>
<wire x1="-38.1" y1="-177.8" x2="-40.64" y2="-177.8" width="0.1524" layer="91"/>
<label x="-45.72" y="-177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-40.64" y1="-177.8" x2="-45.72" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-180.34" x2="-40.64" y2="-177.8" width="0.1524" layer="91"/>
<junction x="-40.64" y="-177.8"/>
<pinref part="R0_100K3" gate="G$1" pin="2"/>
</segment>
<segment>
<label x="-132.08" y="-233.68" size="1.778" layer="95" rot="MR180" xref="yes"/>
<wire x1="-132.08" y1="-233.68" x2="-157.48" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO37"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<wire x1="-152.4" y1="-241.3" x2="-157.48" y2="-241.3" width="0.1524" layer="91"/>
<label x="-152.4" y="-241.3" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO40/TDO"/>
</segment>
<segment>
<label x="-7.62" y="-320.04" size="1.778" layer="95" xref="yes"/>
<wire x1="-25.4" y1="-320.04" x2="-7.62" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="LS1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="B_BATTERY+" class="0">
<segment>
<wire x1="-292.1" y1="-114.3" x2="-276.86" y2="-114.3" width="0.1524" layer="91"/>
<label x="-276.86" y="-114.3" size="1.778" layer="95" xref="yes"/>
<pinref part="U$6" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VBAT"/>
<wire x1="-287.02" y1="-76.2" x2="-269.24" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="-76.2" x2="-269.24" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="-76.2" x2="-259.08" y2="-76.2" width="0.1524" layer="91"/>
<junction x="-269.24" y="-76.2"/>
<label x="-259.08" y="-76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="C2_100NF" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="POW" gate="G$1" pin="2"/>
<pinref part="D12" gate="G$1" pin="CC"/>
<wire x1="119.38" y1="-177.8" x2="119.38" y2="-180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_OFF" class="0">
<segment>
<wire x1="93.98" y1="-154.94" x2="111.76" y2="-154.94" width="0.1524" layer="91"/>
<label x="93.98" y="-154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="D12" gate="G$1" pin="A2"/>
<wire x1="111.76" y1="-154.94" x2="127" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-175.26" x2="111.76" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-175.26" x2="111.76" y2="-154.94" width="0.1524" layer="91"/>
<junction x="111.76" y="-154.94"/>
</segment>
<segment>
<wire x1="-152.4" y1="-220.98" x2="-157.48" y2="-220.98" width="0.1524" layer="91"/>
<label x="-152.4" y="-220.98" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO26"/>
</segment>
</net>
<net name="IN+" class="0">
<segment>
<label x="-287.02" y="-177.8" size="1.778" layer="95" xref="yes"/>
<wire x1="-299.72" y1="-177.8" x2="-287.02" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="J4" gate="J1" pin="VBUS"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="VIN"/>
<label x="-38.1" y="-238.76" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-22.86" y1="-238.76" x2="-30.48" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-238.76" x2="-38.1" y2="-238.76" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-243.84" x2="-30.48" y2="-238.76" width="0.1524" layer="91"/>
<junction x="-30.48" y="-238.76"/>
<pinref part="C8_10UF" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="111.76" y1="-236.22" x2="116.84" y2="-236.22" width="0.1524" layer="91"/>
<label x="116.84" y="-236.22" size="1.778" layer="95" xref="yes"/>
<pinref part="ONE" gate="1" pin="2"/>
</segment>
<segment>
<label x="15.24" y="-223.52" size="1.778" layer="95" xref="yes"/>
<wire x1="5.08" y1="-223.52" x2="15.24" y2="-223.52" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="CE"/>
</segment>
<segment>
<label x="40.64" y="-228.6" size="1.778" layer="95" xref="yes"/>
<wire x1="33.02" y1="-228.6" x2="40.64" y2="-228.6" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="EX_3V" class="0">
<segment>
<wire x1="-104.14" y1="-55.88" x2="-124.46" y2="-55.88" width="0.1524" layer="91"/>
<label x="-104.14" y="-55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="-177.8" y1="-325.12" x2="-170.18" y2="-325.12" width="0.1524" layer="91"/>
<label x="-170.18" y="-325.12" size="1.778" layer="95" xref="yes"/>
<pinref part="U$3" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="IRF4" gate="2" pin="D"/>
<wire x1="40.64" y1="-175.26" x2="40.64" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-177.8" x2="50.8" y2="-177.8" width="0.1524" layer="91"/>
<label x="53.34" y="-177.8" size="1.778" layer="95" xref="yes"/>
<pinref part="ONE2" gate="1" pin="2"/>
<wire x1="50.8" y1="-177.8" x2="53.34" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-170.18" x2="50.8" y2="-177.8" width="0.1524" layer="91"/>
<junction x="50.8" y="-177.8"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="-218.44" y1="-195.58" x2="-213.36" y2="-195.58" width="0.1524" layer="91"/>
<label x="-218.44" y="-195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="EN"/>
</segment>
<segment>
<wire x1="-312.42" y1="-233.68" x2="-302.26" y2="-233.68" width="0.1524" layer="91"/>
<label x="-312.42" y="-233.68" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-302.26" y1="-228.6" x2="-302.26" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="2"/>
<wire x1="-302.26" y1="-233.68" x2="-302.26" y2="-238.76" width="0.1524" layer="91"/>
<junction x="-302.26" y="-233.68"/>
<wire x1="-302.26" y1="-233.68" x2="-289.56" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="-289.56" y1="-233.68" x2="-289.56" y2="-241.3" width="0.1524" layer="91"/>
<pinref part="C2_10UF1" gate="G$1" pin="2"/>
<pinref part="R5_10K2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="5V_OUT" class="0">
<segment>
<wire x1="99.06" y1="-220.98" x2="91.44" y2="-220.98" width="0.1524" layer="91"/>
<label x="99.06" y="-220.98" size="1.778" layer="95" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="OUT+"/>
</segment>
<segment>
<label x="142.24" y="-157.48" size="1.778" layer="95" xref="yes"/>
<wire x1="121.92" y1="-157.48" x2="134.62" y2="-157.48" width="0.1524" layer="91"/>
<pinref part="IRF7319" gate="2" pin="S"/>
<wire x1="134.62" y1="-157.48" x2="134.62" y2="-165.1" width="0.1524" layer="91"/>
<junction x="134.62" y="-157.48"/>
<wire x1="134.62" y1="-157.48" x2="142.24" y2="-157.48" width="0.1524" layer="91"/>
<pinref part="R1_100K" gate="G$1" pin="2"/>
</segment>
</net>
<net name="POWER_MAIN" class="0">
<segment>
<pinref part="IRF7319" gate="1" pin="G"/>
<wire x1="99.06" y1="-182.88" x2="99.06" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-180.34" x2="101.6" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-180.34" x2="93.98" y2="-180.34" width="0.1524" layer="91"/>
<junction x="99.06" y="-180.34"/>
<label x="93.98" y="-180.34" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R0_100K" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-152.4" y1="-236.22" x2="-157.48" y2="-236.22" width="0.1524" layer="91"/>
<label x="-152.4" y="-236.22" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO38"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="-299.72" y1="-185.42" x2="-292.1" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="J4" gate="J1" pin="CC1"/>
<pinref part="R4_5.1K3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="-299.72" y1="-187.96" x2="-271.78" y2="-187.96" width="0.1524" layer="91"/>
<pinref part="J4" gate="J1" pin="CC2"/>
<pinref part="R4_5.1K4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="T_DOWN" class="0">
<segment>
<wire x1="-213.36" y1="-106.68" x2="-208.28" y2="-106.68" width="0.1524" layer="91"/>
<label x="-208.28" y="-106.68" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-213.36" y1="-220.98" x2="-231.14" y2="-220.98" width="0.1524" layer="91"/>
<label x="-231.14" y="-220.98" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO4"/>
</segment>
</net>
<net name="T_RIGHT" class="0">
<segment>
<label x="-193.04" y="-119.38" size="1.778" layer="95" xref="yes"/>
<wire x1="-213.36" y1="-119.38" x2="-193.04" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="7"/>
</segment>
<segment>
<label x="-231.14" y="-231.14" size="1.778" layer="95" rot="MR0" xref="yes"/>
<wire x1="-213.36" y1="-231.14" x2="-231.14" y2="-231.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO8"/>
</segment>
</net>
<net name="T_LEFT" class="0">
<segment>
<wire x1="-213.36" y1="-121.92" x2="-208.28" y2="-121.92" width="0.1524" layer="91"/>
<label x="-208.28" y="-121.92" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="8"/>
</segment>
<segment>
<wire x1="-213.36" y1="-233.68" x2="-218.44" y2="-233.68" width="0.1524" layer="91"/>
<label x="-218.44" y="-233.68" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO9"/>
</segment>
</net>
<net name="T_UP" class="0">
<segment>
<wire x1="-213.36" y1="-124.46" x2="-193.04" y2="-124.46" width="0.1524" layer="91"/>
<label x="-193.04" y="-124.46" size="1.778" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="-213.36" y1="-236.22" x2="-231.14" y2="-236.22" width="0.1524" layer="91"/>
<label x="-231.14" y="-236.22" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO10"/>
</segment>
</net>
<net name="BOOT0" class="0">
<segment>
<wire x1="-231.14" y1="-208.28" x2="-213.36" y2="-208.28" width="0.1524" layer="91"/>
<label x="-231.14" y="-208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO0_BOOT0"/>
</segment>
<segment>
<label x="-266.7" y="-228.6" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="A11" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="-228.6" x2="-266.7" y2="-236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<wire x1="-157.48" y1="-213.36" x2="-132.08" y2="-213.36" width="0.1524" layer="91"/>
<label x="-132.08" y="-213.36" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO19/D-"/>
</segment>
<segment>
<wire x1="-132.08" y1="-154.94" x2="-147.32" y2="-154.94" width="0.1524" layer="91"/>
<label x="-147.32" y="-154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP6" gate="A" pin="1"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<wire x1="-157.48" y1="-215.9" x2="-152.4" y2="-215.9" width="0.1524" layer="91"/>
<label x="-152.4" y="-215.9" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO20/D+"/>
</segment>
<segment>
<wire x1="-132.08" y1="-157.48" x2="-139.7" y2="-157.48" width="0.1524" layer="91"/>
<label x="-139.7" y="-157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP6" gate="A" pin="3"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<wire x1="-157.48" y1="-198.12" x2="-152.4" y2="-198.12" width="0.1524" layer="91"/>
<label x="-152.4" y="-198.12" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="RXD0"/>
</segment>
<segment>
<wire x1="-124.46" y1="-157.48" x2="-106.68" y2="-157.48" width="0.1524" layer="91"/>
<label x="-106.68" y="-157.48" size="1.778" layer="95" xref="yes"/>
<pinref part="JP6" gate="A" pin="4"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<wire x1="-157.48" y1="-195.58" x2="-132.08" y2="-195.58" width="0.1524" layer="91"/>
<label x="-132.08" y="-195.58" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="TXD0"/>
</segment>
<segment>
<wire x1="-124.46" y1="-154.94" x2="-116.84" y2="-154.94" width="0.1524" layer="91"/>
<label x="-116.84" y="-154.94" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP6" gate="A" pin="2"/>
</segment>
</net>
<net name="E1" class="0">
<segment>
<wire x1="-152.4" y1="-231.14" x2="-157.48" y2="-231.14" width="0.1524" layer="91"/>
<label x="-152.4" y="-231.14" size="1.778" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO36"/>
</segment>
<segment>
<wire x1="-132.08" y1="-162.56" x2="-139.7" y2="-162.56" width="0.1524" layer="91"/>
<label x="-139.7" y="-162.56" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP6" gate="A" pin="7"/>
</segment>
</net>
<net name="VIBRATION" class="0">
<segment>
<wire x1="-218.44" y1="-205.74" x2="-213.36" y2="-205.74" width="0.1524" layer="91"/>
<label x="-218.44" y="-205.74" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="IO45_SPIV"/>
</segment>
<segment>
<wire x1="-309.88" y1="-289.56" x2="-309.88" y2="-297.18" width="0.1524" layer="91"/>
<label x="-309.88" y="-289.56" size="1.778" layer="95" rot="MR90" xref="yes"/>
<pinref part="R5_10K3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ISET" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="ISET"/>
<wire x1="-22.86" y1="-228.6" x2="-38.1" y2="-228.6" width="0.1524" layer="91"/>
<pinref part="R6_1.2K" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="ONE" gate="1" pin="1"/>
<wire x1="101.6" y1="-236.22" x2="101.6" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-233.68" x2="91.44" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VIN+"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="CHRG"/>
<wire x1="5.08" y1="-228.6" x2="10.16" y2="-228.6" width="0.1524" layer="91"/>
<pinref part="R6_1.2K1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IRF4" gate="1" pin="D"/>
<wire x1="33.02" y1="-165.1" x2="33.02" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="IRF4" gate="2" pin="G"/>
<wire x1="33.02" y1="-167.64" x2="33.02" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-167.64" x2="33.02" y2="-167.64" width="0.1524" layer="91"/>
<junction x="33.02" y="-167.64"/>
<pinref part="R1_100K4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IRF1" gate="1" pin="D"/>
<wire x1="-33.02" y1="-165.1" x2="-33.02" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="IRF1" gate="2" pin="G"/>
<wire x1="-33.02" y1="-167.64" x2="-33.02" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-167.64" x2="-33.02" y2="-167.64" width="0.1524" layer="91"/>
<junction x="-33.02" y="-167.64"/>
<pinref part="R1_100K1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="4"/>
<wire x1="101.6" y1="-106.68" x2="109.22" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="C"/>
<wire x1="-114.3" y1="-302.26" x2="-114.3" y2="-294.64" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-294.64" x2="-101.6" y2="-294.64" width="0.1524" layer="91"/>
<pinref part="NEGATIVE" gate="1" pin="P"/>
<wire x1="-101.6" y1="-287.02" x2="-101.6" y2="-294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="-96.52" y1="-294.64" x2="-86.36" y2="-294.64" width="0.1524" layer="91"/>
<pinref part="POSITIVE" gate="1" pin="P"/>
<wire x1="-96.52" y1="-287.02" x2="-96.52" y2="-294.64" width="0.1524" layer="91"/>
<pinref part="R5_10K1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="C"/>
<wire x1="-294.64" y1="-312.42" x2="-294.64" y2="-307.34" width="0.1524" layer="91"/>
<wire x1="-294.64" y1="-307.34" x2="-281.94" y2="-307.34" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="-307.34" x2="-281.94" y2="-299.72" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="-307.34" x2="-261.62" y2="-307.34" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="-294.64" y1="-302.26" x2="-294.64" y2="-307.34" width="0.1524" layer="91"/>
<pinref part="-" gate="1" pin="P"/>
<pinref part="C2_10UF2" gate="G$1" pin="1"/>
<junction x="-294.64" y="-307.34"/>
<junction x="-281.94" y="-307.34"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="-294.64" y1="-287.02" x2="-281.94" y2="-287.02" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="-287.02" x2="-281.94" y2="-292.1" width="0.1524" layer="91"/>
<wire x1="-281.94" y1="-287.02" x2="-274.32" y2="-287.02" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-287.02" x2="-274.32" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-284.48" x2="-264.16" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-287.02" x2="-274.32" y2="-304.8" width="0.1524" layer="91"/>
<wire x1="-274.32" y1="-304.8" x2="-261.62" y2="-304.8" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="C"/>
<wire x1="-294.64" y1="-287.02" x2="-294.64" y2="-292.1" width="0.1524" layer="91"/>
<pinref part="+" gate="1" pin="P"/>
<pinref part="C2_10UF2" gate="G$1" pin="2"/>
<pinref part="R5_10K4" gate="G$1" pin="2"/>
<junction x="-281.94" y="-287.02"/>
<junction x="-274.32" y="-287.02"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="B"/>
<wire x1="-299.72" y1="-317.5" x2="-309.88" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="-317.5" x2="-309.88" y2="-307.34" width="0.1524" layer="91"/>
<pinref part="R5_10K3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-299.72" y1="-142.24" x2="-274.32" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="J3" gate="J1" pin="VBUS"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R6_1.2K1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-228.6" x2="25.4" y2="-228.6" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="C"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
